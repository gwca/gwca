export const environment = {
  production: false,
  profile: 'local',
  homeworkURL: 'http://localhost:8080/',
  serviceURL: 'http://localhost:8080/services/'
};
