export const environment = {
  production: false,
  profile: 'test',
  homeworkURL: 'https://2-dot-gwca-classroom.appspot.com/',
  serviceURL: 'https://2-dot-gwca-services.appspot.com/services/'
};
