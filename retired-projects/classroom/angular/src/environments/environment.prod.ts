export const environment = {
  production: true,
  profile: 'prod',
  homeworkURL: 'https://gwca-classroom.appspot.com/',
  serviceURL: 'https://gwca-services.appspot.com/services/'
};
