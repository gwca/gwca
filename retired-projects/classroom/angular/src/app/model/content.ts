/**
 * A course that has a list of lessons.
 */
export class Course {
    courseId: string;
    title: string;
    image: string;
    start: number;
    description: string;
    lessons: Lesson[];

    constructor() {
        this.courseId = '';
        this.title = '';
        this.image = '';
        this.start = 0;
        this.description = '';
        this.lessons = [];
    }
}

/**
 * A lesson of a course that has one or more chapters.
 */
export class Lesson {
    lessonId: string;
    title: string;
    chapters: Chapter[];

    constructor() {
        this.lessonId = '';
        this.title = '';
        this.chapters = [];
    }
}

/**
 * A chapter of a lesson that is one of the following types: [audio, video] 
 * with one or more associated texts (URLs of some html pages)
 */
export class Chapter {
    chapterId: string;
    label: string;
    mediaType: string;
    mediaUrl: string;
    textUrls: string[];

    constructor() {
        this.chapterId = '';
        this.label = '';
        this.mediaType = '';
        this.mediaUrl = '';
        this.textUrls = [];
    }
}

/**
 * A class of all the IDs
 */
export class MaterialID {
    courseId: string;
    lessonId: string;
    chapterId: string;
    type: string;
    assigned: boolean;

    constructor() {
        this.chapterId = '';
        this.lessonId = '';
        this.chapterId = '';
        this.type = '';
        this.assigned = false;
    }
}

/**
 * A TOC item of a learning material.
 */
export class Material {
    id: string;
    type: string;
    image: string;
    shortDesc: string;
    longDesc: string;
    show: boolean;
}
