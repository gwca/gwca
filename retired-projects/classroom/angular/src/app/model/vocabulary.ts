/**
 * A vocabulary course
 */
export class VocabularyCourse {
    courseId: string;
    title: string;
    url: string;
    bgColor: string;
    groups: string[];
}

/**
 * A vocabulary group.
 */
export class VocabularyGroup {
    courseId: string;
    lessonId: string;
    chapterId: string;
    quizletId: string;
    title: string;
    entries: Vocabulary[];
}

/**
 * A vocabulary entry.
 */
export class Vocabulary {
    id: string;
    pinyin: string;
    clipUrl: string;
    entry: CharEntry;
    words: CharEntry[];
    usage: CharEntry;
}

/**
 * A character entry for a vocabulary entry.
 */
export class CharEntry {
    value: string;
    translation: string;
}
