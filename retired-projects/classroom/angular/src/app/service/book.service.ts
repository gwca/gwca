import { Injectable } from '@angular/core';

import { Course, Lesson, Chapter } from '../model/content';
import { AppURL } from './book.properties';

@Injectable({
    providedIn: 'root'
})
export class BookService {

    private PAGE_NOT_FOUND: string = "assets/books/PageNotFound.html";
    private bookIndex: Course[];

    constructor() {
    }

    /**
     * Sets the book index
     * @param courses 
     */
    setBookIndex(courses: Course[]) {
        this.bookIndex = courses;
    }

    /**
     * Gets the book index.
     */
    getBookIndex(): Course[] {
        return this.bookIndex;
    }

    /**
     * Gets a course from the book index by a courseId.
     */
    getCourse(courseId: string): Course {
        if (courseId == null) {
            return this.bookIndex[0];
        }
        for (var i = 0; i < this.bookIndex.length; i++) {
            if (this.bookIndex[i].courseId == courseId) {
                return this.bookIndex[i];
            }
        }
        return this.bookIndex[0];
    }

    /**
     * Gets a lesson from the course by a lessonId.
     */
    getLesson(course: Course, lessonId: string): Lesson {
        if (lessonId == null) {
            return this.getFirstLesson(course);
        }
        for (var i = 0; i < course.lessons.length; i++) {
            if (course.lessons[i].lessonId == lessonId) {
                return course.lessons[i];
            }
        }
        return this.getFirstLesson(course);
    }

    /**
     * Gets the first lesson from the course.
     */
    getFirstLesson(course: Course): Lesson {
        return course.lessons[0];
    }

    /**
     * Gets a chapter from the lesson by chapterId.
     */
    getChapter(lesson: Lesson, chapterId: string): Chapter {
        if (chapterId == null) {
            return this.getFirstChapter(lesson);
        }
        for (var i = 0; i < lesson.chapters.length; i++) {
            if (lesson.chapters[i].chapterId == chapterId) {
                return lesson.chapters[i];
            }
        }
        return this.getFirstChapter(lesson);
    }

    /**
     * Gets the first chapter from the lesson.
     */
    getFirstChapter(lesson: Lesson): Chapter {
        return lesson.chapters[0];
    }

    isVideo(chapter: Chapter): boolean {
        return chapter.mediaType && chapter.mediaType == 'video';
    }

    isAudio(chapter: Chapter): boolean {
        return chapter.mediaType && chapter.mediaType == 'audio';
    }

    getAudioUrl(chapter: Chapter): string {
        return AppURL.audioFolder + chapter.mediaUrl;
    }

    getVideoUrl(chapter: Chapter): string {
        return AppURL.videoFolder + chapter.mediaUrl;
    }
}
