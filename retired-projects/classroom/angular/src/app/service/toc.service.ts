import { Injectable } from '@angular/core';

import { MaterialID, Material } from '../model/content';
import { TableOfContents } from './book.properties';

@Injectable({
    providedIn: 'root'
})
export class TOCService {

    constructor() { }

    /** Gets the list of available learning material list */
    public getMaterialList() {
        return TableOfContents;
    }

    /** Creates an empty MaterialID object */
    public createMaterial(): MaterialID {
        let materialId = new MaterialID();
        return materialId;
    }

    /** Sets the default values if they are not set yet. */
    public setDefaultCourseId(materialId: MaterialID): void {
        const type = materialId.type;
        const courseId = materialId.courseId;
        if (this.isValidType(type)) {
            if (!this.isValidCourse(courseId)) {
                materialId.courseId = this.getFirstCourse(type);
                materialId.lessonId = '';
                materialId.chapterId = '';
            }
        } else {
            const material: Material = TableOfContents[0];
            materialId.type = material.type;
            materialId.courseId = material.id;
            materialId.lessonId = '';
            materialId.chapterId = '';
        }
        materialId.assigned = true;
    }

    /** Gets the first courseId by material type */
    private getFirstCourse(type: string): string {
        for (const material of TableOfContents) {
            if (material.type === type) {
                return material.id;
            }
        }
        return '';
    }

    /** Checks if the courseId is valid. */
    private isValidCourse(courseId: string): boolean {
        for (const material of TableOfContents) {
            if (material.id === courseId) {
                return true;
            }
        }
        return false;
    }

    /** Checks if the material type is valid. */
    private isValidType(type: string): boolean {
        for (const material of TableOfContents) {
            if (material.type === type) {
                return true;
            }
        }
        return false;
    }
}
