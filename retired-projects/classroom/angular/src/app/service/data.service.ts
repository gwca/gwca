import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    private headers = null;

    constructor(private http: HttpClient) {
        this.headers = new HttpHeaders().set('Content-Type', 'application/json; charset=UTF-8');
    }

    /**
     * Calls to HTTP:GET to invoke a data service to retrieve a file from the local folder.
     * The response is a JSON object that is subscribed by the caller of this method.
     * 
     * @param url the URL points to a local file.
     * @return a JSON object that is subscribed by the caller of this method.
     */
    localRead(url: string): Observable<any> {
        return this.http.get(url, { headers: this.headers });
    }

    /**
     * Calls to HTTP:GET to invoke a data service to retrieve some data from the database.
     * The response is a JSON object that is subscribed by the caller of this method.
     * 
     * @param service the restful service name.
     * @return a JSON object that is subscribed by the caller of this method.
     */
    read(service: string): Observable<any> {
        let url = environment.serviceURL + service;
        return this.http.get(url, { headers: this.headers });
    }
}
