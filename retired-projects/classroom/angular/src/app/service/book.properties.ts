import { VocabularyCourse } from '../model/vocabulary';

/**
 * The available courses that have a vocabulary list
 */
export const CourseVocabularyList: VocabularyCourse[] = [{
  courseId: 'npcr1',
  title: 'NPCR-1 Vocabulary',
  url: 'assets/vocabulary/npcr1/',
  bgColor: 'Yellow',
  groups: [
    'npcr1-01-1', 'npcr1-02-1', 'npcr1-03-1', 'npcr1-03-2', 'npcr1-04-1', 'npcr1-04-2',
    'npcr1-05-1', 'npcr1-05-2', 'npcr1-06-1', 'npcr1-06-2', 'npcr1-07-1', 'npcr1-07-2',
    'npcr1-08-1', 'npcr1-08-2', 'npcr1-09-1', 'npcr1-09-2', 'npcr1-10-1', 'npcr1-10-2',
    'npcr1-11-1', 'npcr1-11-2', 'npcr1-12-1', 'npcr1-12-2', 'npcr1-13-1', 'npcr1-13-2',
    'npcr1-14-1', 'npcr1-14-2']
}, {
  courseId: 'npcr2',
  title: 'NPCR-2 Vocabulary',
  url: 'assets/vocabulary/npcr2/',
  bgColor: '',
  groups: [
    'npcr2-15-1', 'npcr2-15-2', 'npcr2-16-1', 'npcr2-16-2', 'npcr2-17-1', 'npcr2-17-2',
    'npcr2-18-1', 'npcr2-18-2', 'npcr2-19-1', 'npcr2-19-2', 'npcr2-20-1', 'npcr2-20-2',
    'npcr2-21-1', 'npcr2-21-2', 'npcr2-22-1', 'npcr2-22-2', 'npcr2-23-1', 'npcr2-23-2',
    'npcr2-24-1', 'npcr2-24-2', 'npcr2-25-1', 'npcr2-25-2', 'npcr2-26-1']
}]

/**
 * The table of contents of the available courses
 */
export const TableOfContents = [
  {
    id: 'bar1',
    type: 'delimiter',
    image: '',
    shortDesc: '-- 课本、视听 --',
    longDesc: '',
    show: true
  }, {
    id: 'pinyin',
    type: 'course',
    image: 'assets/images/sites/pinyin.png',
    shortDesc: '汉语拼音',
    longDesc: "Welcome to the fascinating world of Chinese phonetics and tones! You'll gain a general understanding of how Chinese phonetics work and become familiar with different tones. All the video clips are provided by eChineseLearning.com and made available on YouTube.",
    show: true
  }, {
    id: 'npcr1',
    type: 'course',
    image: 'assets/images/sites/npcr1-cover.jpg',
    shortDesc: '新实用汉语课本 - 1',
    longDesc: "《新实用汉语课本》是专门为北美地区留学生编写的汉语教材，分六级。第一册以立波和他的同学、朋友为主要人物，在有趣的情节中，将结构、功能与文化有机地糅合在一起，是一套编排科学的初级汉语课本。本书共十四课，供初级阶段学习使用。",
    show: true
  }, {
    id: 'npcr2',
    type: 'course',
    image: 'assets/images/sites/npcr2-cover.jpg',
    shortDesc: '新实用汉语课本 - 2',
    longDesc: "本书为《新实用汉语课本》第二册，共十二课（第15—26课）。每课包括课文、生词、注释、练习与运用、阅读和复述、语法、汉字及文化知识等内容，书末附有繁体字课文及词汇、汉字索引以及生词、语法的注释。本书供初级阶段学习使用。",
    show: true
  }, {
    id: 'npcr3',
    type: 'course',
    image: 'assets/images/sites/npcr3-cover.jpg',
    shortDesc: '新实用汉语课本 - 3',
    longDesc: "本书为《新实用汉语课本》第三册，共十二课（从第27-38课）。含对话、生词、语法、练习等内容，并附有生词、语法的注释。本书供初级阶段学习者使用。",
    show: true
  }, {
    id: 'npcr4',
    type: 'course',
    image: 'assets/images/sites/npcr4-cover.jpg',
    shortDesc: '新实用汉语课本 - 4',
    longDesc: "第四册紧接前三册内容，为学习者提供与时俱进的语言知识学习和技能操练的内容板块，同时每课讨论的话题均为与中国国情、习俗和文化密切相关的内容，练习丰富多样。本书可进一步培养学习者运用汉语进行交际的能力。",
    show: true
  }, {
    id: 'npcr5',
    type: 'course',
    image: 'assets/images/sites/npcr5-cover.jpg',
    shortDesc: '新实用汉语课本 - 5',
    longDesc: "本书为《新实用汉语课本》第五册，为中级阶段课本。本册以学习者为中心，采用圆周式编排方法，语言结构、功能、文化等因素多次循环重现。全书内容题材广泛，版块式安排，使核心内容和补充内容分割清晰又易于根据需要进行选择。每课包括课文、生词、注释、练习、运用、会话、阅读、复述、语音、语法、汉字和文化知识等内容。",
    show: true
  }, {
    id: 'npcr6',
    type: 'course',
    image: 'assets/images/sites/npcr6-cover.jpg',
    shortDesc: '新实用汉语课本 - 6',
    longDesc: "《新实用汉语课本6》是为高级水平成人汉语学习者编写的综合汉语教材，既适合学习者自学也可用于课堂教学。本书的编写目的是通过语言结构、语言功能与相关文化知识的学习和听说读写技能训练，进一步提高学习者的汉语综合水平。",
    show: true
  }, {
    id: 'workshop',
    type: 'course',
    image: 'assets/images/sites/independent-study.jpg',
    shortDesc: '阅读与写作',
    longDesc: "今年的阅读与写作课主要是帮助同学们提高阅读和写作的能力。这里是一组文章供大家在这个学年阅读。在每篇文章后，有一些帮助理解文章的问题和作业。另外，除了平时作业中有作文外，每个同学还要写一篇较长的作文《我与长城中文学校》作为毕业班同学的毕业作文。",
    show: true
  }, {
    id: 'reading',
    type: 'course',
    image: 'assets/images/sites/library.jpg',
    shortDesc: '课外阅读',
    longDesc: "这里是一些程度不同的课外阅读材料，供教师在教学过程中配合教材使用。",
    show: true
  }, {
    id: 'bar2',
    type: 'delimiter',
    image: '',
    shortDesc: '-- 学习工具 --',
    longDesc: '',
    show: true
  }, {
    id: 'vocabulary',
    type: 'tool',
    image: 'assets/images/sites/vocabulary.jpg',
    shortDesc: '学习生词',
    longDesc: 'Vocabulary Builder',
    show: true
  }
  , {
    id: 'bar3',
    type: 'delimiter',
    image: '',
    shortDesc: '-- 学校老师 --',
    longDesc: '',
    show: false
  }, {
    id: 'youpinghu',
    type: 'teacher',
    image: 'assets/images/sites/youping.jpg',
    shortDesc: '班里那些事儿',
    longDesc: "自从《明朝那些事》这本书在网上一炮打响之后，一时间冒出来许许多多的从三皇五帝到蒋公毛爷，从街头巷尾到厅堂厨房的那些事儿。现如今，“那些事儿”几乎成了一种特定的文体，就是用轻松甚至调侃的文字来写些严肃的事儿，于是就有了我们《班里那些事儿》。",
    show: false
  }
]

/**
 * Some URLs used by the application.
 */
export const AppURL = {
  swfURL: 'assets/swf/',
  quiziletURL: 'https://quizlet.com/',
  dictionaryURL: 'http://www.archchinese.com/chinese_english_dictionary.html?find=',
  audioFolder: 'https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/audio-clips/',
  videoFolder: 'https://www.youtube.com/embed/'
}
