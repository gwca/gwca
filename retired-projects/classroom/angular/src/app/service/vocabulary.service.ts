import { Injectable } from '@angular/core';
import { VocabularyCourse } from '../model/vocabulary';
import { CourseVocabularyList, AppURL } from './book.properties';

@Injectable({
    providedIn: 'root'
})
export class VocabularyService {

    courses: VocabularyCourse[];

    constructor() {
        this.courses = CourseVocabularyList;
    }

    /**
     * Gets the entire course list.
     */
    getCourses(): VocabularyCourse[] {
        return this.courses;
    }

    /**
     * Gets a course from the course list by a courseId.
     */
    getCourse(courseId: string): VocabularyCourse {
        if (courseId == null) {
            return this.courses[0];
        }
        for (var i = 0; i < this.courses.length; i++) {
            if (this.courses[i].courseId == courseId) {
                return this.courses[i];
            }
        }
        return this.courses[0];
    }

    /**
     * Checks if the groupId is a valid one.
     */
    isValidGroup(course: VocabularyCourse, groupId: string): boolean {
        for (var i = 0; i < course.groups.length; i++) {
            if (groupId == course.groups[i]) {
                return true;
            }
        }
        return false;
    }

    getDictionaryUrl(): string {
        return AppURL.dictionaryURL;
    }

    getSwfUrl(): string {
        return AppURL.swfURL;
    }

    getQuizletUrl(): string {
        return AppURL.quiziletURL;
    }
}
