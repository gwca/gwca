import { Component, OnInit, Input, ViewEncapsulation, HostListener } from '@angular/core';
import { ElementRef, ViewChild } from '@angular/core';

import { DataService } from '../service/data.service';
import { VocabularyService } from '../service/vocabulary.service';
import { Vocabulary, VocabularyGroup, VocabularyCourse } from '../model/vocabulary';
import { MaterialID } from '../model/content';

@Component({
    selector: 'app-builder',
    templateUrl: './builder.component.html',
    styleUrls: ['../classroom.css'],
    encapsulation: ViewEncapsulation.None
})
/**
 * The vocabulary builder class
 */
export class BuilderComponent implements OnInit {

    /** The available courses and the selected course */
    courses: VocabularyCourse[];
    course: VocabularyCourse;

    /** The selected character group ID and the group */
    groupId: string = null;
    isGroupReady: boolean = false;
    charGroup: VocabularyGroup = null;

    /** The character stroke show properties */
    strokeSource: string = '';
    strokeTop: string = '0px';

    /** Some commonly used links */
    swfUrl: string;
    dictUrl: string;
    quizletUrl: string;

    /** the IDs of the selected material (course, lesson and chapter) */
    @Input()
    materialId: MaterialID;

    /** The HTML element for the flash player */
    @ViewChild('flashPlayer', {static: false}) flashPlayer: ElementRef;

    /** The dynamic mouse position for showing the flash player. */
    pageX: number = 0;
    pageY: number = 0;

    /** The informational message on the page */
    message: string = "Click on a character to see its stroke order.";

    constructor(private dataService: DataService, private builderService: VocabularyService) {
    }

    ngOnInit() {
        this.swfUrl = this.builderService.getSwfUrl();
        this.quizletUrl = this.builderService.getQuizletUrl();
        this.dictUrl = this.builderService.getDictionaryUrl();
        this.courses = this.builderService.getCourses();
        this.course = this.builderService.getCourse(this.materialId.courseId);
        this.createSelectedGroup();
        if (this.isAppleDevice()) {
            this.message = '';
        }
    }

    /**
     * Records the current mouse position when the mouse is down.
     */
    @HostListener('mousedown', ['$event'])
    public onMousedown(event: MouseEvent) {
        this.pageX = event.pageX;
        this.pageY = event.pageY;
    }

    /**
     * Sets the selected course and the first group of it.
     */
    public setCourse(course: VocabularyCourse) {
        this.course = this.builderService.getCourse(course.courseId);
        this.highlightSelectedCourse();
        this.groupId = this.course.groups[0];
        this.setGroup();
    }

    /**
     * Retrieves the selected character group.
     */
    public setGroup(): void {
        this.isGroupReady = false;
        let url = this.course.url + this.groupId + '.json';
        this.setCharGroup(url);
    }

    /**
     * Creates a character list of the selected character group.
     */
    public getGroupCharacterList(): string {
        let charList: string = "";
        if (this.isGroupReady) {
            for (var i = 0; i < this.charGroup.entries.length; i++) {
                let charEntry = this.charGroup.entries[i];
                charList += charEntry.entry.value;
                if (i < (this.charGroup.entries.length - 1)) {
                    charList += ',';
                }
            }
        }
        return charList;
    }

    /**
     * Checks if the character entry has a usage.
     */
    public hasUsage(entry: Vocabulary): boolean {
        return (entry.usage.value != null && entry.usage.value.length > 0);
    }

    /**
     * The event handling function for the click event of the Quizlet button
     */
    public doQuizlet() {
        const url = this.quizletUrl + this.charGroup.quizletId;
        const quizlet = window.open(url, '_blank');
        quizlet.focus();
    }

    /**
     * Plays a flash video to show the character strokes.
     */
    public showStrokes(url) {
        if (!this.isAppleDevice()) {
            const top = (this.getMouseY() - 110);
            this.strokeSource = this.swfUrl + url;
            this.strokeTop = top + 'px';
            this.createFlashPlayer();
        }
    }

    /**
     * Turns on/off the flash player.
     */
    public setStrokeShow(show: boolean): void {
        let element: HTMLDivElement = this.flashPlayer.nativeElement;
        element.parentElement.style.display = (show ? 'block' : 'none');
    }

    /** Gets the top level page URL */
    public getCoursesPageURL() {
        return "/";
    }
    
    /**
     * Uses the selected material (course, lesson, chapter) to set
     * the current group.
     */
    private createSelectedGroup(): void {
        if (this.materialId.lessonId == null) {
            this.setCourse(this.course);
        } else {
            this.groupId = this.materialId.courseId + '-';
            if (this.materialId.chapterId == null) {
                this.groupId += this.materialId.lessonId + '-0';
            } else {
                this.groupId += this.materialId.lessonId + '-' + this.materialId.chapterId;
            }

            if (this.builderService.isValidGroup(this.course, this.groupId)) {
                this.highlightSelectedCourse();
                this.setGroup();
            } else {
                this.setCourse(this.course);
            }
        }
    }

    /**
     * Highlights the selected course.
     */
    private highlightSelectedCourse(): void {
        for (let course of this.courses) {
            course.bgColor = '';
        }
        this.course.bgColor = 'Yellow';
    }

    /**
     * Gets a group of characters by groupId.
     * 
     * @param groupId
     * @returns
     */
    private setCharGroup(url: string): void {
        this.dataService.localRead(url)
            .subscribe(data => {
                this.charGroup = data;
                this.isGroupReady = true;
            });
    }

    /**
     * Creates a flash player element and adds it to the page.
     */
    private createFlashPlayer(): void {
        let player: HTMLObjectElement = document.createElement('object');
        player.data = this.strokeSource;
        player.type = 'application/x-shockwave-flash';
        player.width = '200px';
        player.height = '200px';

        this.addParameter(player, 'movie', this.strokeSource);
        this.addParameter(player, 'wmode', "transparent");
        this.addParameter(player, 'allowscriptaccess', 'always');
        this.addParameter(player, 'loop', false);
        this.addParameter(player, 'play', true);

        let element: HTMLDivElement = this.flashPlayer.nativeElement;
        element.innerHTML = '';
        element.appendChild(player);
        element.parentElement.style.top = this.strokeTop;
        element.parentElement.style.display = 'block';
    }

    /**
     * Adds a parameter to the player.
     */
    private addParameter(player: HTMLObjectElement, name: string, value: any) {
        let parameter: HTMLParamElement = document.createElement('param');
        parameter.name = name;
        parameter.value = value;
        player.appendChild(parameter);
    }

    /**
     * Checks if the browser is running on an Apple device that doesn't support flash.
     */
    private isAppleDevice() {
        const mobile = navigator.userAgent.match(/iPhone|iPad|iPod/i);
        return mobile;
    }

    // get the current mouse position
    private getMouseX() {
        return this.pageX;
    }

    private getMouseY() {
        return this.pageY;
    }
}
