import { Component, OnInit } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';

import { TOCService } from './service/toc.service';
import { DataService } from './service/data.service';
import { ContentService } from './service/content.service';
import { BookService } from './service/book.service';
import { MaterialID, Material } from './model/content';
import { Course } from './model/content';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./homepage.css'],
    providers: [Location,
        { provide: LocationStrategy, useClass: PathLocationStrategy }]
})

export class AppComponent implements OnInit {

    pageReady: boolean;
    showMenu: boolean;
    materialId: MaterialID;
    materialList: Material[];

    titleText: string = '长城中文学校在线教室';
    footerText: string = 'Enjoy Learning Chinese with Great Wall Chinese Academy';

    constructor(private location: Location, private dataService: DataService,
        private contentService: ContentService, private tocService: TOCService,
        private bookService: BookService) {
    }

    /**
     * Initializes the book index and binding variables by parsing the query string.
     */
    ngOnInit() {
        this.pageReady = false;
        this.loadBookIndex();
    }

    /** Loads the book index */
    loadBookIndex(): void {
        let service = 'book/bookIndex';
        let bookIndex: Course[] = null;
        this.dataService.read(service)
            .subscribe(data => {
                bookIndex = data.entities;
                this.bookService.setBookIndex(bookIndex);
                this.initMeteralId();
            }, error => {
                console.log("Reading the book index failed:" + error);
            });
    }

    /**
     * Sets the materialId for invoking a material specific application.
     */
    public setMaterial(material: Material): void {
        this.materialId.courseId = material.id;
        this.materialId.type = material.type;
        if (!this.isDelimiter(material)) {
            this.showMenu = false;
            this.materialId.assigned = true;
            this.contentService.setMaterialId(this.materialId);
        }
    }

    //
    // Some helper methods
    //
    public isDelimiter(material: Material): boolean {
        return material.type == "delimiter" && material.show;
    }

    public isTocItem(material: Material): boolean {
        return material.type != "delimiter" && material.show;
    }

    public isContent(): boolean {
        return this.pageReady && (this.materialId.type == "teacher" || this.materialId.type == "course") && this.materialId.assigned;
    }

    public isTool(): boolean {
        return this.pageReady && this.materialId.type == "tool" && this.materialId.assigned;
    }

    public isAssigned(): boolean {
        return this.isContent() || this.isTool();
    }

    /**
     * Initializes the meterial ID with the query string of the URL.
     */
    private initMeteralId(): void {
        this.pageReady = true;
        this.showMenu = true;
        this.materialList = this.tocService.getMaterialList();

        this.materialId = this.tocService.createMaterial();
        this.materialId.assigned = true;

        var path = this.location.path();
        var parameters = this.parseQueryString(path);

        switch (parameters.length) {
            case 0:
                this.materialId.assigned = false;
                break;
            case 1:
                this.materialId.type = parameters[0].value;
                break;
            case 2:
                this.materialId.type = parameters[0].value;
                this.materialId.courseId = parameters[1].value;
                break;
            case 3:
                this.materialId.type = parameters[0].value;
                this.materialId.courseId = parameters[1].value;
                this.materialId.lessonId = parameters[2].value;
                break;
            case 4:
            default:
                this.materialId.type = parameters[0].value;
                this.materialId.courseId = parameters[1].value;
                this.materialId.lessonId = parameters[2].value;
                this.materialId.chapterId = parameters[3].value;
                break;
        }

        this.showMenu = !this.isAssigned();
        this.tocService.setDefaultCourseId(this.materialId);
        this.contentService.setMaterialId(this.materialId);
    }

    /**
     * Parses the query string of the given URL.
     * @param url e.g. https://path?type=course&course=npcr1&lesson=01&chapter=2
     * @returns a list of name/value pairs.
     */
    private parseQueryString(url) {
        var parameters = [];
        if (url.indexOf('?') >= 0) {
            var items = url.split('?');
            var query = items[1];
            var pairs = query.split('&');
            for (var i = 0; i < pairs.length; i++) {
                var pair = pairs[i].split('=');
                parameters.push({
                    name: decodeURIComponent(pair[0]),
                    value: decodeURIComponent(pair[1])
                })
            }
        }
        return parameters;
    }
}
