/**
 * The test environment specific variables.
 */
export const environment = {
  production: false,
  homeworkURL: 'http://localhost:8080/',
  serviceURL: 'http://localhost:8080/services/'
};
