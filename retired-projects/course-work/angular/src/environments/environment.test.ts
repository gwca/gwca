/**
 * The test environment specific variables.
 */
export const environment = {
  production: false,
  homeworkURL: 'https://2-dot-gwca-course-work.appspot.com/',
  serviceURL: 'https://2-dot-gwca-services.appspot.com/services/'};
