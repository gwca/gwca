import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-editor-image',
  templateUrl: '../editor-view/image.html',
  styleUrls: ['../../app.component.css', '../editor.component.css']
})
export class EditorImageComponent extends WorkitemComponent {
}
