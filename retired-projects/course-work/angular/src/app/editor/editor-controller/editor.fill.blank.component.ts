import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-editor-fill-blank',
  templateUrl: '../editor-view/fill-blank.html',
  styleUrls: ['../../app.component.css', '../editor.component.css']
})
export class EditorFillBlankComponent extends WorkitemComponent {
}
