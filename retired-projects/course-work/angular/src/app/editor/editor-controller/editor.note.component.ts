import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-editor-note',
  templateUrl: '../editor-view/note.html',
  styleUrls: ['../../app.component.css', '../editor.component.css']
})
export class EditorNoteComponent extends WorkitemComponent {
}
