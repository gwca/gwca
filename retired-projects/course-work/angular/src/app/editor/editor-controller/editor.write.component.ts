import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-editor-write',
  templateUrl: '../editor-view/write.html',
  styleUrls: ['../../app.component.css', '../editor.component.css']
})
export class EditorWriteComponent extends WorkitemComponent {
}
