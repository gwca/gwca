import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-editor-answer-question',
  templateUrl: '../editor-view/answer-question.html',
  styleUrls: ['../../app.component.css', '../editor.component.css']
})
export class EditorAnswerQuestionComponent extends WorkitemComponent {
}
