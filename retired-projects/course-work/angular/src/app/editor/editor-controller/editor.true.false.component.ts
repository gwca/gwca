import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-editor-true-false',
  templateUrl: '../editor-view/true-false.html',
  styleUrls: ['../../app.component.css', '../editor.component.css']
})
export class EditorTrueFalseComponent extends WorkitemComponent {
}
