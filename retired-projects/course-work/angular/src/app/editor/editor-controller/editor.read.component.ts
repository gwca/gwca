import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-editor-read',
  templateUrl: '../editor-view/read.html',
  styleUrls: ['../../app.component.css', '../editor.component.css']
})
export class EditorReadComponent extends WorkitemComponent {
}
