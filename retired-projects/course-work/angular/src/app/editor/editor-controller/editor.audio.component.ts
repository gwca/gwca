import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-editor-audio',
  templateUrl: '../editor-view/audio.html',
  styleUrls: ['../../app.component.css', '../editor.component.css']
})
export class EditorAudioComponent extends WorkitemComponent {
}
