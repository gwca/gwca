import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-work-image',
  templateUrl: '../work-view/image.html',
  styleUrls: ['../../app.component.css', '../homework.component.css']
})
export class WorkImageComponent extends WorkitemComponent {
}
