import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-work-write',
  templateUrl: '../work-view/write.html',
  styleUrls: ['../../app.component.css', '../homework.component.css']
})
export class WorkWriteComponent extends WorkitemComponent {
}
