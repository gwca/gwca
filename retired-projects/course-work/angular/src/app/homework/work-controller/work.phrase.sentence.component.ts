import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-work-phrase-sentence',
  templateUrl: '../work-view/phrase-sentence.html',
  styleUrls: ['../../app.component.css', '../homework.component.css']
})
export class WorkPhraseSentenceComponent extends WorkitemComponent {
}
