import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-work-true-false',
  templateUrl: '../work-view/true-false.html',
  styleUrls: ['../../app.component.css', '../homework.component.css']
})
export class WorkTrueFalseComponent extends WorkitemComponent {
}
