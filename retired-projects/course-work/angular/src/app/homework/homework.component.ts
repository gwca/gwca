import { Component, OnInit } from '@angular/core';
import { ElementRef, ViewChild } from '@angular/core';

import { Homework } from '../model/Homework';
import { WorkService } from '../service/work.service';
import { PagePanel } from '../model/AppEnums';
import { AppProperties } from '../model/AppProperties';

/**
 * This component is for
 * a teacher to check a submitted homework, browse a homework of the selected student.
 * an admin to browse a homework of the selected student
 * a parent to browse the homework of the associated student.
 * a student to do the assigned homework.
 */
@Component({
  selector: 'app-homework',
  templateUrl: './homework.component.html',
  styleUrls: ['../app.component.css', './homework.component.css']
})
export class HomeworkComponent implements OnInit {

  static oddCol: boolean = false;

  panel: PagePanel;
  mailBody: string;
  mailSubject: string;

  @ViewChild('mailButton', {static: false}) mailButton: ElementRef;

  /** The work service for the html page to reference */
  service: WorkService;

  constructor(private workService: WorkService) {
    this.service = workService;
  }

  /** Loads the homework list for the current member according to the role of the member */
  ngOnInit() {
    this.panel = PagePanel.Work;
    if (this.workService.isTeacher() || this.workService.isAdmin()) {
      this.workService.loadSubmittedHomeworkList()
    } else {
      this.workService.loadMemberHomeworkList();
    }
    this.mailBody = this.mailSubject = '';
  }

  ngAfterViewInit() {
  }

  /** Sends an email to the teacher. */
  public sendEmailToTeacher() {
    let button = this.mailButton.nativeElement;
    button.disabled = true;
    this.workService.sendEmailToTeacher(this.mailSubject, this.mailBody);
  }

  //
  // Some helper methods
  //
  public isOdd(): boolean {
    HomeworkComponent.oddCol = !HomeworkComponent.oddCol;
    return HomeworkComponent.oddCol;
  }

  public isReadyToSend(): boolean {
    return this.mailSubject.length > 4 && this.mailBody.length > 10;
  }

  public getWorkitems(): any[] {
    return this.workService.getWorkitems();
  }

  public trackByIndex(idx: number, obj: any): any {
    return idx;
  }

  //
  // Some helper methods to set or check the current panel type
  //
  public talkToTeacher() {
    this.panel = PagePanel.Notify;
  }

  public createReport() {
    this.panel = PagePanel.Report;
  }

  public backToWork() {
    this.panel = PagePanel.Work;
  }

  public isWorkPanel(): boolean {
    return this.panel === PagePanel.Work;
  }

  public isReportPanel(): boolean {
    return this.panel === PagePanel.Report;
  }

  public isNotifyPanel(): boolean {
    return this.panel === PagePanel.Notify;
  }

  public goToWork(homework: Homework): void {
    this.service.setHomework(homework);
    this.backToWork();
  }

  //
  // Some helper methods to get URLs
  //
  public getSchoolURL() {
    return AppProperties.schoolSite;
  }

  public getSchoolAdminURL() {
    return AppProperties.schoolAdmin;
  }

  public getSchoolCalendarURL() {
    return AppProperties.schoolCalendar;
  }

  public getProjectURL() {
    return AppProperties.projectSite;
  }

  public getClassSiteURL() {
    const classSiteName = this.workService.getClass().classUrl;
    return AppProperties.classSite + classSiteName;
  }

  public getTextbookURL() {
    const courseId = this.workService.getClass().courseId;
    return AppProperties.textbookSite + courseId;
  }
}
