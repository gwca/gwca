import { Component, OnInit } from '@angular/core';
import { AuthService, GoogleLoginProvider, FacebookLoginProvider } from 'angular-6-social-login';

import { WorkService } from './service/work.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

/**
 * This componment is for the landling (home) page.
 */
export class AppComponent implements OnInit {

  /** The email address of the logged in user */
  private userEmail: string;

  /** The constructor */
  constructor(public service: WorkService, private authService: AuthService) {
  }

  /**
   * Reads the class/course/members relationship to start the app. The class data gets
   * loaded into the injected service and it is shared by all the components such as
   * homework, manager, editor, etc.
   */
  ngOnInit(): void {
    this.userEmail = null;
    this.service.loadCachedValues();
    this.service.loadClassData();
  }

  /**
   * Subscribes Google SingIn service to check if a Google/Facebook user has already signed in.
   */
  ngAfterViewInit(): void {
    this.authService.authState.subscribe((user) => {
      if (user != null) { // a social user logged in
        this.userEmail = user.email;
        let found = this.service.findMember(this.userEmail);
        if (!found) {
          this.service.setWarningMessage(this.userEmail + " is not a registered member or the backend server is not up and running.");
          this.userEmail = null;
        }
      } else { // a social user logged out
        this.userEmail = null;
        this.service.setDefaultInfo();
      }
    }, error => {
      console.log(error);
      this.userEmail = null;
      this.service.setRuntimeInfo(error);
    });
  }

  /** Signs in as a Google user. */
  public signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  /** Sings in as a Facebook user */
  // public signInWithFB(): void {
  // this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  // }

  /** Signs out as a social network user and refreshes the home page */
  public signOut(): void {
    this.authService.signOut();
    this.service.initData();
    location.reload();
  }

  /** A service wrapper method to assign the selected role to the current member */
  public assignMemberRole(role: string): void {
    this.service.assignMemberRole(role);
  }

  /** Builds up a role selection list as a dropdown */
  public getRoles(): string[] {
    let roles = [];
    return roles.concat(this.service.getMember().roles);
  }

  //
  // Some helper methods
  //
  public getGreetingText(): string {
    return this.service.getGreetingText();
  }

  public getClassText(): string {
    return this.service.getClassText();
  }

  public getInfoMessage(): string {
    return this.service.getInfoMessage();
  }

  public isDataReady(): boolean {
    return this.service.isDataReady();
  }

  public isLoggedIn(): boolean {
    return this.userEmail != null;
  }

  public isTeacher(): boolean {
    return (this.isSingleRoleMember() && this.service.isTeacher());
  }

  public isAdmin(): boolean {
    return (this.isSingleRoleMember() && this.service.isAdmin());
  }

  public isStudent(): boolean {
    return (this.isSingleRoleMember() && this.service.isStudent());
  }

  public isParent(): boolean {
    return (this.isSingleRoleMember() && this.service.isParent());
  }

  public isMultipRoleMember(): boolean {
    return (this.isLoggedIn() && this.service.isMultiRoleMember());
  }

  public isSingleRoleMember(): boolean {
    return (this.isLoggedIn() && !this.service.isMultiRoleMember());
  }
}
