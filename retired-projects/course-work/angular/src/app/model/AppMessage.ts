
/**
 * The generic response message from the data service.
 */
export class DataResponse {
    classId: string;
    info: ErrorMessage;
    infoList: ErrorMessage[];
    entities: any[];
}

/**
 * The simple response message from the data service.
 */
export class SimpleResponse {
    info: ErrorMessage;
    value: any;
    entities: NameValuePair[];

    constructor(data: any) {
        this.info = data.info;
        this.value = data.value;
        this.entities = data.entities;
    }

    public getValueByName(name: string): string {
        if (this.entities != null && this.entities.length > 0) {
            for (let entity of this.entities) {
                if (entity.name == name) {
                    return entity.value;
                }
            }
        }
        return '';
    }
}

export class NameValuePair {
    name: string;
    value: string;
}

/**
 * The generic error message object returned by the application data service as the common 
 * element of every response message.
 */
export class ErrorMessage {
    errorCode: string;
    severity: string;
    description: string;
    isSevereError: boolean;
}

/**
 * The report module for generating a homework report of class/student
 */
export class ClassReport {
    info: ErrorMessage;
    memberId: string;
    name: string;
    role: string;
    email: string;
    remark: string;
    status: string;
    title: string;
    grade: string;
    assignTime: string;
}

/**
 * The information module of a course (textbook)
 */
export class CourseInfo {
    courseId: string;
    title: string;
    image: string;
    start: string;
    description: string;
    lessons: Lesson[];

    constructor(courseId: string, title: string, lessons: any[]) {
        this.courseId = courseId;
        this.title = title;
        this.lessons = lessons;
    }

    getQueryParam(lessonId: string, chapterId: string) {
        let queryParameters = '?type=course&course=' + this.courseId;
        if (this.isNotEmpty(lessonId)) {
            queryParameters += "&lesson=" + lessonId;
        }
        if (this.isNotEmpty(chapterId)) {
            queryParameters += "&chapter=" + chapterId;
        }
        return queryParameters;
    }

    isNotEmpty(field: any): boolean {
        if (field && typeof field === 'number') {
            return true;
        }
        if (field && typeof field === 'string') {
            return (field && field != null && field.length > 0);
        }
        return (field ? true : false);
    }
}

/**
 * The information module of a lesson of a course (textbook)
 */
export class Lesson {
    lessonId: string;
    title: string;
    chapters: Chapter[];
}

/**
 * The inforamtion module of a chapter of a lesson.
 */
export class Chapter {
    chapterId: string;
    label: string;
    mediaType: string;
    mediaUrl: string;
    textUrls: string[];
}

/**
 * A book title and its online address (url)
 */
export class BookTitle {
    title: string;
    url: string;

    constructor(title: string, url: string) {
        this.title = title;
        this.url = url;
    }
}
