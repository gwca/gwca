
/**
 * All the application properties
 */
export const AppProperties = {
    audioPrefix: 'https://docs.google.com/uc?authuser=0&export=download&id=',
    imagePrefix: 'http://drive.google.com/uc?export=view&id=',
    youtubeEmbedUrl: 'https://www.youtube.com/embed/',
    youtubeRefUrl: 'https://youtu.be/',
    youtubePageUrl: 'https://www.youtube.com/watch?v=',
    googleMail: 'https://mail.google.com/',
    googleDrive: 'https://drive.google.com/drive/',
    schoolSite: 'http://greatwallchineseacademy.org/',
    schoolCalendar: 'http://www.greatwallchineseacademy.org/files20182019/SchoolCalendar20182019.pdf',
    schoolAdmin: 'http://www.greatwallchineseacademy.org/SchoolStaff.htm',
    textbookSiteHome: 'https://gwca-classroom.appspot.com/',
    textbookSite: 'https://gwca-classroom.appspot.com/?type=course&course=',
    projectSite: 'https://sites.google.com/a/greatwallchineseacademy.org/course-work/projects/c12-2018-2019',
    classSite:'https://sites.google.com/a/greatwallchineseacademy.org/',
    refreshCache: true,
    refreshCacheInteval: 24 * 3600 * 1000, // one day
    validateUser: false,
    maxRows: 100
}
