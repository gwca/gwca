
export enum MemberRole {
    TEACHER,
    TA,
    STUDENT,
    PARENT,
    ADMINISTRATOR
}

export enum WorkStatus {
    Unassigned,
    WorkInProgress,
    Review,
    Assigned,
    Submitted,
    Done
}

export enum WorkitemName {
    AnswerQuestion,
    Audio,
    FillBlank,
    Image,
    MultiChoice,
    Note,
    PhraseSentence,
    Read,
    Record,
    TrueFalse,
    Video,
    Write,
    PageLink,
    ConnectSentence
}

export enum ComponentName {
    Editor,
    Worker,
    Manager
}

export enum PagePanel {
    Assign,
    Notify,
    Preview,
    List,
    Report,
    Work,
    Unassign,
    Permission,
    Nothing
}

export enum RecordingStatus {
    NO_MIC,
    OPENING,
    STOPPED,
    RECORDING,
    PAUSED
}
