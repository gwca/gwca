/**
 * The super class of the class/member/course classes.
 */
export class CommonEntity {
    isActive: boolean;
}

/**
 * The class member information.
 */
export class Member extends CommonEntity {
    memberId: string;
    role: string;
    email: string;
    ccEmail: string;
    firstName: string;
    lastName: string;
    chineseName: string;
    relatedMemberIds: string;
    isPrivate: boolean;
    roles: string[];

    // client side only attributes
    isReadonly: boolean;
    infoText: string;

    constructor() {
        super();
        this.memberId = '';
        this.isReadonly = false;
        this.infoText = '';
    }
}

/**
 * The course information.
 */
export class Course extends CommonEntity {
    courseId: string;
    name: string;
    alias: string;
    description: string;
    image: string;

    constructor() {
        super();
        this.courseId = '';
    }
}

/**
 * The class/member/course relationship information retriceved from the data service.
 */
export class Clazz extends CommonEntity {
    classId: string;
    className: string;
    classUrl: string;
    classYear: string;
    courseId: string;
    teacherId: string;

    course: Course;
    members: Member[];

    constructor() {
        super();
        this.classId = '';
    }
}

/** The info class of a member's work. */
export class WorkInfo {
    member: Member;
    course: Course;
    className: string;
    studentIds: string; // space (' ') delimited student IDs.
}

/** The cached class array with a timestamp */
export class ClassCache {
    timestamp: number;
    classes: Clazz[];
}