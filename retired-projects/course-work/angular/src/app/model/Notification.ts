import { Member, Clazz } from './Clazz';

export class Notification {

    constructor() { }

    public getFullName(member: Member): string {
        return member.firstName + ' ' + member.lastName +
            ((member.chineseName != null && member.chineseName.length > 0) ? ' (' + member.chineseName + ')' : '');
    }
}

/**
 * A notification email information.
 */
export class NotificationEmail extends Notification {
    senderEmail: string;
    receiverEmail: string;
    receiverName: string;
    ccEmail: string;
    subject: string
    body: string;
    notifyParent: boolean;

    constructor(sender: Member, receiver: Member, subject: string, body: string, notifyParent: boolean) {
        super();
        if (sender != null && receiver != null) {
            let cname = receiver.chineseName;
            if (cname == null && cname.length == 0) {
                cname = null;
            }
            this.senderEmail = sender.email;
            this.receiverEmail = receiver.email;
            this.receiverName = this.getFullName(receiver);
            this.ccEmail = receiver.ccEmail;
        }
        this.subject = subject;
        this.body = body;
        this.notifyParent = notifyParent;
    }

    public addReplyTo(member: Member) {
        let replyTo = '<div style="margin-top:100px;color:DarkGray">'
            + '<address>Reply this email to <a href="mailto:'
            + member.email + '">' + this.getFullName(member)
            + '</a>.</address></div>';
        this.body = '<div>' + this.body + '</div>' + replyTo;
    }

    public reset() {
        this.body = this.ccEmail = this.receiverEmail = this.senderEmail = this.subject = '';
        this.notifyParent = false;
    }
}

/**
 * An email builder
 */
export class MailBuilder extends Notification {

    homeworkUrl: string

    constructor(url: string) {
        super();
        this.homeworkUrl = url;
    }

    /**
     * Builds an HTML email body for the teacher assigning homework to the students.
     * @param sender
     */
    public buildTeacherAssignStudentBody(sender: Member) {
        const now = this.getSimpleDate();
        let body = this.getBodyFormat();
        body += '<p>Hello $NAME$, </p>\n';
        body += '<p><a href=' + this.homeworkUrl + '>Here</a> is the latest homework assignment.  (' + now + ')</p>\n';
        body += '<p>Thanks!</p>\n';
        body += '<p>' + this.getFullName(sender) + '</p>\n';
        body += '</div>\n';
        // body += '<!-- <div>Additional Message</div> -->';
        return body;
    }

    /**
     * Builds an HTML email body for the teacher notifying admin the homework assignments.
     * @param sender 
     * @param clazz 
     */
    public buildTeacherAssignAdminBody(sender: Member, clazz: Clazz) {
        let body = this.getBodyFormat();
        body += '<p>Hello $NAME$, </p>\n';
        body += '<p>' + this.buildAssignSubject(clazz.className) + '</p>\n';
        body += '<p>The homework is assigned to the following students:</p>\n';
        body += '<ul>\n';
        for (let member of clazz.members) {
            if (member.role === 'STUDENT') {
                body += '<li><a href=' + this.homeworkUrl + '>' + this.getFullName(member) + '</a></li>\n';
            }
        }
        body += '</ul>\n';
        body += '<p>Thanks!</p>\n';
        body += '<p>' + this.getFullName(sender) + '</p>\n';
        body += '</div>\n';
        // body += '<!-- <div>Additional Message</div> -->';
        return body;
    }

    /**
     * Builds an HTML email body for the student notifying the teacher he/she submitted the homework.
     * @param student 
     */
    public buildStudentSubmitBody(student: Member): string {
        const now = this.getSimpleDate();
        return '<p><a href=' + this.homeworkUrl + '>' + this.getFullName(student)
            + '</a> submitted his/her homework (' + now + ')</p>';
    }

    /**
     * Builds an HTML email body for the teacher notifying the student the homework is checked.
     * @param teacher 
     * @param student 
     */
    public buildTeacherCheckBody(teacher: Member, student: Member): string {
        let body = this.getBodyFormat();
        body += '<p>Hello ' + student.firstName + ', </p>';
        body += '<p>I have checked your homework (' + this.getSimpleDate() + ')</p>';
        body += '<p><a href="' + this.homeworkUrl + '">Click Here</a> to review it.</p>';
        body += '<p>Thanks!</p>';
        body += '<p>' + this.getFullName(teacher) + '</p>';
        return body;
    }

    /**
     * Builds an HTML email body for the teacher notifying the student the homework 
     * status is reset to Assigned.
     * @param teacher 
     * @param student 
     */
    public buildTeacherResetBody(teacher: Member, student: Member): string {
        let body = this.getBodyFormat();
        body += '<p>Hello ' + student.firstName + ', </p>';
        body += '<p>I have reset your homework status back to Assigned, you may do it over (' + this.getSimpleDate() + ')</p>';
        body += '<p><a href="' + this.homeworkUrl + '">Click Here</a> to review it.</p>';
        body += '<p>Thanks!</p>';
        body += '<p>' + this.getFullName(teacher) + '</p>';
        return body;
    }

    /**
     * Builds a subject line for the homework assignment.
     * @param className 
     */
    public buildAssignSubject(className: string) {
        const now = this.getSimpleDate();
        return className + ' homework assignment (' + now + ')';
    }

    /**
     * Builds a subject line to notify teacher a student submitted homework.
     * @param student 
     */
    public buildSubmitSubject(student: Member): string {
        return this.getFullName(student) + ' submitted his/her homework (' + this.getSimpleDate() + ')';
    }

    /**
     * Builds a subject line to notify student the homework is checked.
     */
    public buildCheckSubject(): string {
        return 'Teacher checked your homework (' + this.getSimpleDate() + ')';
    }

    /**
     * Builds a subject line to notify student the homework status is reset to Assigned.
     */
    public buildResetSubject(): string {
        return 'Your homework status is reset to Assigned (' + this.getSimpleDate() + ')';
    }

    private getSimpleDate(): string {
        const now = new Date();
        return (now.getMonth() + 1) + '/' + now.getDate() + '/' + now.getFullYear();
    }

    private getBodyFormat(): string {
        return '<div style="font-family:arial,sans-serif;font-size:18px;margin-left:20px;">\n';
    }
}
