import { Injectable } from '@angular/core';
import { StereoAudioRecorder } from 'recordrtc';
import { Observable, Subject } from 'rxjs';
import * as RecordRTC from 'recordrtc';
import * as moment from 'moment';

import { RecordingStatus } from '../model/AppEnums';

interface RecordedAudioOutput {
  blob: Blob;
  title: string;
}

@Injectable({
  providedIn: 'root'
})
export class RecordingService {

  // the properties of the recorder
  private stream: MediaStream;
  private recorder: RecordRTC;
  private recorderConfig: any;

  // the informational properties of the recorder
  private recorderStartTime: moment.MomentInput;
  private recordedTime: moment.Duration;
  private recordingStatus: RecordingStatus;
  private recordedData: Subject<RecordedAudioOutput>;
  private recordingTime: Subject<string>;
  private recordingFailed: Subject<string>;

  // the properties of the player
  private player: any;
  private playingTime: Subject<string>;

  // other properties
  private interval: any;

  /** Initializes all the properties */
  constructor() {
    this.recordingStatus = RecordingStatus.OPENING;
    this.recordedData = new Subject<RecordedAudioOutput>();
    this.recordingTime = new Subject<string>();
    this.recordingFailed = new Subject<string>();
    this.playingTime = new Subject<string>();
    this.recorderConfig = {
      type: 'audio',
      recorderType: StereoAudioRecorder
    };
    this.recorder = null;
    this.stream = null;
    this.interval = null;
    this.recorderStartTime = null;
    this.resetRecordingTime();
  }

  public getRecordedBlob(): Observable<RecordedAudioOutput> {
    return this.recordedData.asObservable();
  }

  public getRecordedTime(): Observable<string> {
    return this.recordingTime.asObservable();
  }

  public getRecordingFailed(): Observable<string> {
    return this.recordingFailed.asObservable();
  }

  public getPlayingTime(): Observable<string> {
    return this.playingTime.asObservable();
  }

  /**
   * Opens the microphone and creates a recorder associated with the microphone.
   * Since it takes a while (a few seconds) some times on some browser, creates
   * one instance of the recorder and keeps using it (the stream and the recorder
   * should not be closed), resets the recorder when it needs to start over.
   * 
   * Don't create a recorder if not needed as it sometimes messup with the vedio
   * or audio players. Calls this method when the Record component gets invoked
   * only for a student when he/she needs to record. See work.record.component.ts.
   */
  public createRecorder() {
    if (this.recorder == null) {
      this.recordingStatus = RecordingStatus.OPENING;
      navigator.mediaDevices.getUserMedia({ audio: true }).then(stream => {
        this.stream = stream;
        this.recorder = RecordRTC(this.stream, this.recorderConfig);
        this.recordingStatus = RecordingStatus.STOPPED;
      }).catch(error => {
        this.recordingStatus = RecordingStatus.NO_MIC;
        this.recordingFailed.next(error);
      });
    }
  }

  /** Starts recording from the microphone using the recorder. */
  public startRecording(): void {
    if (this.toRecord()) {
      this.recordingStatus = RecordingStatus.RECORDING;
      this.recorder.reset();
      this.recorder.startRecording();
      this.recordingTime.next('00:00');
      this.resetRecordingTime();
      this.startRecorderTimer();
    } else {
      this.recordingFailed.next('No recorder got created yet');
    }
  }

  /** Pauses recording */
  public pauseRecording() {
    if (this.toPause()) {
      this.recordingStatus = RecordingStatus.PAUSED;
      let currentTime = moment();
      let duration = moment.duration(currentTime.diff(this.recorderStartTime));
      this.recordedTime.add(duration);
      this.stopTimer();
      this.recorder.pauseRecording();
    }
  }

  /** Resumes recording */
  public resumeRecording() {
    if (this.toResume()) {
      this.recordingStatus = RecordingStatus.RECORDING;
      this.recorderStartTime = moment();
      this.startRecorderTimer();
      this.recorder.resumeRecording();
    }
  }

  /** Stops recording and makes the recorded data available */
  public stopRecording() {
    if (this.toStop()) {
      this.recordingStatus = RecordingStatus.STOPPED;
      this.recorder.stopRecording((blob: any) => {
        const mp3Name = encodeURIComponent(this.getAudioFileName());
        let recordedBlob = this.recorder.getBlob();
        this.recordedData.next({ blob: recordedBlob, title: mp3Name });
        this.stopMedia();
      });
    }
  }

  /** Saves the recorded data into a local disk file */
  public saveRecording() {
    this.recordingStatus = RecordingStatus.STOPPED;
    this.recorder.save(this.getAudioFileName());
  }

  /** Restarts the recording all over */
  public cleanRecording() {
    this.recordingStatus = RecordingStatus.STOPPED;
    this.stopMedia();
  }

  /** Cleans up the resources */
  public abortRecording() {
    this.recordingStatus = RecordingStatus.OPENING;
    this.stopMedia();
    this.recorder = null;
    if (this.stream != null) {
      this.stream.getAudioTracks().forEach(track => track.stop());
      this.stream = null;
    }
  }

  //
  // The methods to play, stop, pause change volumn of the recorded voice
  //
  public play() {
    if (this.toPlay()) {
      this.setVoicePlayer();
      this.player.play();
      this.startPlayerTimer();
    }
  }

  public pause() {
    if (this.toPlay()) {
      this.player.pause();
      this.stopTimer();
    }
  }

  public stop() {
    if (this.toPlay()) {
      this.player.pause();
      this.player.currentTime = 0;
      this.stopTimer();
    }
  }

  public volumeUp() {
    if (this.toPlay()) {
      if (this.player.volume <= 0.8) {
        this.player.volume += 0.2;
      }
    }
  }

  public volumeDown() {
    if (this.toPlay()) {
      if (this.player.volume >= 0.2) {
        this.player.volume -= 0.2;
      }
    }
  }

  //
  // Some utilitiy methods
  //
  private getAudioFileName(): string {
    return "audio_clip.mp3";
  }

  private startRecorderTimer() {
    this.interval = setInterval(
      () => {
        const currentTime = moment();
        const diffTime = moment.duration(currentTime.diff(this.recorderStartTime));
        diffTime.add(this.recordedTime);
        const time = this.toTimeString(diffTime.minutes()) + ':' + this.toTimeString(diffTime.seconds());
        this.recordingTime.next(time);
      },
      1000
    );
  }

  private startPlayerTimer() {
    this.interval = setInterval(
      () => {
        let seconds = Math.trunc(this.player.currentTime);
        let minutes = Math.trunc(seconds / 60);
        seconds = seconds % 60;
        this.playingTime.next(this.toTimeString(minutes) + ':' + this.toTimeString(seconds));
      },
      1000
    );
  }

  private stopTimer() {
    clearInterval(this.interval);
  }

  private resetRecordingTime() {
    this.recorderStartTime = moment();
    this.recordedTime = moment.duration(this.recorderStartTime.diff(this.recorderStartTime));
  }

  private toTimeString(value: any) {
    let val = value;
    if (!value) {
      val = '00';
    }
    if (value < 10) {
      val = '0' + value;
    }
    return val;
  }

  private stopMedia() {
    if (this.recorder != null) {
      this.recordingStatus = RecordingStatus.STOPPED;
      this.resetRecordingTime();
      this.stopTimer();
    }
  }

  private setVoicePlayer() {
    this.player = document.getElementById('voice-player')
  }

  //
  // The methods to check the current recording mode
  //
  public hasMic(): boolean {
    return !(this.recordingStatus == RecordingStatus.NO_MIC);
  }

  public isOpeningMic(): boolean {
    return this.recordingStatus == RecordingStatus.OPENING;
  }

  public toRecord(): boolean {
    return this.recorder != null && this.recordingStatus == RecordingStatus.STOPPED;
  }

  public toStop(): boolean {
    return (this.recordingStatus == RecordingStatus.RECORDING
      || this.recordingStatus == RecordingStatus.PAUSED);
  }

  public toPause(): boolean {
    return (this.recordingStatus == RecordingStatus.RECORDING);
  }

  public toResume(): boolean {
    return (this.recordingStatus == RecordingStatus.PAUSED);
  }

  public toClear(): boolean {
    return (this.recordingStatus == RecordingStatus.STOPPED
      || this.recordingStatus == RecordingStatus.PAUSED);
  }

  public toPlay(): boolean {
    return this.recordingStatus == RecordingStatus.STOPPED
      || this.recordingStatus == RecordingStatus.PAUSED;
  }
}
