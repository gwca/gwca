package org.gwca.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.gwca.error.CommonError;
import org.gwca.error.ErrorSeverity;
import org.gwca.googleapp.impl.GoogleAppException;
import org.gwca.model.message.NameValuePair;
import org.gwca.model.message.SimpleResponse;
import org.gwca.model.message.UploadFileRequest;
import org.gwca.model.message.WorkResponse;
import org.gwca.model.persistence.BreakingNews;
import org.gwca.model.persistence.Clazz;
import org.gwca.model.persistence.DriveFileId;
import org.gwca.model.persistence.DriveFileRole;
import org.gwca.model.persistence.DrivePermission;
import org.gwca.model.persistence.Homework;
import org.gwca.model.persistence.HomeworkId;
import org.gwca.model.persistence.Member;
import org.gwca.model.persistence.MemberRole;
import org.gwca.model.persistence.StorageDescription;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.api.services.drive.model.Permission;

@Service
public class DriveService extends AbstractService {

  /** The default constructor */
  public DriveService() {
    super();
  }

  /**
   * Saves (creates or updates) a homework.
   * 
   * @param homework
   * @return
   */
  public WorkResponse<Homework> saveHomework(Homework homework) {
    CommonError info = null;
    Homework updatedHomework = null;
    try {
      updatedHomework = googleDrive.saveHomework(homework);
      info = createInfo("Homework " + homework.getIdString() + " is saved.");
    } catch (GoogleAppException ex) {
      updatedHomework = homework;
      info = ex.getError();
    }
    WorkResponse<Homework> response = new WorkResponse<Homework>(updatedHomework, info);
    return response;
  }

  /**
   * Deletes a homework.
   * 
   * @param memberId
   * @param courseId
   * @param workNumber
   * @return
   */
  public WorkResponse<Homework> deleteHomework(String memberId, String courseId,
      String workNumber) {
    CommonError info = null;
    HomeworkId homeworkId = new HomeworkId(workNumber, courseId, memberId);
    try {
      boolean deleted = googleDrive.deleteHomework(homeworkId);
      if (deleted) {
        info = createInfo(homeworkId.getIdString() + " is deleted.");
      } else {
        info = createError("HomeworkNotExist", homeworkId.getIdString() + " doesn't exist.");
      }
    } catch (GoogleAppException ex) {
      String msg = "Deleting homework " + homeworkId.getIdString() + " failed.";
      info = ex.getError();
      info.setDescription(msg + info.getDescription());
    }
    WorkResponse<Homework> response = new WorkResponse<Homework>(info);
    return response;
  }

  /**
   * Deletes a list of homework.
   * 
   * @param homeworkIdList
   * @return
   */
  public WorkResponse<Homework> deleteHomeworkList(List<HomeworkId> homeworkIdList) {
    CommonError info = null;
    boolean hasError = false;
    List<CommonError> infoList = new ArrayList<CommonError>();
    for (HomeworkId homeworkId : homeworkIdList) {
      try {
        boolean deleted = googleDrive.deleteHomework(homeworkId);
        info = createInfo("Homework " + homeworkId.getIdString()
            + (deleted ? " is deleted." : " has not been assigned yet."));
      } catch (GoogleAppException ex) {
        String msg = "Deleting homework " + homeworkId.getIdString() + " failed.";
        info = ex.getError();
        info.setDescription(msg + info.getDescription());
        hasError = true;
      }
      infoList.add(info);
    }
    if (hasError) {
      info = this.createError("DeleteHomeworkListFailed", "Saving one or more homework failed.");
    } else {
      info = this.createInfo("Deleting the homework list successed.");
    }
    WorkResponse<Homework> response = new WorkResponse<Homework>(info);
    response.setInfoList(infoList);
    return response;
  }

  /**
   * Saves (creates or updates) a list of homework.
   * 
   * @param homeworkList
   * @return
   */
  public WorkResponse<Homework> saveHomeworkList(List<Homework> homeworkList) {
    CommonError info = null;
    boolean hasError = false;
    List<CommonError> infoList = new ArrayList<CommonError>();
    for (Homework homework : homeworkList) {
      try {
        googleDrive.saveHomework(homework);
        info = createInfo("Homework " + homework.getIdString() + " is saved.");
      } catch (GoogleAppException ex) {
        String msg = "Saving homework " + homework.getIdString() + " failed.";
        info = ex.getError();
        info.setDescription(msg + info.getDescription());
        hasError = true;
      }
      infoList.add(info);
    }
    if (hasError) {
      info = this.createError("SaveHomeworkListFailed", "Saving one or more homework failed.");
    } else {
      info = this.createInfo("Saving the homework list successed.");
    }
    WorkResponse<Homework> response = new WorkResponse<Homework>(info);
    response.setInfoList(infoList);
    return response;
  }


  /**
   * Sets a proper permission to the members' homework folder (Reader, Writer)<br>
   * <li>a student will have an Writer permission to his/her homework folder;</li>
   * <li>a teacher will have an Writer permission to all the students homework folders;</li>
   * <li>a parent will have a Reader permission to his/her children's homework folders;</li>
   * <li>an admin will have a Reader permission to all the students homework folders.</li>
   *
   * @param classId
   * @param members
   * @return
   */
  public WorkResponse<Homework> setUserPermission(String classId, List<String> members) {
    CommonError info = null;
    List<CommonError> infoList = new ArrayList<CommonError>();

    try {

      // create a proper permission for each member according to the role of the member
      Clazz clazz = cacheHandler.getClazz(classId);
      List<DrivePermission> permissions = new ArrayList<DrivePermission>();
      for (String member : members) {
        List<DrivePermission> memberPermissions = createPermissionsForRole(clazz, member);
        permissions.addAll(memberPermissions);
      }

      if (permissions.size() == 0) {
        info = new CommonError();
        info.setErrorCode("SetPermission");
        info.setSeverity(ErrorSeverity.ERROR);
        info.setDescription("No member needs to set permission to.");
        infoList.add(info);
      } else {
        // set the permission to each member
        List<Permission> userPermissions = googleDrive.setUserPermission(permissions);
        for (Permission userPermission : userPermissions) {
          info = new CommonError();
          info.setErrorCode("SetPermission");
          info.setSeverity(ErrorSeverity.INFO);
          info.setDescription("The " + userPermission.getRole() + " permission is set to "
              + userPermission.getEmailAddress());
          infoList.add(info);
        }
      }
    } catch (GoogleAppException ex) {
      String msg = "Setting permission failed.";
      info = ex.getError();
      info.setDescription(msg + info.getDescription());
      infoList.add(info);
    }
    WorkResponse<Homework> response = new WorkResponse<Homework>(info);
    response.setInfoList(infoList);
    return response;
  }

  /**
   * Uploads an audio clip (.mp3, .m4a) or an image file (jpg, jpeg, gif, png).<br>
   * 
   * @param uploadedInputStream
   * @param request
   * @return
   */
  public SimpleResponse uploadFile(MultipartFile file, UploadFileRequest request) {
    CommonError info = null;
    List<NameValuePair> fileAttrs = null;
    try {
      if (request.isValid()) {
        fileAttrs = googleDrive.uploadFile(file.getInputStream(), request);
        info = createInfo("Uploaded " + request.toString());
      } else {
        info = createError("InvalidUploadRequest",
            "Invalid file uploading request " + request.toString());
      }
    } catch (Exception ex) {
      info = createError("UploadingFileFailed", "Uploading file failed " + ex.getMessage());
    }
    SimpleResponse response = new SimpleResponse(fileAttrs, info);
    return response;
  }

  /**
   * Deletes Drive file using the fileLink.
   * 
   * @param uploadedInputStream
   * @param fileDetail
   * @return
   */
  public SimpleResponse deleteFile(String fileLink) {
    CommonError info = null;
    try {
      googleDrive.deleteFile(fileLink);
      info = createInfo("Removed " + fileLink + " from Drive.");
    } catch (GoogleAppException ex) {
      info = ex.getError();
    }
    SimpleResponse response = new SimpleResponse(fileLink, info);
    return response;
  }

  /**
   * Gets the file link if the files exists for a member.
   * 
   * @param memberId
   * @param fileTitle
   * @return
   */
  public SimpleResponse getFileLink(String memberId, String fileTitle) {
    CommonError info = null;
    DriveFileId driveFileId = null;
    try {
      driveFileId = googleDrive.getFileLink(memberId, fileTitle);
      if (driveFileId == null) {
        info = createInfo("File " + fileTitle + " doesn't exist for " + memberId);
      } else {
        info = createInfo("Retrieved a File " + fileTitle + " for: " + memberId);
      }
    } catch (GoogleAppException ex) {
      info = ex.getError();
    }
    SimpleResponse response =
        new SimpleResponse((driveFileId != null ? driveFileId.getFileLink() : ""), info);
    return response;
  }

  /**
   * Gets the content of a homework by homework ID.
   * 
   * @param workNumber
   * @param courseId
   * @param memberId
   * @return
   */
  public WorkResponse<Homework> getHomework(String workNumber, String courseId, String memberId) {
    CommonError info = null;
    Homework homework = null;
    HomeworkId homeworkId = new HomeworkId(workNumber, courseId, memberId);
    homework = googleDrive.getHomework(homeworkId);
    if (homework == null) {
      info = createInfo("Found no homework for: " + homeworkId.getIdString());
    } else {
      info = createInfo("Retrieved a homework for: " + homework.getIdString());
    }
    WorkResponse<Homework> response = new WorkResponse<Homework>(homework, info);
    return response;
  }

  /**
   * Gets a list of homework by courseId and memberId.
   * 
   * @param courseId
   * @param memberId
   * @return
   */
  public WorkResponse<Homework> getHomeworkList(String courseId, String memberId) {
    CommonError info = null;
    List<Homework> homeworkList = new ArrayList<Homework>();
    homeworkList = googleDrive.getHomeworkList(courseId, memberId);
    int size = homeworkList.size();
    if (size == 0) {
      HomeworkId homeworkId = new HomeworkId(courseId, memberId);
      info = createInfo("Found no homework for " + homeworkId.getIdString());
    } else {
      info = createInfo("Retrieved " + size + " homework");
    }
    WorkResponse<Homework> response = new WorkResponse<Homework>(homeworkList, info);
    return response;
  }

  /**
   * Gets a list of submitted homework by courseId a list of member IDs.
   * 
   * @param courseId
   * @param memberId
   * @return
   */
  public WorkResponse<Homework> getSubmittedHomeworkList(String courseId, String memberIDs) {
    CommonError info = null;
    List<Homework> homeworkList = new ArrayList<Homework>();
    List<String> memberIdList = Arrays.asList(memberIDs.split(","));
    homeworkList = googleDrive.getHomeworkList(courseId, memberIdList);
    int size = homeworkList.size();
    if (size == 0) {
      info = createInfo("No submitted homework");
    } else {
      info = createInfo("Retrieved " + size + " submitted homework");
    }
    WorkResponse<Homework> response = new WorkResponse<Homework>(homeworkList, info);
    return response;
  }

  /**
   * Gets a list of homework by courseId, workNumber and a list of member IDs.
   * 
   * @param courseId
   * @param memberId
   * @return
   */
  public WorkResponse<Homework> getHomeworkList(String courseId, String workNumber,
      String memberIDs) {
    CommonError info = null;
    List<Homework> homeworkList = new ArrayList<Homework>();
    List<String> memberIdList = Arrays.asList(memberIDs.trim().split(","));
    homeworkList = googleDrive.getHomeworkList(courseId, workNumber, memberIdList);
    int size = homeworkList.size();
    if (size == 0) {
      HomeworkId homeworkId = new HomeworkId(courseId, workNumber);
      info = createInfo("Found no homework for " + homeworkId.getIdString());
    } else {
      info = createInfo("Retrieved " + size + " homework");
    }
    WorkResponse<Homework> response = new WorkResponse<Homework>(homeworkList, info);
    return response;
  }

  /**
   * Gets a list of break news.
   * 
   * @return
   */
  public WorkResponse<BreakingNews> getBreakingNews() {
    CommonError info = null;
    List<BreakingNews> newsList = googleDrive.getBreakingNews();
    int size = newsList.size();
    if (size == 0) {
      info = createInfo("Found no break news");
    } else {
      info = createInfo("Retrieved " + size + " break news");
    }
    WorkResponse<BreakingNews> response = new WorkResponse<BreakingNews>(newsList, info);
    return response;
  }

  /**
   * Gets the content of the bulletin board.
   * 
   * @return
   */
  public SimpleResponse getBulletinBoard() {
    CommonError info = null;
    String bulletinBoard = googleDrive.getBulletinBoard();
    if (bulletinBoard == null) {
      info = createError("LoadBulletinFailed", "Failed loading the bulletin board.");
    } else {
      info = createInfo("Loaded the content of the bulletin board.");
    }
    SimpleResponse response = new SimpleResponse(bulletinBoard, info);
    return response;
  }

  //
  // Utility methods
  //
  /**
   * Builds up a list of permissions for setting class members' permissions to access Drive
   * file/folder.
   * 
   * @param clazz
   * @param memberId
   * @return
   */
  private List<DrivePermission> createPermissionsForRole(Clazz clazz, String memberId) {

    List<DrivePermission> permissions = new ArrayList<DrivePermission>();
    Member member = cacheHandler.getMember(clazz.getClassId(), memberId);
    MemberRole role = member.getRole();
    DrivePermission permission = null;
    switch (role) {
      case ADMINISTRATOR:
        for (Member classMember : clazz.getMembers()) {
          if (classMember.getRole().isStudent() || classMember.getRole().isTeacher()) {
            permission = createPermission(classMember.getMemberId(), member.getEmail(),
                DriveFileRole.READER);
            if (permission != null) {
              permissions.add(permission);
            }
          }
        }
        break;
      case PARENT:
        String relatedMemberIds = member.getRelatedMemberIds();
        for (String relatedMemberId : relatedMemberIds.split(",")) {
          Member relatedMember = cacheHandler.getMember(clazz.getClassId(), relatedMemberId.trim());
          permission = createPermission(relatedMember.getMemberId(), relatedMember.getEmail(),
              DriveFileRole.READER);
          if (permission != null) {
            permissions.add(permission);
          }
        }
        break;
      case STUDENT:
        permission =
            createPermission(member.getMemberId(), member.getEmail(), DriveFileRole.WRITER);
        if (permission != null) {
          permissions.add(permission);
        }
        break;
      case TEACHER:
      case TA:
        permission =
            createPermission(member.getMemberId(), member.getEmail(), DriveFileRole.WRITER);
        if (permission != null) {
          permissions.add(permission);
        }

        for (Member classMember : clazz.getMembers()) {
          if (classMember.getRole().isStudent()) {
            permission = createPermission(classMember.getMemberId(), classMember.getEmail(),
                DriveFileRole.WRITER);
            if (permission != null) {
              permissions.add(permission);
            }
          }
        }
        break;
      default:
        logger.error("unknown role:" + role.toString());
    }
    return permissions;
  }

  /**
   * Creates a Drive permission for the member.
   * 
   * @param memberId
   * @param email
   * @param role
   * @return
   */
  private DrivePermission createPermission(String memberId, String email, DriveFileRole role) {
    StorageDescription desc = cacheHandler.getStorageDesc(memberId);
    DrivePermission permission = null;
    if (desc != null) {
      permission = new DrivePermission();
      permission.setType("user");
      permission.setFileId(desc.getStorageId());
      permission.setEmail(email);
      permission.setRole(role);
    }
    return permission;
  }
}
