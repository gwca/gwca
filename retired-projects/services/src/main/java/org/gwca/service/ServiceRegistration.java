package org.gwca.service;

import javax.annotation.PostConstruct;

/**
 */
// public class ServiceRegistration extends ResourceConfig {
public class ServiceRegistration {

  // /** The cache handler */
  // @Autowired
  // private CacheHandler cacheHandler = null;


  /**
   * Register JAX-RS application components.
   */
  public ServiceRegistration() {
    // register(RequestContextFilter.class);
    // register(DriveService.class);
    // register(BookService.class);
    // register(SheetService.class);
    // register(EmailService.class);
    // register(AdminService.class);
  }

  /**
   * Initializes the caches after all.
   */
  @PostConstruct
  public void initCaches() {
    // cacheHandler.loadStorageDescriptionCache();
    // cacheHandler.loadTableCache();
    // cacheHandler.loadHomeworkCache(cacheHandler.getClasses());
    // cacheHandler.loadBookIndexCache();
    // cacheHandler.loadBookLessonsCache();
  }

  // public void setCacheHandler(CacheHandler cacheHandler) {
  // this.cacheHandler = cacheHandler;
  // }
}
