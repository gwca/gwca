package org.gwca.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.gwca.error.CommonError;
import org.gwca.model.book.BookIndex;
import org.gwca.model.book.Chapter;
import org.gwca.model.book.Lesson;
import org.gwca.model.message.NameValuePair;
import org.gwca.model.message.SimpleResponse;
import org.gwca.model.message.WorkResponse;
import org.springframework.stereotype.Service;

@Service
public class BookService extends AbstractService {

  /** The default constructor */
  public BookService() {
    super();
  }

  /**
   * Refreshes the book cache.
   *
   * @return
   */
  public SimpleResponse refreshBookCache() {
    CommonError info = null;
    try {
      cacheHandler.loadBookIndexCache();
      cacheHandler.loadBookLessonsCache();
      info = createInfo("The book cache (index and books) is refreshed at:" + new Date());
    } catch (Exception ex) {
      info =
          createError("RefreshCacheFailed", "Refreshing the book cache failed:" + ex.getMessage());
    }
    logger.info(info.toString());
    return new SimpleResponse(info);
  }

  /**
   * Gets the cached lesson list.
   * 
   * @return
   */
  public WorkResponse<BookIndex> getBookIndex() {
    CommonError info = null;
    List<BookIndex> bookIndex = cacheHandler.getBookIndex();
    int size = bookIndex.size();
    if (size == 0) {
      info = createInfo("Found no book.");
    } else {
      info = createInfo("Retrieved " + size + " books");
    }
    WorkResponse<BookIndex> response = new WorkResponse<BookIndex>(bookIndex, info);
    return response;
  }

  /**
   * Gets the cached lesson of a book.
   * 
   * @return
   */
  public SimpleResponse getBookLesson(String bookTitle, String lessonId) {
    CommonError info = null;
    String lessonText = cacheHandler.getBookLesson(bookTitle, lessonId);
    if (lessonText == null || lessonText.length() == 0) {
      info = createInfo("Did not find the lesson " + lessonId + " of the book:" + bookTitle);
    } else {
      info = createInfo("Retrieved the lesson " + lessonId + " of the book:" + bookTitle);
    }
    List<NameValuePair> values = new ArrayList<NameValuePair>();
    NameValuePair pair = new NameValuePair(lessonId, lessonText);
    values.add(pair);
    SimpleResponse response = new SimpleResponse(values, info);
    return response;
  }

  /**
   * Creates a list of textbook query parameters of the specified course
   */
  public SimpleResponse getLessonURLs(String courseId) {

    List<NameValuePair> lessonUrls = new ArrayList<NameValuePair>();
    List<BookIndex> bookIndex = cacheHandler.getBookIndex();

    for (BookIndex book : bookIndex) {
      if (book.getCourseId().equals(courseId)) {
        for (Lesson lesson : book.getLessons()) {
          for (Chapter chapter : lesson.getChapters()) {
            if (showChapter(chapter)) {
              String title = lesson.getTitle() + ", " + chapter.getLabel();
              String url = buildQueryString(courseId, lesson.getLessonId(), chapter.getChapterId());
              NameValuePair lessonURL = new NameValuePair(title, url);
              lessonUrls.add(lessonURL);
            }
          }
        }
      }
    }
    CommonError info = createInfo("Retrieved " + lessonUrls.size() + " lesson URLs.");
    SimpleResponse response = new SimpleResponse(lessonUrls, info);
    return response;
  }

  //
  // utilities
  //
  private String buildQueryString(String courseId, String lessonId, String chapterId) {
    String queryParameters = "?type=course&course=" + courseId;
    if (lessonId != null && lessonId.length() > 0) {
      queryParameters += "&lesson=" + lessonId;
    }
    if (chapterId != null && chapterId.length() > 0) {
      queryParameters += "&chapter=" + chapterId;
    }
    return queryParameters;
  }

  private boolean showChapter(Chapter chapter) {
    String label = chapter.getLabel();
    return (label != null && label.toLowerCase().startsWith("text"));
  }
}
