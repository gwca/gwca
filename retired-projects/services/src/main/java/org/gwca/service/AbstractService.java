package org.gwca.service;

import org.gwca.error.CommonError;
import org.gwca.error.ErrorSeverity;
import org.gwca.googleapp.stub.CacheHandler;
import org.gwca.googleapp.stub.DriveRepository;
import org.gwca.googleapp.stub.MailHandler;
import org.gwca.googleapp.stub.SheetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractService {

  protected Logger logger = LoggerFactory.getLogger(this.getClass().getSimpleName());

  //
  // the injected properties
  //
  @Autowired
  protected DriveRepository googleDrive = null;

  @Autowired
  protected SheetRepository googleSheet = null;

  @Autowired
  protected MailHandler googleMail = null;

  @Autowired
  protected CacheHandler cacheHandler = null;

  public AbstractService() {
    super();
  }

  /**
   * Creates an info type of common error.
   * 
   * @return
   */
  protected CommonError createInfo(String description) {
    CommonError info = new CommonError();
    info.setErrorCode("OK");
    info.setDescription(description);
    info.setSeverity(ErrorSeverity.INFO);
    return info;
  }

  /**
   * Creates an error type of error
   * 
   * @param code
   * @param description
   * @return
   */
  protected CommonError createError(String code, String description) {
    CommonError info = new CommonError();
    info.setErrorCode(code);
    info.setDescription(description);
    info.setSeverity(ErrorSeverity.ERROR);
    return info;
  }

  /**
   * Logs the message accordingly.
   * 
   * @param msg
   */
  protected void logMessage(CommonError msg) {
    if (msg.isSevereError()) {
      logger.error(msg.toString());
    } else {
      logger.info(msg.toString());
    }
  }

  //
  // the injections (for the unit test cases)
  //
  public void setGoogleDrive(DriveRepository googleDrive) {
    this.googleDrive = googleDrive;
  }

  public void setGoogleSheet(SheetRepository googleSheet) {
    this.googleSheet = googleSheet;
  }

  public void setCacheHandler(CacheHandler cacheHandler) {
    this.cacheHandler = cacheHandler;
  }

  public void setGoogleMail(MailHandler googleMail) {
    this.googleMail = googleMail;
  }
}
