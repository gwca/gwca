package org.gwca.service;

import java.util.List;

import org.gwca.error.CommonError;
import org.gwca.googleapp.impl.GoogleAppException;
import org.gwca.model.message.ClassResponse;
import org.gwca.model.persistence.Clazz;
import org.gwca.model.persistence.Member;
import org.springframework.stereotype.Service;

@Service
public class SheetService extends AbstractService {

  /** The default constructor */
  public SheetService() {
    super();
  }

  /**
   * Gets a list of active class/course/members relationship object graph.
   * 
   * @return
   */
  public ClassResponse<Clazz> getActiveClasses() {
    CommonError info = null;
    List<Clazz> classes = null;
    ClassResponse<Clazz> response = null;
    try {
      classes = googleSheet.getClasses();
      info = createInfo("Retrieved a list of classes.");
    } catch (GoogleAppException ex) {
      info = ex.getError();
    }
    response = new ClassResponse<Clazz>(classes, info);
    return response;
  }

  /**
   * Gets a list of the registered active members.<br>
   * This is for migrating the old data only.
   * 
   * @return
   */
  public ClassResponse<Member> getMembers() {
    CommonError info = null;
    List<Member> members = null;
    ClassResponse<Member> response = null;
    try {
      members = googleSheet.getActiveMembers();
      info = createInfo("Retrieved a list of members.");
    } catch (GoogleAppException ex) {
      info = ex.getError();
    }
    response = new ClassResponse<Member>(members, info);
    return response;
  }
}
