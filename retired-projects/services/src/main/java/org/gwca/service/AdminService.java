package org.gwca.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.gwca.error.CommonError;
import org.gwca.model.message.ClassResponse;
import org.gwca.model.message.NameValuePair;
import org.gwca.model.message.SimpleResponse;
import org.gwca.model.persistence.Clazz;
import org.gwca.model.persistence.Member;
import org.gwca.model.persistence.StorageDescription;
import org.gwca.model.persistence.StorageName;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class AdminService extends AbstractService {

  private static final Logger logger = Logger.getLogger("AdminService");

  @Value("${admin.data.url.prefix}")
  private String dataUrlPrefix;

  @Value("${admin.data.url.suffix}")
  private String dataUrlSuffix;

  /** The default constructor */
  public AdminService() {
    super();
  }

  /**
   * Refreshes the homework and the classes caches.
   *
   * @return
   */
  public CommonError refreshCache() {
    CommonError info = null;
    try {
      cacheHandler.loadStorageDescriptionCache();
      cacheHandler.loadTableCache();
      cacheHandler.loadHomeworkCache(cacheHandler.getClasses());
      info = createInfo("The homework and classes caches are refreshed at:" + new Date());
    } catch (Exception ex) {
      info = createError("RefreshCacheFailed", "Refreshing cache failed:" + ex.getMessage());
    }
    logger.info(info.toString());
    return info;
  }

  /**
   * Gets the cached URLs.
   * 
   * @return
   */
  public SimpleResponse getCachedURLs() {
    CommonError info = null;
    List<NameValuePair> values = new ArrayList<NameValuePair>();
    try {
      values
          .add(createUrlPair(cacheHandler.getStorageDesc(StorageName.REGISTRATION_SHEET.value())));
      info = createInfo("Created " + values.size() + " spreadsheet URL(s).");
    } catch (Exception ex) {
      info = createError("GetSheetIDsFailed", "Getting spreadsheet IDs failed:" + ex.getMessage());
    }
    SimpleResponse response = new SimpleResponse(values, info);
    logger.info(info.toString());
    return response;
  }

  private NameValuePair createUrlPair(StorageDescription desc) {
    String url = dataUrlPrefix + desc.getStorageId() + dataUrlSuffix;
    return new NameValuePair(desc.getName(), url);
  }

  /**
   * Validates if the email belongs to a valid member.
   *
   * @return
   */
  public ClassResponse<Member> validateMember(String email) {
    CommonError info = null;
    ClassResponse<Member> response = null;
    try {
      Clazz clazz = null;
      Member member = cacheHandler.getMember(email);
      if (member != null) {
        clazz = cacheHandler.getClazz(member);
      }
      if (member != null && clazz != null) {
        info = createInfo(email + " is validated.");
        response = new ClassResponse<Member>(clazz.getClassId(), member, info);
      } else {
        info = this.createError("InvalidMember", "Invalid member:" + email);
        response = new ClassResponse<Member>(info);
      }
    } catch (Exception ex) {
      info = createError("ValidateMemberFailed", "Validate member failed:" + ex.getMessage());
      response = new ClassResponse<Member>(info);
    }
    logger.info(info.toString());
    return response;
  }
}
