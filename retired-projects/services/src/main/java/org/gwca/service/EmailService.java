package org.gwca.service;

import java.util.ArrayList;
import java.util.List;

import org.gwca.error.CommonError;
import org.gwca.googleapp.impl.GoogleAppException;
import org.gwca.model.message.NotificationRequest;
import org.gwca.model.message.WorkResponse;
import org.gwca.model.persistence.Homework;
import org.springframework.stereotype.Service;

@Service
public class EmailService extends AbstractService {

  /** The default constructor */
  public EmailService() {
    super();
  }

  /**
   * Sends a notification email to the receiver specified by the request.
   * 
   * @return
   */
  public WorkResponse<Homework> sendNotificationEmail(NotificationRequest request) {
    CommonError info = null;
    try {
      googleMail.sendNotification(request);
      info = createInfo(request.getReceiverEmail() + " is notified.");
    } catch (GoogleAppException ex) {
      info = ex.getError();
    }
    logMessage(info);
    WorkResponse<Homework> response = new WorkResponse<Homework>(info);
    return response;
  }


  /**
   * Sends a list of notification email to the receivers specified by the request.
   * 
   * @return
   */
  public WorkResponse<Homework> sendNotificationEmailList(List<NotificationRequest> requestList) {
    CommonError info = null;
    boolean hasError = false;
    List<CommonError> infoList = new ArrayList<CommonError>();

    for (NotificationRequest request : requestList) {
      info = null;
      try {
        googleMail.sendNotification(request);
        info = createInfo(request.getReceiverEmail() + " is notified.");
      } catch (GoogleAppException ex) {
        String msg = "Notifying " + request.getReceiverEmail() + " failed.";
        info = createInfo(request.getReceiverEmail() + " is notified.");
        info = ex.getError();
        info.setDescription(msg + info.getDescription());
        hasError = true;
      }
      infoList.add(info);
    }
    if (hasError) {
      info = this.createError("NotifyMembersFailed", "Notifying members failed.");
    } else {
      info = this.createInfo("The members are notified.");
    }
    WorkResponse<Homework> response = new WorkResponse<Homework>(info);
    response.setInfoList(infoList);
    return response;
  }
}
