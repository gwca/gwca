package org.gwca.model.persistence;

/**
 * The abstract super class for all the Google drive objects.
 * 
 * @author youping.hu
 */
public class DriveFile extends CommonEntity {

  public DriveFile() {
    super();
  }
}
