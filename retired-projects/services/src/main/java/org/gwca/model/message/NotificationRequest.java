package org.gwca.model.message;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * The request class for sending a notification email
 * 
 * @author youping.hu
 */
@Component
public class NotificationRequest {

  @Value("${admin.sender.email}")
  String senderEmail;
  @Value("${admin.receiver.email}")
  String receiverEmail;
  @Value("${admin.receiver.name}")
  String receiverName;
  String ccEmail;
  String subject;
  String body;
  boolean notifyParent;

  /** The default constructor */
  public NotificationRequest() {
    super();
  }

  public String getSenderEmail() {
    return senderEmail;
  }

  public void setSenderEmail(String senderEmail) {
    this.senderEmail = senderEmail;
  }

  public String getReceiverEmail() {
    return receiverEmail;
  }

  public String getReceiverName() {
    return receiverName;
  }

  public void setReceiverName(String receiverName) {
    this.receiverName = receiverName;
  }

  public void setReceiverEmail(String receiverEmail) {
    this.receiverEmail = receiverEmail;
  }

  public String getCcEmail() {
    return ccEmail;
  }

  public void setCcEmail(String ccEmail) {
    this.ccEmail = ccEmail;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public boolean isNotifyParent() {
    return notifyParent;
  }

  public void setNotifyParent(boolean notifyParent) {
    this.notifyParent = notifyParent;
  }
}
