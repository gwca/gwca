package org.gwca.model.workitem;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * The AnswerQuestion workitem.
 * 
 * @author youping.hu
 */
public class AnswerQuestion extends Workitem {

  private List<Question> questions;

  public AnswerQuestion() {
    super();
  }

  public AnswerQuestion(@JsonProperty("description") String description,
      @JsonProperty("title") String title, @JsonProperty("index") Integer index,
      @JsonProperty("checked") boolean checked, @JsonProperty("remark") String remark,
      @JsonProperty("questions") List<Question> questions) {
    super(description, title, index, checked, remark);
    this.questions = questions;
  }

  public List<Question> getQuestions() {
    if (questions == null) {
      questions = new ArrayList<Question>();
    }
    return questions;
  }

  public void setQuestions(List<Question> questions) {
    this.questions = questions;
  }
}
