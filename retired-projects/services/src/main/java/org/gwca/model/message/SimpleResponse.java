package org.gwca.model.message;

import java.util.ArrayList;
import java.util.List;

import org.gwca.error.CommonError;

/**
 * The homework response class
 * 
 * @author youping.hu
 */
public class SimpleResponse {

  private CommonError info;
  private Object value;
  private List<NameValuePair> entities;

  /** The default constructor */
  public SimpleResponse() {
    super();
  }

  /** The copy constructors */
  public SimpleResponse(CommonError error) {
    setInfo(error);
  }

  public SimpleResponse(Object value, CommonError error) {
    setInfo(error);
    setValue(value);
  }
  
  public SimpleResponse(List<NameValuePair> values, CommonError error) {
    setInfo(error);
    if (values == null) {
      values = new ArrayList<NameValuePair>();
    }
    setEntities(values);
  }

  public int getStatus() {
    return (info == null ? 200 : info.isSevereError() ? 500 : 200);
  }

  public CommonError getInfo() {
    return info;
  }

  public void setInfo(CommonError info) {
    this.info = info;
  }

  public Object getValue() {
    return value;
  }

  public void setValue(Object value) {
    this.value = value;
  }

  public List<NameValuePair> getEntities() {
    return entities;
  }

  public void setEntities(List<NameValuePair> values) {
    this.entities = values;
  }
}
