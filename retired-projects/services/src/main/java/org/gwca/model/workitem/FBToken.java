package org.gwca.model.workitem;

/**
 * The fill blank class for the FBStatement token.
 * 
 * @author youping.hu
 */
public class FBToken extends Token {

  private boolean hasBlank;
  private String phrase;

  public FBToken() {
    super();
  }

  public boolean isHasBlank() {
    return hasBlank;
  }

  public void setHasBlank(boolean hasBlank) {
    this.hasBlank = hasBlank;
  }

  public String getPhrase() {
    return phrase;
  }

  public void setPhrase(String phrase) {
    this.phrase = phrase;
  }
}
