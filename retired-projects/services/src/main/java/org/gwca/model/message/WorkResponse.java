package org.gwca.model.message;

import java.util.List;

import org.gwca.error.CommonError;
import org.gwca.model.persistence.DriveFile;

/**
 * The homework response class
 * 
 * @author youping.hu
 */
public class WorkResponse<T extends DriveFile> extends ServiceResponse<T> {

  /** The default constructor */
  public WorkResponse() {
    super();
  }

  /** The copy constructors */
  public WorkResponse(CommonError error) {
    setInfo(error);
  }

  public WorkResponse(T entity, CommonError error) {
    getEntities().clear();
    getEntities().add(entity);
    setInfo(error);
  }

  public WorkResponse(List<T> entities, CommonError error) {
    setEntities(entities);
    setInfo(error);
  }
}
