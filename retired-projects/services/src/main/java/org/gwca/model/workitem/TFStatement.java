package org.gwca.model.workitem;

/**
 * The word class for the PhraseSentence work item.
 * 
 * @author youping.hu
 */
public class TFStatement extends Token {

  private String question;

  public TFStatement() {
    super();
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }
}
