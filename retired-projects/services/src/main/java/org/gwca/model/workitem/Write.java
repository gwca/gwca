package org.gwca.model.workitem;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Write workitem.
 * 
 * @author youping.hu
 */
public class Write extends SimpleItem {

  private String docType;

  public Write() {
    super();
  }

  public Write(@JsonProperty("description") String description, @JsonProperty("title") String title,
      @JsonProperty("index") Integer index, @JsonProperty("checked") boolean checked,
      @JsonProperty("remark") String remark,
      @JsonProperty("instructions") List<String> instructions,
      @JsonProperty("fileId") String fileId, @JsonProperty("url") String url,
      @JsonProperty("text") String text, @JsonProperty("docType") String docType) {
    super(description, title, index, checked, remark, instructions, fileId, url, text);
    this.docType = docType;
  }

  public String getDocType() {
    return docType;
  }

  public void setDocType(String docType) {
    this.docType = docType;
  }
}
