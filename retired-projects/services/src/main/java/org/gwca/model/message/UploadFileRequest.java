package org.gwca.model.message;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.gwca.model.persistence.HomeworkId;

/**
 * The request class for getting and validating a file uploading request.
 * 
 * @author youping.hu
 */
public class UploadFileRequest {

  private static final String VALID_AUDIO_TYPE = "audio/mp3";
  private static final String VALID_IMAGE_TYPE = "image/jpeg";
  private static final String[] values = {"MP3", "M4A", "JPG", "JPEG", "PNG", "GIF"};
  private static final List<String> extensions = Arrays.asList(values);

  private String filename;
  private String filetype;
  private String memberId;
  private String courseId;
  private String workNumber;

  /** The default constructor */
  public UploadFileRequest() {
    super();
  }

  public UploadFileRequest(String filename, String filetype, String memberId, String courseId,
      String workNumber) {
    setFilename(filename);
    setFiletype(filetype);
    setMemberId(memberId);
    setCourseId(courseId);
    setWorkNumber(workNumber);
  }

  public String toString() {
    return filename + ", " + filetype + ", " + workNumber + ", " + courseId + "," + memberId;
  }

  /** Creates a homeworkId object to return */
  public HomeworkId getHomeworkId() {
    return new HomeworkId(workNumber, courseId, memberId);
  }

  /** Creates a Drive file title */
  public String getDriveFileTitle() {
    return getHomeworkId().getIdString() + "_" + getFilenameWithoutPath();
  }

  /** Checks if the request is valid */
  public boolean isValid() {
    return (isNotEmptyId() && isSupportedType() && isValidExtension());
  }

  public boolean isAudio() {
    return filetype.toLowerCase().startsWith("audio");
  }

  public boolean isImage() {
    return filetype.toLowerCase().startsWith("image");
  }

  private String getFilenameWithoutPath() {
    int idx = filename.lastIndexOf("/");
    return (idx < 0 ? filename : filename.substring(idx + 1));
  }

  private boolean isSupportedType() {
    boolean supported = false;
    if (isAudio()) {
      filetype = VALID_AUDIO_TYPE;
      supported = true;
    } else if (isImage()) {
      filetype = VALID_IMAGE_TYPE;
      supported = true;
    }
    return supported;
  }

  private boolean isValidExtension() {
    int idx = filename.lastIndexOf('.');
    if (idx < 0) {
      return false;
    }

    String ext = filename.substring(idx + 1).toUpperCase();
    return extensions.contains(ext);
  }

  private boolean isNotEmptyId() {
    return (!StringUtils.isEmpty(filename) && !StringUtils.isEmpty(memberId)
        && !StringUtils.isEmpty(courseId) && !StringUtils.isEmpty(workNumber)
        && !StringUtils.isEmpty(filetype));
  }

  // the accessors
  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public String getFiletype() {
    return filetype;
  }

  public void setFiletype(String filetype) {
    filetype = filetype.toLowerCase();
    if (filetype.startsWith("image")) {
      this.filetype = VALID_IMAGE_TYPE;
    } else if (filetype.startsWith("audio")) {
      this.filetype = VALID_AUDIO_TYPE;
    } else {
      this.filetype = filetype;
    }
  }

  public String getMemberId() {
    return memberId;
  }

  public void setMemberId(String memberId) {
    this.memberId = memberId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getWorkNumber() {
    return workNumber;
  }

  public void setWorkNumber(String workNumber) {
    this.workNumber = workNumber;
  }
}
