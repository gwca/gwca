package org.gwca.model.message;

public class NameValuePair {
  private String name;
  private String value;
  
  public NameValuePair() {
  }

  public NameValuePair(String name, String value) {
    setName(name);
    setValue(value);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
