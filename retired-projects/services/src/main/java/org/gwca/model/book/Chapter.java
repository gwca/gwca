package org.gwca.model.book;

import java.util.List;

/**
 * A chapter of a lesson that is one of the following types: [audio, video] with one or more
 * associated texts (URLs of some html pages)
 */
public class Chapter {
  private String chapterId;
  private String label;
  private String mediaType;
  private String mediaUrl;
  private List<String> textUrls;

  public Chapter() {}

  public String getChapterId() {
    return chapterId;
  }

  public void setChapterId(String chapterId) {
    this.chapterId = chapterId;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getMediaType() {
    return mediaType;
  }

  public void setMediaType(String mediaType) {
    this.mediaType = mediaType;
  }

  public String getMediaUrl() {
    return mediaUrl;
  }

  public void setMediaUrl(String mediaUrl) {
    this.mediaUrl = mediaUrl;
  }

  public List<String> getTextUrls() {
    return textUrls;
  }

  public void setTextUrls(List<String> textUrls) {
    this.textUrls = textUrls;
  }
}
