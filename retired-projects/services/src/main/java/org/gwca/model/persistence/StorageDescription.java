package org.gwca.model.persistence;

import org.gwca.googleapp.impl.StorageType;

/**
 * The storage description bean.
 * 
 * @author youping.hu
 */
public class StorageDescription {
  
  private String storageId;
  private StorageType storageType;
  private String name;
  private String columns;
  private String description;

  public StorageDescription() {
    super();
  }

  public StorageDescription(String id, StorageType storageType, String name, String column, String description) {
    this.setStorageId(id);
    this.setStorageType(storageType);
    this.setName(name);
    this.setColumns(column);
    this.setDescription(description);
  }

  public StorageDescription(String id, StorageType storageType, String name, String description) {
    this.setStorageId(id);
    this.setStorageType(storageType);
    this.setName(name);
    this.setDescription(description);
  }

  public String getStorageId() {
    return storageId;
  }

  public void setStorageId(String id) {
    this.storageId = id;
  }

  public StorageType getStorageType() {
    return storageType;
  }

  public void setStorageType(StorageType storageType) {
    this.storageType = storageType;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getColumns() {
    return columns;
  }

  public void setColumns(String columns) {
    this.columns = columns;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
