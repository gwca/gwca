package org.gwca.model.persistence;

public enum MemberRole {

  STUDENT("Student"), TEACHER("Teacher"), PARENT("Parent"), ADMINISTRATOR("Administrator"), TA("TA");

  private final String role;

  private MemberRole(String role) {
    this.role = role;
  }

  public String value() {
    return role;
  }

  public static MemberRole fromValue(String roleValue) {
    for (MemberRole role : MemberRole.values()) {
      if (role.value().equals(roleValue)) {
        return role;
      }
    }
    return null;
  }

  public boolean isStudent() {
    return this.equals(MemberRole.STUDENT);
  }

  public boolean isParent() {
    return this.equals(MemberRole.PARENT);
  }

  public boolean isTeacher() {
    return !this.equals(MemberRole.STUDENT);
  }

  public boolean isAdministrator() {
    return this.equals(MemberRole.ADMINISTRATOR);
  }

  public boolean isTA() {
    return this.equals(MemberRole.TA);
  }
}
