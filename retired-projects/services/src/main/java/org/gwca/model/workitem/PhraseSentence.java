package org.gwca.model.workitem;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The PhraseSentence workitem.
 * 
 * @author youping.hu
 */
public class PhraseSentence extends Workitem {

  private List<Word> words;

  public PhraseSentence() {
    super();
  }

  public PhraseSentence(@JsonProperty("description") String description,
      @JsonProperty("title") String title, @JsonProperty("index") Integer index,
      @JsonProperty("checked") boolean checked, @JsonProperty("remark") String remark,
      @JsonProperty("words") List<Word> words) {
    super(description, title, index, checked, remark);
    this.words = words;
  }

  public List<Word> getWords() {
    if (words == null) {
      words = new ArrayList<Word>();
    }
    return words;
  }

  public void setWords(List<Word> words) {
    this.words = words;
  }
}
