package org.gwca.model.persistence;

/**
 * The abstract super class for all the persistence objects.
 * 
 * @author youping.hu
 */
public class CommonEntity {

  public CommonEntity() {
    super();
  }
}
