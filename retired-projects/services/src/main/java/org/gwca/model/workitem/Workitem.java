package org.gwca.model.workitem;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

/**
 * The super class of all types of work items.
 * 
 * @author youping.hu
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "name")
@JsonSubTypes({@Type(value = SimpleItem.class, name = "SimpleItem"),
    @Type(value = AnswerQuestion.class, name = "AnswerQuestion"),
    @Type(value = ConnectSentence.class, name = "ConnectSentence"),
    @Type(value = FillBlank.class, name = "FillBlank"),
    @Type(value = MultiChoice.class, name = "MultiChoice"),
    @Type(value = PhraseSentence.class, name = "PhraseSentence"),
    @Type(value = TrueFalse.class, name = "TrueFalse")})
public abstract class Workitem {

  // private String name;
  private String description;
  private String title;
  private Integer index;
  private String remark;
  private boolean checked;

  public Workitem() {
    super();
  }

  public Workitem(String description, String title, Integer index, boolean checked, String remark) {
    super();
    setDescription(description);
    setTitle(title);
    setIndex(index);
    setChecked(checked);
    setRemark(remark);
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Integer getIndex() {
    return index;
  }

  public void setIndex(Integer index) {
    this.index = index;
  }

  public boolean isChecked() {
    return checked;
  }

  public void setChecked(boolean checked) {
    this.checked = checked;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }
}
