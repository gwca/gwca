package org.gwca.model.workitem;

import java.util.ArrayList;
import java.util.List;

/**
 * The fill blank statement class for the FillBlank work item.
 * 
 * @author youping.hu
 */
public class FBStatement extends Token {

  private List<FBToken> tokens;
  private String selections;

  public FBStatement() {
    super();
  }

  public List<FBToken> getTokens() {
    if (tokens == null) {
      tokens = new ArrayList<FBToken>();
    }
    return tokens;
  }

  public void setTokens(List<FBToken> tokens) {
    this.tokens = tokens;
  }

  public String getSelections() {
    return selections;
  }

  public void setSelections(String selections) {
    this.selections = selections;
  }
}
