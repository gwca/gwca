package org.gwca.model.persistence;

import java.util.ArrayList;
import java.util.List;

/**
 * The class members relationship bean.
 * 
 * @author youping.hu
 */
public class Member extends GoogleSheet {

  private String memberId;
  private MemberRole role;
  private String email;
  private String ccEmail;
  private String firstName;
  private String lastName;
  private String chineseName;
  private String relatedMemberIds;
  private boolean isPrivate;

  private List<MemberRole> roles;

  public Member() {
    super();
  }

  // helpers
  public String createFullName() {
    return firstName + " " + lastName;
  }

  // accessors
  public String getMemberId() {
    return memberId;
  }

  public void setMemberId(String memberId) {
    this.memberId = memberId;
  }

  public MemberRole getRole() {
    return role;
  }

  public void setRole(MemberRole role) {
    this.role = role;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getCcEmail() {
    return ccEmail;
  }

  public void setCcEmail(String ccEmail) {
    this.ccEmail = ccEmail;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getChineseName() {
    return chineseName;
  }

  public void setChineseName(String chineseName) {
    this.chineseName = chineseName;
  }

  public String getRelatedMemberIds() {
    return relatedMemberIds;
  }

  public void setRelatedMemberIds(String relatedMemberIds) {
    this.relatedMemberIds = relatedMemberIds;
  }

  public boolean isPrivate() {
    return isPrivate;
  }

  public void setPrivate(boolean isPrivate) {
    this.isPrivate = isPrivate;
  }

  public List<MemberRole> getRoles() {
    if (roles == null) {
      roles = new ArrayList<MemberRole>();
    }
    return roles;
  }

  public void setRoles(List<MemberRole> roles) {
    this.roles = roles;
  }
}
