package org.gwca.model.message;

import java.util.ArrayList;
import java.util.List;

import org.gwca.error.CommonError;
import org.gwca.model.persistence.CommonEntity;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The response class for data exchanging
 * 
 * @author youping.hu
 */
public abstract class ServiceResponse<T extends CommonEntity> {

  private String classId;
  private CommonError info;
  private List<CommonError> infoList;
  private List<T> entities;


  /** The default constructor */
  public ServiceResponse() {
    super();
  }

  public CommonError getInfo() {
    return info;
  }

  public void setInfo(CommonError info) {
    this.info = info;
  }

  @JsonIgnore
  public T getFirstEntity() {
    if (getEntities().size() > 0) {
      return getEntities().get(0);
    }
    return null;
  }

  public String getClassId() {
    return classId;
  }

  public void setClassId(String classId) {
    this.classId = classId;
  }

  public List<T> getEntities() {
    if (entities == null) {
      entities = new ArrayList<T>();
    }
    return entities;
  }

  public void setEntities(List<T> entities) {
    this.entities = entities;
  }

  public List<CommonError> getInfoList() {
    if (infoList == null) {
      infoList = new ArrayList<CommonError>();
    }
    return infoList;
  }

  public void setInfoList(List<CommonError> infoList) {
    this.infoList = infoList;
  }
}
