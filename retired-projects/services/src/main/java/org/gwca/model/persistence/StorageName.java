package org.gwca.model.persistence;

/**
 * Names for the data storage that could be either a Fusion Table name or a Drive folder name.
 * 
 * @author youping.hu
 *
 */
public enum StorageName {

  ROOT_FOLDER("RootFolder"), 
  HOMEWORK_FOLDER("HomeworkFolder"), 
  BOOK_FOLDER("BookFolder"), 
  COURSE_TABLE("CourseTable"), 
  MEMBER_TABLE("MemberTable"), 
  CLASS_TABLE("ClassTable"), 
  ESSAY_TEMPLATE("EssayTemplate"),
  SLIDE_TEMPLATE("SlideTemplate"),
  BOOK_LIST("BookList"),
  REGISTRATION_SHEET("RegistrationSheet"),
  CLASS_SHEET("Classes"), 
  COURSE_SHEET("Courses"), 
  MEMBER_SHEET("Members"), 
  BREAKING_NEWS("BreakingNews"), 
  BULLETIN_BOARD("BulletinBoard"),
  AUDIO_FOLDER("AudioFolder"),
  UNKOWN("Unknown");

  private final String name;

  private StorageName(String name) {
    this.name = name;
  }

  public String value() {
    return name;
  }

  public static StorageName fromValue(String name) {
    for (StorageName storageName : StorageName.values()) {
      if (storageName.value().equals(name)) {
        return storageName;
      }
    }
    return StorageName.UNKOWN;
  }
}
