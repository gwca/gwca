package org.gwca.model.persistence;

/**
 * The abstract super class for all the google spreadsheet objects.
 * 
 * @author youping.hu
 */
public abstract class GoogleSheet extends CommonEntity {

  private boolean isActive;
  private String storageId;

  public GoogleSheet() {
    super();
  }

  public String getStorageId() {
    return storageId;
  }

  public void setStorageId(String storageId) {
    this.storageId = storageId;
  }

  public boolean isActive() {
    return isActive;
  }

  public void setActive(boolean isActive) {
    this.isActive = isActive;
  }
}
