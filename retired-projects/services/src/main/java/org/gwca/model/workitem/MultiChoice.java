package org.gwca.model.workitem;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The MultiChoice workitem.
 * 
 * @author youping.hu
 */
public class MultiChoice extends Workitem {

  private List<MCStatement> statements;

  public MultiChoice() {
    super();
  }

  public MultiChoice(@JsonProperty("description") String description,
      @JsonProperty("title") String title, @JsonProperty("index") Integer index,
      @JsonProperty("checked") boolean checked, @JsonProperty("remark") String remark,
      @JsonProperty("statements") List<MCStatement> statements) {
    super(description, title, index, checked, remark);
    this.statements = statements;
  }

  public List<MCStatement> getStatements() {
    if (statements == null) {
      statements = new ArrayList<MCStatement>();
    }
    return statements;
  }

  public void setStatements(List<MCStatement> statements) {
    this.statements = statements;
  }
}
