package org.gwca.model.workitem;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Record workitem.
 * 
 * @author youping.hu
 */
public class Record extends SimpleItem {

  public Record() {
    super();
  }

  public Record(@JsonProperty("description") String description,
      @JsonProperty("title") String title, @JsonProperty("index") Integer index,
      @JsonProperty("checked") boolean checked, @JsonProperty("remark") String remark,
      @JsonProperty("instructions") List<String> instructions,
      @JsonProperty("fileId") String fileId, @JsonProperty("url") String url,
      @JsonProperty("text") String text) {
    super(description, title, index, checked, remark, instructions, fileId, url, text);
  }
}
