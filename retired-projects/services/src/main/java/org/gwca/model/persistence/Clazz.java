package org.gwca.model.persistence;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The class members relationship bean.
 * 
 * @author youping.hu
 */
public class Clazz extends GoogleSheet {

  private String classId;
  private String className;
  private String classUrl;
  private String classYear;
  private String courseId;
  private String teacherId;
  private String memberIds;
  
  private Course course;
  private List<Member> members;

  public Clazz() {
    super();
  }

  /**
   * Converts the ',' delimited ID string (memberIds) into a trimmed List<String>
   * @return
   */
  @JsonIgnore
  public List<String> getMemberIdList() {
    String[] idArray = getMemberIds().split(",");
    List<String> memberIds = new ArrayList<String>();
    for (String id: idArray) {
      memberIds.add(id.trim());
    }
    return memberIds;
  }
  
  // the accessors
  public String getClassId() {
    return classId;
  }

  public void setClassId(String classId) {
    this.classId = classId;
  }

  public String getClassName() {
    return className;
  }

  public void setClassName(String className) {
    this.className = className;
  }

  public String getClassUrl() {
    return classUrl;
  }

  public void setClassUrl(String classUrl) {
    this.classUrl = classUrl;
  }

  public String getClassYear() {
    return classYear;
  }

  public void setClassYear(String classYear) {
    this.classYear = classYear;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getTeacherId() {
    return teacherId;
  }

  public void setTeacherId(String teacherId) {
    this.teacherId = teacherId;
  }

  @JsonIgnore
  public String getMemberIds() {
    return memberIds;
  }

  public void setMemberIds(String memberIds) {
    this.memberIds = memberIds;
  }

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public List<Member> getMembers() {
    if (members == null) {
      members = new ArrayList<Member>();
    }
    return members;
  }

  public void setMembers(List<Member> members) {
    this.members = members;
  }
}
