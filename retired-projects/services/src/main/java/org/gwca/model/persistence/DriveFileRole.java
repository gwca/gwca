package org.gwca.model.persistence;

/**
 * A permission of a Drive folder.
 * 
 * @author youping.hu
 *
 */
public enum DriveFileRole {

  READER("reader"), WRITER("writer");

  private final String name;

  private DriveFileRole(String name) {
    this.name = name;
  }

  public String value() {
    return name;
  }

  public static DriveFileRole fromValue(String name) {
    for (DriveFileRole storageName : DriveFileRole.values()) {
      if (storageName.value().equals(name)) {
        return storageName;
      }
    }
    return DriveFileRole.READER;
  }
}
