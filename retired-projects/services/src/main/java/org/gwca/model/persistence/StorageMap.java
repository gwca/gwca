package org.gwca.model.persistence;

import java.util.List;

/**
 * Names for the data storage that could be either a Fusion Table name or a Drive folder name.
 * 
 * @author youping.hu
 *
 */
public class StorageMap {

  // the injected storage descriptions
  private List<StorageDescription> storageDescriptionList;

  public StorageMap() {
  }

  public List<StorageDescription> getStorageDescriptionList() {
    return storageDescriptionList;
  }

  public void setStorageDescriptionList(List<StorageDescription> storageDescriptionList) {
    this.storageDescriptionList = storageDescriptionList;
  }
}
