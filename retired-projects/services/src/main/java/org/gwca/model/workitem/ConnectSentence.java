package org.gwca.model.workitem;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The ConnectSentence workitem.
 * 
 * @author youping.hu
 */
public class ConnectSentence extends Workitem {

  private List<CSLine> lines;

  public ConnectSentence() {
    super();
  }

  public ConnectSentence(@JsonProperty("description") String description,
      @JsonProperty("title") String title, @JsonProperty("index") Integer index,
      @JsonProperty("checked") boolean checked, @JsonProperty("remark") String remark,
      @JsonProperty("lines") List<CSLine> lines) {
    super(description, title, index, checked, remark);
    this.lines = lines;
  }

  public List<CSLine> getLines() {
    if (lines == null) {
      lines = new ArrayList<CSLine>();
    }
    return lines;
  }

  public void setLines(List<CSLine> lines) {
    this.lines = lines;
  }
}
