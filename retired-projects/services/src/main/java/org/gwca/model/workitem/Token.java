package org.gwca.model.workitem;

/**
 * The super class for all types of tokens.
 * 
 * @author youping.hu
 */
public class Token {

  private String answer;
  private boolean checked;
  private String remark;

  public Token() {
    super();
  }

  public String getAnswer() {
    return answer;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }

  public boolean isChecked() {
    return checked;
  }

  public void setChecked(boolean checked) {
    this.checked = checked;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }
}
