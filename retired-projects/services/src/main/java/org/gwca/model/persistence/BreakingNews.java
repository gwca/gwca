package org.gwca.model.persistence;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The break news bean.
 * 
 * @author youping.hu
 */
public class BreakingNews extends DriveFile {

  public static final String DATE_TIME = "MM/dd/yy hh:mm:ss a";

  private String startTime;
  private String expireTime;
  private String title;
  private String event;

  /** The default constructor */
  public BreakingNews() {
    super();
  }

  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  public String getExpireTime() {
    return expireTime;
  }

  public void setExpireTime(String expireTime) {
    this.expireTime = expireTime;
  }

  public Date getStartAt() {
    return parseDate(startTime);
  }

  public Date getExpireAt() {
    return parseDate(expireTime);
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getEvent() {
    return event;
  }

  public void setEvent(String event) {
    this.event = event;
  }

  /**
   * Converts a date string to a date
   *
   * @param dateVal
   * @param format acceptable to SimpleDateFormat
   * @return null on error
   */
  private Date parseDate(String dateVal) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_TIME);
    try {
      if (dateVal != null && dateVal.length() > 0)
        return dateFormat.parse(dateVal);
    } catch (Exception ex) {

    }
    return null;
  }
}
