package org.gwca.model.book;

import java.util.List;

import org.gwca.model.persistence.DriveFile;

/**
 * A book index that has a list of lessons.
 */
public class BookIndex extends DriveFile {
  private String courseId;
  private String title;
  private String image;
  private int start;
  private String description;
  private List<Lesson> lessons;

  public BookIndex() {}

  public String getCourseId() {
    return courseId;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public int getStart() {
    return start;
  }

  public void setStart(int start) {
    this.start = start;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public List<Lesson> getLessons() {
    return lessons;
  }

  public void setLessons(List<Lesson> lessons) {
    this.lessons = lessons;
  }
}
