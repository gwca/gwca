package org.gwca.model.message;

import java.util.List;

import org.gwca.error.CommonError;
import org.gwca.model.persistence.GoogleSheet;

/**
 * The class information response class
 * 
 * @author youping.hu
 */
public class ClassResponse<T extends GoogleSheet> extends ServiceResponse<T> {
  
  /** The default constructor */
  public ClassResponse() {
    super();
  }

  /** The copy constructors */
  public ClassResponse(CommonError error) {
    setInfo(error);
  }

  public ClassResponse(List<T> entities, CommonError error) {
    setEntities(entities);
    setInfo(error);
  }

  public ClassResponse(String classId, T entity, CommonError error) {
    setClassId(classId);
    setInfo(error);
    getEntities().add(entity);
  }
}
