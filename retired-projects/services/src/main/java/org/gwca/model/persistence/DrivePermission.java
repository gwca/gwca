package org.gwca.model.persistence;

/**
 * The Drive folder/file permission description bean.
 * 
 * @author youping.hu
 */
public class DrivePermission {

  private String type;
  private String email;
  private String fileId;
  private DriveFileRole role;

  public DrivePermission() {
    super();
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public DriveFileRole getRole() {
    return role;
  }

  public void setRole(DriveFileRole role) {
    this.role = role;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFileId() {
    return fileId;
  }

  public void setFileId(String fileId) {
    this.fileId = fileId;
  }
}
