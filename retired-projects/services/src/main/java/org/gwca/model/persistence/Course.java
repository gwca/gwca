package org.gwca.model.persistence;

/**
 * The class members relationship bean.
 * 
 * @author youping.hu
 */
public class Course extends GoogleSheet {

  private String courseId;
  private String name;
  private String alias;
  private String description;
  private String image;

  public Course() {
    super();
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String brief) {
    this.description = brief;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }
}
