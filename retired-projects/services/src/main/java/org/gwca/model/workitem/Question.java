package org.gwca.model.workitem;

/**
 * The question class for the AnswerQuestion work item.
 * 
 * @author youping.hu
 */
public class Question extends Token {

  private String question;

  public Question() {
    super();
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }
}
