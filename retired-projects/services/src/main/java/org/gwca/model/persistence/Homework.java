package org.gwca.model.persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.gwca.model.workitem.SimpleItem;
import org.gwca.model.workitem.Workitem;
import org.gwca.model.workitem.Write;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The homework bean.
 * 
 * @author youping.hu
 */
public class Homework extends HomeworkId {

  private String workTitle;
  private String remark;
  private String grade;
  private String status;
  private Date assignTime;
  private Date checkTime;
  private List<? extends Workitem> workitems;

  public Homework() {
    super();
  }

  public Homework(HomeworkId homeworkId) {
    setMemberId(homeworkId.getMemberId());
    setCourseId(homeworkId.getCourseId());
    setWorkNumber(homeworkId.getWorkNumber());
    setWorkitems(null);
  }

  @JsonIgnore
  public HomeworkId getHomeworkId() {
    return new HomeworkId(getWorkNumber(), getCourseId(), getMemberId());
  }

  /**
   * Gets a list of Write work items that the associated Google Drive file has not created (the URL
   * attribute is empty).
   * 
   * @return
   */
  @JsonIgnore
  public List<Write> getWriteItems() {
    List<Write> writeItems = new ArrayList<Write>();
    for (Object workitem : getWorkitems()) {
      if (workitem instanceof Write) {
        Write item = (Write) workitem;
        if (item.getUrl() == null || item.getUrl().length() == 0) {
          writeItems.add((Write) workitem);
        }
      }
    }
    return writeItems;
  }

  /**
   * Gets a list of work items that has some attachments (essay, image, recording, etc.)
   * 
   * @return
   */
  @JsonIgnore
  public List<SimpleItem> getItemsWithAttachment() {
    List<SimpleItem> items = new ArrayList<SimpleItem>();
    Class<?> simpleItemClass = SimpleItem.class;
    for (Object workitem : getWorkitems()) {
      if (simpleItemClass.isAssignableFrom(workitem.getClass())) {
        SimpleItem simpleItem = (SimpleItem) workitem;
        if (simpleItem.getFileId() != null && simpleItem.getFileId().length() > 0) {
          items.add((SimpleItem) workitem);
        }
      }
    }
    return items;
  }

  @JsonIgnore
  public boolean isNew() {
    if (status == null || status.length() == 0) {
      setStatus("Unassigned");
    }
    return (status.equalsIgnoreCase("Unassigned") || status.equalsIgnoreCase("Assigned"));
  }

  // the accessors
  public String getWorkTitle() {
    return workTitle;
  }

  public void setWorkTitle(String workTitle) {
    this.workTitle = workTitle;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public String getGrade() {
    return grade;
  }

  public void setGrade(String grade) {
    this.grade = grade;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Date getAssignTime() {
    return assignTime;
  }

  public void setAssignTime(Date assignTime) {
    this.assignTime = assignTime;
  }

  public Date getCheckTime() {
    return checkTime;
  }

  public void setCheckTime(Date checkTime) {
    this.checkTime = checkTime;
  }

  public List<? extends Workitem> getWorkitems() {
    return workitems;
  }

  public void setWorkitems(List<? extends Workitem> workitems) {
    this.workitems = workitems;
  }
}
