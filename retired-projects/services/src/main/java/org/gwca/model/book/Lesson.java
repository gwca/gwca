package org.gwca.model.book;

import java.util.List;

/**
 * A lesson of a course that has one or more chapters.
 */
public class Lesson {

  private String lessonId;
  private String title;
  private List<Chapter> chapters;

  public Lesson() {}

  public String getLessonId() {
    return lessonId;
  }

  public void setLessonId(String lessonId) {
    this.lessonId = lessonId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public List<Chapter> getChapters() {
    return chapters;
  }

  public void setChapters(List<Chapter> chapters) {
    this.chapters = chapters;
  }
}
