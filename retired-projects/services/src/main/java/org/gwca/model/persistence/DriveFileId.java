package org.gwca.model.persistence;

/**
 * The drive ID class.
 * 
 * @author youping.hu
 */
public class DriveFileId {

  private String fileId;
  private String fileLink;
  
  public DriveFileId() {
    super();
  }
  
  public DriveFileId(String fileId, String fileLink) {
    super();
    this.fileId = fileId;
    this.fileLink = fileLink;
  }

  public String getFileId() {
    return fileId;
  }

  public void setFileId(String fileId) {
    this.fileId = fileId;
  }

  public String getFileLink() {
    return fileLink;
  }

  public void setFileLink(String fileLink) {
    this.fileLink = fileLink;
  }
}
