package org.gwca.model.workitem;

/**
 * The word class for the PhraseSentence work item.
 * 
 * @author youping.hu
 */
public class Word extends Token {

  private String word;

  public Word() {
    super();
  }

  public String getWord() {
    return word;
  }

  public void setWord(String word) {
    this.word = word;
  }
}
