package org.gwca.model.workitem;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

/**
 * The SimpleItem is an abstract class for note, read, audio, video, image, recording, pageLink and
 * write workitems.
 * 
 * @author youping.hu
 */
@JsonTypeInfo(use = Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "name")
@JsonSubTypes({@Type(value = Audio.class, name = "Audio"),
    @Type(value = Image.class, name = "Image"), @Type(value = Note.class, name = "Note"),
    @Type(value = Read.class, name = "Read"), @Type(value = Record.class, name = "Record"),
    @Type(value = Video.class, name = "Video"), @Type(value = Write.class, name = "Write"),
    @Type(value = PageLink.class, name = "PageLink")})
public abstract class SimpleItem extends Workitem {

  private List<String> instructions;
  private String fileId;
  private String url;
  private String text;

  public SimpleItem() {
    super();
  }

  public SimpleItem(@JsonProperty("description") String description,
      @JsonProperty("title") String title, @JsonProperty("index") Integer index,
      @JsonProperty("checked") boolean checked, @JsonProperty("remark") String remark,
      @JsonProperty("instructions") List<String> instructions,
      @JsonProperty("fileId") String fileId, @JsonProperty("url") String url,
      @JsonProperty("text") String text) {
    super(description, title, index, checked, remark);
    this.instructions = instructions;
    this.fileId = fileId;
    this.url = url;
    this.text = text;
  }

  public List<String> getInstructions() {
    if (instructions == null) {
      instructions = new ArrayList<String>();
    }
    return instructions;
  }

  public void setInstructions(List<String> instructions) {
    this.instructions = instructions;
  }

  public String getFileId() {
    return fileId;
  }

  public void setFileId(String fileId) {
    this.fileId = fileId;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }
}
