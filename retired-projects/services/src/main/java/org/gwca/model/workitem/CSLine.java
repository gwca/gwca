package org.gwca.model.workitem;

/**
 * The connect sentence line class for the ConnectSentence work item.
 * 
 * @author youping.hu
 */
public class CSLine extends Token {

  private String lead;
  private String tail;

  public CSLine() {
    super();
  }

  public String getLead() {
    return lead;
  }

  public void setLead(String lead) {
    this.lead = lead;
  }

  public String getTail() {
    return tail;
  }

  public void setTail(String tail) {
    this.tail = tail;
  }
}
