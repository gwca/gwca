package org.gwca.model.workitem;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The FillBlank workitem.
 * 
 * @author youping.hu
 */
public class FillBlank extends Workitem {

  private List<FBStatement> statements;

  public FillBlank() {
    super();
  }

  public FillBlank(@JsonProperty("description") String description,
      @JsonProperty("title") String title, @JsonProperty("index") Integer index,
      @JsonProperty("checked") boolean checked, @JsonProperty("remark") String remark,
      @JsonProperty("statements") List<FBStatement> statements) {
    super(description, title, index, checked, remark);
    this.statements = statements;
  }

  public List<FBStatement> getStatements() {
    if (statements == null) {
      statements = new ArrayList<FBStatement>();
    }
    return statements;
  }

  public void setStatements(List<FBStatement> statements) {
    this.statements = statements;
  }
}
