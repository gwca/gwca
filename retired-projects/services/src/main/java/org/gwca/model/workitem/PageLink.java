package org.gwca.model.workitem;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The PageLink workitem.
 * 
 * @author youping.hu
 */
public class PageLink extends SimpleItem {

  public PageLink() {
    super();
  }

  public PageLink(@JsonProperty("description") String description,
      @JsonProperty("title") String title, @JsonProperty("index") Integer index,
      @JsonProperty("checked") boolean checked, @JsonProperty("remark") String remark,
      @JsonProperty("instructions") List<String> instructions,
      @JsonProperty("fileId") String fileId, @JsonProperty("url") String url,
      @JsonProperty("text") String text) {
    super(description, title, index, checked, remark, instructions, fileId, url, text);
  }
}
