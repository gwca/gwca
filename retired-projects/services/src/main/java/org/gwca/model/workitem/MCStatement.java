package org.gwca.model.workitem;

import java.util.ArrayList;
import java.util.List;

/**
 * The multi-choice statement class for the MultiChoice work item.
 * 
 * @author youping.hu
 */
public class MCStatement extends Token {

  private String question;
  private List<String> choices;

  public MCStatement() {
    super();
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  public List<String> getChoices() {
    if (choices == null) {
      choices = new ArrayList<String>();
    }
    return choices;
  }

  public void setChoices(List<String> choices) {
    this.choices = choices;
  }
}
