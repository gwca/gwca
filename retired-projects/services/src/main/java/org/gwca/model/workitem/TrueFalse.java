package org.gwca.model.workitem;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The TrueFalse workitem.
 * 
 * @author youping.hu
 */
public class TrueFalse extends Workitem {

  private List<TFStatement> statements;

  public TrueFalse() {
    super();
  }

  public TrueFalse(@JsonProperty("description") String description,
      @JsonProperty("title") String title, @JsonProperty("index") Integer index,
      @JsonProperty("checked") boolean checked, @JsonProperty("remark") String remark,
      @JsonProperty("statements") List<TFStatement> statements) {
    super(description, title, index, checked, remark);
    this.statements = statements;
  }

  public List<TFStatement> getStatements() {
    if (statements == null) {
      statements = new ArrayList<TFStatement>();
    }
    return statements;
  }

  public void setStatements(List<TFStatement> statements) {
    this.statements = statements;
  }
}
