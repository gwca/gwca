package org.gwca.model.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The homework ID bean.
 * 
 * @author youping.hu
 */
public class HomeworkId extends DriveFile {

  private String workNumber;
  private String courseId;
  private String memberId;

  /** The default constructor */
  public HomeworkId() {
    super();
  }

  /** The copy constructors */
  public HomeworkId(String workNumber, String courseId, String memberId) {
    this.workNumber = workNumber;
    this.courseId = courseId;
    this.memberId = memberId;
  }

  public HomeworkId(String courseId, String memberId) {
    this.courseId = courseId;
    this.memberId = memberId;
  }

  // helpers
  @JsonIgnore
  public String getIdString() {
    return courseId + (workNumber == null ? "" : "_" + workNumber)
        + (memberId == null ? "" : "_" + memberId);
  }

  @JsonIgnore
  public String getDriveFileTitle() {
    return getIdString() + ".json";
  }

  // the accessors
  public String getWorkNumber() {
    return workNumber;
  }

  public void setWorkNumber(String workNumber) {
    this.workNumber = workNumber;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getMemberId() {
    return memberId;
  }

  public void setMemberId(String studentId) {
    this.memberId = studentId;
  }
}
