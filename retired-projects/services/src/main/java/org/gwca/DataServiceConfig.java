package org.gwca;

import java.util.Date;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.gwca.googleapp.stub.CacheHandler;
import org.gwca.model.message.NotificationRequest;
import org.gwca.model.persistence.StorageDescription;
import org.gwca.model.persistence.StorageName;
import org.gwca.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * Imports all the XML configurations from app-context.xml
 * 
 * @author youping.hu
 *
 */
@Configuration
@ImportResource({"classpath*:app-context.xml"})
@EnableScheduling
public class DataServiceConfig {

  private static final long FOUR_MINUTES = 4 * 60 * 1000;
  private static final Logger logger = Logger.getLogger("DataServiceConfig");

  //
  // the injected properties
  //
  @Autowired
  protected CacheHandler cacheHandler = null;

  @Autowired
  private EmailService emailService;

  @Autowired
  private NotificationRequest notifyRequest;

  /**
   * Loads all the caches. Note that the order of loading matters.
   */
  @PostConstruct
  public void loadCachedObjects() {

    cacheHandler.loadStorageDescriptionCache();
//    cacheHandler.loadTableCache();
//    cacheHandler.loadHomeworkCache(cacheHandler.getClasses());
//    cacheHandler.loadBookIndexCache();

    // take too much time to load the textbook cache, skip it for the local environment.
//    if (!isLocalProfile()) {
//      cacheHandler.loadBookLessonsCache();
//    }
    
    logger.info("\nThe application profile is:" + getProfile() + "\n");
  }

  @Bean
  public CorsFilter corsFilter() {
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    CorsConfiguration config = new CorsConfiguration();
    config.setAllowCredentials(true);
    config.addAllowedOrigin("*");
    config.addAllowedHeader("*");
    config.addAllowedMethod(HttpMethod.OPTIONS);
    config.addAllowedMethod(HttpMethod.GET);
    config.addAllowedMethod(HttpMethod.POST);
    config.addAllowedMethod(HttpMethod.PUT);
    config.addAllowedMethod(HttpMethod.DELETE);
    source.registerCorsConfiguration("/**", config);
    return new CorsFilter(source);
  }

  /**
   * To keep the service awake, touch it every 4 minutes.
   */
  @Scheduled(fixedDelay = FOUR_MINUTES)
  public void keepServiceAwake() {
    try {
      if (!isLocalProfile()) {
        cacheHandler.getStorageDesc(StorageName.CLASS_TABLE.value());
        logger.info("The application is up and running.");
      }
    } catch (Exception ex) {
      try {
        notifyRequest.setSubject("The scheduleTask() failed at:" + new Date());
        notifyRequest.setBody("The exception stack\n" + ex.getStackTrace());
        emailService.sendNotificationEmail(notifyRequest);
      } catch (Exception x) {
        logger.severe("The scheduleTask() failed.\n" + ex.getStackTrace());
        logger.severe("Sending notification failed.\n" + x.getStackTrace());
      }
    }
  }

  private String getProfile() {
    String profile = "No profile is set, check StorageDescription settings";
    StorageDescription desc = cacheHandler.getStorageDesc("Profile");
    if (desc != null) {
      profile = desc.getStorageId();
    }
    return profile;
  }
  
  private boolean isLocalProfile() {
	String profile = getProfile();
    StorageDescription desc = cacheHandler.getStorageDesc("Profile");
    return (desc != null && profile.trim().startsWith("local"));
  }
}
