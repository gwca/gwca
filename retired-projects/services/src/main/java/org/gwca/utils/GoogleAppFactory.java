package org.gwca.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.gwca.googleapp.impl.CacheHandlerImpl;
import org.gwca.googleapp.stub.CacheHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.fusiontables.Fusiontables;
import com.google.api.services.fusiontables.FusiontablesScopes;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;

@Component
public class GoogleAppFactory {

  private static final Logger logger = Logger.getLogger(GoogleAppFactory.class.getSimpleName());

  /** The JSON factory. */
  private static JsonFactory JSON_FACTORY = new JacksonFactory();

  /** The HTTP transport. */
  private static HttpTransport HTTP_TRANSPORT;

  // the injected properties
  /** Application name. */
  @Value("${admin.app.name}")
  private String appName;

  /** The service account ID */
  @Value("${admin.service.account.id}")
  private String serviceAccountId;

  /** The service account user */
  @Value("${admin.service.account.user}")
  private String serviceAccountUser;

  /** The service account file names (json and p12) */
  @Value("${admin.service.account.json}")
  private String serviceAccountJson;
  
  @Value("${admin.service.account.p12}")
  private String serviceAccountP12;

  /** The default constructor */
  public GoogleAppFactory() {}

  /**
   * Creates a cache handler object.
   * 
   * @return
   */
  public CacheHandler createCacheHandler() {
    return new CacheHandlerImpl();
  }

  /**
   * Builds and returns an authorized Drive client service.
   * 
   * @return an authorized Drive client service
   * @throws Exception
   */
  public Drive createDriveClient() {
    try {
      GoogleCredential credential = authorize(DriveScopes.all());
      Drive.Builder builder = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential);
      builder.setApplicationName(appName);
      logger.info("InitDriveServiceOK");
      return builder.build();
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "InitDriveServiceFailed", ex);
      return null;
    }
  }

  /**
   * Builds and returns an authorized Fusion Table client service.
   * 
   * @return an authorized FusionTable client service
   * @throws Exception
   */
  public Fusiontables createFusionTableClient() {
    try {
      GoogleCredential credential = authorize(FusiontablesScopes.all());
      Fusiontables.Builder builder =
          new Fusiontables.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential);
      builder.setApplicationName(appName);
      logger.info("InitTableServiceOK");
      return builder.build();
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "InitTableServiceFailed", ex);
      return null;
    }
  }

  /**
   * Builds and returns an authorized Google Sheets client service.
   * 
   * @return an authorized Google Sheets client service
   * @throws Exception
   */
  public Sheets createSheetClient() {
    try {
      GoogleCredential credential = authorize(SheetsScopes.all());
      Sheets.Builder builder = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential);
      builder.setApplicationName(appName);
      logger.info("InitSheetServiceOK");
      return builder.build();
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "InitSheetServiceFailed", ex);
      return null;
    }
  }

  /**
   * Builds and returns an authorized Gmail client service.
   * 
   * @return an authorized Gmail client service
   * @throws Exception
   */
  public Gmail createGmailClient() {
    try {
      GoogleCredential credential = gmailAuthorize(GmailScopes.all());
      Gmail.Builder builder = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential);
      builder.setApplicationName(appName);
      logger.info("InitGamilServiceOK");
      return builder.build();
    } catch (Exception ex) {
      logger.log(Level.SEVERE, "InitGamilServiceFailed", ex);
      return null;
    }
  }

  /**
   * Creates an authorized Credential object for creating the table and drive services.
   * 
   * @return an authorized Credential object.
   * @throws IOException
   * @throws GeneralSecurityException
   */
  private GoogleCredential authorize(Set<String> scopes)
      throws IOException, GeneralSecurityException {
    HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
    InputStream credentialStream = GoogleAppFactory.class.getResourceAsStream(serviceAccountJson);
    GoogleCredential credential =
        GoogleCredential.fromStream(credentialStream, HTTP_TRANSPORT, JSON_FACTORY);
    credential = credential.createScoped(scopes);

    return credential;
  }

  /**
   * Creates an authorized Credential object for creating a gmail service. It has to use P12 file to
   * authorize the user in order to call setServiceAccountUser(string) to make it work.
   * 
   * @return an authorized Credential object.
   * @throws IOException
   * @throws GeneralSecurityException
   */
  private GoogleCredential gmailAuthorize(Set<String> scopes)
      throws IOException, GeneralSecurityException {
    ClassLoader classLoader = getClass().getClassLoader();
    File keyFile = new File(classLoader.getResource(serviceAccountP12).getFile());
    HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
    GoogleCredential credential =
        new GoogleCredential.Builder().setTransport(HTTP_TRANSPORT).setJsonFactory(JSON_FACTORY)
            .setServiceAccountId(serviceAccountId).setServiceAccountPrivateKeyFromP12File(keyFile)
            .setServiceAccountScopes(scopes).setServiceAccountUser(serviceAccountUser).build();
    return credential;
  }

  // the injections
  public void setAppName(String appName) {
    this.appName = appName;
  }

  public void setServiceAccountId(String serviceAccountId) {
    this.serviceAccountId = serviceAccountId;
  }

  public void setServiceAccountUser(String serviceAccountUser) {
    this.serviceAccountUser = serviceAccountUser;
  }

  public void setServiceAccountJson(String serviceAccountJson) {
    this.serviceAccountJson = serviceAccountJson;
  }

  public void setServiceAccountP12(String serviceAccountP12) {
    this.serviceAccountP12 = serviceAccountP12;
  }
}
