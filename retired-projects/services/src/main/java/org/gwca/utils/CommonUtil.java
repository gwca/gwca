package org.gwca.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.gwca.model.message.NameValuePair;

/**
 * Some handy utility methods.
 * 
 * @author youping.hu
 */
public class CommonUtil {

  public static final String DEFAULT_DATE_FORMAT = "MM-dd-yyyy";
  public static final String TITLE_DATE_FORMAT = "MM-dd-HHmm";

  public CommonUtil() {}

  /**
   * Converts a date string to a Date object
   * 
   * @param dateString
   * @return
   */
  public static Date getDefaultDate(String dateString) {

    try {
      SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
      return dateFormat.parse(dateString);
    } catch (Exception ex) {
      return new Date();
    }
  }

  /**
   * Converts a Date object to a string
   * @param date
   * @return
   */
  public static String getDefaultDateString(Date date) {
    return getFormattedDate(date, DEFAULT_DATE_FORMAT);
  }

  public static String getTitleDateString(Date date) {
    return getFormattedDate(date, TITLE_DATE_FORMAT);
  }

  public static String getFormattedDate(Date date, String format) {
    try {
      SimpleDateFormat dateFormat = new SimpleDateFormat(format);
      return dateFormat.format(date);
    } catch (Exception ex) {
      return "";
    }
  }

  /**
   * Gets the value from a name value pair list by name.
   * @param pairs
   * @param name
   * @return
   */
  public static String getValueByName(List<NameValuePair> pairs, String name) {
    if (pairs != null) {
      for (NameValuePair pair : pairs) {
        if (name.equals(pair.getName())) {
          return pair.getValue();
        }
      }
    }
    return null;
  }
  
  /**
   * Reads the text from a resource (a text file) in the class path.
   * 
   * @param qulifiedName
   * @return
   */
  public static String readResource(String qulifiedName) throws Exception {
    BufferedReader reader = getBufferedReader(qulifiedName);
    if (reader == null) {
      throw new Exception("Cannot read from " + qulifiedName);
    }

    StringBuffer buffer = new StringBuffer();
    String value = null;
    try {
      while ((value = reader.readLine()) != null) {
        buffer.append(value);
      }
    } catch (IOException ex) {
      throw new Exception(ex);
    }
    return buffer.toString();
  }

  /**
   * Gets an input stream from a resource in the class path.
   * 
   * @param qulifiedName
   * @return
   */
  public static InputStream getStreamReader(String qulifiedName) {
    Thread thread = Thread.currentThread();
    ClassLoader loader = thread.getContextClassLoader();
    return loader.getResourceAsStream(qulifiedName);
  }

  /**
   * Gets the URL of a resource.
   * 
   * @param qulifiedName
   * @return
   */
  public static URL getResourceURL(String qulifiedName) {
    Thread thread = Thread.currentThread();
    ClassLoader loader = thread.getContextClassLoader();
    return loader.getResource(qulifiedName);
  }

  /**
   * Gets a buffered reader from a resource in the class path.
   * 
   * @param qulifiedName
   * @return
   */
  public static BufferedReader getBufferedReader(String qulifiedName) {
    InputStream inputStream = getStreamReader(qulifiedName);
    BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStream));
    return streamReader;
  }
}
