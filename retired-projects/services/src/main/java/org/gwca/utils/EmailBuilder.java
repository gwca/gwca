package org.gwca.utils;

import java.util.Date;
import java.util.List;

import org.gwca.model.persistence.Member;
import org.gwca.model.persistence.MemberRole;

/**
 * An email builder class.
 * 
 * @author youping.hu
 */
public class EmailBuilder {

  private static final String homeworkSiteURL = "https://gwca-course-work.appspot.com/";
  private static final String bodyFormat =
      "<div style='font-family:arial,sans-serif;font-size:18px;margin-left:20px;'>";

  private Member teacher;
  private Member student;
  private String className;

  /**
   * The constructor
   * @param teacher
   * @param student
   * @param className
   */
  public EmailBuilder(Member teacher, Member student, String className) {
    setTeacher(teacher);
    setStudent(student);
    setClassName(className);
  }

  /**
   * The constructor
   * @param className
   */
  public EmailBuilder(String className) {
    setClassName(className);
  }

  /**
   * Creates an email subject to notify a student the assignment
   */
  public String studentAssignmentSubject() {
    String now = CommonUtil.getDefaultDateString(new Date());
    return className + " homework assignment (" + now + ")";
  }

  /**
   * Creates an email body to notify a student the assignment
   */
  public String studentAssignmentNotification() {
    String divStart = bodyFormat;
    String text = "<p>Hello " + student.getFirstName() + ",</p><p><a href=" + homeworkSiteURL
        + ">Here</a> is this week's homework assignment.</p><p>Thanks!</p>";
    String signature = "<p>" + teacher.createFullName() + "</p>";
    String divEnd = "</div>";
    return divStart + text + signature + divEnd;
  }

  /**
   * Creates an email subject to notify the school administration of the homework assignment
   */
  public String adminAssignmentSubject() {
    return studentAssignmentSubject();
  }

  /**
   * Creates an email to notify the school administration of the homework assignment
   * 
   * @param members
   */
  public String adminAssignmentNotification(List<Member> members) {
    StringBuilder text = new StringBuilder();
    String divStart = bodyFormat;
    String desc = "<p>" + adminAssignmentSubject()
        + "</p><p>The homework is assigned to the following students:</p>";
    text.append("<ul>");
    for (Member member : members) {
      if (member.getRole().equals(MemberRole.STUDENT)) {
        text.append("<li><a href=" + homeworkSiteURL + ">" + member.createFullName() + "</a>");
        text.append((member.getChineseName() != null ? " (" + member.getChineseName() + ")</li>"
            : "</li>"));
      }
    }
    text.append("</ul>");
    String divEnd = "</div>";
    return divStart + desc + text.toString() + divEnd;
  }

  /**
   * Creates an email subject to notify the teacher a student submitted his/her homework
   */
  public String teacherSubmitSubject() {
    String now = CommonUtil.getDefaultDateString(new Date());
    return student.createFullName() + " submitted his/her homework at "
        + now;
  }

  /**
   * Creates an email to notify the teacher a student submitted his/her homework.
   */
  public String teacherSubmitNotification() {
    String divStart = bodyFormat;
    String text = "<p>" + teacherSubmitSubject() + "</p><p><a href='" + homeworkSiteURL
        + "'>Click Here</a></p>";
    String divEnd = "</div>";
    return divStart + text + divEnd;
  }

  //
  // The accessors
  //
  public Member getTeacher() {
    return teacher;
  }

  public void setTeacher(Member teacher) {
    this.teacher = teacher;
  }

  public Member getStudent() {
    return student;
  }

  public void setStudent(Member student) {
    this.student = student;
  }

  public String getClassName() {
    return className;
  }

  public void setClassName(String className) {
    this.className = className;
  }
}
