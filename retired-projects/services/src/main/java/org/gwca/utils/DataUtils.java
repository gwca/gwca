package org.gwca.utils;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.gwca.model.book.BookIndex;
import org.gwca.model.persistence.BreakingNews;
import org.gwca.model.persistence.CommonEntity;
import org.gwca.model.persistence.Homework;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * The data utility class.
 * 
 * @author youping.hu
 */
public class DataUtils {

  private static final Logger logger = Logger.getLogger(DataUtils.class.getSimpleName());

  private static final String AUDIO_PREFIX =
      "https://docs.google.com/uc?authuser=0&export=download&id=";
  private static final String IMAGE_PREFIX = "http://drive.google.com/uc?export=view&id=";
  private static final String DOC_PREFIX = "https://docs.google.com/document/d/";
  private static final String SLIDE_PREFIX = "https://docs.google.com/presentation/d/";

  /** For JSON and Java conversion */
  protected static ObjectMapper mapper;

  static {
    mapper = new ObjectMapper();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.enable(SerializationFeature.INDENT_OUTPUT);
  }

  public DataUtils() {
    super();
  }

  public String getAudioLink(String googleId) {
    return AUDIO_PREFIX + googleId;
  }

  public String getImageLink(String googleId) {
    return IMAGE_PREFIX + googleId;
  }

  public String getDocumentLink(String googleId) {
    return DOC_PREFIX + googleId;
  }

  public String getSlideLink(String googleId) {
    return SLIDE_PREFIX + googleId + "/edit";
  }

  public String getFileIdFromLink(String fileLink) {
    if (fileLink.startsWith(AUDIO_PREFIX)) {
      return fileLink.substring(AUDIO_PREFIX.length());
    } else if (fileLink.startsWith(IMAGE_PREFIX)) {
      return fileLink.substring(IMAGE_PREFIX.length());
    } else if (fileLink.startsWith(DOC_PREFIX)) {
      fileLink = fileLink.substring(DOC_PREFIX.length());
      int idx = fileLink.indexOf("/");
      if (idx > 0) {
        return fileLink.substring(0, idx);
      } else {
        return fileLink;
      }
    } else {
      return fileLink;
    }
  }

  /**
   * Converts a JSON string to a Java object.
   * 
   * @param jsonString
   * @return
   */
  public <E extends CommonEntity> E toJavaObj(String jsonString, Class<E> type) {
    E obj = null;
    try {
      obj = (mapper.readValue(jsonString, type));
    } catch (Exception ex) {
      logger.log(Level.WARNING, "Convert " + type.getName() + " failed", ex);
    }
    return obj;
  }

  /**
   * Converts a JSON string to a Java List<E> object
   * 
   * @param jsonString
   * @param type
   * @return
   */
  public List<Homework> toHomeworkList(String jsonString) {
    List<Homework> homeworkList = null;
    try {
      homeworkList = mapper.readValue(jsonString, new TypeReference<List<Homework>>() {});
      // objList = mapper.readValue(jsonString, TypeFactory.collectionType(List.class, type));
    } catch (Exception ex) {
      logger.log(Level.WARNING, "Convert homework list failed", ex);
    }
    return homeworkList;
  }

  /**
   * Converts a JSON string to a Java List<E> object
   * 
   * @param jsonString
   * @param type
   * @return
   */
  public List<BookIndex> toBookList(String jsonString) {
    List<BookIndex> bookList = null;
    try {
      bookList = mapper.readValue(jsonString, new TypeReference<List<BookIndex>>() {});
    } catch (Exception ex) {
      logger.log(Level.WARNING, "Convert book list failed", ex);
    }
    return bookList;
  }

  /**
   * Converts a JSON string to a Java List<E> object
   * 
   * @param jsonString
   * @param type
   * @return
   */
  public List<BreakingNews> toBreakingNewsList(String jsonString) {
    List<BreakingNews> newsList = null;
    try {
      newsList = mapper.readValue(jsonString, new TypeReference<List<BreakingNews>>() {});
    } catch (Exception ex) {
      logger.log(Level.WARNING, "Convert break news list failed", ex);
    }
    return newsList;
  }

  /**
   * Converts a Java object to a JSON string
   * 
   * @param javaObject
   * @return
   */
  public String toJsonString(Object javaObject) {
    String jsonString = null;
    try {
      jsonString = mapper.writeValueAsString(javaObject);
    } catch (Exception ex) {
      logger.log(Level.WARNING, "Convert " + javaObject.getClass().getName() + " failed", ex);
    }
    return jsonString;
  }
}
