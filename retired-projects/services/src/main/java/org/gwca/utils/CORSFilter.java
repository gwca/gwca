package org.gwca.utils;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
 * This filter allows CORS accessing from all the origins.
 * 
 * @See web.xml <filter> configuration for more details.
 * @author youping.hu
 *
 */
public class CORSFilter implements Filter {

  /**
   * Default constructor.
   */
  public CORSFilter() {}

  /**
   * @see Filter#destroy()
   */
  public void destroy() {}

  /**
   * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
   */
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {

    ((HttpServletResponse) response).addHeader("Access-Control-Allow-Origin", "*");
    ((HttpServletResponse) response).addHeader("Access-Control-Allow-Headers",
        "origin, content-type, accept, authorization");
    ((HttpServletResponse) response).addHeader("Access-Control-Allow-Credentials", "true");
    ((HttpServletResponse) response).addHeader("Access-Control-Allow-Methods",
        "GET, POST, PUT, DELETE, OPTIONS, HEAD");

    // pass the request along the filter chain
    chain.doFilter(request, response);
  }

  /**
   * @see Filter#init(FilterConfig)
   */
  public void init(FilterConfig fConfig) throws ServletException {}
}

// public class CORSFilter implements ContainerResponseFilter {
// // // @Override
// // public ContainerResponse filter(ContainerRequest request, ContainerResponse response) {
// //
// // response.getHeaders().add("Access-Control-Allow-Origin", "*");
// // response.getHeaders().add("Access-Control-Allow-Headers",
// // "origin, content-type, accept, authorization");
// // response.getHeaders().add("Access-Control-Allow-Credentials", "true");
// // response.getHeaders().add("Access-Control-Allow-Methods",
// // "GET, POST, PUT, DELETE, OPTIONS, HEAD");
// // return response;
// // }
//
// @Override
// public void filter(ContainerRequestContext request, ContainerResponseContext response)
// throws IOException {
// response.getHeaders().add("Access-Control-Allow-Origin", "*");
// response.getHeaders().add("Access-Control-Allow-Headers",
// "origin, content-type, accept, authorization");
// response.getHeaders().add("Access-Control-Allow-Credentials", "true");
// response.getHeaders().add("Access-Control-Allow-Methods",
// "GET, POST, PUT, DELETE, OPTIONS, HEAD");
// }
// }
