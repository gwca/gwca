package org.gwca.controller;

import org.gwca.model.book.BookIndex;
import org.gwca.model.message.SimpleResponse;
import org.gwca.model.message.WorkResponse;
import org.gwca.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/services/book")
public class BookController extends AbstractController {

  @Autowired
  protected BookService bookService;

  /** The default constructor */
  public BookController() {
    super();
  }

  /**
   * Gets the cached lesson list.
   * 
   * @return
   */
  @GetMapping("/bookIndex")
  public ResponseEntity<WorkResponse<BookIndex>> getBookIndex() {
    WorkResponse<BookIndex> bookIndex = bookService.getBookIndex();
    return ResponseEntity.ok(bookIndex);
  }

  /**
   * Gets a lesson of the book.
   * 
   * @param bookName
   * @param fileName
   * @return
   */
  @GetMapping("/lesson/{bookName}/{fileName}")
  public ResponseEntity<SimpleResponse> getBookLesson(@PathVariable("bookName") String bookName,
      @PathVariable("fileName") String fileName) {
    SimpleResponse lesson = bookService.getBookLesson(bookName, fileName);
    return ResponseEntity.ok(lesson);
  }

  /**
   * Creates a list of textbook lesson URLs of the specified course
   * 
   * @param courseName
   * @return
   */
  @GetMapping("/lessonLinks/{courseName}")
  public ResponseEntity<SimpleResponse> getLessonURLs(
      @PathVariable("courseName") String courseName) {
    SimpleResponse urls = bookService.getLessonURLs(courseName);
    return ResponseEntity.ok(urls);
  }
}
