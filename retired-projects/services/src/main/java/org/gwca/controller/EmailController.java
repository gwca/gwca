package org.gwca.controller;

import java.util.List;

import org.gwca.model.message.NotificationRequest;
import org.gwca.model.message.WorkResponse;
import org.gwca.model.persistence.Homework;
import org.gwca.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/services/email")
public class EmailController extends AbstractController {

  @Autowired
  private EmailService emailService;

  /** The default constructor */
  public EmailController() {
    super();
  }

  /**
   * Sends a notification email to the receiver specified by the request.
   * 
   * @return
   */
  @PostMapping("/notification")
  public ResponseEntity<WorkResponse<Homework>> sendNotificationEmail(
      @RequestBody NotificationRequest request) {
    WorkResponse<Homework> response = emailService.sendNotificationEmail(request);
    return ResponseEntity.ok(response);
  }

  /**
   * Sends a list of notification email to the receivers specified by the request.
   * 
   * @return
   */
  @PostMapping("/notifications")
  public ResponseEntity<WorkResponse<Homework>> sendNotificationEmailList(
      @RequestBody List<NotificationRequest> requestList) {
    WorkResponse<Homework> response = emailService.sendNotificationEmailList(requestList);
    return ResponseEntity.ok(response);
  }
}
