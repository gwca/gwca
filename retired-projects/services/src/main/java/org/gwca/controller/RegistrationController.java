package org.gwca.controller;

import org.gwca.model.message.ClassResponse;
import org.gwca.model.persistence.Clazz;
import org.gwca.model.persistence.Member;
import org.gwca.service.SheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/services/table")
public class RegistrationController extends AbstractController {

  @Autowired
  protected SheetService sheetService;

  /** The default constructor */
  public RegistrationController() {
    super();
  }

  /**
   * Gets a list of active class/course/members relationship object graph.
   * 
   * @return
   */
  @GetMapping("/classes")
  public ResponseEntity<ClassResponse<Clazz>> getActiveClasses() {
    ClassResponse<Clazz> response = sheetService.getActiveClasses();
    return ResponseEntity.ok(response);
  }

  /**
   * Validates if the email belongs to a valid member.
   * 
   * @param memberEmail
   * @return
   */
  @GetMapping("/member/{email}")
  public ResponseEntity<ClassResponse<Member>> getMembers() {
    ClassResponse<Member> response = sheetService.getMembers();
    return ResponseEntity.ok(response);
  }
}
