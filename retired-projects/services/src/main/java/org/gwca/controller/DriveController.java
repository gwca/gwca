package org.gwca.controller;

import java.util.List;

import org.gwca.model.message.SimpleResponse;
import org.gwca.model.message.UploadFileRequest;
import org.gwca.model.message.WorkResponse;
import org.gwca.model.persistence.BreakingNews;
import org.gwca.model.persistence.Homework;
import org.gwca.service.DriveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/services/drive")
public class DriveController extends AbstractController {

  @Autowired
  private DriveService driveService;

  /** The default constructor */
  public DriveController() {
    super();
  }

  /**
   * Sets a proper permission to the members' homework folder (Reader, Writer)<br>
   * <li>a student will have an Writer permission to his/her homework folder;</li>
   * <li>a teacher will have an Writer permission to all the students homework folders;</li>
   * <li>a parent will have a Reader permission to his/her children's homework folders;</li>
   * <li>an admin will have a Reader permission to all the students homework folders.</li>
   *
   * @param classId
   * @return
   */
  @PostMapping("/permission/{classId}")
  public ResponseEntity<WorkResponse<Homework>> setUserPermission(
      @PathVariable("classId") String classId, List<String> members) {
    WorkResponse<Homework> response = driveService.setUserPermission(classId, members);
    return ResponseEntity.ok(response);
  }

  /**
   * Uploads an audio clip (.mp3, .m4a) or an image file (jpg, jpeg, gif, png).
   * 
   * @param file
   * @param filename.
   * @param filetype.
   * @param memberId.
   * @param courseId.
   * @param workNumber.
   * @return
   */
  @PostMapping("/file")
  public ResponseEntity<SimpleResponse> uploadFile(@RequestParam("file") MultipartFile file,
      @RequestParam(value = "filename") String filename,
      @RequestParam(value = "filetype") String filetype,
      @RequestParam(value = "memberId") String memberId,
      @RequestParam(value = "courseId") String courseId,
      @RequestParam(value = "workNumber") String workNumber) {
    UploadFileRequest request =
        new UploadFileRequest(filename, filetype, memberId, courseId, workNumber);
    SimpleResponse response = driveService.uploadFile(file, request);
    return ResponseEntity.ok(response);
  }

  /**
   * Deletes Drive file using the fileLink.
   * 
   * @param uploadedInputStream
   * @param fileDetail
   * @return
   */
  @DeleteMapping("/file/{fileLink}")
  public ResponseEntity<SimpleResponse> deleteFile(@PathVariable("fileLink") String fileLink) {
    SimpleResponse response = driveService.deleteFile(fileLink);
    return ResponseEntity.ok(response);
  }

  /**
   * Gets the file link if the files exists for a member.
   * 
   * @param memberId
   * @param fileTitle
   * @return
   */
  @GetMapping("/file/{memberId}/{fileTitle}")
  public ResponseEntity<SimpleResponse> getFileLink(@PathVariable("memberId") String memberId,
      @PathVariable("fileTitle") String fileTitle) {
    SimpleResponse response = driveService.getFileLink(memberId, fileTitle);
    return ResponseEntity.ok(response);
  }

  /**
   * Gets a list of breaking news.
   * 
   * @return
   */
  @GetMapping("/breakingNews")
  public ResponseEntity<WorkResponse<BreakingNews>> getBreakingNews() {
    WorkResponse<BreakingNews> response = driveService.getBreakingNews();
    return ResponseEntity.ok(response);
  }

  /**
   * Gets the content of the bulletin board.
   * 
   * @return
   */
  @GetMapping("/bulletinBoard")
  public ResponseEntity<SimpleResponse> getBulletinBoard() {
    SimpleResponse response = driveService.getBulletinBoard();
    return ResponseEntity.ok(response);
  }
}
