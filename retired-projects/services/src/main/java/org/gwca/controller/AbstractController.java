package org.gwca.controller;

import org.gwca.error.CommonError;
import org.gwca.error.ErrorSeverity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractController {

  private Logger logger = LoggerFactory.getLogger(this.getClass().getSimpleName());

  /** The default constructor */
  public AbstractController() {
    super();
  }

  /**
   * Creates an info type of common error.
   * 
   * @return
   */
  protected CommonError createInfo(String description) {
    CommonError info = new CommonError();
    info.setErrorCode("OK");
    info.setDescription(description);
    info.setSeverity(ErrorSeverity.INFO);
    return info;
  }

  /**
   * Creates an error type of error
   * 
   * @param code
   * @param description
   * @return
   */
  protected CommonError createError(String code, String description) {
    CommonError info = new CommonError();
    info.setErrorCode(code);
    info.setDescription(description);
    info.setSeverity(ErrorSeverity.ERROR);
    return info;
  }

  /**
   * Logs the message accordingly.
   * @param msg
   */
  protected void logMessage(CommonError msg) {
    if (msg.isSevereError()) {
      logger.error(msg.toString());
    } else {
      logger.info(msg.toString());
    }
  }

  /**
   * Creates an HTTP response status code accordingly.
   * 
   * @param info
   * @return
   */
  protected int getProcessStatus(CommonError info) {
    return (info.isSevereError() ? 500 : 200);
  }
}
