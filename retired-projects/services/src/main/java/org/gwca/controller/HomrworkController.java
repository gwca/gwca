package org.gwca.controller;

import java.util.List;

import org.gwca.model.message.WorkResponse;
import org.gwca.model.persistence.Homework;
import org.gwca.model.persistence.HomeworkId;
import org.gwca.service.DriveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/services/homework")
public class HomrworkController extends AbstractController {

  @Autowired
  private DriveService homeworkService;

  /** The default constructor */
  public HomrworkController() {
    super();
  }

  /**
   * Saves (creates or updates) a homework.
   * 
   * @param homework
   * @return
   */
  @PutMapping("/memberWork")
  public ResponseEntity<WorkResponse<Homework>> saveHomework(@RequestBody Homework homework) {
    WorkResponse<Homework> response = homeworkService.saveHomework(homework);
    return ResponseEntity.ok(response);
  }

  /**
   * Deletes a homework.
   * 
   * @param memberId
   * @param courseId
   * @param workNumber
   * @return
   */
  @DeleteMapping("/memberWork/{memberId}/{courseId}/{workNumber}")
  public ResponseEntity<WorkResponse<Homework>> deleteHomework(
      @PathVariable("memberId") String memberId, @PathVariable("courseId") String courseId,
      @PathVariable("workNumber") String workNumber) {
    WorkResponse<Homework> response =
        homeworkService.deleteHomework(memberId, courseId, workNumber);
    return ResponseEntity.ok(response);
  }

  /**
   * Deletes a list of homework.
   * 
   * @param homeworkIdList
   * @return
   */
  @PutMapping("/homeworkIdList")
  public ResponseEntity<WorkResponse<Homework>> deleteHomeworkList(@RequestBody 
      List<HomeworkId> homeworkIdList) {
    WorkResponse<Homework> response = homeworkService.deleteHomeworkList(homeworkIdList);
    return ResponseEntity.ok(response);
  }

  /**
   * Saves (creates or updates) a list of homework.
   * 
   * @param homeworkList
   * @return
   */
  @PutMapping("/homeworkList")
  public ResponseEntity<WorkResponse<Homework>> saveHomeworkList(@RequestBody List<Homework> homeworkList) {
    WorkResponse<Homework> response = homeworkService.saveHomeworkList(homeworkList);
    return ResponseEntity.ok(response);
  }

  /**
   * Gets the content of a homework by homework ID.
   * 
   * @param workNumber
   * @param courseId
   * @param memberId
   * @return
   */
  @GetMapping("/memberWork")
  public ResponseEntity<WorkResponse<Homework>> getHomework(
      @RequestParam("workNumber") String workNumber, @RequestParam("courseId") String courseId,
      @RequestParam("memberId") String memberId) {
    WorkResponse<Homework> response =
        homeworkService.getHomework(workNumber, courseId, memberId);
    return ResponseEntity.ok(response);
  }

  /**
   * Gets a list of homework by courseId and memberId.
   * 
   * @param courseId
   * @param memberId
   * @return
   */
  @GetMapping("/memberWorkList")
  public ResponseEntity<WorkResponse<Homework>> getHomeworkList(
      @RequestParam("courseId") String courseId,
      @RequestParam("memberId") String memberId) {
    WorkResponse<Homework> response = homeworkService.getHomeworkList(courseId, memberId);
    return ResponseEntity.ok(response);
  }

  /**
   * Gets a list of submitted homework by courseId a list of member email.
   * 
   * @param courseId
   * @param memberIds
   * @return
   */
  @GetMapping("/submittedWorkList")
  public ResponseEntity<WorkResponse<Homework>> getSubmittedHomeworkList(
      @RequestParam("courseId") String courseId,
      @RequestParam("memberIds") String memberIds) {
    WorkResponse<Homework> response =
        homeworkService.getSubmittedHomeworkList(courseId, memberIds);
    return ResponseEntity.ok(response);
  }

  /**
   * Gets a list of homework by courseId, workNumber and a list of member email.
   * 
   * @param courseId
   * @param memberIds
   * @return
   */
  @GetMapping("/courseWorkList")
  public ResponseEntity<WorkResponse<Homework>> getHomeworkList(
      @RequestParam("courseId") String courseId, @RequestParam("workNumber") String workNumber,
      @RequestParam("memberIds") String memberIds) {
    WorkResponse<Homework> response =
        homeworkService.getHomeworkList(courseId, workNumber, memberIds);
    return ResponseEntity.ok(response);
  }
}
