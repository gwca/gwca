package org.gwca;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * This is needed for deploying this app to Google AppEngine
 * @author youping.hu
 *
 */
public class DataServiceInitializer extends SpringBootServletInitializer {
  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
      return application.sources(DataServiceApplication.class);
  }
}
