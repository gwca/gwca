package org.gwca.googleapp.stub;

import java.util.List;

import org.gwca.googleapp.impl.GoogleAppException;
import org.gwca.model.persistence.Clazz;
import org.gwca.model.persistence.Member;

public interface TableRepository {

  /**
   * Gets a list of the relationships of the active class members.
   * 
   * @return
   */
  public List<Clazz> getClasses() throws GoogleAppException;

  /**
   * Gets a list of the relationships of the active class members. This is used by the cache handler
   * to retrieve data from the database tables.
   * 
   * @return
   * @throws GoogleAppException
   */
  public List<Clazz> getClassRelationship() throws GoogleAppException;

  /**
   * Gets a list of the active members.
   * 
   * @return
   */
  public List<Member> getActiveMembers() throws GoogleAppException;

  /**
   * Gets the complete list of registered members.
   * 
   * @return
   */
  public List<Member> getAllMembers() throws GoogleAppException;
  
  public void setCacheHandler(CacheHandler cacheHandler);
}
