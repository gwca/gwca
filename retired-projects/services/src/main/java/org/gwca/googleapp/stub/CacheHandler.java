package org.gwca.googleapp.stub;

import java.util.List;
import java.util.Map;

import org.gwca.googleapp.impl.GoogleAppException;
import org.gwca.googleapp.impl.StorageType;
import org.gwca.model.book.BookIndex;
import org.gwca.model.persistence.Clazz;
import org.gwca.model.persistence.Homework;
import org.gwca.model.persistence.HomeworkId;
import org.gwca.model.persistence.Member;
import org.gwca.model.persistence.StorageDescription;

public interface CacheHandler {

  /**
   * Loads the classes and the associated tables into a cache
   * 
   * @param tableRepo
   * @throws GoogleAppException
   */
  public void loadTableCache();

  /**
   * Gets the cached classes and the associated tables
   * 
   * @return
   */
  public List<Clazz> getClasses();

  /**
   * Gets a cached class by id
   * @param classId
   * @return
   */
  public Clazz getClazz(String classId);
  
  /**
   * Loads the homework files of the active students into a cache
   * 
   * @param classes
   * 
   * @return
   */
  public void loadHomeworkCache(List<Clazz> classes);

  /**
   * Adds a new or updates an existing homework in the cache.
   * 
   * @param homework
   * @return
   */
  public List<Homework> updateHomeworkCache(Homework homework);

  /**
   * Removes an existing homework from the cache.
   * 
   * @param homeworkId
   * @return
   */
  public List<Homework> removeHomeworkCache(HomeworkId homeworkId);

  /**
   * Checks if the homework is in the cache.
   * 
   * @param homeworkId
   * @return
   */
  public boolean isHomeworkCached(HomeworkId homeworkId);

  /**
   * Adds a new homework into the cache
   * 
   * @param homeworkList
   */
  public void addHomeworkCache(List<Homework> homeworkList);
  
  /**
   * Deletes the homework from the cache
   * @param homeworkId
   */
  public List<Homework> deleteHomeworkCache(HomeworkId homeworkId);

  /**
   * Gets a cached homework list by homeworkId
   * 
   * @param homeworkId
   * @return
   */
  public List<Homework> getCachedHomework(HomeworkId homeworkId);

  /**
   * Gets a cached homework list by courseId and memberId
   * 
   * @param courseId
   * @param memberId
   * @return
   */
  public List<Homework> getCachedHomework(String courseId, String memberId);

  /**
   * Gets a cached member by classId and memberId
   * @param classId
   * @param memberId
   * @return
   */
  public Member getMember(String classId, String memberId);

  /**
   * Gets a cached member by its email
   * @param email
   * @return
   */
  public Member getMember(String email);
  
  /**
   * Gets a class by a member that is in that class.
   * @param member
   * @return
   */
  public Clazz getClazz(Member member);

  /**
   * Loads the storage descriptions (tables, folders, etc.) into a cache
   * 
   * @throws GoogleAppException
   */
  public void loadStorageDescriptionCache();

  /**
   * Gets a cached storage description by name (table, folder, etc.)
   * 
   * @param name
   * @return
   */
  public StorageDescription getStorageDesc(String name);
  
  /**
   * Adds a new storage description into the cache.
   * @param storageDesc
   */
  public void addStorageDesc(StorageDescription storageDesc);
  
  /**
   * Gets a list of folder descriptions by type
   * 
   * @param storageType
   * @return
   */
  public List<StorageDescription> getFolderDescriptions(StorageType storageType);
  
  /**
   * Loads the book index into the cache
   */
  public void loadBookIndexCache();
  
  /**
   * Loads the lesson lists into the cache.
   */
  public void loadBookLessonsCache();

  /**
   * Gets the book index from the cache
   * @return
   */
  public List<BookIndex> getBookIndex();
  
  /**
   * Gets all the lessons text of the specified book.
   * @param bookTitle
   * @return
   */
  public Map<String, String> getBookLessons(String bookTitle);
  
  /**
   * Gets the lesson text of the specified book by lessonId.
   * @param bookTitle
   * @param lessonId
   * @return
   */
  public String getBookLesson(String bookTitle, String lessonId);
  
}
