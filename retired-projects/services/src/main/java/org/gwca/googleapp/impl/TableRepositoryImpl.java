package org.gwca.googleapp.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.gwca.googleapp.stub.CacheHandler;
import org.gwca.googleapp.stub.TableRepository;
import org.gwca.model.persistence.Clazz;
import org.gwca.model.persistence.Course;
import org.gwca.model.persistence.Member;
import org.gwca.model.persistence.MemberRole;
import org.gwca.model.persistence.StorageDescription;
import org.gwca.model.persistence.StorageName;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.api.services.fusiontables.Fusiontables;
import com.google.api.services.fusiontables.Fusiontables.Query.Sql;
import com.google.api.services.fusiontables.model.Sqlresponse;

public class TableRepositoryImpl extends AbstractApp implements TableRepository {

  private static final Logger logger = Logger.getLogger(TableRepositoryImpl.class.getSimpleName());

  /** The fusion table service */
  private Fusiontables fusionTableClient = null;

  /** The cache handler */
  @Autowired
  private CacheHandler cacheHandler = null;

  /** The default constructor */
  public TableRepositoryImpl() {}

  public Fusiontables getFusiontableClient() {
    if (fusionTableClient == null) {
      this.fusionTableClient = googleAppFactory.createFusionTableClient();
    }
    return fusionTableClient;
  }

  /*
   * (non-Javadoc)
   * 
   * @see TableRepository#getClasses()
   */
  @Override
  public List<Clazz> getClasses() throws GoogleAppException {
    return cacheHandler.getClasses();
  }

  /*
   * (non-Javadoc)
   * 
   * @see TableRepository#getClassRelationship()
   */
  @Override
  public List<Clazz> getClassRelationship() throws GoogleAppException {

    Clazz clazz = null;
    List<Clazz> classes = new ArrayList<Clazz>();
    String statement = activeClassesQuery();

    try {
      Sql sql = getFusiontableClient().query().sql(statement);
      Sqlresponse sqlResp = sql.execute();

      List<List<Object>> rows = sqlResp.getRows();
      if (rows != null && rows.size() > 0) {
        for (List<Object> row : rows) {
          clazz = rowToClass(row);

          // get all the members of the class
          List<Member> members = getClassMembers(clazz);
          clazz.setMembers(members);

          // get the course of the class
          Course course = getCourse(clazz.getCourseId());
          clazz.setCourse(course);

          classes.add(clazz);
        }
      }
      logger.info("Loaded " + classes.size() + " active classes");
      return classes;
    } catch (Exception ex) {
      throw createServiceException("GetClassesFailed", ex);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see TableRepository#getActiveMembers()
   */
  @Override
  public List<Member> getActiveMembers() throws GoogleAppException {
    String query = activeMembersQuery();
    return getMembers(query);
  }

  /*
   * (non-Javadoc)
   * 
   * @see TableRepository#getAllMembers()
   */
  @Override
  public List<Member> getAllMembers() throws GoogleAppException {
    try {
      String statement = allMembersQuery();
      return getMembers(statement);
    } catch (Exception ex) {
      throw createServiceException("GetAllMembersFailed", ex);
    }
  }

  /**
   * Gets all the member objects of the class.
   * 
   * @param clazz
   * @return
   * @throws GoogleAppException
   */
  private List<Member> getClassMembers(Clazz clazz) throws GoogleAppException {
    List<Member> members = new ArrayList<Member>();
    String memberIds = clazz.getMemberIds();
    String statement = memberIDsQuery(memberIds);

    try {
      Sql sql = getFusiontableClient().query().sql(statement);
      Sqlresponse sqlResp = sql.execute();
      List<List<Object>> rows = sqlResp.getRows();
      for (List<Object> row : rows) {
        Member member = rowToMember(row);
        members.add(member);
      }
    } catch (Exception ex) {
      throw createServiceException("GetClassMembersFailed", ex);
    }
    return members;
  }

  /**
   * Gets a course object by courseId
   * 
   * @param courseId
   * @return
   * @throws GoogleAppException
   * @throws Exception
   */
  private Course getCourse(String courseId) throws GoogleAppException {
    Course course = null;
    String statement = courseQuery(courseId);
    try {
      Sql sql = getFusiontableClient().query().sql(statement);
      Sqlresponse sqlResp = sql.execute();
      List<Object> row = getFirstRow(sqlResp);
      course = rowToCourse(row);
    } catch (Exception ex) {
      throw createServiceException("GetCourseFailed", ex);
    }
    return course;
  }

  /**
   * Gets a list of members using the provided query statement.
   * 
   * @param query
   * @return
   * @throws GoogleAppException
   */
  private List<Member> getMembers(String query) throws GoogleAppException {

    Member member = null;
    List<Member> members = new ArrayList<Member>();
    try {
      Sql sql = getFusiontableClient().query().sql(query);
      Sqlresponse sqlResp = sql.execute();
      List<List<Object>> rows = sqlResp.getRows();
      if (rows != null && rows.size() > 0) {
        for (List<Object> row : rows) {
          member = rowToMember(row);
          members.add(member);
        }
      }
    } catch (Exception ex) {
      throw createServiceException("GetMemberFailed", ex);
    }
    return members;
  }

  /**
   * Gets the first row of an SQL execution response.
   * 
   * @param response
   * @return
   */
  private List<Object> getFirstRow(Sqlresponse response) {
    List<List<Object>> rows = response.getRows();
    return ((rows != null && rows.size() > 0) ? response.getRows().get(0) : null);
  }

  /**
   * Converts a members table row to a Member object<br>
   * ROWID, memberId, role, email, ccEmail, firstName, lastName, chineseName, isActive, isPrivate<br>
   * Note that the role column is a comma delimited string, needs to split into roles and 
   * assign them to the role list.
   * @param row
   * @return
   */
  private Member rowToMember(List<Object> row) {
    Member member = new Member();
    member.setStorageId((String) row.get(0));
    member.setMemberId((String) row.get(1));
    member.setEmail((String) row.get(3));
    member.setCcEmail((String) row.get(4));
    member.setFirstName((String) row.get(5));
    member.setLastName((String) row.get(6));
    member.setChineseName((String) row.get(7));
    member.setRelatedMemberIds((String) row.get(8));
    member.setActive(isTrue(row.get(9)));
    member.setPrivate(isTrue(row.get(10)));

    String[] roleArray = ((String) row.get(2)).split(",");
    List<MemberRole> roles = member.getRoles();
    for (String role : roleArray) {
      roles.add(MemberRole.fromValue(role.trim()));
    }
    if (roles.size() > 0) {
      member.setRole(roles.get(0));
    }
    return member;
  }

  /**
   * Converts a courses table row to a Course object<br>
   * ROWID, id, name, alias, description, image, isActive
   * 
   * @param row
   * @return
   */
  private Course rowToCourse(List<Object> row) {
    Course course = new Course();
    course.setStorageId((String) row.get(0));
    course.setCourseId((String) row.get(1));
    course.setName((String) row.get(2));
    course.setAlias((String) row.get(3));
    course.setDescription((String) row.get(4));
    course.setImage((String) row.get(5));
    course.setActive(isTrue(row.get(6)));
    return course;
  }

  /**
   * Converts a classes table row to a ClassMember object<br>
   * ROWID, classId, className, classURL, classYear, teacherId, courseId, memberIds, isActive
   * 
   * @param row
   * @return
   */
  private Clazz rowToClass(List<Object> row) {
    Clazz classMember = new Clazz();
    classMember.setStorageId((String) row.get(0));
    classMember.setClassId((String) row.get(1));
    classMember.setClassName((String) row.get(2));
    classMember.setClassUrl((String) row.get(3));
    classMember.setClassYear((String) row.get(4));
    classMember.setTeacherId((String) row.get(5));
    classMember.setCourseId((String) row.get(6));
    classMember.setMemberIds((String) row.get(7));
    classMember.setActive(isTrue(row.get(8)));
    return classMember;
  }

  /**
   * Builds a query to get all the active members
   * 
   * @return
   */
  private String activeMembersQuery() {
    StorageDescription desc = cacheHandler.getStorageDesc(StorageName.MEMBER_TABLE.value());
    String query =
        "SELECT " + desc.getColumns() + " FROM " + desc.getStorageId() + " WHERE isActive = 'true'";
    return query;
  }

  /**
   * Builds a query to get all the registered members
   * 
   * @return
   */
  private String allMembersQuery() {
    StorageDescription desc = cacheHandler.getStorageDesc(StorageName.MEMBER_TABLE.value());
    String query = "SELECT " + desc.getColumns() + " FROM " + desc.getStorageId();
    return query;
  }

  /**
   * Builds a query to get a list of members by an ID set.
   * 
   * @param memberIds
   * @return
   */
  private String memberIDsQuery(String memberIds) {
    StorageDescription desc = cacheHandler.getStorageDesc(StorageName.MEMBER_TABLE.value());
    String[] ids = memberIds.split(",");
    StringBuilder buffer = new StringBuilder();
    for (String id : ids) {
      buffer.append(quoteValue(id.trim()) + ", ");
    }
    String values = buffer.toString().replaceAll(", $", "");
    String query = "SELECT " + desc.getColumns() + " FROM " + desc.getStorageId()
        + " WHERE memberId IN ( " + values + " )";
    return query;
  }

  /**
   * Builds a query to get a course by ID.
   * 
   * @param courseId
   * @return
   */
  private String courseQuery(String courseId) {
    StorageDescription desc = cacheHandler.getStorageDesc(StorageName.COURSE_TABLE.value());
    String query = "SELECT " + desc.getColumns() + " FROM " + desc.getStorageId() + " WHERE id = "
        + quoteValue(courseId);
    return query;
  }

  /**
   * Builds a query to get all the active classes.
   * 
   * @return
   */
  private String activeClassesQuery() {
    StorageDescription desc = cacheHandler.getStorageDesc(StorageName.CLASS_TABLE.value());
    String query =
        "SELECT " + desc.getColumns() + " FROM " + desc.getStorageId() + " WHERE isActive = 'true'";
    return query;
  }

  /**
   * Checks if the value of the specified column is "true".
   * 
   * @param column
   * @return
   */
  private boolean isTrue(Object column) {
    return (column != null && ((String) column).equalsIgnoreCase("true"));
  }

  /**
   * Adds a pair of single quotes to the string value.
   * 
   * @param value
   * @return
   */
  private String quoteValue(String value) {
    return (value == null ? "''" : "'" + value + "'");
  }

  // the accessors
  public CacheHandler getCacheHandler() {
    return cacheHandler;
  }

  public void setCacheHandler(CacheHandler cacheHandler) {
    this.cacheHandler = cacheHandler;
  }
}
