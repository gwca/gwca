package org.gwca.googleapp.stub;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.gwca.googleapp.impl.GoogleAppException;
import org.gwca.model.book.BookIndex;
import org.gwca.model.message.NameValuePair;
import org.gwca.model.message.UploadFileRequest;
import org.gwca.model.persistence.BreakingNews;
import org.gwca.model.persistence.DriveFileId;
import org.gwca.model.persistence.DrivePermission;
import org.gwca.model.persistence.Homework;
import org.gwca.model.persistence.HomeworkId;
import org.gwca.model.persistence.StorageDescription;
import org.gwca.model.workitem.Write;

import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;

public interface DriveRepository {

  public static final String TEXT_JSON = "text/json";
  public static final String TEXT_HTML = "text/html";
  public static final String APP_JSON = "application/json";
  public static final String GOOGLE_DOC = "application/vnd.google-apps.document";

  /**
   * Uploads a file to Drive.
   * 
   * @param request
   * @param inputStream
   * @return
   * @throws GoogleAppException
   */
  public List<NameValuePair> uploadFile(InputStream inputStream, UploadFileRequest request) throws GoogleAppException;

  /**
   * Removes a file from Drive.
   * 
   * @param fileLink
   * @return
   * @throws GoogleAppException
   */
  public void deleteFile(String fileLink) throws GoogleAppException;

  /**
   * Gets the pre-defined book index from Drive.
   * 
   * @return the book index.
   * @throws GoogleAppException
   */
  public List<BookIndex> getBookIndex() throws GoogleAppException;

  /**
   * Loads all the homework files of the course of the active members (teachers and students) from
   * the active members homework folder.
   * 
   * @param courseId
   */
  public Map<String, List<Homework>> loadHomeworkList(String courseId) throws GoogleAppException;

  /**
   * Loads all the lesson text files of the specified book title from the book folder.
   * 
   * @param bookTitle
   * @return
   * @throws GoogleAppException
   */
  public Map<String, String> loadBookLessons(StorageDescription folderDesc) throws GoogleAppException;

  /**
   * Updates a homework.
   * 
   * @param homework
   * @return
   * @throws GoogleAppException
   */
  public Homework saveHomework(Homework homework) throws GoogleAppException;

  /**
   * Removes a homework from the assigned homework list of a student.
   * 
   * @param homeworkId
   * @throws GoogleAppException
   */
  public boolean deleteHomework(HomeworkId homeworkId) throws GoogleAppException;

  /**
   * Creates a new homework list for a member and a course.
   * 
   * @param homeworkList
   * @param courseId
   * @param memberId
   * @return
   * @throws GoogleAppException
   */
  public List<Homework> createMemberHomework(List<Homework> homeworkList, String courseId,
      String memberId) throws GoogleAppException;

  /**
   * Creates an essay (a Google Docs file) in the member's folder.
   * 
   * @param item
   * @param memberId
   * @return the DriveFileId of the created GoogleDoc file.
   * @throws GoogleAppException
   */
  public DriveFileId createEssay(Write item, String memberId)
      throws GoogleAppException;

  /**
   * Creates a slide (a Google Slide file) in the member's folder.
   * 
   * @param item
   * @param memberId
   * @return the DriveFileId of the created GoogleSlide file.
   * @throws GoogleAppException
   */
  public DriveFileId createSlide(Write item, String memberId)
      throws GoogleAppException;

  /**
   * Sets proper permission to the members' homework folder
   *
   * @param permissions
   * @throws GoogleAppException
   */
  public List<Permission> setUserPermission(List<DrivePermission> permissions)
      throws GoogleAppException;

  /**
   * Gets the file link by memberId and file title.
   * 
   * @param memberId
   * @param fileTitle
   * @return the URL of the file.
   * @throws GoogleAppException
   */
  public DriveFileId getFileLink(String memberId, String fileTitle) throws GoogleAppException;

  /**
   * Gets a list of audio files from a Drive folder.
   * 
   * @return
   * @throws GoogleAppException
   */
  public FileList getAudioFileList() throws GoogleAppException;
  
  /**
   * Gets the content of a homework by homework ID.
   * 
   * @param homeworkId
   * @return
   */
  public Homework getHomework(HomeworkId homeworkId);

  /**
   * Gets a list of homework by courseId and memberId.
   * 
   * @param courseId
   * @param memberId
   * @return
   */
  public List<Homework> getHomeworkList(String courseId, String memberId);

  /**
   * Gets a list of homework by courseId, workNumber and a list of member IDs.
   * 
   * @param courseId
   * @param workNumber
   * @param memberIDs
   * @return
   */
  public List<Homework> getHomeworkList(String courseId, String workNumber, List<String> memberIDs);

  /**
   * Gets a list of submitted homework by courseId and a list of member IDs.
   * 
   * @param courseId
   * @param memberIDs
   * @return
   */
  public List<Homework> getHomeworkList(String courseId, List<String> memberIDs);

  /**
   * Creates a homework folder for the specified member.
   * 
   * @param memberId
   * @return null if failed.
   * @throws GoogleAppException
   */
  public File createHomeworkFolder(String memberId) throws GoogleAppException;

  /**
   * Gets a list of member homework folder descriptions from the homework folder.
   * 
   * @return
   * @throws GoogleAppException
   */
  public List<StorageDescription> getMemberFolderDescriptions() throws GoogleAppException;

  /**
   * Gets a list of book folder descriptions from the book folder.
   * 
   * @return
   * @throws GoogleAppException
   */
  public List<StorageDescription> getBookFolderDescriptions() throws GoogleAppException;

  /**
   * Checks if the member file exists.
   * 
   * @param courseId
   * @param memberId
   * @return
   * @throws GoogleAppException
   */
  public boolean checkMemberFile(String courseId, String memberId) throws GoogleAppException;

  /**
   * Gets a list of breaking news from the news folder.
   * 
   * @return
   * @throws GoogleAppException
   */
  public List<BreakingNews> getBreakingNews();
  
  /**
   * Gets the content of the bulletin board.
   * @return
   */
  public String getBulletinBoard();

  public void setCacheHandler(CacheHandler cacheHandler);
}
