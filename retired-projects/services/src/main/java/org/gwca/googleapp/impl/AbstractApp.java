package org.gwca.googleapp.impl;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.gwca.error.CommonError;
import org.gwca.error.ErrorSeverity;
import org.gwca.utils.GoogleAppFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractApp {

  private static final Logger logger = Logger.getLogger(AbstractApp.class.getSimpleName());

  @Autowired
  protected GoogleAppFactory googleAppFactory;
  
  /** The default constructor */
  public AbstractApp() {}

  /**
   * Creates a WorkServiceException with an error.
   * 
   * @param errorCode
   * @param ex
   * @return
   */
  protected GoogleAppException createServiceException(String errorCode, Throwable ex) {
    CommonError error = new CommonError();
    error.setErrorCode(errorCode);
    error.setDescription(ex);
    error.setSeverity(ErrorSeverity.ERROR);
    logger.log(Level.SEVERE, errorCode, ex);
    return new GoogleAppException(error);
  }

  /**
   * Creates a WorkServiceException with an error.
   * 
   * @param errorCode
   * @param ex
   * @return
   */
  protected GoogleAppException createServiceException(String errorCode, String description) {
    CommonError error = new CommonError();
    error.setErrorCode(errorCode);
    error.setDescription(description);
    error.setSeverity(ErrorSeverity.ERROR);
    logger.log(Level.SEVERE, errorCode, description);
    return new GoogleAppException(error);
  }

  // the injections
  public void setGoogleAppFactory(GoogleAppFactory googleAppFactory) {
    this.googleAppFactory = googleAppFactory;
  }
}
