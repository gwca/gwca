package org.gwca.googleapp.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.gwca.googleapp.stub.CacheHandler;
import org.gwca.googleapp.stub.SheetRepository;
import org.gwca.model.persistence.Clazz;
import org.gwca.model.persistence.Course;
import org.gwca.model.persistence.Member;
import org.gwca.model.persistence.MemberRole;
import org.gwca.model.persistence.StorageDescription;
import org.gwca.model.persistence.StorageName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;

@Service
public class SheetRepositoryImpl extends AbstractApp implements SheetRepository {

  private static final Logger logger = Logger.getLogger(SheetRepositoryImpl.class.getSimpleName());

  /** The Google Sheets service */
  private Sheets sheetClient = null;

  /** The cache handler */
  @Autowired
  private CacheHandler cacheHandler = null;

  /** The default constructor */
  public SheetRepositoryImpl() {}

  /**
   * Creates a Google Sheets client using the injected GoogleAppFactory
   * @return
   */
  public Sheets getSheetsClient() {
    if (sheetClient == null) {
      this.sheetClient = googleAppFactory.createSheetClient();
    }
    return sheetClient;
  }

  /*
   * (non-Javadoc)
   * 
   * @see SheetRepository#getClasses()
   */
  @Override
  public List<Clazz> getClasses() throws GoogleAppException {
    return cacheHandler.getClasses();
  }

  /*
   * (non-Javadoc)
   * 
   * @see SheetRepository#getClassRelationship()
   */
  @Override
  public List<Clazz> getClassRelationship() throws GoogleAppException {

    Clazz clazz = null;
    List<Clazz> classes = new ArrayList<Clazz>();

    try {
      List<List<Object>> rows = readSheet(StorageName.CLASS_SHEET);
      if (rows != null && rows.size() > 0) {
        for (List<Object> row : rows) {
          clazz = rowToClass(row);
          if (clazz.isActive()) {

            // get all the members of the class
            List<Member> members = getClassMembers(clazz);
            clazz.setMembers(members);

            // get the course of the class
            Course course = getCourse(clazz.getCourseId());
            clazz.setCourse(course);

            classes.add(clazz);
          }
        }
      }
      logger.info("Loaded " + classes.size() + " active classes");
      return classes;
    } catch (Exception ex) {
      throw createServiceException("GetClassesFailed", ex);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see SheetRepository#getActiveMembers()
   */
  @Override
  public List<Member> getActiveMembers() throws GoogleAppException {
    List<Member> members = new ArrayList<Member>();
    try {
      List<List<Object>> rows = readSheet(StorageName.MEMBER_SHEET);
      if (rows != null && rows.size() > 0) {
        for (List<Object> row : rows) {
          Member member = rowToMember(row);
          if (member.isActive()) {
            members.add(member);
          }
        }
      }
    } catch (Exception ex) {
      throw createServiceException("GetActiveMembersFailed", ex);
    }
    return members;
  }

  /**
   * Gets all the member objects of the class.
   * 
   * @param clazz
   * @return
   * @throws GoogleAppException
   */
  private List<Member> getClassMembers(Clazz clazz) throws GoogleAppException {
    List<Member> members = new ArrayList<Member>();
    List<String> memberIdList = clazz.getMemberIdList();
    try {
      List<List<Object>> rows = readSheet(StorageName.MEMBER_SHEET);
      if (rows != null && rows.size() > 0) {
        for (List<Object> row : rows) {
          Member member = rowToMember(row);
          if (memberIdList.contains(member.getMemberId())) {
            members.add(member);
          }
        }
      }
    } catch (Exception ex) {
      throw createServiceException("GetClassMembersFailed", ex);
    }
    return members;
  }

  /**
   * Gets a course object by courseId
   * 
   * @param courseId
   * @return
   * @throws GoogleAppException
   * @throws Exception
   */
  private Course getCourse(String courseId) throws GoogleAppException {
    Course course = null;
    try {
      List<List<Object>> rows = readSheet(StorageName.COURSE_SHEET);
      if (rows != null && rows.size() > 0) {
        for (List<Object> row : rows) {
          course = rowToCourse(row);
          if (course.getCourseId().equals(courseId)) {
            return course;
          }
        }
      }
    } catch (Exception ex) {
      throw createServiceException("GetCourseFailed", ex);
    }
    return course;
  }

  /**
   * Converts a members table row to a Member object<br>
   * ROWID, memberId, role, email, ccEmail, firstName, lastName, chineseName, isActive,
   * isPrivate<br>
   * Note that the role column is a comma delimited string, needs to split into roles and assign
   * them to the role list.
   * 
   * @param row
   * @return
   */
  private Member rowToMember(List<Object> row) {
    Member member = new Member();
    member.setMemberId((String) row.get(0));
    member.setEmail((String) row.get(2));
    member.setCcEmail((String) row.get(3));
    member.setFirstName((String) row.get(4));
    member.setLastName((String) row.get(5));
    member.setChineseName((String) row.get(6));
    member.setActive(isTrue(row.get(7)));
    member.setPrivate(isTrue(row.get(8)));
    if (row.size() > 9) {
      member.setRelatedMemberIds((String) row.get(9));
    }
    String[] roleArray = ((String) row.get(1)).split(",");
    List<MemberRole> roles = member.getRoles();
    for (String role : roleArray) {
      roles.add(MemberRole.fromValue(role.trim()));
    }
    if (roles.size() > 0) {
      member.setRole(roles.get(0));
    }
    return member;
  }

  /**
   * Converts a courses table row to a Course object<br>
   * ROWID, id, name, alias, description, image, isActive
   * 
   * @param row
   * @return
   */
  private Course rowToCourse(List<Object> row) {
    Course course = new Course();
    course.setCourseId((String) row.get(0));
    course.setName((String) row.get(1));
    course.setAlias((String) row.get(2));
    course.setDescription((String) row.get(3));
    course.setImage((String) row.get(4));
    course.setActive(isTrue(row.get(5)));
    return course;
  }

  /**
   * Converts a classes table row to a ClassMember object<br>
   * ROWID, classId, className, classURL, classYear, teacherId, courseId, memberIds, isActive
   * 
   * @param row
   * @return
   */
  private Clazz rowToClass(List<Object> row) {
    Clazz classMember = new Clazz();
    classMember.setClassId((String) row.get(0));
    classMember.setClassName((String) row.get(1));
    classMember.setClassYear((String) row.get(2));
    classMember.setCourseId((String) row.get(3));
    classMember.setTeacherId((String) row.get(4));
    classMember.setActive(isTrue(row.get(5)));
    classMember.setMemberIds((String) row.get(6));
    classMember.setClassUrl((String) row.get(7));
    return classMember;
  }

  /**
   * Reads the entire spreadsheet.
   * 
   * @param sheetName
   * @return
   * @throws IOException
   */
  private List<List<Object>> readSheet(StorageName sheetName) throws IOException {
    String registrationSheet = StorageName.REGISTRATION_SHEET.value();
    StorageDescription desc = cacheHandler.getStorageDesc(registrationSheet);
    String sheetId = desc.getStorageId();
    String range = sheetName.value();

    Sheets sheetClient = getSheetsClient();
    Sheets.Spreadsheets.Values.Get request =
        sheetClient.spreadsheets().values().get(sheetId, range);
    ValueRange response = request.execute();

    // remove the title row
    List<List<Object>> rows = response.getValues();
    Iterator<List<Object>> it = rows.iterator();
    it.next();
    it.remove();
    return rows;
  }

  /**
   * Checks if the value of the specified column is "true".
   * 
   * @param column
   * @return
   */
  private boolean isTrue(Object column) {
    return (column != null && ((String) column).equalsIgnoreCase("true"));
  }

  // the accessors
  public CacheHandler getCacheHandler() {
    return cacheHandler;
  }

  public void setCacheHandler(CacheHandler cacheHandler) {
    this.cacheHandler = cacheHandler;
  }
}
