package org.gwca.googleapp.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.gwca.googleapp.stub.CacheHandler;
import org.gwca.googleapp.stub.DriveRepository;
import org.gwca.googleapp.stub.SheetRepository;
import org.gwca.model.book.BookIndex;
import org.gwca.model.persistence.Clazz;
import org.gwca.model.persistence.Homework;
import org.gwca.model.persistence.HomeworkId;
import org.gwca.model.persistence.Member;
import org.gwca.model.persistence.StorageDescription;
import org.gwca.model.persistence.StorageMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CacheHandlerImpl extends AbstractApp implements CacheHandler {

  private static final Logger logger = Logger.getLogger(CacheHandlerImpl.class.getSimpleName());

  /** The Google sheets repository */
  @Autowired
  private SheetRepository googleSheet;

  /** The Google drive repository */
  @Autowired
  private DriveRepository googleDrive;

  /** The descriptions of the static folders and tables */
  @Autowired
  private StorageMap storageMap;

  // the caches
  private List<Clazz> classesCache;
  private List<BookIndex> bookIndexCache;
  private List<StorageDescription> storageDescCache;
  private Map<String, List<Homework>> homeworkCache;
  private Map<String, Map<String, String>> bookLessonsCache;

  /**
   * The default constructor.
   */
  public CacheHandlerImpl() {
    super();
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#loadTableCache()
   */
  @Override
  public void loadTableCache() {
    try {
      classesCache = googleSheet.getClassRelationship();
    } catch (Exception ex) {
      logger.severe("Loading the classes cached failed." + ex.getMessage());
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#loadHomeworkCache(List<Clazz>)
   */
  @Override
  public void loadHomeworkCache(List<Clazz> classes) {
    if (homeworkCache != null) {
      homeworkCache.clear();
    } else {
      homeworkCache = new HashMap<String, List<Homework>>();
    }

    try {
      for (Clazz clazz : classes) {
        Map<String, List<Homework>> worklist = googleDrive.loadHomeworkList(clazz.getCourseId());
        if (worklist != null && worklist.size() > 0) {
          homeworkCache.putAll(worklist);
        }
      }
    } catch (Exception ex) {
      logger.severe("Loading the homework cache failed." + ex.getMessage());
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#loadStorageDescriptionCache()
   */
  @Override
  public void loadStorageDescriptionCache() {

    if (storageDescCache == null) {
      storageDescCache = new ArrayList<StorageDescription>();
    } else {
      storageDescCache.clear();
    }

    // add the descriptions of the injected descriptions of the folders, sheets and
    // files.
    storageDescCache.addAll(storageMap.getStorageDescriptionList());

    // add the folder descriptions of the active students and books dynamically
    try {
      List<StorageDescription> descriptions = googleDrive.getMemberFolderDescriptions();
      storageDescCache.addAll(descriptions);

      descriptions = googleDrive.getBookFolderDescriptions();
      storageDescCache.addAll(descriptions);
    } catch (Exception ex) {
      logger.severe("Loading storage description cache failed." + ex.getMessage());
    }
    logger.info("Loaded " + storageDescCache.size() + " storage description items");
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#loadBookIndexCache()
   */
  @Override
  public void loadBookIndexCache() {
    try {
      bookIndexCache = googleDrive.getBookIndex();
    } catch (Exception ex) {
      logger.severe("Loading the lesson list cache failed." + ex.getMessage());
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#loadBookLessonsCache()
   */
  @Override
  public void loadBookLessonsCache() {

    if (bookLessonsCache == null) {
      bookLessonsCache = new HashMap<String, Map<String, String>>();
    } else {
      bookLessonsCache.clear();
    }

    try {
      for (StorageDescription folderDesc : this.getFolderDescriptions(StorageType.BOOK_FOLDER)) {
        Map<String, String> lessons = googleDrive.loadBookLessons(folderDesc);
        bookLessonsCache.put(folderDesc.getName(), lessons);
      }
    } catch (Exception ex) {
      logger.severe("Loading the book cache failed." + ex.getMessage());
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#getClasses()
   */
  @Override
  public List<Clazz> getClasses() {
    return this.classesCache;
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#getClazz(String)
   */
  @Override
  public Clazz getClazz(String classId) {
    for (Clazz clazz : this.classesCache) {
      if (clazz.getClassId().equals(classId)) {
        return clazz;
      }
    }
    return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see isHomeworkCached(HomeworkId)
   */
  public boolean isHomeworkCached(HomeworkId homeworkId) {
    String cacheKey = createHomeworkCacheKey(homeworkId);
    List<Homework> homeworkList = homeworkCache.get(cacheKey);
    if (homeworkList != null) {
      Iterator<Homework> it = homeworkList.iterator();
      while (it.hasNext()) {
        Homework cachedWork = it.next();
        if (match(cachedWork, homeworkId)) {
          return true;
        }
      }
    }
    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#updateHomeworkCache(Homework)
   */
  @Override
  public List<Homework> updateHomeworkCache(Homework homework) {
    List<Homework> homeworkList = removeHomeworkCache(homework.getHomeworkId());
    homeworkList.add(homework);
    return homeworkList;
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#removeHomeworkCache(HomeworkId)
   */
  @Override
  public List<Homework> removeHomeworkCache(HomeworkId homeworkId) {
    String cacheKey = createHomeworkCacheKey(homeworkId);
    List<Homework> homeworkList = homeworkCache.get(cacheKey);
    if (homeworkList == null) {
      homeworkList = new ArrayList<Homework>();
      homeworkCache.put(cacheKey, homeworkList);
    } else {
      Iterator<Homework> it = homeworkList.iterator();
      while (it.hasNext()) {
        Homework cachedWork = it.next();
        if (match(cachedWork, homeworkId)) {
          it.remove();
        }
      }
    }
    return homeworkList;
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#addHomeworkCache(java.util.List)
   */
  @Override
  public void addHomeworkCache(List<Homework> homeworkList) {
    if (homeworkList != null && homeworkList.size() > 0) {
      String key = createHomeworkCacheKey(homeworkList.get(0));
      homeworkCache.put(key, homeworkList);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#deleteHomeworkCache(HomeworkId)
   */
  @Override
  public List<Homework> deleteHomeworkCache(HomeworkId homeworkId) {
    String cacheKey = createHomeworkCacheKey(homeworkId.getCourseId(), homeworkId.getMemberId());
    List<Homework> homeworkList = homeworkCache.get(cacheKey);
    if (homeworkList != null) {
      Iterator<Homework> it = homeworkList.iterator();
      while (it.hasNext()) {
        Homework cachedWork = it.next();
        if (match(cachedWork, homeworkId)) {
          it.remove();
        }
      }
    }
    return homeworkList;
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#getCachedHomework(HomeworkId)
   */
  @Override
  public List<Homework> getCachedHomework(HomeworkId homeworkId) {
    String cacheKey = createHomeworkCacheKey(homeworkId);
    return homeworkCache.get(cacheKey);
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#getCachedHomework(java.lang.String, java.lang.String)
   */
  @Override
  public List<Homework> getCachedHomework(String courseId, String memberId) {
    String cacheKey = createHomeworkCacheKey(courseId, memberId);
    return homeworkCache.get(cacheKey);
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#getMember(String, String)
   */
  @Override
  public Member getMember(String classId, String memberId) {
    Clazz clazz = getClazz(classId);
    if (clazz != null) {
      for (Member member : clazz.getMembers()) {
        if (member.getMemberId().equals(memberId)) {
          return member;
        }
      }
    }
    return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#getMember(java.lang.String)
   */
  @Override
  public Member getMember(String email) {
    for (Clazz clazz : getClasses()) {
      for (Member member : clazz.getMembers()) {
        if (member.getEmail().equals(email)) {
          return member;
        }
      }
    }
    return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#getClazz(org.gwca.model.persistence.Member)
   */
  @Override
  public Clazz getClazz(Member member) {
    for (Clazz clazz : this.classesCache) {
      for (String memberId : clazz.getMemberIdList()) {
        if (memberId.equals(member.getMemberId())) {
          return clazz;
        }
      }
    }
    return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#getStorageDesc(java.lang.String)
   */
  @Override
  public StorageDescription getStorageDesc(String name) {
    for (StorageDescription desc : storageDescCache) {
      if (name.equals(desc.getName())) {
        return desc;
      }
    }
    return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#addStorageDesc(StorageDescription)
   */
  @Override
  public void addStorageDesc(StorageDescription storageDesc) {
    if (storageDesc != null) {
      this.storageDescCache.add(storageDesc);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#getFolderDescriptions(org.gwca.googleapp.impl.StorageType)
   */
  @Override
  public List<StorageDescription> getFolderDescriptions(StorageType storageType) {
    List<StorageDescription> descriptions = new ArrayList<StorageDescription>();
    for (StorageDescription description : storageDescCache) {
      if (description.getStorageType().equals(storageType)) {
        descriptions.add(description);
      }
    }
    return descriptions;
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#getBookIndex()
   */
  @Override
  public List<BookIndex> getBookIndex() {
    return this.bookIndexCache;
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#getBookLessons(String)
   */
  @Override
  public Map<String, String> getBookLessons(String bookTitle) {
    return this.bookLessonsCache.get(bookTitle);
  }

  /*
   * (non-Javadoc)
   * 
   * @see CacheHandler#getBookLesson(String, String)
   */
  @Override
  public String getBookLesson(String bookTitle, String lessonId) {
    Map<String, String> lessons = this.bookLessonsCache.get(bookTitle);
    logger.info("Getting a lesson text of " + bookTitle + ", " + lessonId);
    return (lessons == null ? "" : lessons.get(lessonId));
  }

  /**
   * Creates a cache key
   * 
   * @param homeworkId
   * @return
   */
  private <E extends HomeworkId> String createHomeworkCacheKey(E homeworkId) {
    return createHomeworkCacheKey(homeworkId.getCourseId(), homeworkId.getMemberId());
  }

  /**
   * Creates a cache key
   * 
   * @param courseId
   * @param memberId
   * @return
   */
  private String createHomeworkCacheKey(String courseId, String memberId) {
    return courseId + "_" + memberId;
  }

  /**
   * Checks if the homeworkId matches the homework
   * 
   * @param homework
   * @param homeworkId
   * @return
   */
  private boolean match(Homework homework, HomeworkId homeworkId) {
    return (homeworkId.getCourseId() != null
        && homeworkId.getCourseId().equals(homework.getCourseId())
        && homeworkId.getWorkNumber() != null
        && homeworkId.getWorkNumber().equals(homework.getWorkNumber())
        && homeworkId.getMemberId() != null
        && homeworkId.getMemberId().equals(homework.getMemberId()));
  }

  // the injections
  public void setGoogleSheet(SheetRepository googleSheet) {
    this.googleSheet = googleSheet;
  }

  public void setGoogleDrive(DriveRepository googleDrive) {
    this.googleDrive = googleDrive;
  }

  public void setStorageMap(StorageMap storageMap) {
    this.storageMap = storageMap;
  }
}
