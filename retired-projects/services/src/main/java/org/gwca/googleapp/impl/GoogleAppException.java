package org.gwca.googleapp.impl;

import org.gwca.error.CommonError;

/**
 * This is a data repository specific exception.
 */
public class GoogleAppException extends Exception {

  private static final long serialVersionUID = -4849314552061193796L;

  private CommonError error;

  public GoogleAppException() {
    super();
  }

  public GoogleAppException(String msg) {
    super(msg);
  }

  public GoogleAppException(CommonError error) {
    super();
    setError(error);
  }

  public CommonError getError() {
    return error;
  }

  public void setError(CommonError error) {
    this.error = error;
  }
}
