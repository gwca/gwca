package org.gwca.googleapp.impl;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;
import org.gwca.googleapp.stub.CacheHandler;
import org.gwca.googleapp.stub.DriveRepository;
import org.gwca.model.book.BookIndex;
import org.gwca.model.message.NameValuePair;
import org.gwca.model.message.UploadFileRequest;
import org.gwca.model.persistence.BreakingNews;
import org.gwca.model.persistence.Clazz;
import org.gwca.model.persistence.DriveFileId;
import org.gwca.model.persistence.DrivePermission;
import org.gwca.model.persistence.Homework;
import org.gwca.model.persistence.HomeworkId;
import org.gwca.model.persistence.Member;
import org.gwca.model.persistence.StorageDescription;
import org.gwca.model.persistence.StorageName;
import org.gwca.model.workitem.SimpleItem;
import org.gwca.model.workitem.Write;
import org.gwca.utils.DataUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpResponse;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.ParentReference;
import com.google.api.services.drive.model.Permission;

@Service
public class DriveRepositoryImpl extends AbstractApp implements DriveRepository {

  private static final Logger logger = Logger.getLogger(DriveRepositoryImpl.class.getSimpleName());
  private static final int BLOCK_SIZE = 1024 * 256;

  /** The data utility */
  private DataUtils dataUtil = new DataUtils();

  /** The Google Drive client */
  private Drive driveClient = null;

  /** The cache handler */
  @Autowired
  private CacheHandler cacheHandler = null;

  /** The default constructor */
  public DriveRepositoryImpl() {}

  /**
   * Uses the GoogleAppFactory to create a DriveClient when needed.
   * 
   * @return
   */
  public Drive getDriveClient() {
    if (driveClient == null) {
      driveClient = googleAppFactory.createDriveClient();
    }
    return driveClient;
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#saveHomework(Homework)
   */
  @Override
  public Homework saveHomework(Homework homework) throws GoogleAppException {
    createDriveFiles(homework);
    List<Homework> homeworkList = cacheHandler.updateHomeworkCache(homework);
    saveMemberHomeworkFile(homeworkList, homework.getCourseId(), homework.getMemberId());
    logger.info("Saved " + homework.getIdString());
    return homework;
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#deleteHomework(HomeworkId)
   */
  @Override
  public boolean deleteHomework(HomeworkId homeworkId) throws GoogleAppException {
    if (cacheHandler.isHomeworkCached(homeworkId)) {
      Homework homeworkToDelete = getCachedHomeworkForMember(homeworkId);
      cleanAttachedFiles(homeworkToDelete);
      List<Homework> homeworkList = cacheHandler.removeHomeworkCache(homeworkId);
      saveMemberHomeworkFile(homeworkList, homeworkId.getCourseId(), homeworkId.getMemberId());
      logger.info("Deleted " + homeworkId.getIdString());
      return true;
    } else {
      logger.info(homeworkId.getIdString() + " is not cached, cannot be deleted.");
      return false;
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#uploadFile(InputStream, UploadFileRequest)
   */
  @Override
  public List<NameValuePair> uploadFile(InputStream inputStream, UploadFileRequest request)
      throws GoogleAppException {

    // locate the member's folder
    String memberId = request.getMemberId();
    StorageDescription memberFolderDesc = cacheHandler.getStorageDesc(memberId);
    if (memberFolderDesc == null) {
      throw createServiceException("MemberNotExist",
          "The member's folder doesn't exist:" + memberId);
    }

    // create a new Drive file with the content from the input stream.
    try {
      List<NameValuePair> fileAttrs = new ArrayList<NameValuePair>();
      ParentReference parentReference = createParentReference(memberFolderDesc.getStorageId());
      File driveFile = new File();
      driveFile.setTitle(request.getDriveFileTitle());
      driveFile.setParents(Collections.singletonList(parentReference));

      // File's content.
      ByteArrayContent mediaContent =
          new ByteArrayContent(request.getFiletype(), IOUtils.toByteArray(inputStream));
      File file = getDriveClient().files().insert(driveFile, mediaContent).execute();
      String fileLink = null;
      if (request.isAudio()) {
        fileLink = dataUtil.getAudioLink(file.getId());
      } else {
        fileLink = dataUtil.getImageLink(file.getId());
      }
      fileAttrs.add(new NameValuePair("fileId", file.getId()));
      fileAttrs.add(new NameValuePair("fileLink", fileLink));
      return fileAttrs;
    } catch (Exception ex) {
      throw createServiceException("UploadFileFailed",
          "Uploading file " + request.getDriveFileTitle() + " failed");
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#getFileLink(String, String)
   */
  @Override
  public DriveFileId getFileLink(String memberId, String fileTitle) throws GoogleAppException {

    // find the file by title
    String fileQuery = memberFileTitleQuery(memberId, fileTitle);
    try {
      FileList workFiles =
          getDriveClient().files().list().setQ(fileQuery).setSpaces("drive").execute();

      if (workFiles != null) {
        List<File> files = workFiles.getItems();
        if (files != null && files.size() > 0) {
          File workFile = files.get(0);
          logger.info("Retrieved " + fileTitle + " for the member " + memberId);
          DriveFileId fileId = new DriveFileId(workFile.getId(), workFile.getDefaultOpenWithLink());
          return fileId;
        }
      }
    } catch (Exception ex) {
      throw createServiceException("FindFileFailed",
          "Finding file " + fileTitle + " for the member " + memberId + " failed");
    }
    return null;
  }


  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#getAudioFileList()
   */
  @Override
  public FileList getAudioFileList() throws GoogleAppException {

    // find the audio files list
    String fileQuery = audioFilesQuery();
    FileList audioFileList = new FileList();
    List<File> audioItems = new ArrayList<>();
    try {
      // get the folders first
      FileList folderList =
          getDriveClient().files().list().setQ(fileQuery).setSpaces("drive").execute();
      List<File> folders = folderList.getItems();
      
      // get all the files of each folder
      if (folders != null && folders.size() > 0) {
        for (File folder : folders) {
          String query = "'" + folder.getId() + "' in parents and trashed = false";
          FileList fileList =
              getDriveClient().files().list().setQ(query).setSpaces("drive").execute();
          audioItems.addAll(fileList.getItems());
        }
      }
      audioFileList.setItems(audioItems);
      return audioFileList;
    } catch (Exception ex) {
      throw createServiceException("FindFileFailed", "Finding audio files failed");
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#deleteFile(String)
   */
  @Override
  public void deleteFile(String fileLink) throws GoogleAppException {
    String fileId = dataUtil.getFileIdFromLink(fileLink);
    if (fileId != null) {
      try {
        getDriveClient().files().delete(fileId).execute();
      } catch (Exception e) {
        throw createServiceException("DeleteFileFailed", "Deleting file " + fileLink + " failed");
      }
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#setUserPermission(List)
   */
  @Override
  public List<Permission> setUserPermission(List<DrivePermission> drivePermissions)
      throws GoogleAppException {
    Drive driveClient = getDriveClient();
    List<Permission> userPermissions = new ArrayList<Permission>();
    Permission userPermission = null;
    try {
      for (DrivePermission drivePermission : drivePermissions) {
        userPermission = new Permission();
        userPermission.setType(drivePermission.getType());
        userPermission.setRole(drivePermission.getRole().value());
        userPermission.setEmailAddress(drivePermission.getEmail());
        userPermission.setValue(drivePermission.getEmail());
        userPermission =
            driveClient.permissions().insert(drivePermission.getFileId(), userPermission).execute();
        userPermissions.add(userPermission);
      }
      logger.info("Set " + userPermissions.size() + " user permissions.");
      return userPermissions;
    } catch (Exception ex) {
      logger.severe("Set permission failed:" + ex.getMessage());
      ex.printStackTrace();
      throw createServiceException("SetPermissionFailed", ex.getMessage());
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#getBookList()
   */
  public List<BookIndex> getBookIndex() throws GoogleAppException {

    List<BookIndex> bookList = null;
    try {

      // get book list file description
      StorageDescription desc = cacheHandler.getStorageDesc(StorageName.BOOK_LIST.value());
      if (desc == null) {
        throw createServiceException("BookListNotExist",
            "The book list file doesn't exist:" + StorageName.BOOK_LIST.value());
      }
      File bookListFile = getDriveClient().files().get(desc.getStorageId()).execute();
      String jsonString = getFileContent(bookListFile);
      bookList = dataUtil.toBookList(jsonString);
      logger.info("Loaded the book list.");
    } catch (Exception ex) {
      logger.log(Level.WARNING, "Caching the book list failed.", ex);
    }
    return bookList;
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#getHomework(HomeworkId)
   */
  @Override
  public Homework getHomework(HomeworkId homeworkId) {
    List<Homework> homeworkList = cacheHandler.getCachedHomework(homeworkId);
    if (homeworkList != null) {
      for (Homework homework : homeworkList) {
        if (match(homework, homeworkId)) {
          logger.info("Retrieved " + homeworkId.getIdString());
          return homework;
        }
      }
    }
    logger.info("File doesn't exist: " + homeworkId.getIdString());
    return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#loadHomeworkList(String)
   */
  @Override
  public Map<String, List<Homework>> loadHomeworkList(String courseId) throws GoogleAppException {

    int count = 0;
    Map<String, List<Homework>> homeworkMap = new HashMap<String, List<Homework>>();
    try {

      // get all the active member homework folder descriptions
      List<StorageDescription> memberFolderDescriptions =
          cacheHandler.getFolderDescriptions(StorageType.MEMBER_FOLDER);

      for (StorageDescription folderDesc : memberFolderDescriptions) {
        // get all the files of each folder
        String fileQuery = homeworkFolderQuery(folderDesc.getStorageId(), courseId);
        FileList workFiles =
            getDriveClient().files().list().setQ(fileQuery).setSpaces("drive").execute();

        for (File workFile : workFiles.getItems()) {
          String homeworkInfo = folderDesc.getName() + "/" + workFile.getTitle();
          try {
            // get the content of each file and add it to the homework cache
            String content = getFileContent(workFile);
            List<Homework> homeworkList = dataUtil.toHomeworkList(content);
            if (homeworkList != null && homeworkList.size() > 0) {
              Homework work = homeworkList.get(0);
              String key = work.getCourseId() + "_" + work.getMemberId();
              homeworkMap.put(key, homeworkList);
              count += homeworkList.size();
            } else {
              logger.warning("Get homework:" + homeworkInfo + " failed");
            }
          } catch (Exception ex) {
            logger.log(Level.WARNING, "Caching file:" + homeworkInfo + " failed.", ex);
          }
        }
      }
      logger.info("Loaded " + count + " homework files of " + courseId + " for "
          + memberFolderDescriptions.size() + " active members");
    } catch (Exception ex) {
      logger.log(Level.WARNING, "Loading homework failed.", ex);
    }
    return homeworkMap;
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#loadBookLessons(StorageDescription)
   */
  @Override
  public Map<String, String> loadBookLessons(StorageDescription folderDesc)
      throws GoogleAppException {

    Map<String, String> lessonMap = new HashMap<String, String>();
    try {

      // get all the files of the folder
      String fileQuery = lessonFolderQuery(folderDesc.getStorageId());
      FileList lessonFiles =
          getDriveClient().files().list().setQ(fileQuery).setSpaces("drive").execute();

      for (File lessonFile : lessonFiles.getItems()) {
        String key = lessonFile.getTitle();
        try {
          // get the content of each file and add it to the map
          String lesson = getFileContent(lessonFile);
          lessonMap.put(key, lesson);
        } catch (Exception ex) {
          logger.log(Level.WARNING,
              "Loading " + key + " of book " + folderDesc.getName() + " failed.", ex);
        }
      }
      logger.info("Loaded " + lessonMap.size() + " lesson files of " + folderDesc.getName());
    } catch (Exception ex) {
      logger.log(Level.WARNING, "Loading lesson files of " + folderDesc.getName() + " failed.", ex);
    }
    return lessonMap;
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#getHomeworkList(String, String)
   */
  @Override
  public List<Homework> getHomeworkList(String courseId, String memberId) {
    List<Homework> homeworkList = null;
    if (courseId != null && memberId != null) {
      homeworkList = cacheHandler.getCachedHomework(courseId, memberId);
    }
    if (homeworkList == null) {
      homeworkList = new ArrayList<Homework>();
    }

    logger.info(
        "Retrieved " + homeworkList.size() + " homework for (" + courseId + ", " + memberId + ")");
    return homeworkList;
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#getHomeworkList(String, List)
   */
  @Override
  public List<Homework> getHomeworkList(String courseId, List<String> memberIDs) {
    List<Homework> homeworkList = new ArrayList<Homework>();
    for (String memberId : memberIDs) {
      memberId = memberId.trim();
      List<Homework> memberHomeworkList = getHomeworkList(courseId, memberId);
      for (Homework homework : memberHomeworkList) {
        if (match(homework, courseId, memberId, "Submitted")) {
          homeworkList.add(homework);
        }
      }
    }
    logger.info("Retrieved " + homeworkList.size() + " submitted homework for (" + courseId + ")");
    return homeworkList;
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#getHomeworkList(String, String, java.util.List)
   */
  @Override
  public List<Homework> getHomeworkList(String courseId, String workNumber,
      List<String> memberIDs) {
    List<Homework> homeworkList = new ArrayList<Homework>();
    for (String memberId : memberIDs) {
      HomeworkId homeworkId = new HomeworkId(workNumber, courseId, memberId.trim());
      Homework homework = getHomework(homeworkId);
      if (homework != null) {
        homeworkList.add(homework);
      } else {
        logger.warning("Get homework:" + homeworkId.getIdString() + " failed");
      }
    }
    logger.info("Retrieved " + homeworkList.size() + " homework for (" + courseId + ", "
        + workNumber + ")");
    return homeworkList;
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#createMemberHomework(List, String, String)
   */
  @Override
  public List<Homework> createMemberHomework(List<Homework> homeworkList, String courseId,
      String memberId) throws GoogleAppException {
    saveMemberHomeworkFile(homeworkList, courseId, memberId);
    return homeworkList;
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#getFolderDescriptions()
   */
  @Override
  public List<StorageDescription> getMemberFolderDescriptions() throws GoogleAppException {
    List<StorageDescription> folderDescriptions = new ArrayList<StorageDescription>(0);
    String query = allMemberFoldersQuery();
    try {
      FileList result = getDriveClient().files().list().setQ(query).setSpaces("drive").execute();
      List<File> folders = result.getItems();
      for (File folder : folders) {
        folderDescriptions.add(new StorageDescription(folder.getId(), StorageType.MEMBER_FOLDER,
            folder.getTitle(), "The homework folder of " + folder.getTitle()));
      }
      return folderDescriptions;
    } catch (Exception ex) {
      throw createServiceException("GetFolderDescriptionFailed", ex);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#getBookFolderDescriptions()
   */
  @Override
  public List<StorageDescription> getBookFolderDescriptions() throws GoogleAppException {
    List<StorageDescription> folderDescriptions = new ArrayList<StorageDescription>(0);
    String query = allBookFoldersQuery();
    try {
      FileList result = getDriveClient().files().list().setQ(query).setSpaces("drive").execute();
      List<File> folders = result.getItems();
      for (File folder : folders) {
        folderDescriptions.add(new StorageDescription(folder.getId(), StorageType.BOOK_FOLDER,
            folder.getTitle(), "The book folder of " + folder.getTitle()));
      }
      return folderDescriptions;
    } catch (Exception ex) {
      throw createServiceException("GetFolderDescriptionFailed", ex);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#createHomeworkFolder(String)
   */
  @Override
  public File createHomeworkFolder(String memberId) throws GoogleAppException {
    File folder = findMemberFolder(memberId);
    if (folder != null) {
      return folder;
    }

    StorageDescription desc = cacheHandler.getStorageDesc(StorageName.HOMEWORK_FOLDER.value());
    ParentReference parentReference = createParentReference(desc.getStorageId());

    try {
      folder = new File();
      folder.setMimeType("application/vnd.google-apps.folder");
      folder.setParents(Collections.singletonList(parentReference));
      folder.setTitle(memberId);
      folder = getDriveClient().files().insert(folder).execute();
      return folder;
    } catch (Exception ex) {
      throw createServiceException("CreateHomeworkFolderFailed", ex);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#checkMemberFile(String, String)
   */
  @Override
  public boolean checkMemberFile(String courseId, String memberId) throws GoogleAppException {
    File workFile = findMemberFile(courseId, memberId);
    return (workFile != null);
  }

  /*
   * (non-Javadoc)
   * 
   * @see GoogleDrive#createEssay(String, String)
   */
  @Override
  public DriveFileId createEssay(Write essay, String memberId) throws GoogleAppException {
    DriveFileId fileId = createGoogleFile(StorageName.ESSAY_TEMPLATE, essay, memberId);
    return fileId;
  }

  /*
   * (non-Javadoc)
   * 
   * @see GoogleDrive#createSlide(PageLink, String)
   */
  @Override
  public DriveFileId createSlide(Write item, String memberId) throws GoogleAppException {
    DriveFileId fileId = createGoogleFile(StorageName.SLIDE_TEMPLATE, item, memberId);
    return fileId;
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#getBreakingNews()
   */
  @Override
  public List<BreakingNews> getBreakingNews() {
    List<BreakingNews> breakingNews = null;
    StorageDescription desc = cacheHandler.getStorageDesc(StorageName.BREAKING_NEWS.value());
    try {
      File bookListFile = getDriveClient().files().get(desc.getStorageId()).execute();
      String jsonString = getFileContent(bookListFile);
      breakingNews = dataUtil.toBreakingNewsList(jsonString);
      logger.info("Loaded " + (breakingNews != null ? breakingNews.size() : 0) + " breaking news.");
    } catch (Exception ex) {
      logger.warning("Found no breaking news:" + ex.getMessage());
    }
    return breakingNews;
  }

  /*
   * (non-Javadoc)
   * 
   * @see DriveRepository#getBulletinBoard()
   */
  @Override
  public String getBulletinBoard() {
    String bulletinBoard = null;
    StorageDescription desc = cacheHandler.getStorageDesc(StorageName.BULLETIN_BOARD.value());
    try {
      File bookListFile = getDriveClient().files().get(desc.getStorageId()).execute();
      bulletinBoard = getFileContent(bookListFile);
      logger.info("Loaded the content of the bulletin board.");
    } catch (Exception ex) {
      logger.warning("Failed loading the bulletin board:" + ex.getMessage());
      ex.printStackTrace();
    }
    return bulletinBoard;
  }

  //
  // the utilities
  //
  /**
   * Creates a GoogleDocs or GoogleSlide file in the specified member folder. If a file in the
   * folder with the same title exists, don't create a new one.
   * 
   * @param storageName
   * @param item
   * @param memberId
   * @return
   * @throws GoogleAppException
   */
  private DriveFileId createGoogleFile(StorageName storageName, Write item, String memberId)
      throws GoogleAppException {

    DriveFileId driveFileId = null;

    // if a file in the folder with the same title exists, don't create a new one.
    driveFileId = getFileLink(memberId, item.getText());
    if (driveFileId != null) {
      logger.info("The file " + memberId + "_" + item.getTitle() + " exists");
      return driveFileId;
    }

    // locate the member's folder
    StorageDescription memberFolderDesc = cacheHandler.getStorageDesc(memberId);
    if (memberFolderDesc == null) {
      throw createServiceException("MemberNotExist",
          "The member's folder doesn't exist:" + memberId);
    }

    // locate the template file
    StorageDescription fileTemplateDesc = cacheHandler.getStorageDesc(storageName.value());
    if (fileTemplateDesc == null) {
      throw createServiceException("FileTemplateNotExist",
          "The template file doesn't exist:" + storageName.value());
    }

    // create a new GoogleDoc/GoogleSlide file and add the text as the content to it.
    // there is an issue creating a new GoogleDoc/GoogleSlide directly, so make a copy
    // from the template file instead.
    boolean isGoogleDoc = storageName == StorageName.ESSAY_TEMPLATE;
    String fileTitle = item.getText();
    StringBuilder content = new StringBuilder();
    for (String instruction : item.getInstructions()) {
      content.append(instruction + "\n");
    }
    String text = content.toString();

    try {
      ParentReference parentReference = createParentReference(memberFolderDesc.getStorageId());
      File driveFile = new File();
      // driveFile.setTitle(fileTitle + "_" + CommonUtil.getTitleDateString(new Date()));
      driveFile.setTitle(fileTitle);
      driveFile.setParents(Collections.singletonList(parentReference));
      driveFile =
          getDriveClient().files().copy(fileTemplateDesc.getStorageId(), driveFile).execute();

      if (isGoogleDoc && text != null && text.length() > 0) {
        ByteArrayContent mediaContent =
            new ByteArrayContent(GOOGLE_DOC, text.getBytes(StandardCharsets.UTF_8));
        getDriveClient().files().update(driveFile.getId(), driveFile, mediaContent).execute();
      }

      String fileId = driveFile.getId();
      String fileLink =
          (isGoogleDoc ? dataUtil.getDocumentLink(fileId) : dataUtil.getSlideLink(fileId));
      driveFileId = new DriveFileId(fileId, fileLink);
      return driveFileId;
    } catch (Exception ex) {
      throw createServiceException("CreateGoogleFileFailed", ex);
    }
  }

  /**
   * Gets a homework from the cached homework list for the specified member.
   * 
   * @param homeworkId
   * @return
   */
  private Homework getCachedHomeworkForMember(HomeworkId homeworkId) {
    List<Homework> workList = cacheHandler.getCachedHomework(homeworkId);
    for (Homework homework : workList) {
      if (homework.getMemberId().equals(homeworkId.getMemberId())
          && homework.getCourseId().equals(homeworkId.getCourseId())
          && homework.getWorkNumber().equals(homeworkId.getWorkNumber())) {
        return homework;
      }
    }
    return null;
  }

  /**
   * Creates Google files (GoogleDoc or GoogleSlide) for the homework assignment.<br>
   * Do not create an essay or slide file for a teacher.<br>
   * Do not override an existing essay or slide file, which is identified by the "url" attribute, as
   * the file could be edited by the student outside the app (e.g. using GoogleDoc.)
   * 
   * @param homework
   * @throws GoogleAppException
   */
  private void createDriveFiles(Homework homework) throws GoogleAppException {

    // do not create any Google file for a teacher
    if (isTeacher(homework.getMemberId())) {
      return;
    }

    // create a homework folder if it doesn't exist
    createHomeworkFolder(homework.getMemberId());

    // create Google files (GoogleDoc or GoogleSlide) for the assignment
    // skip the file creation if it exists already
    List<Write> items = homework.getWriteItems();
    for (Write item : items) {
      String memberId = homework.getMemberId();
      String fileLink = item.getUrl();
      DriveFileId driveFileId = null;
      if (fileLink == null || fileLink.length() == 0) {
        if (isGoogleDoc(item.getDocType())) {
          driveFileId = createEssay(item, memberId);
        } else if (isGoogleSlide(item.getDocType())) {
          driveFileId = createSlide(item, memberId);
        }
        if (driveFileId != null) {
          item.setUrl(driveFileId.getFileLink());
          item.setFileId(driveFileId.getFileId());
        }
      }
    }
  }

  private boolean isGoogleDoc(String type) {
    return type != null && type.equals("GoogleDoc");
  }

  private boolean isGoogleSlide(String type) {
    return type != null && type.equals("GoogleSlide");
  }


  /**
   * Cleans up all the attached files (essay, image, audio etc.) of the specified homework.
   *
   * @param homework
   * @throws GoogleAppException
   */
  private void cleanAttachedFiles(Homework homework) {

    // get a list of work items that may have an attachment
    String fileId = null;
    List<SimpleItem> items = homework.getItemsWithAttachment();
    for (SimpleItem item : items) {
      fileId = item.getFileId();
      if (fileId != null && fileId.length() > 0) {
        try {
          deleteFile(fileId);
        } catch (Exception ex) {
          logger.warning("Delete attachment of " + homework.getIdString() + " failed");
        }
      }
    }
  }

  /**
   * Checks if the member is a teacher
   * 
   * @param memberId
   * @return
   */
  private boolean isTeacher(String memberId) {
    List<Clazz> classes = cacheHandler.getClasses();
    for (Clazz clazz : classes) {
      for (Member member : clazz.getMembers()) {
        if (member.getMemberId().equals(memberId) && member.getRole().isTeacher()) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Updates the content of the member's homework file.
   * 
   * @param workFile
   * @param homeworkList
   * @return
   * @throws GoogleAppException
   */
  private File updateFileContent(File workFile, List<Homework> homeworkList)
      throws GoogleAppException {

    try {
      String content = dataUtil.toJsonString(homeworkList);
      ByteArrayContent mediaContent =
          new ByteArrayContent(APP_JSON, content.getBytes(StandardCharsets.UTF_8));
      Drive.Files.Update update =
          getDriveClient().files().update(workFile.getId(), workFile, mediaContent);
      return update.execute();
    } catch (Exception ex) {
      throw createServiceException("UpdateFileContentFailed", ex);
    }
  }

  /**
   * Creates a new drive file for a member. If the file exists (same file title), overrides its
   * content.
   * 
   * @param homeworkList
   * @param courseId
   * @param memberId
   * @return
   * @throws GoogleAppException
   */
  private File saveMemberHomeworkFile(List<Homework> homeworkList, String courseId, String memberId)
      throws GoogleAppException {

    // if the file already exists, override the content of it
    File workFile = findMemberFile(courseId, memberId);
    if (workFile != null) {
      try {
        return updateFileContent(workFile, homeworkList);
      } catch (Exception ex) {
        throw createServiceException("UpdateFileFailed", ex);
      }
    }

    // create a new member folder if it doesn't exist
    StorageDescription memberFolderDesc = cacheHandler.getStorageDesc(memberId);
    try {
      if (memberFolderDesc == null) {
        File memberFolder = createHomeworkFolder(memberId);
        memberFolderDesc = new StorageDescription(memberFolder.getId(), StorageType.MEMBER_FOLDER,
            memberFolder.getTitle(), "The homework folder of " + memberFolder.getTitle());
        cacheHandler.addStorageDesc(memberFolderDesc);
      }
    } catch (Exception ex) {
      throw createServiceException("CreateMemberFolderFailed", ex);
    }

    // create a new homework file
    try {
      ParentReference parentReference = createParentReference(memberFolderDesc.getStorageId());
      File fileMetadata = new File();
      fileMetadata.setTitle(courseId + "_" + memberId + ".json");
      fileMetadata.setParents(Collections.singletonList(parentReference));
      String content = dataUtil.toJsonString(homeworkList);
      ByteArrayContent mediaContent =
          new ByteArrayContent(APP_JSON, content.getBytes(StandardCharsets.UTF_8));
      Drive.Files.Insert insert =
          getDriveClient().files().insert(fileMetadata, mediaContent).setFields("id, parents");
      return insert.execute();
    } catch (Exception ex) {
      throw createServiceException("CreateFileFailed", ex);
    }
  }

  /**
   * Finds the specified drive file by courseId and memberId.
   * 
   * @param courseId
   * @param memberId
   * @return
   * @throws GoogleAppException
   */
  private File findMemberFile(String courseId, String memberId) throws GoogleAppException {
    File workFile = null;
    String query = homeworkMemberQuery(courseId, memberId);
    if (query == null) {
      Exception ex = new Exception("The member " + memberId + " is not active.");
      throw createServiceException("FindMemberFileFailed", ex);
    }

    try {
      FileList result = getDriveClient().files().list().setQ(query).setSpaces("drive").execute();
      if (result.getItems() != null && result.getItems().size() > 0) {
        workFile = result.getItems().get(0);
      }
      return workFile;
    } catch (Exception ex) {
      throw createServiceException("FindMemberFileFailed", ex);
    }
  }

  /**
   * Gets a member homework folder by title.
   * 
   * @param memberId
   * @return
   * @throws GoogleAppException
   */
  private File findMemberFolder(String memberId) throws GoogleAppException {
    File folder = null;
    StorageDescription desc = cacheHandler.getStorageDesc(memberId);

    try {
      if (desc != null) {
        folder = getDriveClient().files().get(desc.getStorageId()).execute();
        return folder;
      }
      String query = memberFolderQuery(memberId);
      FileList result = getDriveClient().files().list().setQ(query).setSpaces("drive").execute();
      if (result.getItems() != null && result.getItems().size() > 0) {
        folder = result.getItems().get(0);
      }
      return folder;
    } catch (Exception ex) {
      throw createServiceException("FindMemberFolderFailed", ex);
    }
  }

  /**
   * Finds the specified file and gets the content of it as a string.
   * 
   * @param homeworkId
   * @return
   * @throws GoogleAppException
   */
  private String getFileContent(File workFile) throws GoogleAppException {
    String buffer = null;
    String downloadUrl = workFile.getDownloadUrl();
    if (downloadUrl != null && downloadUrl.length() > 0) {
      try {
        HttpResponse resp = getDriveClient().getRequestFactory()
            .buildGetRequest(new GenericUrl(downloadUrl)).execute();
        InputStream stream = resp.getContent();
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] block = new byte[BLOCK_SIZE];
        int length;
        while ((length = stream.read(block)) != -1) {
          result.write(block, 0, length);
        }
        buffer = result.toString(StandardCharsets.UTF_8.name());
      } catch (Exception ex) {
        throw createServiceException("GetFileContentFailed", ex);
      }
    }
    return buffer;
  }

  /**
   * Checks if the courseId, memberId and the work status match the homework
   *
   * @param homework
   * @param courseId
   * @param memberid
   * @param status
   * @return
   */
  private boolean match(Homework homework, String courseId, String memberId, String status) {
    return (courseId != null && courseId.equals(homework.getCourseId()) && memberId != null
        && memberId.equals(homework.getMemberId()) && (homework.getStatus() == null
            || (status != null && status.equals(homework.getStatus()))));
  }

  /**
   * Checks if the homeworkId matches the homework
   * 
   * @param homework
   * @param homeworkId
   * @return
   */
  private boolean match(Homework homework, HomeworkId homeworkId) {
    return (homeworkId.getCourseId() != null
        && homeworkId.getCourseId().equals(homework.getCourseId())
        && homeworkId.getWorkNumber() != null
        && homeworkId.getWorkNumber().equals(homework.getWorkNumber())
        && homeworkId.getMemberId() != null
        && homeworkId.getMemberId().equals(homework.getMemberId()));
  }

  /**
   * Creates a new parent reference object.
   * 
   * @param parentFolderId
   * @return
   */
  private ParentReference createParentReference(String parentFolderId) {
    ParentReference parentReference = new ParentReference();
    parentReference.setId(parentFolderId);
    return parentReference;
  }

  /**
   * Builds a query to find the specified member folder from HOMEWORK folder.
   * 
   * @param homeworkId
   * @return
   */
  private String memberFolderQuery(String memberId) {
    StorageDescription desc = cacheHandler.getStorageDesc(StorageName.HOMEWORK_FOLDER.value());
    String folderId = desc.getStorageId();
    String query = "'" + folderId
        + "' in parents and mimeType='application/vnd.google-apps.folder' and trashed = false and title = '"
        + memberId + "'";
    return query;
  }

  /**
   * Builds a query to find all the member folders from HOMEWORK folder.
   * 
   * @return
   */
  private String allMemberFoldersQuery() {
    StorageDescription desc = cacheHandler.getStorageDesc(StorageName.HOMEWORK_FOLDER.value());
    String folderId = desc.getStorageId();
    String query = "'" + folderId
        + "' in parents and mimeType='application/vnd.google-apps.folder' and trashed = false";
    return query;
  }

  /**
   * Builds a query to find all the book folders from BOOK_FOLDER folder.
   * 
   * @return
   */
  private String allBookFoldersQuery() {
    StorageDescription desc = cacheHandler.getStorageDesc(StorageName.BOOK_FOLDER.value());
    String folderId = desc.getStorageId();
    String query = "'" + folderId
        + "' in parents and mimeType='application/vnd.google-apps.folder' and trashed = false";
    return query;
  }

  /**
   * Builds a query to find the specified homework file (course, member).
   * 
   * @param courseId
   * @param memberId
   * @return
   */
  private String homeworkMemberQuery(String courseId, String memberId) {
    StorageDescription desc = cacheHandler.getStorageDesc(memberId);
    if (desc == null) {
      // the member is either non-existing or inactive
      return null;
    }
    String folderId = desc.getStorageId();
    String filename = courseId + "_" + memberId + ".json";
    String query =
        "'" + folderId + "' in parents and trashed = false and title = '" + filename + "'";
    return query;
  }

  /**
   * Builds a query to find the specified Drive file by memberId and file title.
   * 
   * @param memberId
   * @param fileTitle
   * @return
   */
  private String memberFileTitleQuery(String memberId, String fileTitle) {
    StorageDescription desc = cacheHandler.getStorageDesc(memberId);
    if (desc == null) {
      // the member is either non-existing or inactive
      return null;
    }
    String folderId = desc.getStorageId();
    String query =
        "'" + folderId + "' in parents and trashed = false and title = '" + fileTitle + "'";
    return query;
  }

  /**
   * Builds a query to find the audio file list from specified Drive file folder.
   * 
   * @param memberId
   * @param fileTitle
   * @return
   */
  private String audioFilesQuery() {
    StorageDescription desc = cacheHandler.getStorageDesc(StorageName.AUDIO_FOLDER.value());
    if (desc == null) {
      return null;
    }
    String folderId = desc.getStorageId();
    String query = "'" + folderId + "' in parents and trashed = false";
    return query;
  }

  /**
   * Builds a query to find all the JSON files from the folder.
   * 
   * @param folderId
   * @param prefix
   * @return
   */
  private String homeworkFolderQuery(String folderId, String prefix) {
    String query = "'" + folderId + "' in parents and trashed = false and title contains '" + prefix
        + "' and (mimeType = '" + DriveRepository.APP_JSON + "' or mimeType = '"
        + DriveRepository.TEXT_JSON + "')";
    return query;
  }

  /**
   * Builds a query to find all the HTML files from the folder.
   * 
   * @param folderId
   * @param prefix
   * @return
   */
  private String lessonFolderQuery(String folderId) {
    String query = "'" + folderId + "' in parents and trashed = false and mimeType = '"
        + DriveRepository.TEXT_HTML + "'";
    return query;
  }

  // the accessors
  public CacheHandler getCacheHandler() {
    return cacheHandler;
  }

  public void setCacheHandler(CacheHandler cacheHandler) {
    this.cacheHandler = cacheHandler;
  }
}
