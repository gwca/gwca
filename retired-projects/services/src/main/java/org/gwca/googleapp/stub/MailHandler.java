package org.gwca.googleapp.stub;

import org.gwca.googleapp.impl.GoogleAppException;
import org.gwca.model.message.NotificationRequest;

public interface MailHandler {

  /**
   * Sends a notification email using the information of the request.
   * 
   * @param request
   * @throws GoogleAppException
   */
  public void sendNotification(NotificationRequest request) throws GoogleAppException;

}
