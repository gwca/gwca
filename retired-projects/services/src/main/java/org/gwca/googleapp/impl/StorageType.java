package org.gwca.googleapp.impl;

public enum StorageType {
    FUSION_TABLE, DRIVE_FOLDER, MEMBER_FOLDER, BOOK_FOLDER, DRIVE_FILE, DOC_TEMPLATE, DRIVE_SHEET;
}
