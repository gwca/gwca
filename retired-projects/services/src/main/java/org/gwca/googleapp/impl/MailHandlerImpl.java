package org.gwca.googleapp.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.gwca.googleapp.stub.MailHandler;
import org.gwca.model.message.NotificationRequest;
import org.springframework.stereotype.Service;

import com.google.api.client.util.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;

@Service
public class MailHandlerImpl extends AbstractApp implements MailHandler {

  private static final String authorizedSender = "me";

  /** The Google Mail service */
  private Gmail gmailClient = null;

  /** The default constructor */
  public MailHandlerImpl() {
    super();
  }

  /**
   * The constructor that creates a Google mail service.
   */
  public Gmail getGmailClient() {
    if (gmailClient == null) {
      this.gmailClient = googleAppFactory.createGmailClient();
    }
    return gmailClient;
  }

  /*
   * (non-Javadoc)
   * 
   * @see MailHandler#sendNotification(NotificationRequest)
   */
  @Override
  public void sendNotification(NotificationRequest request) throws GoogleAppException {
    try {
      MimeMessage notification = createMimeMessage(request);
      Message message = encodeMailMessage(notification);
      getGmailClient().users().messages().send(authorizedSender, message).execute();
    } catch (Exception ex) {
      throw createServiceException("SendNotificationEmailFailed", ex.getMessage());
    }
  }

  /**
   * Creates an email message from a request.
   * 
   * @param request
   * @return
   * @throws MessagingException
   */
  private MimeMessage createMimeMessage(NotificationRequest request) throws MessagingException {

    String from = request.getSenderEmail();
    String to = request.getReceiverEmail();
    String cc = request.getCcEmail();
    String subject = request.getSubject();
    String content = request.getBody();

    Properties props = new Properties();
    Session session = Session.getDefaultInstance(props, null);
    MimeMessage message = new MimeMessage(session);
    message.addRecipient(RecipientType.TO, new InternetAddress(to));
    if (request.isNotifyParent() && cc != null && cc.length() > 0) {
      message.addRecipient(RecipientType.CC, new InternetAddress(cc));
    }
    
    // the setFrom() and addFrom() methods don't work (Gmail blocks spamming?)
    if (from != null) {
      InternetAddress[] senders = new InternetAddress[1];
      senders[0] = new InternetAddress(from);
//      message.addFrom(senders);
      message.setFrom(senders[0]);
    }
    message.setSubject(subject);
    message.setContent(content, "text/html; charset=utf-8");
    return message;
  }

  /**
   * Create a message from an email.
   *
   * @param mailMessage Email to be set to raw of message
   * @return a message containing a base64url encoded email
   * @throws IOException
   * @throws MessagingException
   */
  private Message encodeMailMessage(MimeMessage mailMessage)
      throws MessagingException, IOException {
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    mailMessage.writeTo(buffer);
    byte[] bytes = buffer.toByteArray();
    String encodedMessage = Base64.encodeBase64URLSafeString(bytes);
    Message message = new Message();
    message.setRaw(encodedMessage);
    return message;
  }

  // /**
  // * Creates a SendAs for the "from" attribute.<br>
  // * This method doesn't work consistently
  // *
  // * @param sender
  // * @return
  // * @throws IOException
  // */
  // protected void createFrom(Member sender) {
  // try {
  // SendAs sendAs = new SendAs();
  // sendAs.setDisplayName(sender.getFullName());
  // sendAs.setSendAsEmail(sender.getEmail());
  // mailService.users().settings().sendAs().create(authorizedSender, sendAs);
  // } catch (Exception ex) {
  // ex.printStackTrace();
  // }
  // }
}
