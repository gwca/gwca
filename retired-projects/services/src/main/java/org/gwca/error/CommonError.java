package org.gwca.error;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;

/**
 * A common error object.
 */
public class CommonError implements Serializable, Cloneable {

  private static final long serialVersionUID = 7398632682684906333L;

  private String errorCode;
  private String description;
  private ErrorSeverity severity;

  public CommonError() {}

  public CommonError(String errorCode, ErrorSeverity severity, String description) {
    setErrorCode(errorCode);
    setSeverity(severity);
    setDescription(description);
  }

  public boolean isSevereError() {
    return severity != null && severity.equals(ErrorSeverity.ERROR)
        || severity.equals(ErrorSeverity.FATAL);
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setDescription(Throwable ex) {
    if (ex != null) {
      ByteArrayOutputStream outStream = new ByteArrayOutputStream();
      PrintWriter writer = new PrintWriter(outStream);
      ex.printStackTrace(writer);
      writer.flush();
      setDescription(outStream.toString());
      writer.close();
    }
  }

  public ErrorSeverity getSeverity() {
    return severity;
  }

  public void setSeverity(ErrorSeverity severity) {
    this.severity = severity;
  }

  @Override
  public String toString() {
    return errorCode + "|" + description + "|" + severity;
  }

  @Override
  public CommonError clone() {
    try {
      return (CommonError) super.clone();
    } catch (Exception ex) {
      return this;
    }
  }
}
