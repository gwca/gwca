package org.gwca.error;

public enum ErrorSeverity {

  FATAL("FATAL"), ERROR("ERROR"), WARN("WARN"), INFO("INFO"), DEBUG("DEBUG"), TRACE("TRACE");

  private final String severity;

  private ErrorSeverity(String severity) {
    this.severity = severity;
  }

  public String value() {
    return severity;
  }

  public static ErrorSeverity fromValue(String severity) {
    return valueOf(severity);
  }

  public boolean isSevereError() {
    return (this.equals(ErrorSeverity.ERROR) || this.equals(ErrorSeverity.FATAL));
  }
}
