package org.gwca.service;

import static org.junit.Assert.assertTrue;

import org.gwca.model.message.SimpleResponse;
import org.gwca.model.message.WorkResponse;
import org.gwca.model.persistence.BreakingNews;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/test-context.xml"})
public class DriveQueriesTest extends AbstractServiceTestCase {

  public DriveQueriesTest() {
    super();
  }

  /**
   * Initializes the caches before running any test case.
   */
  @Before
  public void initCaches() {
    cacheHandler.loadStorageDescriptionCache();
  }

  /**
   * Tests finding the content of the bulletin board
   * 
   * @throws Exception
   */
  @Test
  public void testBulletinBoard() {
    SimpleResponse response = driveService.getBulletinBoard();
    assertTrue(response != null);
    String board = (String) response.getValue();
    assertTrue(board != null);
    logger.info("The bulletin board: " + board);
  }

  /**
   * Tests finding a list of breaking news
   * 
   * @throws Exception
   */
  @Test
  public void testBreakingNews() {
    WorkResponse<BreakingNews> response = driveService.getBreakingNews();
    assertTrue(response != null);
    BreakingNews news = (BreakingNews) response.getFirstEntity();
    assertTrue(news != null);
    logger.info("The first break news: " + news.getTitle() + " startAt: " + news.getStartAt()
        + " expire at: " + news.getExpireAt() + " event: " + news.getEvent());
  }
}
