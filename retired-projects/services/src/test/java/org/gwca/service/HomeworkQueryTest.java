package org.gwca.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.gwca.model.message.ClassResponse;
import org.gwca.model.message.WorkResponse;
import org.gwca.model.persistence.Clazz;
import org.gwca.model.persistence.Homework;
import org.gwca.model.persistence.Member;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/test-context.xml"})
public class HomeworkQueryTest extends AbstractServiceTestCase {

  public HomeworkQueryTest() {
    super();
  }

  /**
   * Initializes the caches before running any test case.
   */
  @Before
  public void initCaches() {
    cacheHandler.loadStorageDescriptionCache();
    cacheHandler.loadTableCache();
    cacheHandler.loadHomeworkCache(cacheHandler.getClasses());
  }

  /**
   * Tests the following queries: 1. Gets the active class/member/course relationship from the
   * sheets<br>
   * 1. Gets all the active members<br>
   * 2. Gets the active members<br>
   * 3. Gets a list of homework by courseId, workNumber and a list of member IDs.<br>
   * 4. Gets a list of homework by courseId and memberId.<br>
   * 5. Gets a list of homework by courseId and workNumber for multiple students.<br>
   * 6. Gets a list of submitted homework by courseId a list of member IDs. 6. Gets the content of a
   * homework by homework ID (workNumber, memberId, courseId).<br>
   * 
   * @throws Exception
   */
  @Test
  public void testFinders() throws Exception {
    loadRelationship();
    loadActiveMembers();
    getMemberWorkList();
    getCourseWorkList();
    getSubmittedWorkList();
    getHomework();
  }

  /**
   * Tests getting the class/course/member relationship
   */
  private void loadRelationship() {
    ClassResponse<Clazz> response = sheetService.getActiveClasses();
    assertTrue(response != null);
    Clazz clazz = (Clazz) response.getFirstEntity();
    assertTrue(clazz != null && clazz.getMembers().size() == 4);
    assertTrue(clazz.getTeacherId().equals("youping-hu"));
    logger.info("The class/course/member relationship is loaded");
  }

  /**
   * Tests getting all the active members
   */
  private void loadActiveMembers() {
    ClassResponse<Member> response = sheetService.getMembers();
    assertNotNull(response);
    List<Member> members = (List<Member>) response.getEntities();
    assertTrue(verifyMember(members));
    logger.info("All the active members are loaded");
  }

  /**
   * Tests getting a list of homework of a member for the specified course
   * 
   * @throws Exception
   */
  private void getMemberWorkList() throws Exception {
    String memberId = "jane-doe";
    String courseId = "workshop";
    WorkResponse<Homework> response = driveService.getHomeworkList(courseId, memberId);
    assertNotNull(response);
    List<Homework> homeworkList = response.getEntities();
    assertTrue(verifyHomework(homeworkList));
    logger.info("Funnd " + homeworkList.size() + " homework files");
  }

  /**
   * Test getting a homework by course, member and work number
   * 
   * @throws Exception
   */
  private void getHomework() throws Exception {
    String courseId = "workshop";
    String workNumber = "50_1";
    String memberId = "jane-doe";
    WorkResponse<Homework> response = driveService.getHomework(workNumber, courseId, memberId);
    assertNotNull(response);
    Homework homework = response.getFirstEntity();
    assertTrue(homework.getMemberId().equals(memberId));
    logger.info("Funnd a homework for " + homework.getHomeworkId().getIdString());
  }

  /**
   * Test getting a list of homework of the course/workNumber for multiple students
   * 
   * @throws Exception
   */
  private void getCourseWorkList() throws Exception {
    String courseId = "workshop";
    String workNumber = "50_1";
    String memberIDs = "jane-doe, john-doe";
    WorkResponse<Homework> response = driveService.getHomeworkList(courseId, workNumber, memberIDs);
    assertNotNull(response);
    List<Homework> homeworkList = response.getEntities();
    assertTrue(homeworkList.size() == 2);
    logger.info("Funnd " + homeworkList.size() + " homework files");
  }

  /**
   * Tests getting a list of submitted homework files.
   * 
   * @throws Exception
   */
  private void getSubmittedWorkList() throws Exception {
    String courseId = "workshop";
    String memberIDs = "jane-doe, john-doe";
    WorkResponse<Homework> response = driveService.getSubmittedHomeworkList(courseId, memberIDs);
    assertNotNull(response);
    List<Homework> homeworkList = response.getEntities();
    assertTrue(homeworkList.size() == 1);
    logger.info("Funnd " + homeworkList.size() + " submitted homework files");
  }

  /**
   * Checks the member list contains the teacher.
   * 
   * @param members
   * @return
   */
  private boolean verifyMember(List<Member> members) {
    if (members.size() < 1) {
      return false;
    }
    for (Member member : members) {
      if (member.getMemberId().equals("youping-hu")) {
        return true;
      }
    }
    return false;
  }

  /**
   * Checks the homework list contains jane-doe/workshop.
   * 
   * @param homeworkList
   * @return
   */
  private boolean verifyHomework(List<Homework> homeworkList) {
    if (homeworkList.size() < 1) {
      return false;
    }
    for (Homework homework : homeworkList) {
      if (homework.getMemberId().equals("jane-doe") && homework.getCourseId().equals("workshop")) {
        return true;
      }
    }
    return false;
  }
}
