package org.gwca.service;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.gwca.error.CommonError;
import org.gwca.model.message.NotificationRequest;
import org.gwca.model.message.WorkResponse;
import org.gwca.model.persistence.Homework;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/test-context.xml"})
public class EmailTest extends AbstractServiceTestCase {

  public EmailTest() {
    super();
  }

  /**
   * Initializes the caches before running any test case.
   */
  @Before
  public void initCaches() {
    cacheHandler.loadStorageDescriptionCache();
    cacheHandler.loadTableCache();
  }

  /**
   * Tests sending email to the receivers.
   * 
   * @throws Exception
   */
  @Test
  public void testSendEmail() throws Exception {
    List<NotificationRequest> requestList = createRequestList();

    // send multiple email
    logger.info("Sending multiple email ...");
    WorkResponse<Homework> response = emailService.sendNotificationEmailList(requestList);
    assertTrue(response != null);
    List<CommonError> messages = response.getInfoList();
    assertTrue(messages.size() == 2);

    for (CommonError message : messages) {
      logger.info(message.getErrorCode() + ", " + message.getDescription());
    }

    // send a single email
    logger.info("Sending an email ...");
    response = emailService.sendNotificationEmail(requestList.get(0));
    assertTrue(response != null);
    CommonError info = response.getInfo();
    assertTrue(info.getErrorCode().equals("OK"));
    logger.info(info.getErrorCode() + ", " + info.getDescription());
  }

  /**
   * Creates a list of teacher to students assignment notifications
   * 
   * @return
   */
  private List<NotificationRequest> createRequestList() {
    List<NotificationRequest> list = new ArrayList<NotificationRequest>();
    NotificationRequest request = new NotificationRequest();
    request.setSenderEmail("youping_hu@greatwallchineseacademy.org");
    request.setReceiverEmail("youpingh@gmail.com");
    request.setReceiverName("John Doe");
    request.setSubject("teacher to John Doe: assignment");
    request.setBody("<p>Hello John,</p> <p>There is a new assignment.</p>");
    list.add(request);

    request = new NotificationRequest();
    request.setSenderEmail("youping_hu@greatwallchineseacademy.org");
    request.setReceiverEmail("youpingh@yahoo.com");
    request.setReceiverName("Jane Doe");
    request.setSubject("teacher to Jane Doe: assignment");
    request.setBody("<p>Hello Jane,</p> <p>There is a new assignment.</p>");
    list.add(request);
    return list;
  }
}
