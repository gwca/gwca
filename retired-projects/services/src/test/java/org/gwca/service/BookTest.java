package org.gwca.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.gwca.error.CommonError;
import org.gwca.model.book.BookIndex;
import org.gwca.model.message.NameValuePair;
import org.gwca.model.message.SimpleResponse;
import org.gwca.model.message.WorkResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/test-context.xml"})
public class BookTest extends AbstractServiceTestCase {

  public BookTest() {
    super();
  }

  /**
   * Initializes the caches before running any test case.
   */
  @Before
  public void initCaches() {
    cacheHandler.loadStorageDescriptionCache();
    cacheHandler.loadBookIndexCache();
    cacheHandler.loadBookLessonsCache();
  }

  /**
   * Tests the following queries: <br>
   * 1. Gets the cached book index<br>
   * 2. Gets the cached book lesson<br>
   * 3. Gets the list of lesson URLs.<br>
   * 4. Refresh the book cache.<br>
   * 
   * @throws Exception
   */
  @Test
  public void testFinders() throws Exception {
    getBookIndexTest();
    getBookLessonTest();
    getLessonUrlTest();
    refreshBookCacheTest();
  }

  /**
   * Tests getting the cached book list.
   * 
   * @throws Exception
   */
  private void getBookIndexTest() throws Exception {
    WorkResponse<BookIndex> response = bookService.getBookIndex();
    assertTrue(response != null);
    List<BookIndex> bookIndex = response.getEntities();
    logger.info("Funnd " + bookIndex.size() + " books");
  }

  /**
   * Tests getting the cached lesson URLs.
   * 
   * @throws Exception
   */
  private void getLessonUrlTest() throws Exception {
    String courseId = "workshop";
    SimpleResponse response = bookService.getLessonURLs(courseId);
    assertNotNull(response);
    assertTrue(response.getEntities() != null && response.getEntities().size() > 0);
    logger.info("Funnd " + response.getEntities().size() + " URLs");
  }

  /**
   * Test getting a cached book lesson.
   * 
   * @throws Exception
   */
  private void getBookLessonTest() throws Exception {
    String bookTitle = "workshop";
    String lessonTitle = "bloggers-1.html";
    SimpleResponse response = bookService.getBookLesson(bookTitle, lessonTitle);
    assertFalse(response == null);
    assertTrue(response.getEntities() != null && response.getEntities().size() == 1);
    NameValuePair pair = response.getEntities().get(0);
    assertEquals(pair.getName(), lessonTitle);
    assertTrue(pair.getValue() != null && pair.getValue().length() > 0);
    logger.info("Funnd one lesson from workshop");
  }

  /**
   * Tests refreshing the book cache.
   * 
   * @throws Exception
   */
  private void refreshBookCacheTest() throws Exception {
    SimpleResponse response = bookService.refreshBookCache();
    CommonError info = response.getInfo();
    assertFalse(info.isSevereError());
    logger.info(info.getDescription());
  }
}
