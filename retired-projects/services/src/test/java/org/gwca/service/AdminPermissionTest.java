package org.gwca.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.gwca.error.CommonError;
import org.gwca.model.message.ClassResponse;
import org.gwca.model.message.WorkResponse;
import org.gwca.model.persistence.Homework;
import org.gwca.model.persistence.Member;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/test-context.xml"})
public class AdminPermissionTest extends AbstractServiceTestCase {

  public AdminPermissionTest() {
    super();
  }

  /**
   * Initializes the caches before running any test case.
   */
  @Before
  public void initCaches() {
    cacheHandler.loadStorageDescriptionCache();
    cacheHandler.loadTableCache();
  }

  /**
   * Tests validating a member by its email.
   * 
   * @throws Exception
   */
  @Test
  public void testValidateMember() throws Exception {
    String email = "youpingh@gmail.com";
    ClassResponse<Member> response = adminService.validateMember(email);
    assertFalse(response == null);
    assertTrue(response.getClassId().equals("C12-2018"));
    assertTrue(response.getFirstEntity().getEmail().equals(email));
  }

  /**
   * Tests setting writer permission to a student folder
   * 
   * @throws Exception
   */
  @Test
  public void testSetStudentPermission() throws Exception {
    List<String> memberIds = new ArrayList<String>();
    memberIds.add("jane-doe");
    String classId = "C12-2018";

    WorkResponse<Homework> response = this.driveService.setUserPermission(classId, memberIds);
    assertTrue(response != null);
    List<CommonError> errors = response.getInfoList();
    assertTrue(errors.size() == 1);

    for (CommonError error : errors) {
      logger.info(error.getErrorCode() + ", " + error.getDescription());
    }
  }

  /**
   * Tests setting the writer permission to all the folders of the class members as a teacher.
   * 
   * @throws Exception
   */
  @Test
  public void testSetTeacherPermission() throws Exception {
    List<String> memberIds = new ArrayList<String>();
    memberIds.add("youping-hu");
    String classId = "C12-2018";

    WorkResponse<Homework> response = this.driveService.setUserPermission(classId, memberIds);
    assertTrue(response != null);
    List<CommonError> errors = response.getInfoList();
    assertTrue(errors.size() == 3);

    for (CommonError error : errors) {
      logger.info(error.getErrorCode() + ", " + error.getDescription());
    }
  }
}
