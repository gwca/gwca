package org.gwca.service;

import org.gwca.googleapp.stub.CacheHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractServiceTestCase {

  protected Logger logger = LoggerFactory.getLogger(this.getClass().getName());

  @Autowired
  protected DriveService driveService;

  @Autowired
  protected SheetService sheetService;

  @Autowired
  protected AdminService adminService;

  @Autowired
  protected BookService bookService;

  @Autowired
  protected EmailService emailService;

  @Autowired
  protected CacheHandler cacheHandler;
}
