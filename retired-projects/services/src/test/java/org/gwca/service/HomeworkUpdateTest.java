package org.gwca.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.gwca.error.CommonError;
import org.gwca.model.message.NameValuePair;
import org.gwca.model.message.SimpleResponse;
import org.gwca.model.message.UploadFileRequest;
import org.gwca.model.message.WorkResponse;
import org.gwca.model.persistence.Homework;
import org.gwca.utils.CommonUtil;
import org.gwca.utils.DataUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/test-context.xml"})
public class HomeworkUpdateTest extends AbstractServiceTestCase {

  private static DataUtils dataUtil = new DataUtils();
  private static final String testHomeworkFile = "jane-update-test.json";

  // for creating and then deleting an image and an audio clip
  private static String imageFileLink;
  private static String audioFileLink;

  public HomeworkUpdateTest() {
    super();
  }

  /**
   * Initializes the caches before running any test case.
   */
  @Before
  public void initCaches() {
    cacheHandler.loadStorageDescriptionCache();
    cacheHandler.loadTableCache();
    cacheHandler.loadHomeworkCache(cacheHandler.getClasses());
  }

  /**
   * Tests the following updates:<br>
   * 1. Saves (creates or updates) a homework.<br>
   * 2. Saves (creates or updates) a list of homework.<br>
   * 3. Uploads an audio clip or an image file. <br>
   * 4. Deletes a homework by ID (workNumber, memberId, courseId).<br>
   * 5. Deletes a Drive file by fileLink.<br>
   * 
   * @throws Exception
   */
  @Test
  public void testUpdates() throws Exception {
    createHomework();
    updateHomework();
    updateHomeworkList();
    createMediaFiles();
    deleteMediaFiles();
  }

  /**
   * Tests creating a new homework
   */
  private void createHomework() throws Exception {
    Homework homework = createHomework("Assigned");
    WorkResponse<Homework> response = driveService.saveHomework(homework);
    assertNotNull(response);
    assertTrue(response.getInfo().getErrorCode().equals("OK"));
    Homework returnedHomework = (Homework) response.getFirstEntity();
    assertTrue(returnedHomework != null && returnedHomework.getStatus().equals("Assigned"));
    logger.info("The homework is created.");
  }

  /**
   * Tests updating an existing homework
   */
  private void updateHomework() throws Exception {
    Homework homework = createHomework("Submitted");
    WorkResponse<Homework> response = driveService.saveHomework(homework);
    assertNotNull(response);
    assertTrue(response.getInfo().getErrorCode().equals("OK"));
    Homework returnedHomework = (Homework) response.getFirstEntity();
    assertTrue(returnedHomework != null && returnedHomework.getStatus().equals("Submitted"));
    logger.info("The homework is updated.");
  }

  /**
   * Tests updating homework list
   */
  private void updateHomeworkList() throws Exception {
    List<Homework> homeworkList = new ArrayList<Homework>();
    Homework homework = createHomework("Submitted");
    homeworkList.add(homework);

    homework = createHomework("Assigned");
    homework.setMemberId("john-doe");
    homeworkList.add(homework);

    WorkResponse<Homework> response = driveService.saveHomeworkList(homeworkList);
    assertNotNull(response);
    assertTrue(response.getInfo().getErrorCode().equals("OK"));
    List<CommonError> errors = response.getInfoList();
    assertTrue(errors != null && errors.size() == 2);
    logger.info("The homework list is updated.");
  }

  /**
   * Tests creating an image file and an audio clip
   */
  private void createMediaFiles() throws Exception {

    String memberId = "john-doe";
    String courseId = "workshop";
    String workNumber = "60_1";

    // upload an image
    String filetype = "image";
    String filename = "01-boeing-787.jpg";
    MultipartFile file = createMultipartFile(filename);
    UploadFileRequest request =
        createUploadRequest(filename, filetype, memberId, courseId, workNumber);

    SimpleResponse response = driveService.uploadFile(file, request);
    assertNotNull(response);
    assertTrue(response.getInfo().getErrorCode().equals("OK"));
    assertTrue(response.getEntities().size() > 0);
    NameValuePair pair = response.getEntities().get(0);
    imageFileLink = pair.getValue();
    logger.info("Uploaded an image:" + pair.getName() + ":" + pair.getValue());

    // upload an audio clip
    filetype = "audio";
    filename = "01-PeterAndTheWolf.mp3";
    file = createMultipartFile(filename);
    request = createUploadRequest(filename, filetype, memberId, courseId, workNumber);

    response = driveService.uploadFile(file, request);
    assertNotNull(response);
    assertTrue(response.getInfo().getErrorCode().equals("OK"));
    assertTrue(response.getEntities().size() > 0);
    pair = response.getEntities().get(0);
    audioFileLink = pair.getValue();
    logger.info("Uploaded an audio clip:" + pair.getName() + ":" + pair.getValue());
  }

  /**
   * Tests deleting an image file and an audio clip
   */
  private void deleteMediaFiles() throws Exception {

    // delete the image file just created
    SimpleResponse response = driveService.deleteFile(imageFileLink);
    assertNotNull(response);
    assertTrue(response.getInfo().getErrorCode().equals("OK"));
    logger.info("Deleted an image:" + imageFileLink);

    // delete the audio clip just created
    response = driveService.deleteFile(audioFileLink);
    assertNotNull(response);
    assertTrue(response.getInfo().getErrorCode().equals("OK"));
    logger.info("Deleted an audio click:" + audioFileLink);
  }

  /**
   * Creates a file uploading information request
   * 
   * @param filename
   * @return
   * @throws IOException
   */
  private MultipartFile createMultipartFile(String filename) throws IOException {
    InputStream inputStream = CommonUtil.getStreamReader(filename);
    MultipartFile file = new MockMultipartFile(filename, inputStream);
    return file;
  }

  /**
   * Creates a file uploading request
   * 
   * @param fileInfo
   * @return
   */
  private UploadFileRequest createUploadRequest(String filename, String filetype, String memberId,
      String courseId, String workNumber) {
    UploadFileRequest request =
        new UploadFileRequest(filename, filetype, memberId, courseId, workNumber);
    return request;
  }

  /**
   * Creates a testing homework.
   * 
   * @param status
   * @return
   * @throws Exception
   */
  private Homework createHomework(String status) throws Exception {
    Homework homework = null;
    Calendar checkDate = Calendar.getInstance();
    Calendar assignDate = Calendar.getInstance();
    assignDate.set(Calendar.DAY_OF_MONTH, assignDate.get(Calendar.DAY_OF_MONTH - 5));
    String content = CommonUtil.readResource(testHomeworkFile);
    homework = dataUtil.toJavaObj(content, Homework.class);
    homework.setAssignTime(assignDate.getTime());
    homework.setCheckTime(checkDate.getTime());
    homework.setStatus(status);
    return homework;
  }
}
