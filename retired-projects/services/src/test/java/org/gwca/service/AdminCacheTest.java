package org.gwca.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.gwca.error.CommonError;
import org.gwca.model.message.NameValuePair;
import org.gwca.model.message.SimpleResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/test-context.xml"})
public class AdminCacheTest extends AbstractServiceTestCase {

  public AdminCacheTest() {
    super();
  }

  /**
   * Initializes the caches before running any test case.
   */
  @Before
  public void initCaches() {
    cacheHandler.loadStorageDescriptionCache();
    cacheHandler.loadTableCache();

    ReflectionTestUtils.setField(this.adminService, "dataUrlPrefix",
        "https://docs.google.com/spreadsheets/d/");
    ReflectionTestUtils.setField(this.adminService, "dataUrlSuffix", "/edit");
  }

  /**
   * Tests getting the cached URL(s) of the information spreadsheets
   * 
   * @throws Exception
   */
  @Test
  public void cachedURLsTest() throws Exception {
    SimpleResponse response = adminService.getCachedURLs();
    assertFalse(response == null);
    assertTrue(response.getEntities() != null && response.getEntities().size() == 1);
    NameValuePair pair = response.getEntities().get(0);
    assertEquals(pair.getName(), "RegistrationSheet");
    assertEquals(pair.getValue(),
        "https://docs.google.com/spreadsheets/d/1f7VZyHMgmfafmXHNsaEpvI1DS8FUFBevepILZkKyMEo/edit");
    logger.info("Retrieved the cached URL(s)");
  }

  /**
   * Tests refreshing the application cache.
   * 
   * @throws Exception
   */
  @Test
  public void refreshCacheTest() throws Exception {
    CommonError info = adminService.refreshCache();
    assertTrue(info.getDescription().startsWith("The homework and classes caches are refreshed"));
    logger.info("The cached is refreshed");
  }
}
