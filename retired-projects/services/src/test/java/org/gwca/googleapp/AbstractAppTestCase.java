package org.gwca.googleapp;

import org.gwca.googleapp.stub.CacheHandler;
import org.gwca.googleapp.stub.DriveRepository;
import org.gwca.googleapp.stub.MailHandler;
import org.gwca.googleapp.stub.SheetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractAppTestCase {

  protected Logger logger = LoggerFactory.getLogger(this.getClass().getName());

  @Autowired
  protected DriveRepository googleDrive;

  @Autowired
  protected SheetRepository googleSheet;

  @Autowired
  protected MailHandler googleMail;

  @Autowired
  protected CacheHandler cacheHandler;

  //
  // The injections
  //
  public void setGoogleDrive(DriveRepository googleDrive) {
    this.googleDrive = googleDrive;
  }

  public void setGoogleSheet(SheetRepository googleSheet) {
    this.googleSheet = googleSheet;
  }

  public void setGoogleMail(MailHandler googleMail) {
    this.googleMail = googleMail;
  }

  public void setCacheHandler(CacheHandler cacheHandler) {
    this.cacheHandler = cacheHandler;
  }
}
