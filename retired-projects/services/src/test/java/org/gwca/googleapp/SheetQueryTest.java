package org.gwca.googleapp;

import java.util.List;

import org.gwca.model.persistence.Clazz;
import org.gwca.model.persistence.Member;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(locations = {"/test-context.xml"})
public class SheetQueryTest extends AbstractAppTestCase {

  /** Initializes the caches for all the test cases */
  @Before
  public void initCaches() {
    cacheHandler.loadStorageDescriptionCache();
    cacheHandler.loadTableCache();
  }

  @Test
  public void testFinders() throws Exception {
    findActiveClasses();
    findActiveMembers();
  }

  /**
   * Finds a list of the active classes
   * @throws Exception
   */
  private void findActiveClasses() throws Exception {
    List<Clazz> classes = googleSheet.getClasses();
    for (Clazz clazz : classes) {
      logger.info("Class:" + clazz.getClassName() + " Course:" + clazz.getCourse().getName());
      logger.info(clazz.getClassName() + ", " + " number of members:" + clazz.getMembers().size());
      for (Member member : clazz.getMembers()) {
        logger.info(member.getChineseName() + ", " + " role:" + member.getRole());
      }
    }
  }

  /**
   * Finds a list of the active members
   * @throws Exception
   */
  private void findActiveMembers() throws Exception {
    List<Member> members = googleSheet.getActiveMembers();
    logger.info("Active members");
    for (Member member : members) {
      logger.info(member.getChineseName() + ", " + " role:" + member.getRole());
    }
  }
}
