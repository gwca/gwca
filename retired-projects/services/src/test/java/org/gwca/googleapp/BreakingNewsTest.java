package org.gwca.googleapp;

import java.util.List;

import org.gwca.model.persistence.BreakingNews;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(locations = {"/test-context.xml"})
public class BreakingNewsTest extends AbstractAppTestCase {

  /** Initializes the caches for all the test cases */
  @Before
  public void initCaches() {
    cacheHandler.loadStorageDescriptionCache();
  }

  @Test
  public void testFinders() throws Exception {
    getBreakingNews();
  }

  private void getBreakingNews() throws Exception {
    List<BreakingNews> newsList = googleDrive.getBreakingNews();
    for (BreakingNews news : newsList) {
      logger.info("Title:" + news.getTitle() + " start at:" + news.getStartAt() + " expore at:"
          + news.getExpireAt());
    }
    logger.info("Number of breaking news:" + newsList.size() + "\n");
  }
}
