package org.gwca.googleapp;

import java.io.FileWriter;
import java.util.List;

import org.gwca.googleapp.impl.GoogleAppException;
import org.gwca.model.book.BookIndex;
import org.gwca.model.book.Chapter;
import org.gwca.model.book.Lesson;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(locations = {"/test-context.xml"})
public class AudioListTest extends AbstractAppTestCase {

  List<File> audioFiles = null;

  /** Initializes the caches for all the test cases */
  @Before
  public void initCaches() {
    cacheHandler.loadStorageDescriptionCache();
    cacheHandler.loadBookIndexCache();
  }

  @Test
  public void testFinders() throws Exception {
    audioFiles = getAudioFileList();
    List<BookIndex> bookIndex = cacheHandler.getBookIndex();

    // update the mediaUrl of each chapter with Drive audio file ID
    for (BookIndex idx : bookIndex) {
      List<Lesson> lessons = idx.getLessons();
      for (Lesson lesson : lessons) {
        List<Chapter> chapters = lesson.getChapters();
        for (Chapter chapter : chapters) {
          String mediaUrl = chapter.getMediaUrl();
          String mediaType = chapter.getMediaType();
          if (mediaType != null) {
            chapter.setMediaType(mediaType.toLowerCase());
          }
          if (mediaType != null && mediaType.equalsIgnoreCase("audio") && mediaUrl != null
              && mediaUrl.length() > 0) {
            // "mediaUrl": "npcr1/NPCR1-Workbook-02.mp3",
            String[] items = mediaUrl.split("/");
            if (items.length > 1) {
              String name = items[1];
              File audioFile = findAudioFile(name);
              if (audioFile != null) {
                chapter.setMediaUrl(audioFile.getId());
              }
            }
          }
        }
      }
    }

    // print the BookIndex Json
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    gson.toJson(bookIndex, new FileWriter("D:/tmp/bookIndex.json"));
    logger.info("converted to D:/tmp/bookIndex.json");
  }

  private List<File> getAudioFileList() throws GoogleAppException {
    FileList audioFileList = googleDrive.getAudioFileList();
    List<File> files = audioFileList.getItems();
    if (files != null && files.size() > 0) {
      File audioFile = files.get(0);
      logger.info("Retrieved " + audioFile.getTitle() + " id: " + audioFile.getId());
    }
    logger.info("Number of the audio files:" + files.size() + "\n");
    return files;
  }

  private File findAudioFile(String title) {
    for (File audioFile : audioFiles) {
      if (audioFile.getTitle().equals(title)) {
        return audioFile;
      }
    }
    return null;
  }
}
