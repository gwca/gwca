package org.gwca.googleapp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.gwca.model.persistence.Homework;
import org.gwca.utils.CommonUtil;
import org.gwca.utils.DataUtils;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(locations = {"/test-context.xml"})
public class FileUpdateTest extends AbstractAppTestCase {

  private static List<String> homeworkList = null;
  private static DataUtils dataUtil = new DataUtils();

  /** Initializes the caches for all the test cases */
  @Before
  public void initCaches() {
    cacheHandler.loadStorageDescriptionCache();
    cacheHandler.loadTableCache();
    cacheHandler.loadHomeworkCache(cacheHandler.getClasses());
    homeworkList = new ArrayList<String>();
    homeworkList.add("item-read.json");
    homeworkList.add("item-write.json");
    // homeworkList.add("item-image.json");
    // homeworkList.add("item-record.json");
    // homeworkList.add("item-video.json");
    // homeworkList.add("item-note.json");
    // homeworkList.add("item-audio.json");
    // homeworkList.add("item-answer-question.json");
    // homeworkList.add("item-connect-sentence.json");
    // homeworkList.add("item-fill-blank.json");
    // homeworkList.add("item-multi-choice.json");
    // homeworkList.add("item-phrase-sentence.json");
    // homeworkList.add("item-true-false.json");
  }

  @Test
  public void testUpdaters() throws Exception {
    saveStudentHomework("john-doe");
  }

  /**
   * Tests creating and then updating a homework for a student
   */
  private void saveStudentHomework(String studentId) throws Exception {

    for (String filename : homeworkList) {
      saveStudentHomework(studentId, filename);
      logger.info("Homework " + filename + " is saved for " + studentId);
    }
  }

  /**
   * Tests creating and then updating a homework for a student
   */
  private void saveStudentHomework(String studentId, String filename) throws Exception {

    Homework homework = null;
    Calendar checkDate = Calendar.getInstance();
    Calendar assignDate = Calendar.getInstance();
    assignDate.set(Calendar.DAY_OF_MONTH, assignDate.get(Calendar.DAY_OF_MONTH - 5));
    String content = CommonUtil.readResource(filename);
    homework = dataUtil.toJavaObj(content, Homework.class);
    homework.setAssignTime(assignDate.getTime());
    homework.setCheckTime(checkDate.getTime());
    homework.setMemberId(studentId);
    homework.setStatus("Assigned");
    googleDrive.saveHomework(homework);
  }
}
