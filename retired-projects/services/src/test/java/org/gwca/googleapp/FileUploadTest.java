package org.gwca.googleapp;

import java.io.InputStream;
import java.util.List;

import org.gwca.model.message.NameValuePair;
import org.gwca.model.message.UploadFileRequest;
import org.gwca.utils.CommonUtil;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(locations = {"/test-context.xml"})
public class FileUploadTest extends AbstractAppTestCase {

  private static final String audioClip = "01-PeterAndTheWolf.mp3";
  private static final String photoFile = "01-boeing-787.jpg";

  /** Initializes the caches for all the test cases */
  @Before
  public void initCaches() {
    cacheHandler.loadStorageDescriptionCache();
  }

  /**
   * Tests uploading a local image and an audio clip to Drive
   */
  @Test
  public void uploadFile() throws Exception {
    
    // upload an audio clip and then delete it
    InputStream inputStream = CommonUtil.getStreamReader(audioClip);
    UploadFileRequest request = new UploadFileRequest();
    request.setCourseId("npcr1");
    request.setMemberId("john-doe");
    request.setWorkNumber("audio-1");
    request.setFilename(audioClip);
    request.setFiletype("audio/mp3");

    List<NameValuePair> fileAttrs = googleDrive.uploadFile(inputStream, request);
    String fileLink = CommonUtil.getValueByName(fileAttrs, "fileLink");
    inputStream.close();
    googleDrive.deleteFile(fileLink);
    logger.info("Audio clip is uploaded and then deleted " + audioClip);

    // upload an image and then delete it
    inputStream = CommonUtil.getStreamReader(photoFile);
    request.setWorkNumber("photo-1");
    request.setFilename(photoFile);
    request.setFiletype("image/jpeg");

    fileAttrs = googleDrive.uploadFile(inputStream, request);
    fileLink = CommonUtil.getValueByName(fileAttrs, "fileLink");
    inputStream.close();
    googleDrive.deleteFile(fileLink);
    logger.info("Image file is uploaded and then deleted " + photoFile);
  }
}
