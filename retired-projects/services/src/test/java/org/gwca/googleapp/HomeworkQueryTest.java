package org.gwca.googleapp;

import java.util.Arrays;
import java.util.List;

import org.gwca.model.persistence.Homework;
import org.gwca.model.persistence.StorageDescription;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(locations = {"/test-context.xml"})
public class HomeworkQueryTest extends AbstractAppTestCase {

  private static String C12_IDS = " john-doe youping-hu";

  /** Initializes the caches for all the test cases */
  @Before
  public void initCaches() {
    cacheHandler.loadStorageDescriptionCache();
    cacheHandler.loadTableCache();
    cacheHandler.loadHomeworkCache(cacheHandler.getClasses());
  }
  
  @Test
  public void testFinders() throws Exception {
    getMemberFolderDescriptions();
    getMemberWorkList();
    getCourseWorkList();
    getSubmittedWorkList();
  }

  private void getMemberFolderDescriptions() throws Exception {
    logger.info("Refresh the member folder descriptions");
    List<StorageDescription> descriptions = googleDrive.getMemberFolderDescriptions();
    for (StorageDescription description : descriptions) {
      logger.info(description.getName() + ", " + description.getDescription());
    }
    logger.info("Number of descriptions:" + descriptions.size() + "\n");
  }

  private void getMemberWorkList() throws Exception {
    logger.info("Get the member work list for workshop_john-doe");
    List<Homework> homeworkList = googleDrive.getHomeworkList("workshop", "john-doe");
    for (Homework homework : homeworkList) {
      logger.info("workshop, John Doe, title:" + homework.getWorkTitle());
    }
    logger.info("Funnd " + homeworkList.size() + " homework files\n");
  }

  private void getCourseWorkList() throws Exception {
    logger.info("Get the work list for " + C12_IDS);
    List<String> members = Arrays.asList(C12_IDS.trim().split(" "));
    List<Homework> homeworkList = googleDrive.getHomeworkList("workshop", "read-items", members);
    for (Homework homework : homeworkList) {
      logger.info(
          "workshop, read-items, title:" + homework.getWorkTitle() + " member:" + homework.getMemberId());
    }
    logger.info("Funnd " + homeworkList.size() + " homework files\n");
  }

  private void getSubmittedWorkList() throws Exception {
    logger.info("Get the submitted workshop homework list for " + C12_IDS);
    List<String> members = Arrays.asList(C12_IDS.trim().split(" "));
    List<Homework> homeworkList = googleDrive.getHomeworkList("workshop", members);
    for (Homework homework : homeworkList) {
      logger.info(
          "workshop, read-items, title:" + homework.getWorkTitle() + " member:" + homework.getMemberId());
    }
    logger.info("Funnd " + homeworkList.size() + " submitted homework files");
  }
}
