package org.gwca.googleapp;

import java.util.Map;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(locations = {"/test-context.xml"})
public class BookQueryTest extends AbstractAppTestCase {

  /** Initializes the caches for all the test cases */
  @Before
  public void initCaches() {
    cacheHandler.loadStorageDescriptionCache();
    cacheHandler.loadBookIndexCache();
    cacheHandler.loadBookLessonsCache();
  }
  
  @Test
  public void testFinders() throws Exception {
    getBookLessons();
  }

  private void getBookLessons() {
    Map<String, String> lessons = cacheHandler.getBookLessons("workshop");
    logger.info("Number of lessons of the book workshop:" + lessons.size() + "\n");
  }
}
