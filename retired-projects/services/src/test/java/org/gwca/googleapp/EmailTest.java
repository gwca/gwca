package org.gwca.googleapp;

import java.util.ArrayList;
import java.util.List;

import org.gwca.model.message.NotificationRequest;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(locations = {"/test-context.xml"})
public class EmailTest extends AbstractAppTestCase {

  @Test
  public void sendNotifications() throws Exception {
    List<NotificationRequest> requestList = createRequestList();
    for (NotificationRequest request : requestList) {
      googleMail.sendNotification(request);
      logger.info("Sent a notification email to " + request.getReceiverName());
    }
    logger.info("All Done");
  }

  /**
   * Creates a list of teacher to students assignment notifications
   * 
   * @return
   */
  private List<NotificationRequest> createRequestList() {
    List<NotificationRequest> list = new ArrayList<NotificationRequest>();
    NotificationRequest request = new NotificationRequest();
    request.setSenderEmail("youping_hu@greatwallchineseacademy.org");
    request.setReceiverEmail("youpingh@gmail.com");
    request.setReceiverName("John Doe");
    request.setSubject("teacher to John Doe: assignment");
    request.setBody("<p>Hello John,</p> <p>There is a new assignment.</p>");
    list.add(request);

    request = new NotificationRequest();
    request.setSenderEmail("youping_hu@greatwallchineseacademy.org");
    request.setReceiverEmail("youpingh@yahoo.com");
    request.setReceiverName("Jane Doe");
    request.setSubject("teacher to Jane Doe: assignment");
    request.setBody("<p>Hello Jane,</p> <p>There is a new assignment.</p>");
    list.add(request);
    return list;
  }
}
