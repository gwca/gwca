package org.gwca.googleapp;

import java.util.Arrays;

import org.gwca.model.persistence.DriveFileId;
import org.gwca.model.workitem.Write;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(locations = {"/test-context.xml"})
public class EssaySlideTest extends AbstractAppTestCase {

  /** Initializes the caches for all the test cases */
  @Before
  public void initCaches() {
    cacheHandler.loadStorageDescriptionCache();
  }

  @Test
  public void testCreators() throws Exception {
    createEssayTest();
    createSlideTest();
  }

  private void createEssayTest() throws Exception {
    Write essay = createEssay("GoogleDoc");
    String memberId = "john-doe";
    DriveFileId driveFileId = googleDrive.createEssay(essay, memberId);
    logger.info("Created an essay:" + driveFileId.getFileLink());
  }

  private void createSlideTest() throws Exception {
    Write slide = createEssay("GoogleSlide");
    String memberId = "john-doe";
    DriveFileId driveFileId = googleDrive.createSlide(slide, memberId);
    logger.info("Created a slide:" + driveFileId.getFileLink());
  }

  /**
   * Creates a testing essay.
   * 
   * @return
   */
  private Write createEssay(String type) {
    String[] instructins = {"Do X", "Do Y"};
    Write essay = new Write();
    essay.setDocType(type);
    essay.setText(type + "CreationTest");
    essay.setInstructions(Arrays.asList(instructins));
    return essay;
  }

}
