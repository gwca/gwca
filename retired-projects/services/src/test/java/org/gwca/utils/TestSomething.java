package org.gwca.utils;

import java.util.Date;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestSomething {

  private Logger logger = LoggerFactory.getLogger(TestSomething.class.getSimpleName());

  @Test
  public void testDateFormat() {
    logger.info("Default date:" + CommonUtil.getDefaultDateString(new Date()));
    logger.info("Title date:" + CommonUtil.getTitleDateString(new Date()));
  }

  @Test
  public void testRandom() {
    for (int i = 0; i < 20; i++) {
      logger.info(i + ":" + (int) (Math.random() * 4 + 1));
    }
  }
}
