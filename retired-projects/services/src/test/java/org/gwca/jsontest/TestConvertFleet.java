package org.gwca.jsontest;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class TestConvertFleet {

  protected Logger logger = LoggerFactory.getLogger(TestConvertFleet.class.getSimpleName());

  private ObjectMapper mapper;

  @Before
  public void createMapper() {
    mapper = new ObjectMapper();
    // mapper.enableDefaultTyping();
    mapper.enable(SerializationFeature.INDENT_OUTPUT);
  }

  @Test
  public void bothWays() throws Exception {
    Fleet fleet = createVehicles();
    String jsonString = mapper.writeValueAsString(fleet);
    logger.info("Java to JSON:\n" + jsonString);

    fleet = mapper.readValue(jsonString, Fleet.class);
    logger.info("JSON to Java:" + fleet.getName() + ": " + fleet.getVehicles().get(0).toString()
        + ", " + fleet.getVehicles().get(1).toString());

    assertTrue(fleet.getVehicles().get(0) instanceof Car);
    assertTrue(fleet.getVehicles().get(1) instanceof Truck);
  }

  @Test
  public void jsonToJava() {
    String jsonString =
        "{\"name\" : \"A fleet\",\"vehicles\" : [ {\"type\" : \"car\",\"make\" : \"Mercedes-Benz\",\"model\" : \"S500\",\"seatingCapacity\" : 5,\"topSpeed\" : 250.0}, {\"type\" : \"truck\",\"make\" : \"Isuzu\",\"model\" : \"NQR\",\"payloadCapacity\" : 7500.0} ] }";
    try {
      Fleet fleet = mapper.readValue(jsonString, Fleet.class);
      logger.info("JSON to Java:" + fleet.getName() + ": " + fleet.getVehicles().get(0).toString()
          + ", " + fleet.getVehicles().get(1).toString());
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  private Fleet createVehicles() {
    Car car = new Car("Mercedes-Benz", "S500", 5, 250.0);
    Truck truck = new Truck("Isuzu", "NQR", 7500.0);

    List<Vehicle> vehicles = new ArrayList<>();
    vehicles.add(car);
    vehicles.add(truck);

    Fleet fleet = new Fleet();
    fleet.setName("A fleet");
    fleet.setVehicles(vehicles);
    return fleet;
  }
}
