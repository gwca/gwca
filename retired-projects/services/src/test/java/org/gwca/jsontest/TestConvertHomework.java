package org.gwca.jsontest;

import java.util.List;

import org.gwca.model.persistence.Homework;
import org.gwca.utils.CommonUtil;
import org.gwca.utils.DataUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TestConvertHomework {

  private static Logger logger = LoggerFactory.getLogger("JsonUtilTest");

  private static final String HOMEWORK_ONE = "homework-one.json";
  private static final String HOMEWORK_LIST = "test.json";
  private static final DataUtils dataUtil = new DataUtils();

  /**
   * Converts a homework.
   */
//  @Test
  public void convertHomework() {
    try {
      String jsonString = CommonUtil.readResource(HOMEWORK_ONE);
      Homework homework = dataUtil.toJavaObj(jsonString, Homework.class);

      jsonString = dataUtil.toJsonString(homework);
      logger.info("homework JSON:\n" + jsonString);

    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Converts a list of homework .
   */
  @Test
  public void convertHomeworkList() {
    try {
      String jsonString = CommonUtil.readResource(HOMEWORK_LIST);
      List<Homework> homeworkList = dataUtil.toHomeworkList(jsonString);
      for (Homework work : homeworkList) {
        logger.info("homework:" + work.getIdString());
      }
      logger
          .info("Converted homework list:" + (homeworkList == null ? "null" : homeworkList.size()));

      jsonString = dataUtil.toJsonString(homeworkList);
      logger.info("homework list JSON:\n" + jsonString);

    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
