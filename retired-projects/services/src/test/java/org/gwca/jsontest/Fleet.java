package org.gwca.jsontest;

import java.util.List;

public class Fleet {

  private String name;
  private List<Vehicle> vehicles;

  public Fleet() {}

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Vehicle> getVehicles() {
    return vehicles;
  }

  public void setVehicles(List<Vehicle> vehicles) {
    this.vehicles = vehicles;
  }

}
