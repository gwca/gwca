/**
 * The production environment specific variables.
 */
export const environment = {
  production: true,
  homeworkURL: 'https://gwca-course-work.appspot.com/',
  serviceURL: 'https://gwca-services.appspot.com/services/'
};
