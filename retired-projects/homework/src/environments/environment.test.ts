/**
 * The test environment specific variables.
 */
export const environment = {
  production: false,
  homeworkURL: 'https://20-dot-gwca-course-work.uc.r.appspot.com/',
  serviceURL: 'https://20-dot-gwca-services.uc.r.appspot.com/services/'
};
