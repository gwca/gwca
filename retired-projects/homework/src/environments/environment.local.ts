/**
 * The test environment specific variables.
 */
export const environment = {
  production: false,
  homeworkURL: 'http://localhost:4200/',
  serviceURL: 'http://localhost:8080/services/'
};
