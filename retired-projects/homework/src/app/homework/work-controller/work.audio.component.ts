import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-work-audio',
  templateUrl: '../work-view/audio.html',
  styleUrls: ['../../app.component.css', '../homework.component.css']
})
export class WorkAudioComponent extends WorkitemComponent {
}
