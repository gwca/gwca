import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-work-connect-sentence',
  templateUrl: '../work-view/connect-sentence.html',
  styleUrls: ['../../app.component.css', '../homework.component.css']
})
export class WorkConnectSentenceComponent extends WorkitemComponent {
}
