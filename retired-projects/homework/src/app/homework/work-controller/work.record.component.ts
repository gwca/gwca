import { Component, OnDestroy } from '@angular/core';
import { SafeUrl } from '@angular/platform-browser';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-work-record',
  templateUrl: '../work-view/record.html',
  styleUrls: ['../../app.component.css', '../homework.component.css']
})
export class WorkRecordComponent extends WorkitemComponent implements OnDestroy {

  // The voice recorder and player related properties
  private recorderInfo: string;
  private playingTime: string;
  private recordedTime: string;
  private recordUrl: SafeUrl;

  /** Creates a recorder in the service and subscribes its properties */
  ngOnInit() {
    // create a recorder only when it's needed
    if (this.needsRecorder()) {
      this.recordingService.createRecorder();
      this.recordingService.getRecordingFailed().subscribe((info) => {
        this.recorderInfo = info;
      });
      this.recordingService.getRecordedTime().subscribe((time) => {
        this.recordedTime = time;
      });
      this.recordingService.getRecordedBlob().subscribe((data) => {
        this.recordUrl = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(data.blob));
      });
      this.recordingService.getPlayingTime().subscribe((time) => {
        this.playingTime = time;
      });
    }
  }

  //
  // The methods to check the current recording mode
  //
  public needsRecorder() {
    return (this.isStudent() && this.isWork() && this.isRecord());
  }

  public hasMic(): boolean {
    return this.recordingService.hasMic();
  }

  public isOpeningMic(): boolean {
    return this.hasMic() && this.recordingService.isOpeningMic();
  }

  public isShowRecorder(): boolean {
    return this.hasMic() && !this.isOpeningMic();
  }

  public toRecord(): boolean {
    return this.recordUrl == null && this.recordingService.toRecord();
  }

  public toStop(): boolean {
    return this.recordingService.toStop();
  }

  public toPause(): boolean {
    return this.recordingService.toPause();
  }

  public toResume(): boolean {
    return this.recordingService.toResume();
  }

  public toClear(): boolean {
    return this.recordingService.toClear();
  }

  public toPlay(): boolean {
    return this.recordUrl != null && this.recordingService.toPlay();
  }

  //
  // The voice recorder wrapper methods
  //
  public startRecording() {
    if (this.toRecord()) {
      this.recordingService.startRecording();
    } else if (this.toResume()) {
      this.recordingService.resumeRecording();
    }
  }

  public pauseRecording() {
    this.recordingService.pauseRecording();
  }

  public stopRecording() {
    this.recordingService.stopRecording();
  }

  public saveRecording() {
    this.recordingService.saveRecording();
  }

  public cleanRecording() {
    this.recordUrl = null;
    this.recordingService.cleanRecording();
  }

  public abortRecording() {
    this.recordUrl = null;
    this.recordingService.abortRecording();
  }

  //
  // The audio player wrapper methods
  //
  public play() {
    this.recordingService.play();
  }

  public pause() {
    this.recordingService.pause();
  }

  public stop() {
    this.recordingService.stop();
  }

  public volumeUp() {
    this.recordingService.volumeUp();
  }

  public volumeDown() {
    this.recordingService.volumeDown();
  }

  //
  // Some utility methods
  //
  public getRecordingInfo(): string {
    if (this.recorderInfo) {
      return this.recorderInfo;
    } else {
      return "Opening microphone ...";
    }
  }

  public getRecordedTime(): string {
    return this.recordedTime;
  }

  public getPlayingTime(): string {
    return this.playingTime;
  }

  public getRecordURL(): SafeUrl {
    return this.recordUrl;
  }

  /** Releases the resources when it's done */
  ngOnDestroy(): void {
    if (this.needsRecorder()) {
      this.abortRecording();
    }
  }
}
