import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-work-multi-choice',
  templateUrl: '../work-view/multi-choice.html',
  styleUrls: ['../../app.component.css', '../homework.component.css']
})
export class WorkMultiChoiceComponent extends WorkitemComponent {
}
