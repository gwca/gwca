import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-work-answer-question',
  templateUrl: '../work-view/answer-question.html',
  styleUrls: ['../../app.component.css', '../homework.component.css']
})
export class WorkAnswerQuestionComponent extends WorkitemComponent {
}
