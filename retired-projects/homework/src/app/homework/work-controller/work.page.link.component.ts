import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-work-page-link',
  templateUrl: '../work-view/page-link.html',
  styleUrls: ['../../app.component.css', '../homework.component.css']
})
export class WorkPageLinkComponent extends WorkitemComponent {
}
