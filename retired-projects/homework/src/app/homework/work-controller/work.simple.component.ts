import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-work-simple',
  templateUrl: '../work-view/simple.html',
  styleUrls: ['../../app.component.css', '../homework.component.css']
})
export class WorkSimpleComponent extends WorkitemComponent {
}
