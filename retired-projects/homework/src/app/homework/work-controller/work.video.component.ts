import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-work-video',
  templateUrl: '../work-view/video.html',
  styleUrls: ['../../app.component.css', '../homework.component.css']
})
export class WorkVideoComponent extends WorkitemComponent {
}
