import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-work-fill-blank',
  templateUrl: '../work-view/fill-blank.html',
  styleUrls: ['../../app.component.css', '../homework.component.css']
})
export class WorkFillBlankComponent extends WorkitemComponent {
}
