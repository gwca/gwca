import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SocialLoginModule, GoogleLoginProvider, AuthServiceConfig, FacebookLoginProvider } from "angular-6-social-login";
// import { DataTableModule } from 'angular5-data-table';
import { QuillModule } from 'ngx-quill';

import { AppComponent } from './app.component';
import { WorkitemComponent } from './workitem/workitem.component';

import { HomeworkComponent } from './homework/homework.component';
import { WorkAnswerQuestionComponent } from './homework/work-controller/work.answer.question.component';
import { WorkAudioComponent } from './homework/work-controller/work.audio.component';
import { WorkConnectSentenceComponent } from './homework/work-controller/work.connect.sentence.component';
import { WorkFillBlankComponent } from './homework/work-controller/work.fill.blank.component';
import { WorkImageComponent } from './homework/work-controller/work.image.component';
import { WorkMultiChoiceComponent } from './homework/work-controller/work.multi.choice.component';
import { WorkPhraseSentenceComponent } from './homework/work-controller/work.phrase.sentence.component';
import { WorkRecordComponent } from './homework/work-controller/work.record.component';
import { WorkTrueFalseComponent } from './homework/work-controller/work.true.false.component';
import { WorkVideoComponent } from './homework/work-controller/work.video.component';
import { WorkSimpleComponent } from './homework/work-controller/work.simple.component';
import { WorkWriteComponent } from './homework/work-controller/work.write.component';
import { WorkPageLinkComponent } from './homework/work-controller/work.page.link.component';

import { EditorComponent } from './editor/editor.component';
import { EditorAudioComponent } from './editor/editor-controller/editor.audio.component';
import { EditorConnectSentenceComponent } from './editor/editor-controller/editor.connect.sentence.component';
import { EditorReadComponent } from './editor/editor-controller/editor.read.component';
import { EditorNoteComponent } from './editor/editor-controller/editor.note.component';
import { EditorVideoComponent } from './editor/editor-controller/editor.video.component';
import { EditorImageComponent } from './editor/editor-controller/editor.image.component';
import { EditorRecordComponent } from './editor/editor-controller/editor.record.component';
import { EditorTrueFalseComponent } from './editor/editor-controller/editor.true.false.component';
import { EditorAnswerQuestionComponent } from './editor/editor-controller/editor.answer.question.component';
import { EditorPhraseSentenceComponent } from './editor/editor-controller/editor.phrase.sentence.component';
import { EditorMultiChoiceComponent } from './editor/editor-controller/editor.multi.choice.component';
import { EditorFillBlankComponent } from './editor/editor-controller/editor.fill.blank.component';
import { EditorWriteComponent } from './editor/editor-controller/editor.write.component';
import { EditorPageLinkComponent } from './editor/editor-controller/editor.page.link.component';

import { ManagerComponent } from './manager/manager.component';
import { NgxScrollTopModule } from 'ngx-scrolltop';

// the next 2 itmes are used by Google Sign On
let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("418246311926-nq0japmia9c3u77bmdfc7to8heter4d8.apps.googleusercontent.com")
  },
  // {
  //   id: FacebookLoginProvider.PROVIDER_ID,
  //   provider: new FacebookLoginProvider("158136911725480")
  // }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    WorkitemComponent,
    ManagerComponent,

    HomeworkComponent,
    WorkAnswerQuestionComponent,
    WorkAudioComponent,
    WorkConnectSentenceComponent,
    WorkFillBlankComponent,
    WorkImageComponent,
    WorkMultiChoiceComponent,
    WorkPhraseSentenceComponent,
    WorkRecordComponent,
    WorkTrueFalseComponent,
    WorkVideoComponent,
    WorkSimpleComponent,
    WorkWriteComponent,
    WorkPageLinkComponent,

    EditorComponent,
    EditorAnswerQuestionComponent,
    EditorAudioComponent,
    EditorConnectSentenceComponent,
    EditorFillBlankComponent,
    EditorImageComponent,
    EditorMultiChoiceComponent,
    EditorNoteComponent,
    EditorPhraseSentenceComponent,
    EditorTrueFalseComponent,
    EditorReadComponent,
    EditorRecordComponent,
    EditorVideoComponent,
    EditorWriteComponent,
    EditorPageLinkComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    HttpClientModule,
    SocialLoginModule,
    QuillModule,
    NgxScrollTopModule
  ],
  providers: [{
    provide: AuthServiceConfig,
    useFactory: provideConfig
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
