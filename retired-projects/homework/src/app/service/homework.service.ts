import { Injectable } from '@angular/core';

import { WorkitemName } from '../model/AppEnums';
import { Homework, Workitem } from '../model/Homework';
import { SimpleItem, TrueFalse, MultiChoice, FillBlank } from '../model/Homework';
import { AnswerQuestion, PhraseSentence, ConnectSentence } from '../model/Homework';
import { Read, Note, Write, Video, Audio, Image, Record, PageLink } from '../model/Homework';

@Injectable({
  providedIn: 'root'
})
export class HomeworkService {

  workitemList: any[] = null;

  constructor() {
    this.workitemList = [
      new Read(), new Write(), new Note(), new Video(), new Audio(), new Image(),
      new Record(), new PageLink(), new TrueFalse(4), new MultiChoice(4, 4), new FillBlank(4, 4),
      new AnswerQuestion(4), new PhraseSentence(4)
    ];
  }

  /** Adds some meta attributes for editing after the homework is retrieved from the server */
  public addWorkitemMeta(homework: Homework): void {

    // add some meta attributes if don't exist and set the default values
    let existingItems = [];
    for (let workitem of homework.workitems) {
      workitem.display = true;
      workitem.removed = false;
      workitem.empty = false;
      existingItems.push(workitem.name);
    }

    // add the non-existing items into the item list as the removed ones
    for (let workitem of this.getWorkitemList()) {
      if (!existingItems.includes(workitem.name)) {
        let item = JSON.parse(JSON.stringify(workitem));
        item.display = false;
        item.removed = true;
        item.empty = true;
        homework.workitems.push(item);
      }
    }
  }

  /** Organizes the workitem list before saving it */
  public organizeWorkitems(homework: Homework): void {

    // clean up each workitem accordingly
    let itemList = [];
    for (let workitem of homework.workitems) {
      let cleanItem = this.cleanupWorkitem(workitem);
      if (this.isValidItem(cleanItem)) {
        itemList.push(cleanItem);
      }
    }

    // sort the list by item.index
    itemList.sort(function (item1, item2) {
      return item1.index - item2.index;
      // return item1.index.localeCompare(item2.index, undefined, { numeric: true, sensitivity: 'base' });
    });

    homework.workitems = itemList;
  }

  /**
   * Checks if the workitem is valid.
   * @param workitem 
   */
  private isValidItem(workitem: Workitem): boolean {
    return !(workitem.empty || workitem.removed);
  }

  /**
   * Removes all the empty attributes from a workitem. If the workitem does not contain
   * any valid content, sets it as an empty workitem.
   * @param workitem
   */
  public cleanupWorkitem(workitem: any) {
    let isEmptyItem: boolean = true;
    let itemName: WorkitemName = this.getWorkitemNameEnum(workitem.name);
    switch (+itemName) {
      case WorkitemName.TrueFalse:
        let tfItem = workitem as TrueFalse;
        let tfStatements = [];
        for (let statement of tfItem.statements) {
          if (statement && statement.question.length > 0) {
            tfStatements.push(statement);
            isEmptyItem = false;
          }
        }
        tfItem.statements = tfStatements;
        workitem = tfItem;
        break;
      case WorkitemName.MultiChoice:
        let mcItem = workitem as MultiChoice;
        let mcStatements = [];
        for (let statement of mcItem.statements) {
          if (statement && statement.question.length > 0) {
            mcStatements.push(statement);
            isEmptyItem = false;
          }
        }
        mcItem.statements = mcStatements;
        workitem = mcItem;
        break;
      case WorkitemName.FillBlank:
        let fbItem = workitem as FillBlank;
        let fbStatements = [];
        for (let statement of fbItem.statements) {
          let fbTokens = [];
          for (let token of statement.tokens) {
            if (token && token.phrase.length > 0) {
              fbTokens.push(token);
            }
          }
          statement.tokens = fbTokens;
          if (statement.tokens.length > 0) {
            fbStatements.push(statement);
            isEmptyItem = false;
          }
        }
        fbItem.statements = fbStatements;
        workitem = fbItem;
        break;
      case WorkitemName.AnswerQuestion:
        let aqItem = workitem as AnswerQuestion;
        let psQuestions = [];
        for (let question of aqItem.questions) {
          if (question && question.question.length > 0) {
            psQuestions.push(question);
            isEmptyItem = false;
          }
        }
        aqItem.questions = psQuestions;
        workitem = aqItem;
        break;
      case WorkitemName.PhraseSentence:
        let psItem = workitem as PhraseSentence;
        let psWords = [];
        for (let word of psItem.words) {
          if (word && word.word.length > 0) {
            psWords.push(word);
            isEmptyItem = false;
          }
        }
        psItem.words = psWords;
        workitem = psItem;
        break;
      case WorkitemName.ConnectSentence:
        let csItem = workitem as ConnectSentence;
        let csLines = [];
        for (let line of csItem.lines) {
          if (line && line.lead.length > 0 && line.tail.length > 0) {
            csLines.push(line);
            isEmptyItem = false;
          }
        }
        csItem.lines = csLines;
        workitem = csItem;
        break;
      case WorkitemName.Read:
      case WorkitemName.Note:
      case WorkitemName.Write:
      case WorkitemName.Image:
      case WorkitemName.Record:
      case WorkitemName.Video:
      case WorkitemName.Audio:
      case WorkitemName.PageLink:
        let simpleItem = workitem as SimpleItem;
        let instructions = [];
        for (let introduction of simpleItem.instructions) {
          if (introduction && introduction.length > 0) {
            instructions.push(introduction);
          }
        }
        isEmptyItem = instructions.length == 0;
        simpleItem.instructions = instructions;
        workitem = simpleItem;
        break;
      default:
    }
    workitem.empty = isEmptyItem;
    return workitem;
  }

  /** Gets a WorkitemNumbe enum by name */
  public getWorkitemNameEnum(name: string): WorkitemName {
    var enumMember: any;
    for (enumMember in WorkitemName) {
      if (name == WorkitemName[enumMember]) {
        return enumMember;
      }
    }
    return -1;
  }

  /** Creats a list of workitems with the default values */
  public getWorkitemList(): any[] {
    let clonedList = JSON.parse(JSON.stringify(this.workitemList));
    return clonedList;
  }

  /**
   * Return the list of available workitem names
   */
  public getWorkitemNames(): string[] {
    let names = Object.keys(WorkitemName).filter((item) => {
      return isNaN(Number(item));
    });
    return names;
  }

  /**
   * Creates a blank workitem by name
   * @param name
   */
  public createWorkitem(name: string): any {
    for (let workitem of this.workitemList) {
      if (workitem.name === name) {
        let newItem = JSON.parse(JSON.stringify(workitem));
        return newItem;
      }
    }
    return null;
  }
}
