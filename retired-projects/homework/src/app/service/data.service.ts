import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    private headers = null;

    constructor(private http: HttpClient) {
        this.headers = new HttpHeaders().set('Content-Type', 'application/json; charset=UTF-8');
    }

    /**
     * Calls to HTTP:GET to invoke a data service to retrieve some data from the database.
     * The response is a JSON object that is subscribed by the caller of this method.
     * 
     * @param service the restful service name.
     * @return
     */
    read(service: string): Observable<any> {
        let url = environment.serviceURL + service;
        return this.http.get(url, { headers: this.headers });
    }

    /**
     * Calls to HTTP:POST to invoke a data service to create a new entity in the database.
     * The response is a JSON object that is subscribed by the caller of this method.
     * 
     * @param service the restful service name.
     * @param model a JSON object for creating a database entity.
     * @return
     */
    create(service: string, model: any): Observable<any> {
        let url = environment.serviceURL + service;
        return this.http.post(url, model, { headers: this.headers });
    }

    /**
     * Calls to HTTP:PUT to invoke a data service to update an existing database entity.
     * The response is a JSON object that is subscribed by the caller of this method.
     * 
     * @param service the restful service name.
     * @param model a JSON object for creating a database entity.
     * @return
     */
    update(service: string, model: any): Observable<any> {
        let url = environment.serviceURL + service;
        return this.http.put(url, model, { headers: this.headers });
    }

    /**
     * Calls to HTTP:DELETE to invoke a data service to delete an existing database entity.
     * The response is a JSON object that is subscribed by the caller of this method.
     * 
     * @param service the restful service name.
     * @return
     */
    delete(service: string): Observable<any> {
        let url = environment.serviceURL + service;
        return this.http.delete(url, { headers: this.headers });
    }

    /**
     * Calls to HTTP:POST to invoke a data service to upload a file.
     * The response is a JSON object that is subscribed by the caller of this method.
     *
     * @param service the restful service name.
     * @param model a FormData object for uploading a media file (audio or image).
     * @return
     */
    upload(service: string, model: any): Observable<any> {
        // it's import to delete the Content-Type from the header to avoid using the default one.
        let url = environment.serviceURL + service;
        let headers = new HttpHeaders().set('Accept', 'application/json, */*');
        headers.delete('Content-Type');
        return this.http.post(url, model, { headers: headers });
    }
}
