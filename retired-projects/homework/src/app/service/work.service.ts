import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { AppProperties } from '../model/AppProperties';
import { WorkitemName } from '../model/AppEnums';
import { DataService } from './data.service';
import { UserService } from './user.service';
import { HomeworkService } from './homework.service';
import { Member, Clazz } from '../model/Clazz';
import { Homework, SimpleItem, Write } from '../model/Homework';
import { DataResponse, ErrorMessage, BookTitle, ClassReport, SimpleResponse } from '../model/AppMessage';
import { NotificationEmail, MailBuilder } from '../model/Notification';

@Injectable({
  providedIn: 'root'
})
export class WorkService {

  /** The greeting texts on the header */
  private greetingText: string;
  private classText: string;

  /** The current class member (set by the login process) */
  private member: Member;

  /** The current class/course/members relationship */
  private clazz: Clazz;

  /** The book titles of the course (pre-loaded for the logged in member) */
  private bookTitles: BookTitle[];

  /** The class/course/members relationship array (pre-loaded for the logged in member) */
  private classes: Clazz[];

  /** The current homework the member is working on */
  private homework: Homework;

  /** A copy of the current homework for checking if the homework has been changed */
  private origHomework: string;

  /** The list of homework to select */
  private homeworkList: Homework[];

  /** The list of work item names */
  private workitemNames: string[];

  /** The ClassReport array for creating a student status report or a process report. */
  private managerReport: ClassReport[];

  /** The response message populated by the data service */
  private dataResponse: DataResponse;

  /** To indicate if the retrieved data is ready to use */
  private dataReady: boolean;

  /** To indicate if thereis a file uploading is in progress */
  private uploading: boolean;

  constructor(private userService: UserService, private dataService: DataService,
    private homeworkService: HomeworkService) {
    this.initData();
  }

  /** Cleans up all the cached data while the user is logging out */
  public initData() {
    this.clazz = new Clazz();
    this.member = new Member();
    this.homework = new Homework();
    this.homeworkList = [];
    this.classes = [];
    this.bookTitles = [];
    this.managerReport == [];
    this.greetingText = 'GWCA Homework Online';
    this.classText = '';
    this.uploading = false;
    this.cloneHomework();
    this.setDefaultInfo();
    this.workitemNames = [];
    for (let idx in WorkitemName) {
      if (typeof WorkitemName[idx] === 'number')
        this.workitemNames.push(idx);
    }
    this.setDirty();
  }

  /** Gets the pre-loaded book titles for the current member */
  public getBookTitles(): BookTitle[] {
    return this.bookTitles;
  }

  /** Loads up the class/course/members relationships from the server */
  public loadClassData(): void {
    this.setDirty();
    let queryString = 'table/classes';
    this.dataService.read(queryString)
      .subscribe(response => {
        this.dataResponse = response;
        this.classes = this.dataResponse.entities;
        this.userService.cacheActiveClasses(this.classes);
        this.setGreetingInfo();
        console.log('The class/member/course relationship data is loaded');
        this.setReady();
      }, error => {
        console.log(error);
        this.userService.setRuntimeInfo(error);
        this.setReady();
      });
  }

  /** Loads up the some cached values from the server */
  public loadCachedValues(): void {
    this.setDirty();
    let queryString = 'admin/cache/values';
    this.dataService.read(queryString)
      .subscribe(response => {
        this.userService.cacheValues(response.entities)
        console.log('The cache values are loaded');
        this.setReady();
      }, error => {
        console.log(error);
        this.userService.setRuntimeInfo(error);
        this.setReady();
      });
  }

  /** Loads the homework list for the current member */
  public loadMemberHomeworkList() {
    let queryString = 'homework/memberWorkList?memberId=' + this.member.memberId
      + '&courseId=' + this.clazz.courseId;
    this.loadHomeworkList(queryString);
  }

  /** Loads all the submitted homework list for the teacher */
  public loadSubmittedHomeworkList() {
    let IDs = this.getStudentMemberIds();
    let queryString = 'homework/submittedWorkList?courseId=' + this.clazz.courseId
      + '&memberIds=' + IDs;
    this.loadHomeworkList(queryString);
  }

  /** Loads the book titles of the course the member is learning */
  loadBookTitles(courseId: string) {
    let queryString = 'book/lessonLinks/' + courseId;
    this.dataService.read(queryString)
      .subscribe(data => {
        let bookTitle: BookTitle = null;
        let response: SimpleResponse = new SimpleResponse(data);
        for (let entity of response.entities) {
          bookTitle = new BookTitle(entity.name, AppProperties.textbookSiteHome + entity.value);
          this.bookTitles.push(bookTitle);
        }
      }, error => {
        this.userService.setRuntimeErrorMessage('Finding book titles for ' + courseId + ' failed');
      });
  }

  /**
   * Loads the homework list for the specified student.
   * @param memberId 
   */
  public loadStudentHomeworkList(memberId: string): void {
    let queryString = 'homework/memberWorkList?memberId=' + memberId
      + '&courseId=' + this.clazz.courseId;
    this.loadHomeworkList(queryString);
  }

  /** Creates or updates the current homework */
  public saveHomework(): void {
    this.setWarningMessage('Saving ...');
    this.homeworkService.organizeWorkitems(this.homework);
    let queryString = 'homework/memberWork';
    this.dataService.update(queryString, this.homework)
      .subscribe(data => {
        this.dataResponse = data;
        this.updateHomeworkList(this.dataResponse.entities[0]);
        this.cloneHomework();
        this.setInfoMessage('');
        this.notifyStatus();
      }, error => {
        this.handleServerError(error);
      });
  }

  /** Deletes the current homework (this is for the editor only) */
  public deleteHomework(): void {
    this.setWarningMessage('Deleting ...');
    this.homeworkService.organizeWorkitems(this.homework);
    let queryString = 'homework/memberWork' + this.homework.memberId + '/'
      + this.homework.courseId + '/' + this.homework.workNumber;
    this.dataService.delete(queryString)
      .subscribe(data => {
        this.dataResponse = data;
        let info = this.dataResponse.info;
        if (info.isSevereError) {
          this.setWarningMessage(info.description);
        } else {
          this.removeHomeworkList();
          this.setInfoMessage(info.description);
        }
      }, error => {
        this.handleServerError(error);
      });
  }

  /** Creates or updates the homework list for the weekly assignment */
  public saveHomeworkList(homeworkList: Homework[]): void {
    let queryString = 'homework/homeworkList';
    this.saveOrUpdateHomeworkList(queryString, homeworkList);
  }

  /** Deletes the homework list for an un-assignment */
  public deleteHomeworkList(homeworkIdList: any[]): void {
    let queryString = 'homework/homeworkIdList';
    this.saveOrUpdateHomeworkList(queryString, homeworkIdList);
  }

  /**
   * Uploads the selected file using a FormData object. Some additional information
   * are added to the "file" attribute of the FormData to be used by the server to
   * create the file title.
   * @param event
   * @param workitem 
   */
  public uploadDriveFile(event: any, workitem: SimpleItem): void {
    this.uploading = true;
    let fileSelected: File = event.target.files[0];
    let formData = new FormData();
    formData.append('file', fileSelected);
    let queryString = 'drive/file?filename=' + fileSelected.name
      + '&filetype=' + fileSelected.type + '&memberId='
      + this.homework.memberId + '&courseId=' + this.homework.courseId + '&workNumber='
      + this.homework.workNumber;
    this.dataService.upload(queryString, formData)
      .subscribe(data => {
        let response: SimpleResponse = new SimpleResponse(data);
        workitem.url = response.getValueByName('fileLink');
        workitem.fileId = response.getValueByName('fileId');
        this.saveHomework();
        this.userService.setRuntimeInfoMessage(queryString + ' is uploaded');
        this.uploading = false;
      }, error => {
        this.userService.setRuntimeErrorMessage('Uploading ' + queryString + ' failed');
        this.uploading = false;
      });
  }

  /**
   * Deletes the specified drive file (image, clip, etc.).
   * @param workitem 
   */
  public deleteDriveFile(workitem: SimpleItem): void {
    let fileInfo = 'The media file of ' + workitem.title;
    let fileid = this.getDriveFileId(workitem.url);
    let queryString = 'drive/file/' + fileid;
    this.dataService.delete(queryString)
      .subscribe(data => {
        workitem.url = null;
        this.saveHomework();
        this.userService.setRuntimeInfoMessage(fileInfo + ' is deleted');
      }, error => {
        this.userService.setRuntimeErrorMessage('Deleting ' + fileInfo + ' failed');
      });
  }

  /** Retrieves a homework list by memberId and courseId to create a homework status report */
  public createStatusReport(): void {
    this.setDirty();
    this.managerReport = [];
    for (let member of this.clazz.members) {
      if (this.userService.isRoleStudent(member.role)) {
        let queryString = 'homework/memberWorkList?courseId=' + this.clazz.courseId +
          '&memberId=' + member.memberId;
        this.dataService.read(queryString)
          .subscribe(data => {
            this.dataResponse = data;
            this.createMemberStatusReport(member);
            this.setReady();
            this.setDefaultInfo();
          }, error => {
            this.handleServerError(error);
          });
      }
    }
  }

  /** Sends the notification email to the students, teacher and admin for an assignment. */
  public notifyAssignment(studentMailBody: string, adminMailBody: string, studentSubject: string,
    adminSubject: string, notifyParent: boolean) {
    this.setDirty();
    let requestList = [];
    for (let receiver of this.clazz.members) {
      let body = null;
      let request = null;
      let role = receiver.role;
      if (this.userService.isRoleStudent(role)) {
        body = studentMailBody.replace('$NAME$', receiver.firstName);
        request = new NotificationEmail(this.member, receiver, studentSubject, body, notifyParent);
        request.addReplyTo(this.member);
        requestList.push(request);
      } else if (this.userService.isRoleAdmin(role) || this.userService.isRoleTeacher(role)) {
        body = adminMailBody.replace('$NAME$', receiver.firstName);
        request = new NotificationEmail(this.member, receiver, adminSubject, body, false);
        request.addReplyTo(this.member);
        requestList.push(request);
      }
    }

    // send email to the members to notify them about the assignment
    let queryString = 'email/notifications';
    this.dataService.create(queryString, requestList)
      .subscribe(data => {
        this.createProcessReport(data.infoList);
        this.setReady();
      }, error => {
        let infoList = [];
        infoList.push(error);
        this.createProcessReport(infoList);
        this.setReady();
      });
  }

  /** Notifies the teacher/student when the homework status is changed */
  public notifyStatus(): void {
    let mailBuilder = new MailBuilder(environment.homeworkURL);
    let status = this.homework.status;
    let notifyMessage = '';
    let subject: string, body: string;
    let receiver: Member;
    let sender = this.member;
    if (this.isNotifyStudent(status)) {
      receiver = this.findStudent(this.homework.memberId);
      subject = mailBuilder.buildCheckSubject();
      body = mailBuilder.buildTeacherCheckBody(sender, receiver);
      notifyMessage = 'The student ' + receiver.firstName + ' '
        + receiver.lastName + ' is notifyed.';
    } else if (this.isNotifyTeacher(status)) {
      receiver = this.findTeacher();
      subject = mailBuilder.buildSubmitSubject(sender);
      body = mailBuilder.buildStudentSubmitBody(sender);
      notifyMessage = 'Thank you for submitting your homework';
    } else {
      return;
    }
    let request = new NotificationEmail(sender, receiver, subject, body, false);
    this.sendEmail(request, notifyMessage);
  }

  /**
   * Sends an email to the teacher.
   * @param subject
   * @param body 
   */
  public sendEmailToTeacher(subject: string, body: string): void {
    let teacher = this.findTeacher();
    let request = new NotificationEmail(this.member, teacher, subject, body, false);
    let notifyMessage = 'Sent an email to the teacher (' + teacher.firstName + ' '
      + teacher.lastName + ')';
    this.sendEmail(request, notifyMessage);
  }

  /** Gets the Google drive file id from the URL */
  public getDriveFileId(url: string): string {
    let idx = url.indexOf('&id=');
    if (idx < 0) {
      return url;
    } else {
      return url.substring(idx + 4);
    }
  }

  /** Sets the proper permission to the members Drive folder according to the member's role */
  public setUserPermission(memberList: string[]): void {
    let queryString = 'drive/permission/' + this.clazz.classId;
    this.dataService.create(queryString, memberList)
      .subscribe(data => {
        this.createProcessReport(data.infoList);
        this.setReady();
      }, error => {
        let infoList = [];
        infoList.push(error);
        this.createProcessReport(infoList);
        this.setReady();
      });
  }

  /** Finds the homework from the homework list and replaces it with the current one. */
  public updateHomeworkList(homework: Homework): void {
    this.homeworkService.addWorkitemMeta(homework);
    this.homework = homework;
    this.cloneHomework();
    for (let i = 0; i < this.homeworkList.length; i++) {
      let work = this.homeworkList[i];
      if (homework.workNumber === work.workNumber
        && homework.courseId === work.courseId
        && homework.memberId === work.memberId) {
        this.homeworkList[i] = homework;
        return;
      }
    }
  }

  /**
   * Removes the current homework from the homework list and sets the first homework
   * of the list as the current one.
   * @param homework 
   */
  public removeHomeworkList(): void {
    let idx = -1;
    for (let i = 0; i < this.homeworkList.length; i++) {
      let work = this.homeworkList[i];
      if (this.homework.workNumber === work.workNumber
        && this.homework.courseId === work.courseId
        && this.homework.memberId === work.memberId) {
        idx = i;
      }
    }

    if (idx >= 0) {
      this.homeworkList.splice(idx, 1);
      this.homework = this.homeworkList[0];
      this.cloneHomework();
    }
  }

  /**
   * Validates the member by email
   * @param userEmail
   */
  public validateMember(userEmail: string): void {
    let queryString = 'admin/validate/' + userEmail;
    this.dataService.read(queryString)
      .subscribe(data => {
        this.dataResponse = data;
        // TODO if this service is used, check the response to see if the
        // TODO member got validated
        let classId = this.dataResponse.classId;
        let member = this.dataResponse.entities[0];
        this.setReady();
      }, error => {
        // TODO error handling
        this.setReady();
      });
  }

  /**
   * Looks up the class member using the user's email. This makes sure that only
   * the registered member with a valid Google/Facebook email address is able to
   * use the application.
   */
  public findMember(userEmail: string): boolean {
    for (let clazz of this.classes) {
      for (let member of clazz.members) {
        if (member.email === userEmail) {
          this.assignClassMember(clazz, member);
          this.loadBookTitles(clazz.courseId);
          return true;
        }
      }
    }
    return false;
  }

  /** When a role is selected for a member, who has multiple roles, resets the role of it */
  public assignMemberRole(role: string): void {
    this.member.role = role;
    this.member.roles = [];
    this.assignClassMember(this.clazz, this.member);
  }

  /** Wrapers of the homeworkService */
  public addWorkitemMeta(homework: Homework): void {
    this.homeworkService.addWorkitemMeta(homework);
  }

  public createWorkitem(name: string): any {
    return this.homeworkService.createWorkitem(name);
  }

  public orgWorkitems(homework: Homework) {
    this.homeworkService.organizeWorkitems(homework);
  }

  /** Sets the current class in the multi-classes situation */
  public setClass(clazz: Clazz): void {
    this.clazz = clazz;
    this.setGreetingInfo();
    this.loadSubmittedHomeworkList();
  }

  /** Sets the greeting texts on the header */
  public setGreetingInfo() {
    if (this.member && this.member.memberId) {
      if (this.member.isReadonly) {
        this.greetingText = 'Hello ' + this.member.infoText;
      } else {
        this.greetingText = 'Hello ' + this.member.firstName;
      }
      if (this.clazz) {
        this.classText = ' (' + this.clazz.className + '/' + this.clazz.courseId + ')';
      }
    } else {
      this.greetingText = 'GWCA Homework Online';
      this.classText = '';
    }
  }

  /** Sets the current homework the member will be working on */
  public setHomework(homework: Homework) {
    this.setDirty();
    this.saveChangedHomework();
    this.homework = homework;
    this.cloneHomework();
    this.setDefaultInfo();
    this.setReady();
  }

  /** Checks if the current member (teacher) is teaching multiple classes */
  public isMultipleClasses(): boolean {
    return (this.classes && this.classes.length > 1);
  }

  /** If the current homework has been changed, confirms and saves it or rolls it back */
  public saveChangedHomework(): void {
    if (this.isHomeworkChanged()) {
      let msg = 'The current homework has been changed, do you want to save it?'
        + ' (click OK to save)';
      if (confirm(msg)) {
        this.saveHomework();
      } else {
        this.restoreHomework();
      }
    }
  }

  /** Changes the homework status and saves it. */
  public changeHomeworkStatus(status: string) {
    if (this.userService.isStatusDone(status)) {
      this.homework.checkTime = new Date();
    } else {
      this.homework.checkTime = null;
    }

    // if the teacher reset the status from Submitted to Assigned, notify the student
    if (this.userService.isStatusSubmitted(this.homework.status)
      && this.userService.isStatusAssigned(status)) {
      let mailBuilder = new MailBuilder(environment.homeworkURL);
      let student = this.findStudent(this.homework.memberId);
      let subject = mailBuilder.buildResetSubject();
      let body = mailBuilder.buildTeacherResetBody(this.member, student);
      let notifyMessage = 'The student ' + student.firstName + ' '
        + student.lastName + ' is notifyed that the homework status is reset.';
      let request = new NotificationEmail(this.member, student, subject, body, false);
      this.sendEmail(request, notifyMessage);
    }
    this.homework.status = status;
    this.saveHomework();
  }

  //
  // The private service utilities
  //
  /** Updates the homework list for saving or deleting a homework */
  private saveOrUpdateHomeworkList(queryString: string, valueList: any): void {
    this.setDirty();
    this.dataService.update(queryString, valueList)
      .subscribe(data => {
        this.createProcessReport(data.infoList);
        this.setReady();
      }, error => {
        let infoList = [];
        infoList.push(error);
        this.createProcessReport(infoList);
        this.setReady();
      });
  }

  /** Creates a report of the (un)assign/notify processes */
  private createProcessReport(infoList: ErrorMessage[]) {
    this.managerReport = [];
    for (let info of infoList) {
      let row = new ClassReport();
      row.info = info;
      this.managerReport.push(row);
    }
  }

  /** Creates a homework status report using the retrieved data of the member */
  private createMemberStatusReport(member: Member) {

    // sort the list by the assigned date
    this.dataResponse.entities = this.dataResponse.entities.sort(function (work1, work2) {
      if (work1.assignTime === work2.assignTime) {
        return 0;
      } else {
        return (work1.assignTime < work2.assignTime ? -1 : 1);
      }
    });

    // create a report
    for (let i = 0; i < this.dataResponse.entities.length; i++) {
      let row = new ClassReport();
      let work = this.dataResponse.entities[i];
      row.name = '';
      if (i == 0) {
        row.name = member.firstName + ' ' + member.lastName + ' (' + member.chineseName + ')';
      }
      row.title = work.workTitle;
      row.grade = work.grade;
      row.status = work.status;
      row.assignTime = this.getSimpleDate(work.assignTime);
      this.managerReport.push(row);
    }
  }

  /** Loads the homework list for the member to work on */
  private loadHomeworkList(queryString: string): void {
    if (this.confirmReload()) {
      this.setDirty();
      this.dataService.read(queryString)
        .subscribe(data => {
          this.dataResponse = data;
          this.createWorklist();
          this.setReady();
        }, error => {
          this.handleServerError(error);
          this.setReady();
        });
    }
  }

  /**
   * Sends an email to the receiver and shows a message after the email is sent.
   * @param request 
   * @param notifyMessage 
   */
  private sendEmail(request: NotificationEmail, notifyMessage: string): void {
    let queryString = 'email/notification';
    this.dataService.create(queryString, request)
      .subscribe(data => {
        alert(notifyMessage);
      }, error => {
        this.handleServerError(error);
      });
  }

  /**
   * Assigns the current class and member using the login info. If the logged-in 
   * user is a parent, assigns the current member to the related student and makes it
   * a readonly member for the parent to browse the homework of his child.
   * 
   * @param clazz
   * @param member 
   */
  private assignClassMember(clazz: Clazz, member: Member): void {
    this.setDirty();
    this.clazz = clazz;
    this.member = member;
    if (this.isParent()) {
      for (let classMember of clazz.members) {
        if (member.relatedMemberIds === classMember.memberId) {
          this.member = classMember;
          this.member.isReadonly = true;
          this.member.infoText = member.firstName + ' (as ' + classMember.firstName + ')';
        }
      }
    } else if (this.isAdmin()) {
      this.member.isReadonly = true;
      this.member.infoText = this.member.firstName;
    } else {
      this.member.isReadonly = false;
    }
    this.setGreetingInfo();
    console.log('Member:' + this.member.firstName + ' ' + this.member.lastName + ' - '
      + member.email);
    this.setReady();
  }

  /** Creates a homework list for the logged in member  */
  private createWorklist() {
    let info = this.dataResponse.info;
    this.userService.setRuntimeInfo(info);
    if (!info.isSevereError) {
      this.homeworkList = this.dataResponse.entities;
      for (let homework of this.homeworkList) {
        this.homeworkService.addWorkitemMeta(homework);
      }
      this.setWorklist();
      this.userService.setRuntimeInfo(null);
    } else {
      this.userService.setRuntimeInfo(info);
    }
  }

  /**
   * Sorts the homework list according to the role of the current member
   * and pupulates the first one as the current homework.
   */
  private setWorklist() {
    if (this.isTeacher()) {
      this.sortByWorkNumber(this.homeworkList);
    } else {
      this.sortByDate(this.homeworkList);
    }

    // populate the first homework
    if (this.homeworkList.length > 0) {
      this.homework = this.homeworkList[0];
    } else {
      this.homework = new Homework();
    }
    this.cloneHomework();
  }

  /** Sorts the homework list by the status (ascendingly) and assign date (decendingly) */
  private sortByDate(homeworkList: Homework[]): void {
    let instance = this;
    homeworkList.sort(function (work1, work2) {
      if (work1.status === work2.status) {
        if (work1.assignTime == work2.assignTime) {
          return 0;
        }
        return (work1.assignTime < work2.assignTime ? 1 : -1);
      }
      let status1 = instance.userService.getWorkStatusEnum(work1.status);
      let status2 = instance.userService.getWorkStatusEnum(work2.status);
      return (status1 < status2 ? -1 : 1);
    });
  }

  /** Sorts the homework list by the work number */
  private sortByWorkNumber(homeworkList: Homework[]): void {
    homeworkList.sort(function (work1, work2) {
      if (work1.workNumber === work2.workNumber) {
        return 0;
      }
      return (work1.workNumber > work2.workNumber ? 1 : -1);
    });
  }

  /**
   * Looks up the class member by id.
   */
  public lookupMember(memberId: string): Member {
    for (let clazz of this.classes) {
      for (let member of clazz.members) {
        if (member.memberId === memberId) {
          return member;
        }
      }
    }
    return null;
  }

  /** If the current homework has been changed, confirms continue to reload */
  private confirmReload(): boolean {
    if (this.isHomeworkChanged()) {
      let msg = 'The current homework has been changed, all the changes will be lost'
        + ' if you reload the homework list. Do you want to continue?';
      return confirm(msg);
    }
    return true;
  }

  /** Handles a server error (network or backend server severe error) */
  private handleServerError(error: ErrorMessage): void {
    this.dataResponse.info = error;
    this.dataResponse.entities = [];
    this.userService.setRuntimeInfo(error);
  }

  /** Clones the current homework for checking if it has been changed */
  private cloneHomework(): void {
    this.origHomework = JSON.stringify(this.homework).trim();
  }

  /** Checks if the current homework has been changed. */
  private isHomeworkChanged(): boolean {
    let workString = JSON.stringify(this.homework).trim();
    return workString != this.origHomework;
  }

  /** Finds the class teacher */
  private findTeacher(): Member {
    for (let member of this.clazz.members) {
      if (this.userService.isRoleTeacher(member.role)) {
        return member;
      }
    }
    return null;
  }

  /** Finds a student by ID */
  private findStudent(memberId: string): Member {
    for (let member of this.clazz.members) {
      if (member.memberId === memberId) {
        return member;
      }
    }
    return null;
  }

  /** Restores the changed homework to the original value */
  private restoreHomework(): void {
    this.homework = JSON.parse(this.origHomework);
    this.updateHomeworkList(this.homework);
  }

  /** Creates a comma delimited student IDs string */
  private getStudentMemberIds(): string {
    let ids: string = '';
    for (let member of this.clazz.members) {
      if (this.userService.isRoleStudent(member.role)) {
        ids += member.memberId + ',';
      }
    }
    let idx = ids.lastIndexOf(',');
    return (idx > 0 ? ids.substring(0, idx) : ids);
  }

  //
  // Some helper methods to manage the data readyness
  //
  public setReady(): void {
    this.dataReady = true;
  }

  public setDirty(): void {
    this.dataReady = false;
  }

  public isDataReady(): boolean {
    return this.dataReady;
  }

  public isUploading(): boolean {
    return this.uploading;
  }

  //
  // Some helper methods to manage the error message
  //
  public createDefaultInfo(): ErrorMessage {
    return this.userService.createDefaultInfo();
  }

  public setDefaultInfo() {
    let info = this.createDefaultInfo();
    this.setRuntimeInfo(info);
  }

  public setRuntimeInfo(info: ErrorMessage): void {
    this.userService.setRuntimeInfo(info);
  }

  public setInfoMessage(message: string): void {
    let info: ErrorMessage = this.createDefaultInfo();
    info.description = message;
    this.userService.setRuntimeInfo(info);
  }

  public setWarningMessage(message: string): void {
    let info: ErrorMessage = this.userService.createErrorMessage(message);
    this.userService.setRuntimeInfo(info);
  }

  public isRuntimeInfo(): boolean {
    let info = this.userService.getRuntimeInfo();
    return !info.isSevereError;
  }

  public isRuntimeError(): boolean {
    let info = this.userService.getRuntimeInfo();
    return info.isSevereError;
  }

  public getInfoMessage(): string {
    return this.userService.getRuntimeInfo().description;
  }

  public isHomeworkAvailable(): boolean {
    return this.homework && !this.userService.isStatusUnassigned(this.homework.status);
  }

  //
  // Some helper methods to check the role of the current member.
  //
  public isTeacher(): boolean {
    return (this.isValidMember() && this.userService.isRoleTeacher(this.member.role));
  }

  public isAdmin(): boolean {
    return (this.isValidMember() && this.userService.isRoleAdmin(this.member.role));
  }

  public isStudent(): boolean {
    return (this.isValidMember() && this.userService.isRoleStudent(this.member.role));
  }

  public isParent(): boolean {
    return (this.isValidMember() && this.userService.isRoleParent(this.member.role));
  }

  public isMultiRoleMember(): boolean {
    return (this.isValidMember() && this.member.roles.length > 1);
  }

  public isStudentMember(member: Member): boolean {
    return this.userService.isRoleStudent(member.role);
  }

  public isReadOnly(): boolean {
    return (this.isValidMember() && this.member.isReadonly);
  }

  public isValidMember(): boolean {
    return (this.member.memberId != null && this.member.memberId.length > 0);
  }

  public isValidHomework(): boolean {
    return (this.homework.workNumber != null && this.homework.workNumber.length > 0);
  }

  //
  // Some helper methods to check the work status
  //
  public isWork(): boolean {
    return this.homework && (this.userService.isStatusAssigned(this.homework.status)
      || this.userService.isStatusWIP(this.homework.status));
  }

  public isReview(): boolean {
    return this.homework && (this.userService.isStatusReview(this.homework.status)
      || this.isSubmitted() || this.isDone());
  }

  public isSubmitted(): boolean {
    return this.homework && (this.userService.isStatusSubmitted(this.homework.status));
  }

  public isDone(): boolean {
    return this.homework && (this.userService.isStatusDone(this.homework.status));
  }

  //
  // Some helper methods to check who to notify
  //
  /** Checks if it needs to notify the teacher about the homework status change */
  private isNotifyTeacher(status: string): boolean {
    return (this.userService.isStatusSubmitted(status) && this.isStudent());
  }

  /** Checks if it needs to notify the student about the homework status change */
  private isNotifyStudent(status: string): boolean {
    return (this.userService.isStatusDone(status) && this.isTeacher());
  }

  //
  // Getters of some of the properties of the service
  //
  public getManagerReport(): ClassReport[] {
    return this.managerReport;
  }

  public resetManagerReport(): void {
    this.managerReport = [];
  }

  public getClass(): Clazz {
    return this.clazz;
  }

  public getClassName(): string {
    return this.clazz.className;
  }

  public getClasses(): Clazz[] {
    return this.classes;
  }

  public getDefaultWorkitems(): any[] {
    return this.homeworkService.getWorkitemList();
  }

  public getWorkitems(): any[] {
    return this.homework.workitems;
  }

  public getWorkitemNames(): string[] {
    return this.workitemNames;
  }

  public getMember(): Member {
    return this.member;
  }

  public getMembers(): Member[] {
    return this.clazz.members;
  }

  public getWorkRemark(): string {
    return this.homework.remark;
  }

  public getWorkNumber(): string {
    return this.homework.workNumber;
  }

  public getWorkCourseId(): string {
    return this.homework.courseId;
  }

  public getWorkMemberId(): string {
    return this.homework.memberId;
  }

  public getHomework(): Homework {
    return this.homework;
  }

  public getOrigHomework(): string {
    return this.origHomework;
  }

  public getHomeworkList(): Homework[] {
    return this.homeworkList;
  }

  public getWorkTitle(): string {
    return this.homework.workTitle;
  }

  public getWorkGrade(): string {
    return this.homework.grade;
  }

  public getWorkStatusIcon(): string {
    return this.getStatusIcon(this.homework.status);
  }

  public getWorkCheckTime(): string {
    return this.getSimpleDate(this.homework.checkTime);
  }

  public getWorkAssignTime(): string {
    return this.getSimpleDate(this.homework.assignTime);
  }

  public getStatusIcon(status): string {
    return this.userService.getStatusIcon(status);
  }

  public getSimpleDate(date: Date): string {
    return this.userService.getSimpleDate(date);
  }

  public getGreetingText() {
    return this.greetingText;
  }

  public getClassText() {
    return this.classText;
  }
}
