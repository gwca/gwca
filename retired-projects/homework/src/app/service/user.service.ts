import { Injectable } from '@angular/core';

import { Workitem } from '../model/Homework';
import { Clazz, ClassCache } from '../model/Clazz';
import { AppProperties } from '../model/AppProperties';
import { MemberRole, WorkitemName, WorkStatus } from '../model/AppEnums';
import { ErrorMessage, NameValuePair } from '../model/AppMessage';

const APP_INFO = "AppInfoMessage";
const CLASSES_CACHE = "ClassesCache";

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor() {
    }

    //
    // Some utility methods to work with HTTP session
    //
    public addJsonToSession(name: string, item: any): void {
        if (item && item != "undefined" && item != "null") {
            localStorage.setItem(name, JSON.stringify(item));
        }
    }

    public getJsonFromSession(name: string): any {
        let item: string = localStorage.getItem(name);
        if (item && item != "undefined" && item != "null") {
            return JSON.parse(item);
        }
        return null;
    }

    /**
     * Caches the class/course/members relationships
     * @param classes
     */
    public cacheActiveClasses(classes: Clazz[]) {
        let classCache: ClassCache = new ClassCache();
        classCache.timestamp = new Date().getTime();
        classCache.classes = classes;
        this.addJsonToSession(CLASSES_CACHE, classCache);
    }

    /**
     * Gets the cached class/course/members relationships.
     */
    public getCachedClasses(): Clazz[] {
        const classCache: ClassCache = this.getJsonFromSession(CLASSES_CACHE);
        if (classCache == null) {
            return null;
        }
        const now: number = new Date().getTime();
        const cacheTime: number = classCache.timestamp;
        return ((now - cacheTime > AppProperties.refreshCacheInteval) ? null : classCache.classes);
    }

    /** Cache a list of string values */
    public cacheValues(values: NameValuePair[]): void {
        for (let item of values) {
            const name = item.name;
            const value = item.value;
            localStorage.setItem(name, value);
            // console.log("cached:" + name + ',' + value);
        }
    }

    /** Gets a cached string value by name */
    public getCachedValue(name: string): string {
        return localStorage.getItem(name);
    }

    /**
     * Checks if the value matches the member role.
     * @param value
     * @param role 
     */
    public isMemberRole(value: string, role: MemberRole) {
        const roleValue: string = MemberRole[role];
        return (value === roleValue);
    }

    /**
     * Checks if the role is an administrator
     * @param value
     */
    public isRoleAdmin(value: string) {
        return this.isMemberRole(value, MemberRole.ADMINISTRATOR);
    }

    /**
     * Checks if the role is a teacher
     * @param value
     */
    public isRoleTeacher(value: string) {
        return this.isMemberRole(value, MemberRole.TEACHER);
    }

    /**
    * Checks if the role is a student
    * @param value
    */
    public isRoleStudent(value: string) {
        return this.isMemberRole(value, MemberRole.STUDENT);
    }

    /**
    * Checks if the role is a parent
    * @param value
    */
    public isRoleParent(value: string) {
        return this.isMemberRole(value, MemberRole.PARENT);
    }

    /**
     * Checks if the name matches the work status.
     * @param value 
     * @param status 
     */
    public isWorkStatus(value: string, status: WorkStatus) {
        const workStatus: string = WorkStatus[status];
        return (value === workStatus);
    }

    public getWorkStatusEnum(value: string): WorkStatus {
        var enumMember: any;
        for (enumMember in WorkStatus) {
            if (value == WorkStatus[enumMember]) {
                return enumMember;
            }
        }
        return 100;
    }

    /**
    * Checks if the work status is assigned
    * @param value
    */
    public isStatusAssigned(value: string) {
        return this.isWorkStatus(value, WorkStatus.Assigned);
    }

    /**
    * Checks if the work status is unassigned
    * @param value
    */
    public isStatusUnassigned(value: string) {
        return this.isWorkStatus(value, WorkStatus.Unassigned);
    }

    /**
    * Checks if the work status is work in progress
    * @param value
    */
    public isStatusWIP(value: string) {
        return this.isWorkStatus(value, WorkStatus.WorkInProgress);
    }

    /**
    * Checks if the work status is review
    * @param value
    */
    public isStatusReview(value: string) {
        return this.isWorkStatus(value, WorkStatus.Review);
    }

    /**
    * Checks if the work status is submitted
    * @param value
    */
    public isStatusSubmitted(value: string) {
        return this.isWorkStatus(value, WorkStatus.Submitted);
    }

    /**
    * Checks if the work status is done
    * @param value
    */
    public isStatusDone(value: string) {
        return this.isWorkStatus(value, WorkStatus.Done);
    }

    /**
     * Checks if the name matches the workitem name.
     * @param value 
     * @param name 
     */
    public isWorkitem(item: Workitem, name: WorkitemName) {
        const itemName: string = WorkitemName[name];
        return (item.name === itemName);
    }

    /**
     * Checks if the workitem name is AnswerQuestion.
     * @param value 
     */
    public isItemAnswerQuestion(item: Workitem) {
        return this.isWorkitem(item, WorkitemName.AnswerQuestion);
    }

    /**
     * Checks if the workitem name is Audio.
     * @param value 
     */
    public isItemAudio(item: Workitem) {
        return this.isWorkitem(item, WorkitemName.Audio);
    }

    /**
     * Checks if the workitem name is FillBlank.
     * @param value 
     */
    public isItemFillBlank(item: Workitem) {
        return this.isWorkitem(item, WorkitemName.FillBlank);
    }

    /**
     * Checks if the workitem name is Image.
     * @param value 
     */
    public isItemImage(item: Workitem) {
        return this.isWorkitem(item, WorkitemName.Image);
    }

    /**
     * Checks if the workitem name is MultiChoice.
     * @param value 
     */
    public isItemMultiChoice(item: Workitem) {
        return this.isWorkitem(item, WorkitemName.MultiChoice);
    }

    /**
     * Checks if the workitem name is Note.
     * @param value 
     */
    public isItemNote(item: Workitem) {
        return this.isWorkitem(item, WorkitemName.Note);
    }

    /**
     * Checks if the workitem name is PhraseSentence.
     * @param value 
     */
    public isItemPhraseSentence(item: Workitem) {
        return this.isWorkitem(item, WorkitemName.PhraseSentence);
    }
    
    /**
     * Checks if the workitem name is Read.
     * @param value 
     */
    public isItemRead(item: Workitem) {
        return this.isWorkitem(item, WorkitemName.Read);
    }

    /**
     * Checks if the workitem name is PageLink.
     * @param value 
     */
    public isItemPageLink(item: Workitem) {
        return this.isWorkitem(item, WorkitemName.PageLink);
    }

    /**
     * Checks if the workitem name is Record.
     * @param value 
     */
    public isItemRecord(item: Workitem) {
        return this.isWorkitem(item, WorkitemName.Record);
    }

    /**
     * Checks if the workitem name is TrueFalse.
     * @param value 
     */
    public isItemTrueFalse(item: Workitem) {
        return this.isWorkitem(item, WorkitemName.TrueFalse);
    }

    /**
     * Checks if the workitem name is Video.
     * @param value 
     */
    public isItemVideo(item: Workitem) {
        return this.isWorkitem(item, WorkitemName.Video);
    }

    /**
     * Checks if the workitem name is Write.
     * @param value 
     */
    public isItemWrite(item: Workitem) {
        return this.isWorkitem(item, WorkitemName.Write);
    }

    /**
     * Checks if the workitem name is ConnectSentence.
     * @param value 
     */
    public isItemConnectSentence(item: Workitem) {
        return this.isWorkitem(item, WorkitemName.ConnectSentence);
    }

    /** Gets the status' icon image  */
    public getStatusIcon(status: string): string {
        if (this.isStatusAssigned(status)) {
            return 'assets/images/assign.jpg';
        } else if (this.isStatusWIP(status) || this.isStatusReview(status)) {
            return 'assets/images/wip.png';
        } else if (this.isStatusSubmitted(status)) {
            return 'assets/images/submit.jpg';
        } else if (this.isStatusDone(status)) {
            return 'assets/images/right.jpg';
        }
        return '';
    }

    /**
     * Saves a runtime information into the session
     * @param info the error message to save
    */
    public setRuntimeInfo(info: ErrorMessage): void {
        if (!(info && info != null)) {
            info = this.createDefaultInfo();
        }
        this.addJsonToSession(APP_INFO, info);
    }

    /**
     * Saves a runtime error message into the session
     * @param info the error message to save
    */
    public setRuntimeErrorMessage(message: string): void {
        const info = this.createErrorMessage(message);
        this.setRuntimeInfo(info);
    }

    /**
     * Saves a runtime info message into the session
     * @param info the error message to save
    */
    public setRuntimeInfoMessage(message: string): void {
        const info = this.createInfoMessage(message);
        this.setRuntimeInfo(info);
    }

    public resetRuntimeInfo(): void {
        let info = this.createDefaultInfo()
        this.addJsonToSession(APP_INFO, info);
    }

    /**
     * Gets the previousely saved runtime information or the default info if nothing got saved.
     */
    public getRuntimeInfo(): ErrorMessage {
        let info = this.getJsonFromSession(APP_INFO);
        if (info == null) {
            info = this.createDefaultInfo();
        }
        return info;
    }

    public createDefaultInfo(): ErrorMessage {
        let info = new ErrorMessage();
        info.errorCode = 'GenericInfo';
        info.severity = 'INFO';
        info.isSevereError = false;
        info.description = '';
        return info;
    }

    public createErrorMessage(message: string): ErrorMessage {
        const info: ErrorMessage = this.createDefaultInfo();
        info.errorCode = 'GenericError';
        info.severity = 'ERROR';
        info.isSevereError = true;
        info.description = message;
        return info;
    }

    public createInfoMessage(message: string): ErrorMessage {
        const info: ErrorMessage = this.createDefaultInfo();
        info.errorCode = 'OK';
        info.severity = 'INFO';
        info.isSevereError = false;
        info.description = message;
        return info;
    }

    public isRuntimeError(): boolean {
        const info = this.getRuntimeInfo();
        return info.isSevereError;
    }

    public isRuntimeInfo(): boolean {
        const info = this.getRuntimeInfo();
        return !info.isSevereError;
    }

    public getSimpleDate(date: Date): string {
        if (date) {
            const simpleDate = new Date(date);
            return (simpleDate.getMonth() + 1) + '/' + simpleDate.getDate() + '/' + simpleDate.getFullYear();
        }
        return '';
    }
}
