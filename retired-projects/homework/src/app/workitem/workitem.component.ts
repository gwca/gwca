import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

import { AppProperties } from '../model/AppProperties';
import { UserService } from '../service/user.service';
import { WorkService } from '../service/work.service';
import { RecordingService } from '../service/recording.service';
import {
  SimpleItem, TrueFalse, TFStatement, AnswerQuestion, Question, PhraseSentence,
  MultiChoice, MCStatement, FillBlank, FBStatement, FBToken, ConnectSentence,
  Word, CSLine, Token, Write
} from '../model/Homework';
import { DocType } from '../model/AppEnums';

/**
 * This component is the super class of all the components in editor/editor-controller
 * and all the components in homework/work-controller. It is for 
 * a teacher to edit a specified workitem of a piece of homework (add/update/delete)
 * a student to work on a specified workitem of a piece of homework
 */
@Component({
  template: '',
})
export class WorkitemComponent implements OnInit {

  /** The current workitem imported from the worker, editor or reviewer */
  @Input() workitem: any;

  /** The following two services are also used by some child components. */
  recordingService: RecordingService;
  sanitizer: DomSanitizer;

  constructor(private service: WorkService, private userService: UserService,
    sanitizer: DomSanitizer, recordingService: RecordingService) {
    this.recordingService = recordingService;
    this.sanitizer = sanitizer;
  }

  ngOnInit() {
  }

  /**
   * It's important to have this in order to make those dynamically created "textarea"
   * fields working properly. For some reason, a dynamically created "textarea" cannot
   * keep focus on the input area without this method. See read.html, write.html for
   * the use cases.
   * @param idx
   * @param obj 
   */
  public trackByIndex(idx: number, obj: any): any {
    return idx;
  }

  /** Gets a safe URL for a video clip */
  public getSafeUrl(url: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  /** Checks if the workitem has a valid URL */
  public hasUrl(workitem: SimpleItem) {
    return (workitem.url && workitem.url.length > 0);
  }

  public isTrue(answer: string) {
    return (answer && answer.toUpperCase() === 'TRUE');
  }

  public isFalse(answer: string) {
    return (answer && answer.toUpperCase() === 'FALSE');
  }

  public useAudioURL(workitem: SimpleItem): void {
    let url = workitem.url;
    if (url && url.length > 0 && url.startsWith(AppProperties.sharedLinkPrefix)) {
      let fileid = url.replace(AppProperties.sharedLinkPrefix, '');
      fileid = fileid.split('/')[0];
      workitem.url = AppProperties.audioPrefix + fileid;
      this.workitem.url = workitem.url;
    }
    //https://drive.google.com/file/d/1ddLI6HVn94DBjTt2KKPBJMLtpBC0_sw6/view?usp=sharing
  }

  public isAudioReady(workitem: SimpleItem): boolean {
    let url = workitem.url;
    if (url && url.length > 0) {
      url = workitem.url.toUpperCase();
      return url.endsWith('.MP3') || url.endsWith('.M4A') || url.startsWith(AppProperties.audioPrefix);
    }
    return false;
  }

  public isDisplay(): boolean {
    return this.workitem.display;
  }

  public isVideoReady(workitem: SimpleItem): boolean {
    const url = this.toYoutubeEmbedUrl(workitem.url);
    workitem.url = url;
    return (url && url.length > 0 && url.startsWith(AppProperties.youtubeEmbedUrl));
  }

  /** Converts the YouTube page or reference URL to YouTube embed URL */
  private toYoutubeEmbedUrl(url: string) {
    let youtubeId = null;
    if (url) {
      if (url.startsWith(AppProperties.youtubePageUrl)) {
        youtubeId = url.substring(AppProperties.youtubePageUrl.length);
      } else if (url.startsWith(AppProperties.youtubeRefUrl)) {
        youtubeId = url.substring(AppProperties.youtubeRefUrl.length);
      }
    }
    return (youtubeId != null ? AppProperties.youtubeEmbedUrl + youtubeId : url);
  }

  //
  // Some helper methods for checking the workitem type
  //
  public isSimpleItem(): boolean {
    return this.isDisplay() && (this.userService.isItemNote(this.workitem)
      || this.userService.isItemRead(this.workitem));
  }

  public isNote(): boolean {
    return this.userService.isItemNote(this.workitem) && this.isDisplay();
  }

  public isWrite(): boolean {
    return this.userService.isItemWrite(this.workitem) && this.isDisplay();
  }

  public isRecord(): boolean {
    return this.userService.isItemRecord(this.workitem) && this.isDisplay();
  }

  public isImage(): boolean {
    return this.userService.isItemImage(this.workitem) && this.isDisplay();
  }

  public isVideo(): boolean {
    return this.userService.isItemVideo(this.workitem) && this.isDisplay();
  }

  public isAudio(): boolean {
    return this.userService.isItemAudio(this.workitem) && this.isDisplay();
  }

  public isRead(): boolean {
    return this.userService.isItemRead(this.workitem) && this.isDisplay();
  }

  public isPageLink(): boolean {
    return this.userService.isItemPageLink(this.workitem) && this.isDisplay();
  }

  public isMultiChoice(): boolean {
    return this.userService.isItemMultiChoice(this.workitem) && this.isDisplay();
  }

  public isAnswerQuestion(): boolean {
    return this.userService.isItemAnswerQuestion(this.workitem) && this.isDisplay();
  }

  public isFillBlank(): boolean {
    return this.userService.isItemFillBlank(this.workitem) && this.isDisplay();
  }

  public isTrueFalse(): boolean {
    return this.userService.isItemTrueFalse(this.workitem) && this.isDisplay();
  }

  public isConnectSentence(): boolean {
    return this.userService.isItemConnectSentence(this.workitem) && this.isDisplay();
  }

  public isPhraseSentence(): boolean {
    return this.userService.isItemPhraseSentence(this.workitem) && this.isDisplay();
  }

  //
  // Wrappers of the service methods
  //
  public uploadDriveFile(event: any, workitem: SimpleItem): void {
    this.service.uploadDriveFile(event, workitem);
  }

  public deleteDriveFile(workitem: SimpleItem): void {
    this.service.deleteDriveFile(workitem);
  }

  public isStudent() {
    return this.service.isStudent();
  }

  public isTeacher() {
    return this.service.isTeacher();
  }

  public isParent() {
    return this.service.isParent();
  }

  public isAdmin() {
    return this.service.isAdmin();
  }

  public isWork() {
    return this.service.isWork();
  }

  public isReview() {
    return this.service.isReview();
  }

  public isSubmitted(): boolean {
    return this.service.isSubmitted();
  }

  public isDone(): boolean {
    return this.service.isDone();
  }

  public isReadOnly(): boolean {
    return this.service.isReadOnly();
  }

  //
  // Some utilities for the editor
  //
  /** Gets a list of textbook query parameters */
  public getCourseQueryParameters() {
    return this.service.getBookTitles();
  }

  public addInstruction(workitem: SimpleItem): void {
    if (!workitem.instructions || workitem.instructions == null) {
      workitem.instructions = [];
    }
    workitem.instructions.push('');
  }

  public addTFStatement(workitem: TrueFalse): void {
    if (!workitem.statements || workitem.statements == null) {
      workitem.statements = [];
    }
    workitem.statements.push(new TFStatement());
  }

  public setFileType(workitem: Write, type: string): void {
    workitem.docType = type;
  }

  public getSelectedType(workitem: Write) : string {
    if (workitem.docType == null || workitem.docType.length == 0) {
      workitem.docType = DocType[DocType.GoogleDoc];
    }
    return workitem.docType;
  }
  
  public addQuestion(workitem: AnswerQuestion): void {
    if (!workitem.questions || workitem.questions == null) {
      workitem.questions = [];
    }
    workitem.questions.push(new Question());
  }

  public addWord(workitem: PhraseSentence): void {
    if (!workitem.words || workitem.words == null) {
      workitem.words = [];
    }
    workitem.words.push(new Word());
  }

  public addLine(workitem: ConnectSentence): void {
    if (!workitem.lines || workitem.lines == null) {
      workitem.lines = [];
    }
    workitem.lines.push(new CSLine());
  }

  public addMCStatement(workitem: MultiChoice): void {
    if (!workitem.statements || workitem.statements == null) {
      workitem.statements = [];
    }
    const statement = new MCStatement(4);
    workitem.statements.push(statement);
  }

  public addChoice(statement: MCStatement): void {
    if (!statement.choices || statement.choices == null) {
      statement.choices = [];
    }
    statement.choices.push('');
  }

  public addFBStatement(workitem: FillBlank): void {
    if (!workitem.statements || workitem.statements == null) {
      workitem.statements = [];
    }
    const statement = new FBStatement(4);
    workitem.statements.push(statement);
  }

  public addToken(statement: FBStatement): void {
    if (!statement.tokens || statement.tokens == null) {
      statement.tokens = [];
    }
    statement.tokens.push(new FBToken);
  }

  public remove(): void {
    this.workitem.removed = true;
  }

  //
  // Some utilities for the worker
  //
  public isUploading(): boolean {
    return this.service.isUploading();
  }

  public getAnswer(answer: string): string {
    return (answer && answer.length > 0 ? answer : '_?_');
  }

  public setAnswer(token: Token): void {
    if (this.service.isSubmitted()) {
      token.checked = !token.checked;
    }
  }

  public isAnswered(answer: string): boolean {
    return (answer && answer.length > 0);
  }

  public getCheckImage(checked: boolean): string {
    return (checked ? 'assets/images/right.jpg' : 'assets/images/wrong.jpg');
  }

  public hasRemark(item): boolean {
    return item && item.remark && item.remark.length > 0;
  }

  public hasLink(workitem: SimpleItem): boolean {
    return workitem && workitem.url && workitem.url.length > 0;
  }
}
