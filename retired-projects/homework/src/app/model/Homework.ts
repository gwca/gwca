import { WorkitemName, WorkStatus, DocType } from '../model/AppEnums';

/** The homework ID object */
export class HomeworkID {
    courseId: string;
    workNumber: string;
    memberId: string;

    constructor() {
        this.courseId = '';
        this.workNumber = '';
        this.memberId = '';
    }
}

/** The homework object with a list of work items */
export class Homework {
    courseId: string;
    workNumber: string;
    memberId: string;
    workTitle: string;
    remark: string;
    grade: string;
    status: string;
    assignTime: Date;
    checkTime: Date;
    workitems: any[];

    constructor() {
        this.courseId = '';
        this.workNumber = '';
        this.memberId = '';
        this.workTitle = null;
        this.remark = null;
        this.grade = null;
        this.status = WorkStatus[WorkStatus.Unassigned];
        this.assignTime = null;
        this.checkTime = null;
        this.workitems = [];
    }
}

/** The super class for Workitem */
export class WorkitemMeta {

    display: boolean;
    removed: boolean;
    empty: boolean;

    constructor() {
        this.display = true;
        this.removed = false;
        this.empty = true;
    }

    isRemoved(): boolean {
        return this.removed;
    }

    isDisplay(): boolean {
        return this.display;
    }

    isEmpty(): boolean {
        return this.empty;
    }
}

/** The super class for all the workitems */
export class Workitem extends WorkitemMeta {
    name: string;
    title: string;
    index: number;
    remark: string;
    checked: boolean;
    description: string;

    constructor(name: string, description: string, title: string, index: number) {
        super();
        this.name = name;
        this.title = title;
        this.index = index;
        this.description = '作业：' + description;
    }
}

/** The super class for all the tokens */
export class Token {
    answer: any;
    checked: boolean;
    remark: string;
    constructor() {
        this.answer = '';
        this.checked = false;
        this.remark = '';
    }
}

/** The PhraseSentence workitem */
export class PhraseSentence extends Workitem {
    words: Word[];
    constructor(num: number) {
        super(WorkitemName[WorkitemName.PhraseSentence], '组词造句', '组词造句', 20);
        this.words = [];
        for (let i = 0; i < num; i++) {
            this.words.push(new Word());
        }
    }

    isEmpty(): boolean {
        return super.isEmpty() || this.isEmptyWords();
    }

    private isEmptyWords(): boolean {
        let isEmpty = true;
        for (let i = 0; i < this.words.length; i++) {
            if (this.words[i].word.length > 0) {
                isEmpty = false;
            }
        }
        return isEmpty;
    }
}

export class Word extends Token {
    word: string;
    constructor() {
        super();
        this.word = '';
    }
}

/** The TrueFalse workitem */
export class TrueFalse extends Workitem {
    statements: TFStatement[];
    constructor(num: number) {
        super(WorkitemName[WorkitemName.TrueFalse], '判断对错', '判断下列句子的对错', 30);
        this.statements = [];
        for (let i = 0; i < num; i++) {
            this.statements.push(new TFStatement());
        }
    }
}

export class TFStatement extends Token {
    question: string;
    constructor() {
        super();
        this.question = '';
    }
}

/** The MultiChoice workitem */
export class MultiChoice extends Workitem {
    statements: MCStatement[];
    constructor(nq: number, nc: number) {
        super(WorkitemName[WorkitemName.MultiChoice], '多项选择', '选择正确的答案', 40);
        this.statements = [];
        for (let i = 0; i < nq; i++) {
            const statement = new MCStatement(nc);
            this.statements.push(statement);
        }
    }
}

export class MCStatement extends Token {
    question: string;
    choices: string[];
    constructor(num: number) {
        super();
        this.question = '';
        this.choices = [];
        for (let j = 0; j < num; j++) {
            this.choices.push('');
        }
    }
}

/** The FillBlank workitem */
export class FillBlank extends Workitem {
    statements: FBStatement[];
    constructor(qn: number, bn: number) {
        super(WorkitemName[WorkitemName.FillBlank], '填空', '选择适当的词填空', 50);
        this.statements = [];
        for (let i = 0; i < qn; i++) {
            this.statements.push(new FBStatement(bn));
        }
    }
}

export class FBStatement {
    tokens: FBToken[];
    selections: string;
    constructor(num: number) {
        this.selections = '';
        this.tokens = [];
        for (let i = 0; i < num; i++) {
            this.tokens.push(new FBToken());
        }
    }
}

export class FBToken extends Token {
    hasBlank: boolean;
    phrase: string;
    constructor() {
        super();
        this.hasBlank = false;
        this.phrase = '';
    }
}

/** The AnswerQuestion workitem */
export class AnswerQuestion extends Workitem {
    questions: Question[];
    constructor(num: number) {
        super(WorkitemName[WorkitemName.AnswerQuestion], '回答问题', '回答下列问题', 60);
        this.questions = [];
        for (let i = 0; i < num; i++) {
            this.questions.push(new Question());
        }
    }
}

export class Question extends Token {
    question: string;
    constructor() {
        super();
        this.question = '';
    }
}

/** The ConnectSentence workitem */
export class ConnectSentence extends Workitem {
    lines: CSLine[];
    constructor(num: number) {
        super(WorkitemName[WorkitemName.ConnectSentence], '连接句子', '连接句子', 70);
        this.lines = [];
        for (let i = 0; i < num; i++) {
            this.lines.push(new CSLine());
        }
    }
}

export class CSLine extends Token {
    lead: string;
    tail: string;
    constructor() {
        super();
        this.lead = '';
        this.tail = '';
    }
}

/** The SimpleItem for read, note, audio, video, record, image, pageLink and write work items */
export class SimpleItem extends Workitem {
    instructions: string[];
    fileId: string;
    url: string;
    text: string;
    constructor(name: string, title: string, description: string, index: number) {
        super(name, title, description, index);
        this.fileId = '';
        this.url = '';
        this.text = '';
        this.instructions = [''];
    }
}

/** The Read workitem */
export class Read extends SimpleItem {
    constructor() {
        super(WorkitemName[WorkitemName.Read], '阅读理解', '阅读理解', 10);
    }
}

/** The Note workitem */
export class Note extends SimpleItem {
    constructor() {
        super(WorkitemName[WorkitemName.Note], '通知', '通知', 80);
    }
}

/** The Video workitem */
export class Audio extends SimpleItem {
    constructor() {
        super(WorkitemName[WorkitemName.Audio], '听力练习', '听力练习', 90);
    }
}

/** The Video workitem */
export class Video extends SimpleItem {
    constructor() {
        super(WorkitemName[WorkitemName.Video], '视听练习', '视听练习', 100);
    }
}

/** The Record workitem */
export class Record extends SimpleItem {
    constructor() {
        super(WorkitemName[WorkitemName.Record], '朗读录音', '朗读录音', 110);
    }
}

/** The Image workitem */
export class Image extends SimpleItem {
    constructor() {
        super(WorkitemName[WorkitemName.Image], '贴图', '贴图', 120);
    }
}

/** The PageLink workitem */
export class PageLink extends SimpleItem {
    constructor() {
        super(WorkitemName[WorkitemName.PageLink], '链接', '链接', 130);
    }
}

/** The Write workitem */
export class Write extends SimpleItem {
    docType: string;
    constructor() {
        super(WorkitemName[WorkitemName.Write], '写作文', '写作文', 140);
        this.docType = DocType[DocType.GoogleDoc];
    }
}
