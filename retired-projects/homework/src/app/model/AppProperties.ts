
/**
 * All the application properties
 */
export const AppProperties = {
    sharedLinkPrefix: 'https://drive.google.com/file/d/',
    audioPrefix: 'https://docs.google.com/uc?export=download&id=',
    imagePrefix: 'http://drive.google.com/uc?export=view&id=',
    youtubeEmbedUrl: 'https://www.youtube.com/embed/',
    youtubeRefUrl: 'https://youtu.be/',
    youtubePageUrl: 'https://www.youtube.com/watch?v=',
    googleMail: 'https://mail.google.com/',
    googleDrive: 'https://drive.google.com/drive/',
    schoolSite: 'https://greatwallchineseacademy.org/',
    schoolCalendar: 'https://www.greatwallchineseacademy.org/assets/pdf/school/SchoolCalendar20202021.pdf',
    schoolAdmin: 'https://www.greatwallchineseacademy.org/?id=Staff',
    textbookSiteHome: 'https://gwca-classroom.appspot.com/',
    textbookSite: 'https://gwca-classroom.appspot.com/?type=course&course=',
    projectSite: 'https://sites.google.com/greatwallchineseacademy.org/c12/classroom/project',
    classSite:'https://sites.google.com/greatwallchineseacademy.org/',
    refreshCache: true,
    refreshCacheInteval: 24 * 3600 * 1000, // one day
    validateUser: false,
    maxRows: 100
}
