import { Component, OnInit } from '@angular/core';
import { ElementRef, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { environment } from '../../environments/environment';
import { WorkService } from '../service/work.service';
import { UserService } from '../service/user.service';
import { Homework, HomeworkID } from '../model/Homework';
import { PagePanel } from '../model/AppEnums';
import { ClassReport } from '../model/AppMessage';
import { Member, Clazz } from '../model/Clazz';
import { MailBuilder } from '../model/Notification';
import { AppProperties } from '../model/AppProperties';

/**
 * This component is for a teacher to manage (assignment, notification, report, preview, etc.) 
 * the homework of the course the teacher is teaching. 
 */
@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css', '../app.component.css']
})
export class ManagerComponent implements OnInit {

  /** The current work panel and its title */
  panel: PagePanel;
  panelTitle: string;

  /** The read-only member (usually a parent) */
  reviewMember: Member;

  /** The member list for grantting permission to */
  memberList: string[];

  /** The indicator of copying the assignment notification to the parents */
  studentMailBody: string;
  adminMailBody: string;
  studentSubject: string;
  adminSubject: string
  ccParent: boolean;

  /** The work service for the html page to reference */
  service: WorkService;

  /** The HTML button elements */
  @ViewChild('assignButton', { static: false }) assignButton: ElementRef;
  @ViewChild('notifyButton', { static: false }) notifyButton: ElementRef;
  @ViewChild('permissionButton', { static: false }) permissionButton: ElementRef;

  constructor(private workService: WorkService, private userService: UserService,
    private sanitizer: DomSanitizer) {
    this.service = workService;
  }

  ngOnInit() {
    this.ccParent = false;
    this.studentMailBody = this.adminMailBody = this.studentSubject = this.adminSubject = '';
  }

  ngAfterViewInit() {
    this.list();
  }

  /** A service wrapper method */
  public getWorkitems(): any[] {
    return this.service.getWorkitems();
  }

  /** Assigns the selected homework to the class members. */
  public assign() {
    this.panel = PagePanel.Assign;
    this.panelTitle = 'Homework Assignment';
    this.service.setDirty();
  }

  /** Unassigns the selected homework from the class members. */
  public unassign() {
    this.panel = PagePanel.Unassign;
    this.panelTitle = 'Homework Unassignment';
    this.service.setDirty();
  }

  /** Notifies the class members about the new assignment. */
  public notify() {
    this.panel = PagePanel.Notify;
    this.panelTitle = 'Assignment Notification';
    let mailBuilder = new MailBuilder(environment.homeworkURL);
    let member = this.service.getMember();
    let clazz = this.service.getClass();
    this.studentMailBody = mailBuilder.buildTeacherAssignStudentBody(member);
    this.adminMailBody = mailBuilder.buildTeacherAssignAdminBody(member, clazz);
    this.studentSubject = this.adminSubject = mailBuilder.buildAssignSubject(clazz.className);
    this.service.setDirty();
  }

  /** Previews the selected homework */
  public preview() {
    this.panel = PagePanel.Preview;
    this.panelTitle = 'Homework Preview';
  }

  /** Grants the user permission (e.g. Editor, Viewer) */
  public grantPermission() {
    this.panel = PagePanel.Permission;
    this.panelTitle = 'Grant User Permission';
    this.service.resetManagerReport();
    this.memberList = [];
    this.service.setDirty();
  }

  /** Lists the members of the class */
  public list() {
    this.panel = PagePanel.List;
    this.panelTitle = 'Class Members of ' + this.service.getClass().className;
    this.service.resetManagerReport();
    this.createReviewMember();
    let members = this.sortMembers();

    for (let member of members) {
      let row = new ClassReport();
      row.memberId = member.memberId;
      row.name = member.firstName + ' ' + member.lastName + ' (' + member.chineseName + ')';
      row.role = member.role;
      row.email = member.email;
      this.service.getManagerReport().push(row);
    }
  }

  /** Creates a homework status report of all the students of the class */
  public report() {
    this.panel = PagePanel.Report;
    this.panelTitle = 'Homework Status Report of ' + this.service.getClass().className;
    this.service.setWarningMessage('Creating report ...');
    this.service.setDirty();
    this.service.createStatusReport();
  }

  /** Add a member to the list to grant the permission to */
  public addMember(member: Member): void {
    this.memberList.push(member.memberId);
  }

  /** Assigns the selected homework to all the students of the class. */
  public assignHomework() {
    this.service.setDirty();
    let title = this.service.getHomework().workTitle;
    let button = this.assignButton.nativeElement;
    button.disabled = true;
    button.innerHTML = 'Assigning homework ' + title + ' ...';

    let workList = [];
    let template = JSON.stringify(this.service.getHomework());
    for (let member of this.service.getClass().members) {
      if (this.userService.isRoleStudent(member.role)) {
        let assignedWork: Homework = JSON.parse(template);
        assignedWork.memberId = member.memberId;
        assignedWork.assignTime = new Date();
        this.service.orgWorkitems(assignedWork);
        workList.push(assignedWork);
      }
    }
    if (workList.length > 0) {
      this.service.saveHomeworkList(workList);
    } else {
      this.panelTitle = 'No student to assign this homework';
    }
  }

  /** Unassigns the selected homework from all the students of the class. */
  public unassignHomework() {
    this.service.setDirty();
    let template = this.service.getHomework();
    let button = this.assignButton.nativeElement;
    button.disabled = true;
    button.innerHTML = 'Unassigning homework ' + template.workTitle + ' ...';

    let workIdList = [];
    for (let member of this.service.getClass().members) {
      if (this.userService.isRoleStudent(member.role)) {
        let homeworkId = new HomeworkID();
        homeworkId.courseId = template.courseId;
        homeworkId.workNumber = template.workNumber;
        homeworkId.memberId = member.memberId;
        workIdList.push(homeworkId);
      }
    }
    if (workIdList.length > 0) {
      this.service.deleteHomeworkList(workIdList);
    } else {
      this.panelTitle = 'No student to unassign this homework';
    }
  }

  /** Sends the notification email for the assignment. */
  public notifyMembers(): void {
    this.service.setDirty();
    let button = this.notifyButton.nativeElement;
    button.disabled = true;
    button.innerHTML = 'Notifying members ...';
    this.service.notifyAssignment(this.studentMailBody, this.adminMailBody, this.studentSubject, this.adminSubject, this.ccParent);
  }

  /** Sets the default permission to the selected members */
  public setUserPermission(): void {
    let button = this.permissionButton.nativeElement;
    button.disabled = true;
    button.innerHTML = 'Setting permission ...';
    this.service.setUserPermission(this.memberList);
  }

  /** Gets a safe URL for a video or audio clip */
  public getSafeUrl(url: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  /** Gets the students name list for display */
  public getStudentList(): string {
    let list = '';
    for (let member of this.service.getClass().members) {
      if (this.userService.isRoleStudent(member.role)) {
        list += member.firstName + " " + member.lastName + ", ";
      }
    }
    return this.getCleanList(list);
  }

  /** Gets the admin and teacher name list for display */
  public getAdminList(): string {
    let list = '';
    for (let member of this.service.getClass().members) {
      if (this.userService.isRoleAdmin(member.role) || this.userService.isRoleTeacher(member.role)) {
        list += member.firstName + " " + member.lastName + ", ";
      }
    }
    return this.getCleanList(list);
  }

  /** Gets the student, teacher and admin name list for display */
  public getMemberList(): string {
    let list = '';
    for (let member of this.service.getClass().members) {
      if (!this.userService.isRoleParent(member.role)) {
        list += member.firstName + " " + member.lastName + ", ";
      }
    }
    return this.getCleanList(list);
  }

  private getCleanList(list: string): string {
    let idx = list.lastIndexOf(',');
    return (idx > 0 ? list.substring(0, idx) : list);
  }

  /** Gets the display message for (un)assigning homework */
  public getAssigningMessage(): string {
    if (this.isAssign()) {
      return 'Assign homework to the following members:';
    } else {
      return 'Unassign homework of the following members:';
    }
  }

  /** Checks if the current row number is an odd number */
  public isOdd(rowNumber: number): boolean {
    return (rowNumber % 2) == 1;
  }

  /**
   * Sorts the members by role and name.
   */
  public sortMembers(): Member[] {
    let members = this.service.getClass().members.sort(function (member1, member2) {
      if (member1.role === member2.role) {
        if (member1.firstName === member2.firstName) {
          return 0;
        } else {
          return (member1.firstName < member2.firstName ? -1 : 1);
        }
      } else {
        return (member1.role < member2.role ? -1 : 1);
      }
    });
    return members;
  }

  /** Some wrapers of the work service methods */
  public getClasses(): Clazz[] {
    return this.service.getClasses();
  }

  public isCurrentClass(clazz: Clazz): boolean {
    return this.service.getClass().classId == clazz.classId;
  }

  public setClass(clazz: Clazz): void {
    this.service.setClass(clazz);
  }

  public trackByIndex(idx: number, obj: any): any {
    return idx;
  }

  public hasReport():boolean {
    return this.service.getManagerReport().length > 0;
  }

  /** Clones the current member as a homework review member by setting its role to STUDENT  */
  private createReviewMember(): void {
    this.reviewMember = JSON.parse(JSON.stringify(this.service.getMember()));
    this.reviewMember.role = 'STUDENT';
  }

  // URLs of the external services
  public getRefreshDataURL() {
    return environment.serviceURL + 'admin/cache';
  }

  public getRefreshBookURL() {
    return environment.serviceURL + 'admin/bookcache';
  }

  public getRegistrationURL() {
    return this.userService.getCachedValue("RegistrationSheet");
  }

  public getGmailURL() {
    return AppProperties.googleMail;
  }

  public getDriveURL() {
    return AppProperties.googleDrive;
  }

  // check the panel name
  public isPreview(): boolean {
    return this.panel === PagePanel.Preview;
  }

  public isReport(): boolean {
    return this.panel === PagePanel.Report;
  }

  public isList(): boolean {
    return this.panel === PagePanel.List;
  }

  public isAssign(): boolean {
    return this.panel === PagePanel.Assign;
  }

  public isUnassign(): boolean {
    return this.panel === PagePanel.Unassign;
  }

  public isNotify(): boolean {
    return this.panel === PagePanel.Notify;
  }

  public isGrantPermission(): boolean {
    return this.panel === PagePanel.Permission;
  }

  public isNothing(): boolean {
    return this.panel === PagePanel.Nothing;
  }

  // check the workitem type
  public isSimpleItem(workitem: any): boolean {
    return workitem.display && (this.userService.isItemNote(workitem)
      || this.userService.isItemRead(workitem)
      || this.userService.isItemImage(workitem)
      || this.userService.isItemRecord(workitem)
      || this.userService.isItemWrite(workitem));
  }

  public isNote(workitem: any): boolean {
    return workitem.display && this.userService.isItemNote(workitem);
  }

  public isWrite(workitem: any): boolean {
    return workitem.display && this.userService.isItemWrite(workitem);
  }

  public isRecord(workitem: any): boolean {
    return workitem.display && this.userService.isItemRecord(workitem);
  }

  public isImage(workitem: any): boolean {
    return workitem.display && this.userService.isItemImage(workitem);
  }

  public isVideo(workitem: any): boolean {
    return workitem.display && this.userService.isItemVideo(workitem);
  }

  public isAudio(workitem: any): boolean {
    return workitem.display && this.userService.isItemAudio(workitem);
  }

  public isRead(workitem: any): boolean {
    return workitem.display && this.userService.isItemRead(workitem);
  }

  public isMultiChoice(workitem: any): boolean {
    return workitem.display && this.userService.isItemMultiChoice(workitem);
  }

  public isAnswerQuestion(workitem: any): boolean {
    return workitem.display && this.userService.isItemAnswerQuestion(workitem);
  }

  public isFillBlank(workitem: any): boolean {
    return workitem.display && this.userService.isItemFillBlank(workitem);
  }

  public isTrueFalse(workitem: any): boolean {
    return workitem.display && this.userService.isItemTrueFalse(workitem);
  }

  public isPhraseSentence(workitem: any): boolean {
    return workitem.display && this.userService.isItemPhraseSentence(workitem);
  }

  public isConnectSentence(workitem: any): boolean {
    return workitem.display && this.userService.isItemConnectSentence(workitem);
  }
}
