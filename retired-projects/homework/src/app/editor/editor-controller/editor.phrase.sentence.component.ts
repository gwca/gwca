import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-editor-phrase-sentence',
  templateUrl: '../editor-view/phrase-sentence.html',
  styleUrls: ['../../app.component.css', '../editor.component.css']
})
export class EditorPhraseSentenceComponent extends WorkitemComponent {
}
