import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-editor-multi-choice',
  templateUrl: '../editor-view/multi-choice.html',
  styleUrls: ['../../app.component.css', '../editor.component.css']
})
export class EditorMultiChoiceComponent extends WorkitemComponent {
}
