import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-editor-connect-sentence',
  templateUrl: '../editor-view/connect-sentence.html',
  styleUrls: ['../../app.component.css', '../editor.component.css']
})
export class EditorConnectSentenceComponent extends WorkitemComponent {
}
