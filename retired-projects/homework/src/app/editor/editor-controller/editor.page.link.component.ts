import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-editor-page-link',
  templateUrl: '../editor-view/page-link.html',
  styleUrls: ['../../app.component.css', '../editor.component.css']
})
export class EditorPageLinkComponent extends WorkitemComponent {
}
