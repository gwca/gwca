import { Component } from '@angular/core';
import { WorkitemComponent } from '../../workitem/workitem.component';

@Component({
  selector: 'app-editor-video',
  templateUrl: '../editor-view/video.html',
  styleUrls: ['../../app.component.css', '../editor.component.css']
})
export class EditorVideoComponent extends WorkitemComponent {
}
