import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ElementRef, ViewChild } from '@angular/core';

import { WorkService } from '../service/work.service';
import { Workitem, Homework } from '../model/Homework';
import { WorkStatus } from '../model/AppEnums';

/**
 * This component is for a teacher to create/edit the homework of 
 * the course he/she is teaching.
 */
@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['../app.component.css', './editor.component.css']
})
export class EditorComponent implements OnInit {

  /** The form for creating or copying a homework */
  newHomeworkForm: FormGroup;

  @ViewChild('newHomworkModal', { static: false }) newHomworkModal: ElementRef;

  constructor(private service: WorkService, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.initNewHomeworkForm();
  }

  ngAfterViewInit() {
  }

  /** Makes a copy of the current homework object to edit */
  public copyHomework(): void {
    this.modalService.open(this.newHomworkModal).result.then((result) => {
      this.service.saveChangedHomework();
      this.copyCurrentHomework();
    });
  }

  /** Creates a blank homework object to edit */
  public createHomework(): void {
    this.modalService.open(this.newHomworkModal).result.then((result) => {
      this.service.saveChangedHomework();
      this.createNewHomework();
    });
  }

  /** Deletes the current homework */
  public deleteHomework(): void {
    const homework = this.service.getHomework();
    const msg = 'Are you sure you want to delete the homework ' + homework.workTitle + ', '
      + homework.workNumber + '? (OK to delete)';
    if (confirm(msg)) {
      this.service.deleteHomework();
    }
  }

  /** Do nothing if the homework creation is cancelled */
  public cancelCreation(): void { }

  //
  // Some helper methods
  //
  public trackByIndex(idx: number, obj: any): any {
    return idx;
  }

  public setDisplay(workitem: Workitem) {
    workitem.display = !workitem.display;
  }

  /** Creates a new workitem and adds it to the current workitem list as the first one */
  public addWorkitem(name: string): void {
    const item = this.service.createWorkitem(name);
    this.service.getWorkitems().splice(0, 0, item);
  }

  public isRemoved(workitem: Workitem): boolean {
    return workitem.removed;
  }

  public getWorkitems(): any[] {
    return this.service.getWorkitems();
  }

  public isDataReady(): boolean {
    return this.service.isDataReady();
  }

  public getWorkTitle(): string {
    return this.service.getWorkTitle();
  }

  public getWorkitemNames(): string[] {
    return this.service.getWorkitemNames();
  }

  public saveHomework(): void {
    this.service.saveHomework();
  }

  public isRuntimeInfo(): boolean {
    return this.service.isRuntimeInfo();
  }

  public isRuntimeError(): boolean {
    return this.service.isRuntimeError();
  }

  public getInfoMessage(): string {
    return this.service.getInfoMessage();
  }

  public getHomeworkList(): Homework[] {
    return this.service.getHomeworkList();
  }

  public setHomework(homework: Homework): void {
    this.service.setHomework(homework);
  }

  public getWorkCourseId(): string {
    return this.service.getWorkCourseId();
  }

  public getWorkMemberId(): string {
    return this.service.getWorkMemberId();
  }

  public getWorkNumber(): string {
    return this.service.getWorkNumber()
  }

  /** Initializes the homework creation form */
  private initNewHomeworkForm() {
    this.newHomeworkForm = new FormGroup({
      workTitle: new FormControl('', [Validators.required], this.isWorkTitleUnique.bind(this)),
      workNumber: new FormControl('', [Validators.required, Validators.pattern('[A-Za-z0-9_]+')],
        this.isWorkNumberUnique.bind(this))
    });
  }

  /** Creates a blank homework object with some data from the form */
  private createNewHomework() {
    const currentWork = this.service.getHomework();
    let newWork = new Homework();
    newWork.courseId = currentWork.courseId;
    newWork.memberId = currentWork.memberId;
    newWork.workTitle = this.newHomeworkForm.get('workTitle').value;
    newWork.workNumber = this.newHomeworkForm.get('workNumber').value;
    newWork.status = WorkStatus[WorkStatus.Assigned];
    newWork.workitems = this.service.getDefaultWorkitems();
    this.saveNewHomework(newWork);
  }

  /** Clones a homework object with some data from the form */
  private copyCurrentHomework() {
    const currentWork = this.service.getHomework();
    let newWork = JSON.parse(JSON.stringify(currentWork));
    newWork.workNumber = this.newHomeworkForm.get('workNumber').value;
    newWork.workTitle = this.newHomeworkForm.get('workTitle').value;
    newWork.status = WorkStatus[WorkStatus.Assigned];
    this.saveNewHomework(newWork);
  }

  /** Sets the newly created homework as the current one and saves it. */
  private saveNewHomework(homework: Homework): void {
    this.service.addWorkitemMeta(homework);
    this.service.setHomework(homework);
    this.service.getHomeworkList().push(homework);
    this.service.saveHomework();
  }

  /** Checks if the work number is unique */
  private isWorkNumberUnique(control: FormControl) {
    let promise = null;
    promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        const workNumber = control.value;
        let isTaken = false;
        for (let work of this.service.getHomeworkList()) {
          if (work.workNumber === workNumber) {
            isTaken = true;
            break;
          }
        }
        if (isTaken) {
          // create an error for the form
          resolve({ 'numberTaken': true });
        } else {
          resolve(null);
        }
      }, 100);
    });
    return promise;
  }

  /** Checks if the work title is unique */
  private isWorkTitleUnique(control: FormControl) {
    let promise = null;
    promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        const workTitle = control.value;
        let isTaken = false;
        for (let work of this.service.getHomeworkList()) {
          if (work.workTitle === workTitle) {
            isTaken = true;
            break;
          }
        }
        if (isTaken) {
          // create an error for the form
          resolve({ 'titleTaken': true });
        } else {
          resolve(null);
        }
      }, 100);
    });
    return promise;
  }
}
