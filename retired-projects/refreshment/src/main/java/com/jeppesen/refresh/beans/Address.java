package com.jeppesen.refresh.beans;

public class Address {
  private Country country;

  public Address() {}

  public Country getCountry() {
    return country;
  }

  public void setCountry(Country country) {
    this.country = country;
  }

}
