package com.jeppesen.refresh.beans;

public class Country {
  private String isocode;
  
  public Country() {
  }

  public String getIsocode() {
    return isocode;
  }

  public void setIsocode(String isocode) {
    this.isocode = isocode;
  }
}
