package com.jeppesen.refresh.streams;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AnonymousClasses {

  private static Logger logger = LoggerFactory.getLogger(AnonymousClasses.class.getSimpleName());

  interface HelloWorld {
    public void greet();

    public void greetSomeone(String someone);
  }

  public void sayHello() {

    /**
     * Declare a local class EnglishGreeting
     *
     */
    class EnglishGreeting implements HelloWorld {
      String name = "world";

      public void greet() {
        greetSomeone("world");
      }

      public void greetSomeone(String someone) {
        name = someone;
        logger.info("Hello " + name);
      }
    }

    /**
     * Declares a local variable as an instance of the local class: EnglishGreeting.
     */
    HelloWorld englishGreeting = new EnglishGreeting();

    /**
     * An anonymous class is an expression that uses an anonymous class in the initialization
     * statement of the local variable frenchGreeting
     */
    HelloWorld frenchGreeting = new HelloWorld() {
      String name = "tout le monde";

      public void greet() {
        greetSomeone("tout le monde");
      }

      public void greetSomeone(String someone) {
        name = someone;
        logger.info("Salut " + name);
      }
    };

    /**
     * Uses an anonymous class in the initialization statement of the local variable spanishGreeting
     */
    HelloWorld spanishGreeting = new HelloWorld() {
      String name = "mundo";

      public void greet() {
        greetSomeone("mundo");
      }

      public void greetSomeone(String someone) {
        name = someone;
        logger.info("Hola, " + name);
      }
    };

    // Use the three local variables to say hello in 3 different languages.
    englishGreeting.greet();
    frenchGreeting.greetSomeone("Fred");
    spanishGreeting.greet();
  }
}
