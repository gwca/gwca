package com.jeppesen.refresh.streams;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LambdaExpression {

  private static Logger logger = LoggerFactory.getLogger(LambdaExpression.class.getSimpleName());

  private StateOwner stateOwner;

  public LambdaExpression() {
    this.stateOwner = new StateOwner();
  }

  /**
   * Adds three listeners to the StateOwner class.
   */
  public void addListeners() {

    // add a listener as a Lambda expression that implements the ListenerZero interface
    // with no parameter, no return
    stateOwner.addListenerZero(() -> {
      logger.info("Here is in the listerner zero");
    });

    // add a listener as a Lambda expression that implements the ListenerOne interface
    // with one parameter, no return. Since it has just one parameter and a single
    // statement in the function body, the () and {} can be omitted.
    stateOwner.addListenerOne(
        state -> logger.info("Here is in the listerner one, the state is (" + state + ")"));

    // add a listener as a Lambda expression that implements the ListenerTwo interface
    // with two parameters, return a string
    stateOwner.addListenerTwo((state1, state2) -> {
      logger.info("Here is in the listerner two, the states are (" + state1 + ", " + state2 + ")");
      return "states are (" + state1 + ", " + state2 + ")";
    });
  }

  public StateOwner getStateOwner() {
    return stateOwner;
  }
}


/**
 * A StateOwner class that uses different listeners to handle different events. Note that the
 * listeners can be added as some concrete classes, anonymous classes or lambda expressions.
 */
class StateOwner {
  private static Logger logger = LoggerFactory.getLogger("StateOwner");

  private ListenerZero listenerZero;
  private ListenerOne listenerOne;
  private ListenerTwo listenerTwo;

  public void addListenerZero(ListenerZero listener) {
    this.listenerZero = listener;
    logger.info("Added a listener zero");
  }

  public void addListenerOne(ListenerOne listener) {
    this.listenerOne = listener;
    logger.info("Added a listener one");
  }

  public void addListenerTwo(ListenerTwo listener) {
    this.listenerTwo = listener;
    logger.info("Added a listener two");
  }

  /**
   * Uses the listeners to do something.
   * @param state1
   * @param state2
   */
  public void useListerns(String state1, String state2) {
    listenerZero.changeState();
    listenerOne.changeState(state1);

    // use the implemented changeStates(s1, s2) of the interface ListenerTwo;
    String result = listenerTwo.changeState(state1, state2);
    logger.info("The return value of the listenerTwo:" + result);

    // use the default and static methods of the interface ListnerTwo
    logger.info(listenerTwo.showState("The test state"));
    logger.info(ListenerTwo.showStates("The test state1", "and test state2"));
  }
}


/**
 * The listener interface used by the StateOwner class.
 */
interface ListenerZero {
  public void changeState();
}


/**
 * The listener interface used by the StateOwner class.
 */
interface ListenerOne {
  public void changeState(String state);
}


/**
 * The listener interface used by the StateOwner class. It has only one abstract (unimplemented)
 * method and some default and static methods, so it still can be implemented by a Lambda
 * Expression.
 */
interface ListenerTwo {
  public String changeState(String state1, String state2);

  default public String showState(String state) {
    return "The default state is (" + state + ")";
  }

  public static String showStates(String state1, String state2) {
    return "The static states are (" + state1 + ", " + state2 + ")";
  }
}
