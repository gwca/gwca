package com.jeppesen.refresh.beans;

public class User {

  private Address address;
  private String email;
  private String number;

  public User(String email, String number) {
    this.email = email;
    this.number = number;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }
}
