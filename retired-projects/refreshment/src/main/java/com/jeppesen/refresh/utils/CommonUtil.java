package com.jeppesen.refresh.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Some handy utility methods.
 * 
 * @author youping.hu
 */
public class CommonUtil {

  public static final String DEFAULT_DATE_FORMAT = "MM-dd-yyyy";

  public CommonUtil() {}

  /**
   * Converts a date string to a Date object
   * 
   * @param dateString
   * @return
   */
  public static Date getDefaultDate(String dateString) {

    try {
      SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
      return dateFormat.parse(dateString);
    } catch (Exception ex) {
      return new Date();
    }
  }

  /**
   * Converts a Date object to a string
   * @param date
   * @return
   */
  public static String getDefaultDateString(Date date) {
    try {
      SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
      return dateFormat.format(date);
    } catch (Exception ex) {
      return "";
    }
  }

  /**
   * Reads the text from a resource (a text file) in the class path.
   * 
   * @param qulifiedName
   * @return
   */
  public static String readResource(String qulifiedName) throws Exception {
    BufferedReader reader = getBufferedReader(qulifiedName);
    if (reader == null) {
      throw new Exception("Cannot read from " + qulifiedName);
    }

    StringBuffer buffer = new StringBuffer();
    String value = null;
    try {
      while ((value = reader.readLine()) != null) {
        buffer.append(value);
      }
    } catch (IOException ex) {
      throw new Exception(ex);
    }
    return buffer.toString();
  }

  /**
   * Gets an input stream from a resource in the class path.
   * 
   * @param qulifiedName
   * @return
   */
  public static InputStream getStreamReader(String qulifiedName) {
    Thread thread = Thread.currentThread();
    ClassLoader loader = thread.getContextClassLoader();
    return loader.getResourceAsStream(qulifiedName);
  }

  /**
   * Gets the URL of a resource.
   * 
   * @param qulifiedName
   * @return
   */
  public static URL getResourceURL(String qulifiedName) {
    Thread thread = Thread.currentThread();
    ClassLoader loader = thread.getContextClassLoader();
    return loader.getResource(qulifiedName);
  }

  /**
   * Gets a buffered reader from a resource in the class path.
   * 
   * @param qulifiedName
   * @return
   */
  public static BufferedReader getBufferedReader(String qulifiedName) {
    InputStream inputStream = getStreamReader(qulifiedName);
    BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStream));
    return streamReader;
  }
}
