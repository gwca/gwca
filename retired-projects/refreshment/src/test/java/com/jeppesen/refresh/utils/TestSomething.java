package com.jeppesen.refresh.utils;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestSomething {

  protected Logger logger = LoggerFactory.getLogger(TestSomething.class.getSimpleName());

  @Test
  public void testRandom() {
    for (int i = 0; i < 20; i++) {
      logger.info(i + ":" + (int) (Math.random() * 4 + 1));
    }
  }
}
