package com.jeppesen.refresh.streams;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LambdaExpressionTest {

  private static Logger logger = LoggerFactory.getLogger(LambdaExpressionTest.class.getSimpleName());

  @Test
  public void testLambdaExpression() {
    LambdaExpression expr = new LambdaExpression();

    logger.info("Adding listeners to the owner");
    expr.addListeners();

    logger.info("Using the listeners of the owner");
    StateOwner stateOwner = expr.getStateOwner();
    stateOwner.useListerns("the 1st state", "the 2nd state");

  }
}
