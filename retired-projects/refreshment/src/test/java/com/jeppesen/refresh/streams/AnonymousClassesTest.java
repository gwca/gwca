package com.jeppesen.refresh.streams;

import org.junit.Test;

import com.jeppesen.refresh.streams.AnonymousClasses;

public class AnonymousClassesTest {

  @Test
  public void testHelloWorld() {
    AnonymousClasses myApp = new AnonymousClasses();
    myApp.sayHello();
  }
}
