package com.jeppesen.refresh.streams;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.Test;

import com.jeppesen.refresh.beans.User;

public class OptionalTest {

  // private static Logger logger = LoggerFactory.getLogger(OptionalTest.class.getSimpleName());

  @Test(expected = NoSuchElementException.class)
  public void whenCreateEmptyOptional_thenNull() {
    Optional<User> emptyOpt = Optional.empty();
    emptyOpt.get();
  }

  @Test(expected = NullPointerException.class)
  public void whenCreateOfEmptyOptional_thenNullPointerException() {
    // use Optional.ofNullable(obj) instead of Optional.of(obj) if not sure the obj is null.
    User user = null;
    Optional<User> opt = Optional.ofNullable(user);
    assertFalse(opt.isPresent());
    Optional.of(user);
  }

  @Test
  public void whenCheckIfPresent_thenOk() {
    User user = new User("john@gmail.com", "1234");
    Optional<User> opt = Optional.ofNullable(user);

    assertTrue(opt.isPresent());
    assertEquals(user.getEmail(), opt.get().getEmail());

    // the lambda expression gets executed only when ifPresent() is true
    opt.ifPresent(u -> assertEquals(user.getEmail(), u.getEmail()));
  }

  @Test
  public void whenEmptyValue_thenReturnDefault() {
    User user1 = null;
    User user2 = new User("anna@gmail.com", "1234");
    User result = Optional.ofNullable(user1).orElse(user2);

    assertEquals(user2.getEmail(), result.getEmail());

    user1 = new User("john@gmail.com", "1234");
    result = Optional.ofNullable(user1).orElse(user2);

    assertEquals(user1.getEmail(), result.getEmail());
  }
}
