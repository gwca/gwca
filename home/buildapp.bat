
REM
REM gcloud and node.js must be installed and in the path.
REM

echo "Build the application"
ng build --prod

echo "Deploy the application"
gcloud config set project great-wall-chinese-academy
gcloud app deploy -v %1


