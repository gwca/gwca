// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  appAddress: 'http://localhost:4200/',
  serviceURL: 'https://2-dot-gwca-services.appspot.com/services/',
  driveUrl: 'https://www.googleapis.com/drive/v2/files/',
  appKey: 'AIzaSyBU3xYCOz2XoE5GceIGaXqzfgdEXD8TdcY',
  breakingNewsId: '1qbxb_0-RWsbeWSaKGtIzHnO-8VWGRbCC',
  bulletinBoardId: '1O9_wDiC7y0W_TXHreBnLHyn62TqSgG3t',
  pageIndexId: '1lHnyg8NPvCfJDlcwgKlr3GpUr3GfNdnT'
};
