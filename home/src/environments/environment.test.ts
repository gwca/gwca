export const environment = {
  production: false,
  appAddress: 'http://localhost:4200/',
  serviceURL: 'https://2-dot-gwca-services.appspot.com/services/',
  driveUrl: 'https://www.googleapis.com/drive/v2/files/',
  appKey: 'AIzaSyBU3xYCOz2XoE5GceIGaXqzfgdEXD8TdcY',
  breakingNewsId: '1T4yAGTCXU-wZYuRJA1KnTb43MGlGuHzm',
  bulletinBoardId: '162WiegiPNRnwRZ6eDbuLaGOKevj3iMYa',
  pageIndexId: '1lHnyg8NPvCfJDlcwgKlr3GpUr3GfNdnT'
};
