export const environment = {
  production: true,
  appAddress: 'https://great-wall-chinese-academy.appspot.com/',
  serviceURL: 'https://gwca-services.appspot.com/services/',
  driveUrl: 'https://www.googleapis.com/drive/v2/files/',
  appKey: 'AIzaSyBU3xYCOz2XoE5GceIGaXqzfgdEXD8TdcY',
  breakingNewsId: '1qbxb_0-RWsbeWSaKGtIzHnO-8VWGRbCC',
  bulletinBoardId: '1O9_wDiC7y0W_TXHreBnLHyn62TqSgG3t',
  pageIndexId: '1lHnyg8NPvCfJDlcwgKlr3GpUr3GfNdnT'
};
