import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { SimpleTimer } from 'ng2-simple-timer';
import { PageService } from '../service/page.service';
import { ButtonTitle, AppProperty } from '../model/AppProperties';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css', '../app.component.css'],
    encapsulation: ViewEncapsulation.None
})
/**
 * The Home class
 */
export class HomeComponent implements OnInit {

    // the button titles
    buttonTitle: any;

    // the variables to control how many and which image to show in the slideshow pannel
    private slideNumber: number;
    private slidesToShow: number;
    private slideTimerName: string;
    private slideImages: HTMLImageElement[];
    @ViewChild('slideShow', { static: false }) slideShow: ElementRef;

    /** The home card deck element */
    @ViewChild('homeCards', { static: false }) homeCards: ElementRef;

    @ViewChild('board', { static: false }) board: ElementRef;

    constructor(private pageService: PageService, private timer: SimpleTimer) {
    }

    ngOnInit() {
        this.slideTimerName = 'SlideShowTimer';
        this.initSlideImages();
        this.createSlideShowTimer();
        this.pageService.loadBreakingNews();
        this.buttonTitle = ButtonTitle;
    }

    ngAfterViewInit() {
    }

    ngOnDestroy() {
        this.deleteSlideTimer();
    }

    //
    // Wrappers of the service methods
    //
    public isMobile(): boolean {
        return this.pageService.isMobile();
    }

    public setStoryPage(): void {
        this.pageService.setPageIndex('Story', 'story');
    }

    public setPageIndex(pageId: string, folder: string): void {
        this.pageService.setPageIndex(pageId, folder);
    }

    public getCardImage(): string {
        if (this.isMobile()) {
            return 'assets/images/site/cards-mobile.jpg';
        } else {
            return 'assets/images/site/cards.jpg';
        }
    }

    public getBreakingNews() {
        return this.pageService.getBreakingNews();
    }

    public hasBreakingNews(): boolean {
        return this.pageService.hasBreakingNews();
    }

    public turnOffNews(): void {
        this.pageService.turnOffNews();
    }

    /** Gets the coordinates of the upper-left and lower-right corners. */
    public getCoords(sequence: number): string {
        let coords = '';
        let margin = 10;
        if (this.homeCards) {
            let image = this.homeCards.nativeElement;
            let cardWidth = image.width / 6;
            let height = image.height;
            let startX = (sequence - 1) * cardWidth + margin;
            let startY = margin;
            let endX = sequence * cardWidth - margin;
            let endY = height - margin;
            coords = startX + ", " + startY + ", " + endX + ", " + endY;
        }
        return coords;
    }

    /** Deletes the slideshow timer */
    public deleteSlideTimer() {
        this.timer.delTimer(this.slideTimerName);
    }

    /** Creates a timer to change the slide show image periodically */
    private createSlideShowTimer(): void {
        this.timer.newTimerCD(this.slideTimerName, AppProperty.slideShowTimerInteval);
        this.timer.subscribe(this.slideTimerName, () => this.setNextSlide());
    }

    /** Preloads the slideshow images and initializes the controlling properties */
    private initSlideImages() {
        this.slideNumber = -1;
        this.slidesToShow = 9;
        this.slideImages = [];
        let imgSrc = '';

        // preload the images before the slideshow starts
        for (let i = 1; i <= this.slidesToShow; i++) {
            if (i < 10) {
                imgSrc = AppProperty.imageLocation + 'slides/0' + i + '.jpg';
            } else {
                imgSrc = AppProperty.imageLocation + 'slides/' + i + '.jpg';
            }
            let img: HTMLImageElement = <HTMLImageElement>document.createElement("IMG");
            img.className = 'slideshow-image'
            img.src = imgSrc;
            this.slideImages.push(img);
        }
    }

    /** Re-creates the slideshow element with the next image */
    private setNextSlide(): void {

        // set the next slide number
        if (this.slideNumber < (this.slidesToShow - 1)) {
            this.slideNumber++;
        } else {
            this.slideNumber = 0;
        }

        // re-create the slideshow element
        let divId = 'newShowDiv12345';
        let show = this.slideShow.nativeElement;
        let prevNode = document.getElementById(divId);
        let div = document.createElement("DIV");
        div.id = divId;
        div.className = 'slide-animate';
        div.appendChild(this.slideImages[this.slideNumber]);
        if (prevNode) {
            prevNode.parentNode.replaceChild(div, prevNode);
        } else {
            show.appendChild(div);
        }
    }

    /** Programmatically clicks on the next slide button */
    private clickButton(): void {
        let clickEvent = new MouseEvent("click", {
            "view": window,
            "bubbles": true,
            "cancelable": false
        });
        let button = document.getElementById('nextButton');
        button.dispatchEvent(clickEvent);
    }
}
