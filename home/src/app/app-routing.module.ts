import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { AboutComponent } from './content/about/about.component';
// import { HomeComponent } from './home/home.component';

// it's difficult to deploy to Google AppEngine, so give it up.
const routes: Routes = [
  // { path: 'home', component: HomeComponent },
  // { path: 'content/pdf:name', component: AboutComponent },
  // { path: '', redirectTo: 'home', pathMatch: 'full' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes,
    { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
