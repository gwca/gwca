import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { PageService } from '../service/page.service';
import { PageIndex } from '../model/AppModules';
import { PageType } from '../model/AppEnums';

@Component({
    selector: 'app-content',
    templateUrl: './content.component.html',
    styleUrls: ['./content.component.css', '../app.component.css'],
    encapsulation: ViewEncapsulation.None
})
/**
 * The content components
 */
export class ContentComponent implements OnInit {

    constructor(private pageService: PageService) {
    }

    ngOnInit() {
    }

    openInNewTab(url: string): void {
        let win = window.open(url, '_blank');
        win.focus();
    }

    // Utilities
    public isHrefItem(pageIndex: PageIndex): boolean {
        return pageIndex.type == PageType.HREF;
    }

    public isOtherItem(pageIndex: PageIndex): boolean {
        return pageIndex.type != PageType.TITLE && pageIndex.type != PageType.HREF;
    }

    public isTitleItem(pageIndex: PageIndex): boolean {
        return pageIndex.type == PageType.TITLE;
    }

    //
    // Service utility wrapper methods
    //
    public setPageIndex(pageId: string, folder: string): void {
        this.pageService.setPageIndex(pageId, folder);
    }

    public getPageIndices(): PageIndex[] {
        return this.pageService.getPageIndices();
    }

    public hasSideBar(): boolean {
        return this.pageService.hasSideBar();
    }

    public isMobile(): boolean {
        return this.pageService.isMobile();
    }

    public isHref(): boolean {
        return this.pageService.isHref();
    }

    public isPDF(): boolean {
        return this.pageService.isPDF();
    }

    public isHtml(): boolean {
        return this.pageService.isHtml();
    }

    public isImage(): boolean {
        return this.pageService.isImage();
    }

    public isDrive(): boolean {
        return this.pageService.isDrive();
    }

    public getContent() {
        return this.pageService.getContent();
    }

    public getPageTitle(): string {
        return this.pageService.getPageTitle();
    }

    public getHrefAddress() {
        return this.pageService.getHrefAddress();
    }

    public getSafeAddress() {
        return this.pageService.getSafeAddress();
    }
}
