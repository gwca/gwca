import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { DomSanitizer, SafeHtml, SafeResourceUrl } from '@angular/platform-browser';

import { PageType } from '../model/AppEnums';
import { PageIndex } from '../model/AppModules';
import { AppProperty } from '../model/AppProperties';
import { BreakingNews } from '../model/AppModules';
import { DataService } from './data.service';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class PageService {

    /** The page index array that is initialized by the caller (the landing page) */
    private pageIndices: PageIndex[];

    /** The current page index that is used to load the page content. */
    private pageIndex: PageIndex;

    /** The external site URL (plain text) */
    private hrefAddress: string;

    /** The Drive file URL (safe resource) */
    private safeAddress: SafeResourceUrl;

    /** The page content (safe html) */
    private contentText: SafeHtml;

    /** The breaking news that get loaded remotely */
    breakingNews: BreakingNews[];
    showBreakingNews: boolean;

    /** The bulletin board that gets loaded remotely */
    private bulletinBoard: SafeHtml = null;
    private bulletinReady: boolean;

    /**
     * Initializes everything. The title and meta services are for dynamically setting
     * the page title and meta that are used by Search Engines (Google, Bing, etc.).
     */
    constructor(private sanitizer: DomSanitizer, private dataService: DataService,
        private title: Title, private meta: Meta) {
        this.initPageProp();
    }

    /**
     * Sets the page indices of the app. It is called by the landing page initially
     * to cash all the page indices. And then sets the default page index to "Home".
     * The indices can be loaded from a remote database, a local JSON file or simply
     * hard coded.
     * @param indices 
     */
    public setPageIndices(indices: any[]): void {
        this.pageIndices = [];
        for (let idx of indices) {
            let pageIndex = new PageIndex(idx);
            this.pageIndices.push(pageIndex);
        }
        this.pageIndex = this.getPageIndex('Home', '');
    }

    /**
     * Gets a page index by pageId and folder from the page indices
     * @param pageId 
     * @param folder 
     */
    public getPageIndex(pageId: string, folder: string): PageIndex {
        let pgIdx: PageIndex = null;
        for (let item of this.pageIndices) {
            if (item.folder == folder && item.pageId === pageId) {
                pgIdx = item;
            }
            if (item.pageId === 'Home') {
                pgIdx = item;
            }
        }
        return pgIdx;
    }

    /**
     * Gets a page index by pageId from the page indices
     * @param pageId 
     */
    public getPageIndexById(pageId: string): PageIndex {
        let home: PageIndex = null;
        for (let item of this.pageIndices) {
            if (item.pageId === pageId) {
                return item;
            }
            if (item.pageId === 'Home') {
                home = item;
            }
        }
        return home;
    }

    /**
     * Gets all the page indices of the folder of the current pageIndex
     */
    public getPageIndices(): PageIndex[] {
        let items: PageIndex[] = [];
        for (let item of this.pageIndices) {
            if (item.folder === this.pageIndex.folder) {
                items.push(item);
            }
        }
        return items;
    }

    /**
     * Sets the current page description by pageId and folder name
     * @param pageId 
     * @param folder 
     */
    public setPageIndex(pageId: string, folder: string) {
        this.pageIndex = this.getPageIndex(pageId, folder);
        this.updateMeta();
        this.createContent();
    }

    /**
     * Sets the current page description by pageId 
     * @param pageId 
     */
    public setPageIndexById(pageId: string) {
        this.pageIndex = this.getPageIndexById(pageId);
        this.updateMeta();
        this.createContent();
    }

    /**
     * Gets the page URL by pageId 
     * @param pageId 
     */
     public getPageURLById(pageId: string) {
        let pageIndex = this.getPageIndexById(pageId);
        return this.sanitizer.bypassSecurityTrustResourceUrl(pageIndex.file);
    }

    /** Loads the remotely stored breaking news if it is not loaded yet */
    public loadBreakingNews(): void {
        if (this.breakingNews.length == 0) {
            this.dataService.readDriveFile(environment.breakingNewsId)
                .subscribe(data => {
                    this.populateNews(data);
                }, error => {
                    console.log(error);
                });
        }
    }

    /** Loads the bulletin board remotely if it is not loaded yet */
    public loadBulletinBoard(): void {
        if (this.bulletinBoard == null) {
            this.bulletinReady = false;
            this.dataService.readDriveFile(environment.bulletinBoardId)
                .subscribe(data => {
                    this.bulletinBoard = this.sanitizer.bypassSecurityTrustHtml(data);
                    this.bulletinReady = true;
                }, error => {
                    console.log(error);
                    this.bulletinReady = true;
                });
        }
    }

    /** Loads the bullentin board locally (for testing only) */
    public loadBulletinBoardLocally(): void {
        this.bulletinReady = false;
        let url = "assets/content/bulletin/bulletin-board.html";
        this.dataService.localRead(url)
            .subscribe(data => {
                this.bulletinBoard = this.sanitizer.bypassSecurityTrustHtml(data);
                this.bulletinReady = true;
            }, error => {
                console.log(error);
                this.bulletinReady = true;
            });
    }

    //
    // Some utility methods
    //
    public isPageIndexSet(): boolean {
        return this.pageIndices && this.pageIndices.length > 0;
    }
    
    public hasSideBar(): boolean {
        return this.pageIndex && this.pageIndex.hasSideBar();
    }

    public isHome(): boolean {
        return this.pageIndex && this.pageIndex.pageId === 'Home';
    }

    public isBulletin(): boolean {
        return this.pageIndex && this.pageIndex.pageId === 'BulletinBoard';
    }

    public isContent(): boolean {
        return !this.isHome() && !this.isBulletin();
    }

    public isHref(): boolean {
        return this.pageIndex && this.pageIndex.type === PageType.HREF;
    }

    public isHtml(): boolean {
        return this.pageIndex && this.pageIndex.type === PageType.HTML;
    }

    public isImage(): boolean {
        return this.pageIndex && this.pageIndex.type === PageType.IMAGE;
    }

    public isDrive(): boolean {
        return this.pageIndex && this.pageIndex.type === PageType.DRIVE;
    }

    public isPDF(): boolean {
        return this.pageIndex && this.pageIndex.type === PageType.PDF;
    }

    public isTitle(): boolean {
        return this.pageIndex && this.pageIndex.type === PageType.TITLE;
    }

    public isMobile() {
        let check = false;
        (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor);
        return check;
    }

    public getPageFolder(): string {
        return (this.pageIndex ? this.pageIndex.folder : '');
    }

    public getPageFile(): string {
        return (this.pageIndex ? this.pageIndex.file : '');
    }

    public getContent() {
        return this.contentText;
    }

    public getPageTitle(): string {
        return (this.pageIndex ? this.pageIndex.title : '');
    }

    public getPageType(): PageType {
        return (this.pageIndex ? this.pageIndex.type : PageType.UNKNOWK);
    }

    public getPageUrl(): string {
        return (this.pageIndex ? this.pageIndex.file : null);
    }

    public getHrefAddress() {
        return this.hrefAddress
        // 'https://cors-anywhere.herokuapp.com/' + this.pdfAddress;
        // return this.sanitizer.bypassSecurityTrustResourceUrl(this.pdfAddress);
    }

    public getSafeAddress() {
        return this.safeAddress
    }

    public getBulletinBoard() {
        return this.bulletinBoard;
    }

    public isBulletinReady(): boolean {
        return this.bulletinReady;
    }

    public getBreakingNews() {
        return this.breakingNews;
    }

    /** Checks if there is at least one breaking news */
    public hasBreakingNews(): boolean {
        return this.showBreakingNews && this.breakingNews.length > 0;
    }

    /** Turns off all the breaking news */
    public turnOffNews(): void {
        this.showBreakingNews = false;
    }

    /** Populates the newly received breaking news and remove the previous ones */
    private populateNews(entities: any) {
        let newsList = JSON.parse(entities);
        if (entities && entities.length > 0) {
            let numNews: number = this.breakingNews.length;
            // add the newly retrieved effective news to the array
            for (let entity of newsList) {
                let news = new BreakingNews(entity);
                if (news.isEffective()) {
                    this.breakingNews.push(news);
                }
            }
            // remove the old ones
            if (numNews > 0) {
                this.breakingNews.splice(0, numNews);
            }
        }
    }

    /**
     * Updates the page title and description when its content gets changed. This is
     * for getting a better Google search result.
     */
    private updateMeta(): void {
        let titleMessage = this.getPageTitle() + " " + AppProperty.metaTitleSuffix
        this.title.setTitle(titleMessage);
        this.meta.updateTag({ name: 'description', content: titleMessage });
    }

    /**
     * Creates a page content according to the page type of the current page index. If the
     * content is in a local html file (type = HTML), It reads it from that HTML file and
     * populates the veriable contentText with it. Otherwise it sets the external URL or 
     * Drive link for the content.component class to use.
     * 
     * In order to use the styles of the HTML content, which is loaded from a local html file, 
     * correctly, the content.component must set the encapsulation to ViewEncapsulation.None.
     * @see content.component.ts @Component.
     */
    private createContent(): void {
        this.contentText = '';
        let type = this.getPageType();
        switch (type) {
            case PageType.HREF:
                this.hrefAddress = this.getPageFile();
                break;
            case PageType.YOUTUBE:
            case PageType.DRIVE:
            case PageType.PDF:
                this.safeAddress = this.sanitizer.bypassSecurityTrustResourceUrl(this.getPageFile());
                break;
            case PageType.HTML:
                this.hrefAddress = this.createLocalPageUrl();
                this.dataService.localRead(this.hrefAddress)
                    .subscribe(data => {
                        data = this.updateURLs(data);
                        this.contentText = this.sanitizer.bypassSecurityTrustHtml(data);
                    }, error => {
                        this.contentText = this.sanitizer.bypassSecurityTrustHtml(error);
                        console.log(error);
                    });
                break;
            case PageType.IMAGE:
                let code = this.createImagePage();
                this.contentText = this.sanitizer.bypassSecurityTrustHtml(code);
                break;
            case PageType.COMPONENT:
                this.hrefAddress = this.getPageFile();
                break;
            case PageType.TITLE:
                break;
        }
    }

    /** 
     * Updates the URLs in the page html code with its actual URLs when 
     * find the occurences of PAGE_ID="page id value" 
     */
    private updateURLs(text: string) {
        let idx = text.indexOf('PAGE_ID');
        let pageId = '';
        let pageIndex = null;
        let pageURL = null;
        while (idx > 0) {
            let nameValue = text.substring(idx);
            let values = nameValue.split('"');
            if (values.length > 1) {
                pageId = values[1];
                pageIndex = this.getPageIndexById(pageId);
                pageURL = ' href="' + pageIndex.file + '" ';
                text = text.replace('PAGE_ID', pageURL + 'REPLACED_URL');
            }
            idx = text.indexOf('PAGE_ID');
        }
        return text;
    }

    /** Gets the address of an HTML page */
    private createLocalPageUrl(): string {
        return AppProperty.contentLocation + this.getPageFolder() + "/" + this.getPageFile();
    }

    /** Creates a full image page */
    private createImagePage(): string {
        let code = '<div class="card pb-2 mb-2">'
            + '<div class="card-body pb-2">'
            + '<img src="' + this.getPageFile() + '" class="story-full-image" />'
            + '</div></div>';
        return code;
    }

    /** Initializes the page properties */
    private initPageProp(): void {
        this.hrefAddress = null;
        this.contentText = null;
        this.safeAddress = null;
        this.pageIndex = null;
        this.pageIndices = [];
        this.breakingNews = [];
        this.showBreakingNews = true;
        this.bulletinBoard = null;
        this.bulletinReady = false;
    }
}
