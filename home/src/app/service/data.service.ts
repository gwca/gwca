import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable'
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    constructor(private httpClient: HttpClient) {
    }

    /**
     * Calls to HTTP:GET to invoke a Google Drive API service to retrieve the specified file from Drive.
     * The response is a textual string that is subscribed by the caller of this method.
     * 
     * @return a text string of the file.
     */
    public readDriveFile(fileId: string): Observable<any> {
        let fileUrl = this.buildDriveUrl(fileId);
        return this.httpClient.get(fileUrl, { responseType: 'text' });
    }

    /**
     * Calls to HTTP:GET to invoke a data service to retrieve a file from the local folder.
     * The response is a textual string that is subscribed by the caller of this method.
     * 
     * @param url the URL points to a local file.
     * @return a text string that is subscribed by the caller of this method.
     */
    public localRead(url: string): Observable<any> {
        return this.httpClient.get(url, { responseType: 'text' });
    }

    /**
     * Calls to HTTP:GET to invoke a data service to retrieve some data from the database.
     * The response is a JSON object that is subscribed by the caller of this method.
     * 
     * @param service the restful service name.
     * @return a JSON object that is subscribed by the caller of this method.
     */
    public read(service: string): Observable<any> {
        let url = environment.serviceURL + service;
        return this.httpClient.get(url);
    }

    /**
     * Builds a file downloading url for either Breaking News or Bulletin Board.
     * @param fileId
     * @returns 
     */
    private buildDriveUrl(fileId: string) {
        let url = environment.driveUrl + fileId + '?alt=media&key=' + environment.appKey;
        return url;
    }
}
