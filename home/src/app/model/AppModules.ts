import { PageType } from './AppEnums';

/** The page index to specify the current page properties */
export class PageIndex {
    pageId: string;
    folder: string;
    file: string;
    title: string;
    type: PageType;

    constructor(idx: any) {
        this.pageId = idx.pageId.trim();
        this.folder = idx.folder.trim();
        this.file = idx.file.trim();
        this.title = idx.title.trim();
        this.type = this.getEnum(idx.type);
    }

    public hasSideBar(): boolean {
        switch (this.folder) {
            case 'clips':
            case 'bulletin':
                return false;
            default:
                return true;
        }
    }

    private getEnum(value: string): PageType {
        return PageType[value.toUpperCase()];
    }
}

/** The breaking news class */
export class BreakingNews {
    startTime: string;
    expireTime: string;
    title: string;
    event: string;

    startAt: number;
    expireAt: number;

    constructor(news: any) {
        this.startTime = news.startTime;
        this.expireTime = news.expireTime;
        this.title = news.title;
        this.event = news.event;
        this.startAt = this.toUTC(this.startTime);
        this.expireAt = this.toUTC(this.expireTime);
    }

    /** Checks if this breaking news is effective */
    public isEffective(): boolean {
        let now: Date = new Date();
        let utcTime: number = Date.UTC(now.getFullYear(), now.getMonth(), now.getDate(),
            now.getHours(), now.getMinutes(), now.getSeconds());
        return utcTime > this.startAt && utcTime < this.expireAt;
    }

    /** To UTC time */
    private toUTC(time: string): number {
        let now: Date = new Date(time);
        let utcTime: number = Date.UTC(now.getFullYear(), now.getMonth(), now.getDate(),
            now.getHours(), now.getMinutes(), now.getSeconds());
        return utcTime;
    }
}
