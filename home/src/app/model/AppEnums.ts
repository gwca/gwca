
export enum PageType {
    COMPONENT, // component
    HREF, // externally referenced (pdf, html, etc.)
    HTML, // locally stored html
    DRIVE, // embeded from Drive (doc, slide, sheet, form, etc.)
    YOUTUBE, // embeded from YouTube
    PDF, // embeded pdf
    IMAGE, // full page image
    TITLE, // dropdown list title 
    UNKNOWK
}
