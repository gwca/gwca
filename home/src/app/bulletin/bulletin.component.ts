import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { PageService } from '../service/page.service';

@Component({
    selector: 'app-bulletin',
    templateUrl: './bulletin.component.html',
    styleUrls: ['./bulletin.component.css', '../app.component.css'],
    encapsulation: ViewEncapsulation.None
})
/**
 * The Bulletin class
 */
export class BulletinComponent implements OnInit {

    constructor(private pageService: PageService) {
    }

    ngOnInit() {
        this.pageService.loadBulletinBoard();
    }

    ngOnDestroy() {
    }

    //
    // service wrappers
    //
    public isBulletinReady(): boolean {
        return this.pageService.isBulletinReady();
    }

    public getBulletinBoard() {
        return this.pageService.getBulletinBoard();
    }
}
