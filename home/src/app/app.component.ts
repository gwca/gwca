import { Component, OnInit } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';

import { PageService } from './service/page.service';
import { DataService } from './service/data.service';
import { ButtonTitle } from './model/AppProperties';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy }]
})
export class AppComponent implements OnInit {

  /** The button titles */
  buttonTitle: any;

  constructor(private location: Location, private pageService: PageService, private dataService: DataService) {
  }

  ngOnInit() {
    this.buttonTitle = ButtonTitle;
    this.loadPageIndices();
  }

  ngAfterViewInit() {
  }

  //
  // Service method wrappers
  //
  public isMobile(): boolean {
    return this.pageService.isMobile();
  }

  public isHome(): boolean {
    return this.pageService.isHome();
  }

  public isBulletin(): boolean {
    return this.pageService.isBulletin();
  }

  public isContent(): boolean {
    return this.pageService.isContent();
  }

  public setPageIndex(pageId: string, folder: string) {
    this.pageService.setPageIndex(pageId, folder);
  }

  /** Loads the remotely (if failed, locally) stored page indices */
  private loadPageIndices(): void {
    if (!this.pageService.isPageIndexSet()) {
      console.log('getting the page indices remotely');
      this.dataService.readDriveFile(environment.pageIndexId)
        .subscribe(data => {
          let pageIndices = JSON.parse(data);
          this.pageService.setPageIndices(pageIndices);
          this.setPageBody();
        }, error => {
          console.log('getting the page indices remotely failed:' + error);
          console.log('getting the page indices locally');
          let url = "assets/resources/page-index.json";
          this.dataService.localRead(url)
            .subscribe(data => {
              let pageIndices = JSON.parse(data);
              this.pageService.setPageIndices(pageIndices);
              this.setPageBody();
            }, error => {
              console.log('getting the page indices locally failed:' + error);
            })
        });
    }
  }

  /**
   * Parses the query string of the given URL (e.g. https://host:port?page=pageId)
   * and sets the current page index accordingly.
   */
  private setPageBody(): void {
    let path = this.location.path();
    if (path.indexOf('?') >= 0) {
      let items = path.split('=');
      if (items.length > 1) {
        let pageId = items[1];
        if (pageId.startsWith('about')) {
          this.setPageIndex('AboutUs', 'about');
        } else if (pageId.startsWith('contact')) {
          this.setPageIndex('ContactUs', 'about');
        } else if (pageId.startsWith('resource')) {
          this.setPageIndex('CollegeCorner', 'links');
        } else if (pageId.startsWith('event')) {
          this.setPageIndex('DragonBoat', 'event');
        } else if (pageId.startsWith('registration')) {
          this.setPageIndex('StudentApplication', 'registration');
        } else if (pageId.startsWith('story')) {
          this.setPageIndex('Story', 'story');
        } else if (pageId.startsWith('community')) {
          this.setPageIndex('Partners', 'links');
        } else {
          this.pageService.setPageIndexById(pageId);
        }
        if (this.pageService.isHref()) {
          window.open(this.pageService.getPageUrl(), "_blank");
        }
      }
    }
  }
}
