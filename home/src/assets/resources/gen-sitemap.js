// Requiring fs module in which 
// readFile function is defined. 
const fs = require('fs')

// Reading data in utf-8 format 
// which is a type of character set. 
// Instead of 'utf-8' it can be  
// other character set also like 'ascii' 
fs.readFile('assets/resources/page-index.json', 'utf-8', (err, data) => {
    if (err) throw err;
    // use the "file" attribute to create a sitemap URL. 
    let pageIndices = JSON.parse(data);
    let prefix = 'https://www.greatwallchineseacademy.org';
    let url = '';
    for (let pageIndex of pageIndices) {
        switch (pageIndex.type) {
            case "html":
                url = prefix + '?pageId=' + pageIndex.pageId;
                console.log(url);
                break;
            case "href":
                let path = pageIndex.file;
                if (path.startsWith('assets')) {
                    url = prefix + '/' + path;
                } else {
                    url = path;
                    console.log(url);
                }
                break;
            default:
                break;
        }
    }
})
