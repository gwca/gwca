# README #

### What is this repository for? ###

The source code of all GWCA projects:

* about-c12 -- an Open Shot Video project to create the About C12 video clip.
* home -- the official website of GWCA
* textbooks -- all the textbooks including the audio and video clips online.
* dev-notes -- the development documents
* retired-projects -- some retired projects that are no longer in use.

### The development code management ###

* for a developer, create a branch from the 'dev' branch, e.g. your-name.
* make code changes in your branch
* build, test and deploy (we may need to deploy from the 'dev' branch instead)
* create a pull request to merge others' code from the 'dev' branch to your branch
* create a pull request to merge your code to the 'dev' branch to make your changes visiable to others

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact