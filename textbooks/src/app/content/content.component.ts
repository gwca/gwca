import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ElementRef, ViewChild } from '@angular/core';

import { ContentService } from '../service/content.service';
import { Lesson, Chapter } from '../model/content';

@Component({
    selector: 'app-content',
    templateUrl: './content.component.html',
    styleUrls: ['./content.component.css'],
    encapsulation: ViewEncapsulation.None
})

/**
 * The course class.
 */
export class ContentComponent implements OnInit {

    /** the HTML elements of the page of which the contents are dynamically assigned. */
    @ViewChild('lessonList', {static: false}) lessonList: ElementRef;
    @ViewChild('audioPlayer', {static: false}) audioPlayer: ElementRef;

    /** The book service for the html page to reference */
    service: ContentService;

    /** The constructor */
    constructor(private contentService: ContentService) {
        this.service = contentService;
    }

    /** Initializes the page contents */
    ngOnInit() {
    }

    ngAfterViewInit() {
        this.createPageContent();
    }

    /**
     * The event handler when a new lesson gets selected.
     * @param lesson
     */
    public setLesson(lesson: Lesson) {
        this.contentService.setLesson(lesson);
        this.createPageContent();
    }

    /** 
     * The event handler when a chapter gets selected. 
     * @param chapter
     */
    public setChapter(chapter: Chapter): void {
        this.contentService.setChapter(chapter);
        this.createPageContent();
    }

    /**
     * For a video chapter, gets the next section of its text.
     */
    public nextText(): void {
        const textID = this.contentService.getNextTextID();
        this.contentService.createChapterTextContent(textID);
    }

    /** Gets the top level page URL */
    public getCoursesPageURL() {
        return "/";
    }

    /**
     * Creates the chapter page contents and bind them to the page.
     */
    private createPageContent() {
        const textID: string = this.contentService.getTextID();
        this.contentService.createChapterTextContent(textID);
        this.highlightSelectedLesson();
        if (this.service.hasAudioClip()) {
            this.audioPlayer.nativeElement.load();
        }
    }

    /**
     * Highlights the title of the selected lesson.
     */
    private highlightSelectedLesson() {
        let titles = this.lessonList.nativeElement.querySelectorAll('li');
        for (let titleItem of titles) {
            let title = titleItem.querySelector('span');
            if (title.innerHTML == this.contentService.getLessonTitle()) {
                title.style.backgroundColor = 'Yellow'
            } else {
                title.style.backgroundColor = ''
            }
        }
    }
}
