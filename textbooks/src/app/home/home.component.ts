import { Component, OnInit } from '@angular/core';

import { BookService } from '../service/book.service';
import { ContentService } from '../service/content.service';
import { Material, MaterialID } from '../model/content';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})

/**
 * The home class.
 */
export class HomeComponent implements OnInit {

    /** The constructor */
    constructor(private bookService: BookService, private contentService: ContentService) {
    }

    /** Initializes the page contents */
    ngOnInit() {
    }

    /** Returns the book TOC */
    public getBookTOC(): Material[] {
        return this.bookService.getBookTOC();
    }

    /** Sets the materialId for invoking a material specific application. */
    public setMaterial(material: Material): void {
        let materialId: MaterialID = new MaterialID();
        materialId.courseId = material.id;
        materialId.assigned = true;
        this.contentService.setMaterialId(materialId);
    }
}
