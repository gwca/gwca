import { Component, OnInit } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';

import { DataService } from './service/data.service';
import { ContentService } from './service/content.service';
import { BookService } from './service/book.service';
import { MaterialID } from './model/content';
import { AppURL } from './model/AppProperties';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [Location,
        { provide: LocationStrategy, useClass: PathLocationStrategy }]
})

export class AppComponent implements OnInit {

    footerText: string = 'Enjoy Learning Chinese with Great Wall Chinese Academy';
    errorMessage: string = '';

    constructor(private location: Location, private dataService: DataService,
        private contentService: ContentService, private bookService: BookService) {
    }

    /** Initializes the book index and binding variables by parsing the query string. */
    ngOnInit() {
        this.loadBookIndexRemotely();
        // this.loadBookIndex();
    }

    // some service methods wrapers.
    public isMaterialAssigned(): boolean {
        return this.contentService.isMaterialAssigned();
    }

    public getBookDescription(): string {
        return this.contentService.getBookDescription();
    }

    public resetMaterial(): void {
        this.contentService.initData();
    }

    public hasError(): boolean {
        return this.errorMessage.length > 0;
    }

    /** Loads the book index remotely */
    private loadBookIndexRemotely(): void {
        this.dataService.readDriveFile(AppURL.bookIndexDriveID)
            .subscribe(data => {
                let bookIndex = JSON.parse(data);
                this.bookService.setBookIndex(bookIndex);
                this.initMeteralId();
            }, error => {
                this.createError("Reading the book index remotely failed:", error);
                console.log(this.errorMessage);
            });
    }

    /** Create an error message for the GUI to display */
    private createError(message: string, error: HttpErrorResponse): void {
        let respMsg = error.message;
        respMsg = respMsg.replace('&key=' + AppURL.appKey, '');
        respMsg = respMsg.replace(AppURL.bookIndexDriveID, '');
        this.errorMessage = message + respMsg;
    }

    /** Initializes the meterial ID with the query string of the URL. */
    private initMeteralId(): void {
        let path = this.location.path();
        let parameters = this.parseQueryString(path);
        let materialId = new MaterialID();
        materialId.assigned = true;

        switch (parameters.length) {
            case 0:
                materialId.assigned = false;
                break;
            case 1:
                materialId.courseId = parameters[0].value;
                break;
            case 2:
                materialId.courseId = parameters[0].value;
                materialId.lessonId = parameters[1].value;
                break;
            case 3:
            default:
                materialId.courseId = parameters[0].value;
                materialId.lessonId = parameters[1].value;
                materialId.chapterId = parameters[2].value;
                break;
        }
        this.contentService.setMaterialId(materialId);
    }

    /**
     * Parses the query string of the given URL. To be backword compitable, filter out
     * the "type=xxx" name/value pair as it is no longer needed.
     * @param url e.g. https://path?type=course&course=npcr1&lesson=01&chapter=2
     * @returns a list of name/value pairs.
     */
    private parseQueryString(url: string) {
        let parameters = [];
        if (url.indexOf('?') >= 0) {
            let items = url.split('?');
            let query = items[1];
            let pairs = query.split('&');
            for (let i = 0; i < pairs.length; i++) {
                let pair = pairs[i].split('=');
                if (pair[0] != 'type') {
                    parameters.push({
                        name: decodeURIComponent(pair[0]),
                        value: decodeURIComponent(pair[1])
                    })
                }
            }
        }
        return parameters;
    }

    //
    // The following private methods are used for generating the book index for all the books. 
    // One time only
    //

    /** Loads the book index locally */
    private loadBookIndex(): void {
        let bookIdx = 'BookIndex/book-index.json';
        this.dataService.readLocalFile(bookIdx)
            .subscribe(data => {
                let bookIndex = data;
                this.bookService.setBookIndex(bookIndex);
                this.initMeteralId();
                this.generateBookInfo();
                this.generateVideoInfo();
            }, error => {
                this.createError("Reading the book index locally failed:", error);
                console.log(this.errorMessage);
            });
    }

    /** Generates the video info lists */
    private generateVideoInfo() {
        let names: string[] = ['npcr1', 'npcr2', 'npcr3', 'npcr4'];
        for (let name of names) {
            this.loadAndUpdateVideoInfo(name);
        }
    }

    /** Loads the file info list and update the video info with file IDs */
    private loadAndUpdateVideoInfo(name: string): void {
        let bookIdx = 'BookIndex/drive-file-list/' + name + '-video.json';
        this.dataService.readLocalFile(bookIdx)
            .subscribe(data => {
                let fileList = data;
                this.updateVideoInfo(name, fileList);
            }, error => {
                console.log("Reading the file info failed:" + name + error);
            });
    }

    /** Updates the video info with file IDs  "npcr1/lesson-07-1.html" "NPCR1-Lesson07.mp4" */
    private updateVideoInfo(name: string, fileList: any): void {
        for (let book of this.bookService.getBookIndex()) {
            if (name === book.courseId) {
                for (let lesson of book.lessons) {
                    let lessonId = lesson.lessonId;
                    for (let chapter of lesson.chapters) {
                        if (chapter.mediaType == 'video') {
                                for (let item of fileList.items) {
                                    let videoId = item.id;
                                    let lessonNum = item.title.substring(12, 14);
                                    if (lessonNum === lessonId) {
                                        chapter.mediaUrl = videoId;
                                    }
                            }
                        }
                    }
                }
                let bookString = JSON.stringify(book, null, 2);
                console.log(bookString);
            }
        }
    }

    /** Generates the book info lists */
    private generateBookInfo() {
        let names: string[] = ['pinyin', 'npcr1', 'npcr2', 'npcr3', 'npcr4', 'npcr5', 'npcr6', 'workshop', 'reading'];
        for (let name of names) {
            this.loadAndUpdateBookIdx(name);
        }
    }

    /** Loads the file info list and update the book index with file IDs */
    private loadAndUpdateBookIdx(name: string): void {
        let bookIdx = 'BookIndex/drive-file-list/' + name + '.json';
        this.dataService.readLocalFile(bookIdx)
            .subscribe(data => {
                let fileList = data;
                this.updateBookIndex(name, fileList);
            }, error => {
                console.log("Reading the file info failed:" + name + error);
            });
    }

    /** Updates the book index with file IDs */
    private updateBookIndex(name: string, fileList: any): void {
        for (let book of this.bookService.getBookIndex()) {
            if (name === book.courseId) {
                for (let lesson of book.lessons) {
                    for (let chapter of lesson.chapters) {
                        for (let url of chapter.textUrls) {
                            for (let item of fileList.items) {
                                let textId = item.id;
                                let fileName = item.title;
                                if (url.endsWith('/' + fileName)) {
                                    if (!chapter.hasOwnProperty('textIDs')) {
                                        chapter.textIDs = [];
                                    }
                                    chapter.textIDs.push(textId);
                                }
                            }
                        }
                    }
                }
                let bookString = JSON.stringify(book, null, 2);
                console.log(bookString);
            }
        }
    }
}
