import { Injectable } from '@angular/core';

import { Book, Lesson, Chapter, Material } from '../model/content';
import { AppURL } from '../model/AppProperties';

@Injectable({
    providedIn: 'root'
})
export class BookService {

    private bookIndex: Book[];
    private bookTOC: Material[];

    constructor() {
    }

    /**
     * Sets the book index
     * @param books 
     */
    public setBookIndex(books: Book[]) {
        this.bookIndex = books;
        this.createBookTOC();
    }

    /** Gets the book index. */
    public getBookIndex(): Book[] {
        return this.bookIndex;
    }

    /** Gets the book TOC */
    public getBookTOC(): Material[] {
        return this.bookTOC;
    }

    /** Gets a book from the book index by a courseId. */
    public getBook(courseId: string): Book {
        if (courseId == null) {
            return this.bookIndex[0];
        }
        for (var i = 0; i < this.bookIndex.length; i++) {
            if (this.bookIndex[i].courseId == courseId) {
                return this.bookIndex[i];
            }
        }
        return this.bookIndex[0];
    }

    /** Gets a lesson from the course by a lessonId. */
    public getLesson(book: Book, lessonId: string): Lesson {
        if (lessonId == null) {
            return this.getFirstLesson(book);
        }
        for (var i = 0; i < book.lessons.length; i++) {
            if (book.lessons[i].lessonId == lessonId) {
                return book.lessons[i];
            }
        }
        return this.getFirstLesson(book);
    }

    /** Gets a chapter from the lesson by chapterId. */
    public getChapter(lesson: Lesson, chapterId: string): Chapter {
        if (chapterId == null) {
            return this.getFirstChapter(lesson);
        }
        for (var i = 0; i < lesson.chapters.length; i++) {
            if (lesson.chapters[i].chapterId == chapterId) {
                return lesson.chapters[i];
            }
        }
        return this.getFirstChapter(lesson);
    }

    //
    // Utilities
    //
    public isVideo(chapter: Chapter): boolean {
        return chapter.mediaType && chapter.mediaType == 'video';
    }

    public isAudio(chapter: Chapter): boolean {
        return chapter.mediaType && chapter.mediaType == 'audio';
    }

    public getAudioUrl(chapter: Chapter): string {
        return AppURL.audioFolder + chapter.mediaUrl;
    }

    public getVideoUrl(chapter: Chapter): string {
        // return AppURL.videoFolder + chapter.mediaUrl;
        return AppURL.driveVideoFolder + chapter.mediaUrl + '/preview';
    }

    /** Creates a book TOC when the book index is set */
    private createBookTOC(): void {
        this.bookTOC = [];
        for (let book of this.bookIndex) {
            if (book.courseId != 'youpinghu') {
                let material = new Material(book);
                this.bookTOC.push(material);
            }
        }
    }

    /** Gets the first lesson from the course. */
    private getFirstLesson(book: Book): Lesson {
        return book.lessons[0];
    }

    /** Gets the first chapter from the lesson. */
    private getFirstChapter(lesson: Lesson): Chapter {
        return lesson.chapters[0];
    }
}
