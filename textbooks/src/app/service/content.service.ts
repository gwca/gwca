import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { DataService } from './data.service';
import { BookService } from './book.service';

import { Book, Lesson, Chapter } from '../model/content';
import { MaterialID } from '../model/content';
import { AppURL } from '../model/AppProperties';

@Injectable({
    providedIn: 'root'
})
export class ContentService {

    /** the selected course, lesson and chapter. */
    private book: Book;
    private lesson: Lesson;
    private chapter: Chapter;

    /** the text index of a video chapter */
    private textIndex: number;

    /** the starting number of the lesson list */
    private startFrom: number;

    /** the chapter text bound to the text panel */
    private chapterText: string;

    /** the error message */
    private message: string;

    /** video and audio flags */
    private hasVideo: boolean;
    private hasAudio: boolean;

    /**
     * The ID of the selected material (course, lesson and chapter) 
     * This object gets passed in by the app.component while it is
     * invoking a child component of this (course, teacher).
     */
    private materialId: MaterialID;

    constructor(private bookService: BookService, private dataService: DataService,
        private sanitizer: DomSanitizer) {
        this.initData();
    }

    /**
     * Sets the materialId
     * @param materialId
     */
    public setMaterialId(materialId: MaterialID): void {
        this.materialId = materialId;
        if (materialId.assigned) {
            this.setSelectedMaterial();
        }
    }

    /** Checks if the learning material is assigned */
    public isMaterialAssigned(): boolean {
        return (this.materialId && this.materialId.assigned);
    }

    /** Gets the book description */
    public getBookDescription(): string {
        if (this.book && this.book.description.length > 0) {
            return this.book.description;
        }
        return 'Textbooks Online';
    }

    /**
     * The event handler when a new lesson gets selected.
     * @param lesson
     */
    public setLesson(lesson: Lesson) {
        this.materialId.lessonId = lesson.lessonId;
        this.materialId.chapterId = lesson.chapters[0].chapterId;
        this.setSelectedMaterial();
    }

    /** 
     * The event handler when a chapter gets selected. 
     * @param chapter
     */
    public setChapter(chapter: Chapter): void {
        this.materialId.chapterId = chapter.chapterId;
        this.setSelectedMaterial();
    }

    public setMessage(message: string) : void {
        this.message = message;
    }

    /** Checks if there is a video clip available */
    public hasVideoClip(): boolean {
        return this.hasVideo;
    }

    /** Checks if there is an audio clip available */
    public hasAudioClip(): boolean {
        return this.hasAudio;
    }

    /** 
     * Checks if the incoming chapter is same as the selected one.
     * @param chapter
     */
    public isSelectedChapter(chapter: Chapter): boolean {
        var selected = (chapter === this.chapter);
        return selected;
    }

    /**
     * Creates the chapter text content by reading it from an HTML file. Also checks
     * if there are voieo and audio clips available from this chapter.
     * To make it work, this componment must set the encapsulation to use the style for the innerHTML.
     * encapsulation: ViewEncapsulation.None, (in @component)
     * @param textId
     */
    public createChapterTextContent(textId: string): void {
        this.dataService.readDriveFile(textId)
            .subscribe(data => {
                this.chapterText = data;
                this.hasVideo = this.bookService.isVideo(this.chapter);
                this.hasAudio = this.bookService.isAudio(this.chapter);
            }, error => {
                let respMsg = error.message.replace('&key=' + AppURL.appKey, '');
                respMsg = respMsg.replace(textId, '');
                respMsg = "Reading the book " + textId + " failed: " + respMsg;
                this.setMessage(respMsg);
                console.log(respMsg);
            });
    }

    /** Gets the next section of the text for a video chapter. */
    public getNextTextID(): string {
        this.textIndex++;
        if (this.textIndex >= this.chapter.textIDs.length) {
            this.textIndex = 0;
        }
        return this.chapter.textIDs[this.textIndex];
    }

    /** Gets the text id of the text section. */
    public getTextID(): string {
        return this.chapter.textIDs[this.textIndex];
    }

    /** Gets the lesson title */
    public getLessonTitle(): string {
        return this.lesson.title;
    }

    /** Checks if the current chapter is a video and has multiple text sections. */
    public isMultiText(): boolean {
        return (this.chapter != null &&
            this.chapter.textIDs.length > 1 &&
            this.bookService.isVideo(this.chapter));
    }

    /** Checks if the current lesson has multiple chapters */
    public isMultiChapters(): boolean {
        return this.lesson.chapters.length > 1;
    }

    /** Gets a safe audio URL */
    public getAudioSource() {
        const url = this.bookService.getAudioUrl(this.chapter);
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

    /** Gets a safe video URL */
    public getVideoSource() {
        const url = this.bookService.getVideoUrl(this.chapter);
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

    //
    // The getters of the veriables
    //
    public getStartFrom() {
        return this.startFrom;
    }

    public getChapterText() {
        return this.chapterText;
    }

    public getMessage() {
        return this.message;
    }

    public getCourse() {
        return this.book;
    }

    public getLesson() {
        return this.lesson;
    }

    public getChapter() {
        return this.chapter;
    }

    public getCourseLessons() {
        return this.book.lessons;
    }

    public getLessonChapters() {
        return this.lesson.chapters;
    }

    /** Initializes all the variables */
    public initData() {
        this.book = new Book();
        this.lesson = new Lesson;
        this.chapter = new Chapter;
        this.materialId = new MaterialID();
        this.textIndex = 0;
        this.startFrom = 0;
        this.chapterText = '';
        this.message = '';
        this.hasVideo = false;
        this.hasAudio = false;
    }

    /** Initializes the binding variables. */
    private setSelectedMaterial(): void {
        this.book = this.bookService.getBook(this.materialId.courseId);
        this.lesson = this.bookService.getLesson(this.book, this.materialId.lessonId);
        this.chapter = this.bookService.getChapter(this.lesson, this.materialId.chapterId);
        this.startFrom = this.book.start;
        this.textIndex = 0;
    }
}
