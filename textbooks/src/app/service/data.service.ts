import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppURL } from '../model/AppProperties';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    private jsonHeaders = null;

    constructor(private httpClient: HttpClient) {
        this.jsonHeaders = new HttpHeaders().set('Content-Type', 'application/json; charset=UTF-8');
    }

    /**
     * Calls to HTTP:GET to invoke a Google Drive API service to retrieve the specified file from Drive.
     * The response is a textual string that is subscribed by the caller of this method.
     * 
     * @return a text string of the file.
     */
    public readDriveFile(fileId: string): Observable<any> {
        let fileUrl = this.buildDriveUrl(fileId);
        return this.httpClient.get(fileUrl, { responseType: 'text' });
    }

    /**
     * Calls to HTTP:GET to invoke a data service to retrieve some data from the database.
     * The response is a JSON object that is subscribed by the caller of this method.
     * 
     * @param service the restful service name.
     * @return a JSON object that is subscribed by the caller of this method.
     */
    readLocalFile(service: string): Observable<any> {
        let url = AppURL.textFolder + service;
        return this.httpClient.get(url, { headers: this.jsonHeaders });
    }

    // /**
    //  * Calls to HTTP:GET to invoke a data service to retrieve a file from the local folder.
    //  * The response is a text string that is subscribed by the caller of this method.
    //  * 
    //  * @param url the URL points to a local file.
    //  * @return a string that is subscribed by the caller of this method.
    //  */
    // readPage(service: string): Observable<any> {
    //     let url = AppURL.textFolder + service;
    //     return this.httpClient.get(url, {responseType: 'text'});
    // }

    /**
     * Builds a file downloading url for either Breaking News or Bulletin Board.
     * @param fileId
     * @returns 
     */
    private buildDriveUrl(fileId: string) {
        let url = AppURL.driveUrl + fileId + '?alt=media&key=' + AppURL.appKey;
        return url;
    }
}
