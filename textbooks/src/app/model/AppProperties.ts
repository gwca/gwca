/**
 * Some URLs used by the application.
 */
export const AppURL = {
  quiziletURL: 'https://quizlet.com/',
  audioFolder: 'https://docs.google.com/uc?export=download&id=',
  textFolder: 'assets/books/',

  appKey: 'AIzaSyAc95QQS_JYEPg8EAa2bERiH4102aeekk0',
  driveUrl: 'https://www.googleapis.com/drive/v2/files/',
  bookIndexDriveID: '1c3BPCRWemukV21GoKNxCC6q7RUhc1jQf',
  driveVideoFolder: 'https://drive.google.com/file/d/',

  // deprecated
  swfURL: 'assets/swf/',
  videoFolder: 'https://www.youtube.com/embed/',
  dictionaryURL: 'http://www.archchinese.com/chinese_english_dictionary.html?find=',
}
