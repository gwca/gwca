/**
 * A book that is used by a course and has a list of lessons.
 */
export class Book {
    courseId: string;
    title: string;
    image: string;
    start: number;
    description: string;
    lessons: Lesson[];

    constructor() {
        this.courseId = '';
        this.title = '';
        this.image = '';
        this.start = 0;
        this.description = '';
        this.lessons = [];
    }
}

/**
 * A lesson of a book that has one or more chapters.
 */
export class Lesson {
    lessonId: string;
    title: string;
    chapters: Chapter[];

    constructor() {
        this.lessonId = '';
        this.title = '';
        this.chapters = [];
    }
}

/**
 * A chapter of a lesson that is one of the following types: [audio, video] 
 * with one or more associated texts (File IDs and URLs of the html pages)
 */
export class Chapter {
    chapterId: string;
    label: string;
    mediaType: string;
    mediaUrl: string;
    textUrls: string[];
    textIDs: string[];

    constructor() {
        this.chapterId = '';
        this.label = '';
        this.mediaType = '';
        this.mediaUrl = '';
        this.textUrls = [];
        this.textIDs = [];
    }
}

/**
 * An identity of a piece of learning material.
 */
export class MaterialID {
    courseId: string;
    lessonId: string;
    chapterId: string;
    assigned: boolean;

    constructor() {
        this.chapterId = '';
        this.lessonId = '';
        this.chapterId = '';
        this.assigned = false;
    }
}

/**
 * A TOC item of a learning material.
 */
export class Material {
    id: string;
    image: string;
    title: string;
    description: string;

    constructor(book: Book) {
        this.id = book.courseId;
        this.image = book.image;
        this.title = book.title;
        this.description = book.description;
    }
}
